# Benome

[Benome](https://benome.ca) captures, processes, and visualizes observed behavior. It's a user interface, a prediction engine, and a self-persuasion system all tied into a neat package.

## Building the client JS bundle

#### Install latest NodeJS & NPM
[NodeJS installer page](https://nodejs.org/en/download/package-manager/)

#### Clone the repository
	git clone https://bitbucket.org/shazel/benome.git

#### Install dependencies via NPM

	cd apps/benome
	npm install

#### Build the JS bundle
	npm start (or npm run-script build)

## Setting up the server

*Coming soon*