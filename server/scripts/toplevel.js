var system = require('system');
var args = system.args;

if (args.length === 1) {
    console.log('URL missing');
    phantom.exit();
}

var url = args[1],
    width = 640,
    height = 480;

if (args.length === 4) {
    width = parseInt(args[2]) || width;
    height = parseInt(args[3]) || height;
}

var page = require('webpage').create();
page.viewportSize = {
    width: width,
    height: height
};

page.open(url, function() {
    setTimeout(function() {
        var img = page.renderBase64();
        var fs = require('fs');
        fs.write('/dev/stdout', img, 'w');
        phantom.exit();
    }, 500);
});

