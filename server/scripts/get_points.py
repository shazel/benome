#!/usr/bin/python

if __name__ == '__main__':
    import sys
    from nudge.nudge_functions import get_nudge_options
    from benome.user_db import DBInterface
    from benome.container_router import call_container
    
    db_path = '/opt/benome/users.db'
    dbi = DBInterface(db_path)
    user = dbi.get_user(username=sys.argv[1])
    context_id = int(sys.argv[2])

    try:
        points = call_container('get_points', data=context_id, user=user)
    except Exception as e:
        print user, user.user_uuid
        print e
    else:
        print points