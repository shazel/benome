import sqlite3
from uuid import uuid4

db = sqlite3.connect('/opt/benome/data/steve/oct-24-2016.sql.db.2')

query = 'SELECT ID FROM NewNodes'
result = db.execute(query).fetchall()

for (node_id, ) in result:
    uuid = str(uuid4())
    query = 'UPDATE NewNodes SET UUID = ? WHERE ID = ?'
    db.execute(query, (
        uuid,
        node_id
    ))

db.commit()
db.close()