# Copyright 2016 Steve Hazel. All rights reserved.

import simplejson
import psycopg2
import psycopg2.extras
import random
import time
from uuid import uuid4
from dateutil import parser as date_parser
from datetime import datetime, timedelta

from benome.container_router import call_container
from benome.utils import is_uuid, init_logger

log_path = '/opt/benome/logs/user_db.log'
log = init_logger(log_path=log_path, quiet=False, logger_name='UserDB')

SYSTEM_UUID = 'eeeeeeee-ffff-0000-1111-222233334444'
ANON_UUID = 'deeeeeee-ffff-0000-1111-222233334444'

base_user_query = '''
SELECT
    U.ID AS                         user_id,
    U.UUID AS                       user_uuid,

    U.DateAdded AS                  date_added,
    U.AddedBy AS                    added_by,
    U.VerifiedDate AS               verified_date,
    U.AccountType AS                account_type,

    U.Username AS                   username,
    U.FriendlyName AS               friendly_name,
    U.PhoneNumber AS                phone_number,
    U.PhoneNumberVerified AS        phone_number_verified,
    U.EmailAddress AS               email_address,
    U.EmailAddressVerified AS       email_address_verified,
    U.Timezone AS                   timezone,
    U.Config AS                     config,
    U.AvatarUrl AS                  avatar_url,

    U.Active AS                     active,
    U.ForceFriendlyName AS          force_friendly_name,
    U.RequestsEnabled AS            requests_enabled,
    U.AcceptsRequests AS            accepts_requests,

    U.EmailEnabled AS               email_enabled,
    U.SMSEnabled AS                 sms_enabled,
    U.MMSEnabled AS                 mms_enabled,

    U.FeedbackFrequency AS          feedback_frequency,
    U.RequestThreshold AS           request_threshold,
    U.MaxRequestsPerDay AS          max_requests_per_day,
    U.MaxRequestsPerDayPerUser AS   max_requests_per_day_per_user,
    
    U.BeginTime AS                  begin_time,
    U.EndTime AS                    end_time,
    U.SummaryTime AS                summary_time,

    U.AddedActivities AS            added_activities,
    U.AddedSupporters AS            added_supporters,
    U.AddedExports AS               added_exports,
    U.NumNudges AS                  num_nudges,

    U.AddedActivitiesSMS AS         added_activities_sms,
    U.AddedSupportersSMS AS         added_supporters_sms,
    U.AddedExportsSMS AS            added_exports_sms,
    U.NumNudgesSMS AS               num_nudges_sms,

    U.MaxActivities AS              max_activities,
    U.MaxSupporters AS              max_supporters,
    U.MaxExports AS                 max_exports,

    U.NumActivities AS              num_activities,
    U.NumSupporters AS              num_supporters,
    U.NumExports AS                 num_exports
FROM
    Users U
WHERE
    Deleted IS FALSE
'''

base_nonuser_query = '''
SELECT
    U.ID AS                         nonuser_id,
    U.DateAdded AS                  date_added,
    U.PhoneNumber AS                phone_number,
    U.EmailAddress AS               email_address,
    U.ContactMethod AS              contact_method,
    U.Timezone AS                   timezone,
    U.State AS                      state,
    U.Active AS                     active
FROM
    NonUsers U
WHERE
    Deleted IS FALSE
'''

base_group_query = '''
SELECT
    G.ID AS                         group_id,
    G.DateAdded AS                  date_added,
    G.CreatedByUserID AS            created_by,
    G.Name AS                       name,
    G.Description AS                description,
    G.Active AS                     active,
    G.MaxMembers AS                 max_members,
    G.InviteOnly AS                 invite_only,
    G.Visible AS                    visible,
    G.MembersVisible AS             members_visible,
    G.RequestThreshold AS           request_threshold,
    G.MaxOutgoingRequestsPerDay AS  max_outgoing_requests,
    G.FeedbackFrequency AS          feedback_frequency,
    G.MaxIncomingRequestsPerDay AS  max_incoming_requests
FROM
    Groups G
WHERE
    Deleted IS FALSE
'''

# Gets group members
base_group_member_query = '''
SELECT
    M.UserID AS                     user_id,
    M.ID AS                         group_member_id,
    M.DateAdded AS                  member_date_added,
    M.Active AS                     member_active,
    M.Visible AS                    member_visible,
    M.FriendlyName AS               member_friendly_name,
    M.AvatarUrl AS                  member_avatar_url,
    M.RequestThreshold AS           member_request_threshold,
    M.MaxIncomingRequestsPerDay AS  member_max_incoming_requests
FROM
    Groups G,
    GroupMembers M,
    Users U
WHERE
    G.ID = %s 
    AND
    M.GroupID = G.ID
    AND
    M.UserID = U.ID
    AND
    U.Deleted IS FALSE
    AND
    G.Deleted IS FALSE
    AND
    M.Deleted IS FALSE
'''

# Gets supporters
base_in_connection_query = '''
SELECT
    E.ID AS                         export_id,
    C.ID AS                         connection_id,
    C.Active AS                     active,
    C.Verified AS                   verified,
    C.OutUserID AS                  out_user_id,
    C.FriendlyName AS               friendly_name,
    C.RequestThreshold AS           request_threshold,
    C.MaxRequestsPerDay AS          max_requests_per_day,
    C.FeedbackFrequency AS          feedback_frequency,
    U.FriendlyName AS               outuser_friendly_name,
    U.PhoneNumber AS                phone_number,
    U.EmailAddress AS               email_address,
    U.ForceFriendlyName AS          force_friendly_name, 
    U.Active AS                     outuser_active,
    U.RequestsEnabled AS            requests_enabled, 
    U.AcceptsRequests AS            accepts_requests,
    U.EmailEnabled AS               email_enabled,
    U.SMSEnabled AS                 sms_enabled,
    U.MMSEnabled AS                 mms_enabled,
    U.RequestThreshold AS           outuser_request_threshold,
    U.MaxRequestsPerDay AS          outuser_max_requests_per_day,
    U.MaxRequestsPerDayPerUser AS   outuser_max_requests_per_day_per_user,
    U.FeedbackFrequency AS          feedback_frequency,
    U.BeginTime AS                  begin_time,
    U.EndTime AS                    end_time,
    U.SummaryTime AS                summary_time
FROM
    UserExports E,
    UserConnections C,
    Users U
WHERE
    E.UserID = %s 
    AND
    E.ID = C.ExportID
    AND
    C.OutUserID = U.ID
    AND
    U.Deleted IS FALSE
    AND
    C.Deleted IS FALSE
    AND
    E.Deleted IS FALSE
'''


def datetime_to_sqlite(dt=None):
    if not dt:
        dt = datetime.utcnow()
    return dt.replace(microsecond=0).isoformat(' ')

class UserNotFound(Exception):
    pass

class NonUserNotFound(Exception):
    pass

class GroupNotFound(Exception):
    pass

class UserExistsError(Exception):
    pass

class UUIDExistsError(Exception):
    pass

class UsernameExistsError(Exception):
    pass

class PhoneNumberExistsError(Exception):
    pass

class EmailExistsError(Exception):
    pass

class DBInterface(object):
    UserNotFound = UserNotFound
    NonUserNotFound = NonUserNotFound
    GroupNotFound = GroupNotFound

    def __init__(self, db_path=None):
        self.init_db(db_path)

    def init_db(self, db_path, init_system_users=False):
        db_name = 'benome'
        if db_path and 'test' in db_path:
            db_name = 'benometest'

        db = psycopg2.connect(dbname=db_name, user='benome', password='benome', host='localhost', cursor_factory=psycopg2.extras.DictCursor)
        self.db = db

        self.init_tables(db, init_system_users=init_system_users)
        db.commit()
        return db

    def init_tables(self, db, init_system_users=False):
        cursor = db.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS Users (
            ID SERIAL PRIMARY KEY,
            UUID TEXT NOT NULL UNIQUE,
            Username TEXT NULL UNIQUE,
            Password TEXT NULL,
            Salt TEXT NULL,
            AddedBy INTEGER NULL,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            VerifiedDate TIMESTAMP NULL,
            AccountType TEXT NOT NULL DEFAULT 'Free',
            FriendlyName TEXT NULL,
            AvatarUrl TEXT NULL,
            PhoneNumber TEXT NULL UNIQUE,
            PhoneNumberVerified BOOLEAN NOT NULL DEFAULT FALSE,
            PhoneNumberVerificationSentDate TIMESTAMP NULL,
            EmailAddress TEXT NULL UNIQUE,
            EmailAddressVerified BOOLEAN NOT NULL DEFAULT FALSE,
            EmailVerificationSentDate TIMESTAMP NULL,
            Timezone TEXT NOT NULL,
            Deleted BOOLEAN DEFAULT FALSE,

            Config TEXT,

            Active BOOLEAN NOT NULL DEFAULT FALSE,
            ForceFriendlyName BOOLEAN DEFAULT FALSE,
            RequestsEnabled BOOLEAN DEFAULT FALSE,
            AcceptsRequests BOOLEAN DEFAULT FALSE,
            EmailEnabled BOOLEAN DEFAULT FALSE,
            SMSEnabled BOOLEAN DEFAULT FALSE,
            MMSEnabled BOOLEAN DEFAULT FALSE,
            FeedbackFrequency INTEGER NOT NULL DEFAULT 6,
            RequestThreshold INTEGER NOT NULL DEFAULT 80,
            MaxRequestsPerDay INTEGER NOT NULL DEFAULT 4,
            MaxRequestsPerDayPerUser INTEGER NOT NULL DEFAULT 2,
            BeginTime INTEGER NOT NULL DEFAULT 28800,
            EndTime INTEGER NOT NULL DEFAULT 79200,
            SummaryTime INTEGER NOT NULL DEFAULT 28200,

            AddedActivities INTEGER NOT NULL DEFAULT 0,
            AddedSupporters INTEGER NOT NULL DEFAULT 0,
            AddedExports INTEGER NOT NULL DEFAULT 0,
            NumNudges INTEGER NOT NULL DEFAULT 0,

            AddedActivitiesSMS INTEGER NOT NULL DEFAULT 0,
            AddedSupportersSMS INTEGER NOT NULL DEFAULT 0,
            AddedExportsSMS INTEGER NOT NULL DEFAULT 0,
            NumNudgesSMS INTEGER NOT NULL DEFAULT 0,

            MaxActivities INTEGER NOT NULL DEFAULT 0,
            MaxSupporters INTEGER NOT NULL DEFAULT 0,
            MaxExports INTEGER NOT NULL DEFAULT 0,

            NumActivities INTEGER NOT NULL DEFAULT 0,
            NumSupporters INTEGER NOT NULL DEFAULT 0,
            NumExports INTEGER NOT NULL DEFAULT 0
        )''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UniqueUsername ON Users(Username) WHERE Deleted IS FALSE''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UniquePhoneNumber ON Users(PhoneNumber) WHERE Deleted IS FALSE''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UniqueEmailAddress ON Users(EmailAddress) WHERE Deleted IS FALSE''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UniqueUUID ON Users(UUID) WHERE Deleted IS FALSE''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS UserState (
            UserID SERIAL PRIMARY KEY,
            Key TEXT,
            State TEXT,

            FOREIGN KEY(UserID) REFERENCES Users(ID))
        ''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UniqueUserState ON UserState(UserID, Key)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS UserFeatures (
            UserID INTEGER,
            FeatureName TEXT,
            FeatureEnabled BOOLEAN NOT NULL DEFAULT TRUE,

            FOREIGN KEY(UserID) REFERENCES Users(ID))
        ''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UniqueUserFeatures ON UserFeatures(UserID, FeatureName)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS UserExports (
            ID SERIAL PRIMARY KEY,
            UserID INTEGER NOT NULL,
            Name TEXT NOT NULL DEFAULT 'global',
            Deleted BOOLEAN NOT NULL DEFAULT FALSE,
            Active BOOLEAN NOT NULL DEFAULT TRUE,
            Private BOOLEAN DEFAULT TRUE,
            
            FOREIGN KEY(UserID) REFERENCES Users(ID)
        )''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UniqueUserExports ON UserExports(UserID, Name)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS UserConnections (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            ExportID INTEGER NOT NULL,
            OutUserID INTEGER NOT NULL,
            FriendlyName TEXT NOT NULL,
            Verified BOOLEAN NOT NULL DEFAULT FALSE,
            Deleted BOOLEAN NOT NULL DEFAULT FALSE,
            Active BOOLEAN NOT NULL DEFAULT TRUE,
            RequestThreshold INTEGER NULL,
            MaxRequestsPerDay INTEGER NOT NULL DEFAULT 1,
            FeedbackFrequency INTEGER NOT NULL DEFAULT 6,
            
            FOREIGN KEY(OutUserID) REFERENCES Users(ID),
            FOREIGN KEY(ExportID) REFERENCES UserExports(ID)
        )''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UserConnectionMap ON UserConnections(ExportID, OutUserID)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS Groups (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            CreatedByUserID INTEGER NOT NULL,
            Name TEXT NOT NULL,
            Description TEXT NOT NULL DEFAULT '',
            Deleted BOOLEAN NOT NULL DEFAULT FALSE,
            Active BOOLEAN NOT NULL DEFAULT TRUE,

            MaxMembers INTEGER NOT NULL DEFAULT 0,
            InviteOnly BOOLEAN NOT NULL DEFAULT TRUE,
            Visible BOOLEAN NOT NULL DEFAULT TRUE,
            MembersVisible BOOLEAN NOT NULL DEFAULT FALSE,

            --
            -- Various defaults which limit the demands a group can place on a member
            --

            -- Number between 0 and 100, lower number means a worse situation is needed before a nudge request is emitted
            RequestThreshold INTEGER NOT NULL DEFAULT 80,

            -- No matter how many requests are queued up, each group member won't receive more than N per day
            MaxOutgoingRequestsPerDay INTEGER NOT NULL DEFAULT 1,   

            -- Every N days, a group-related status will be sent
            FeedbackFrequency INTEGER NOT NULL DEFAULT 6,

            --
            -- Various defaults which limit the demands a member can place on the group
            --

            -- Don't accept more than N requests per day from any member (to avoid dominating/spamming, to encourage membership in multiple groups)
            MaxIncomingRequestsPerDay INTEGER NOT NULL DEFAULT 3,

            FOREIGN KEY(CreatedByUserID) REFERENCES Users(ID)
        )''')

        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UniqueGroupName ON Groups(Name)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS GroupMembers (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            GroupID INTEGER NOT NULL,
            UserID INTEGER NOT NULL,
            FriendlyName TEXT NULL,
            AvatarUrl TEXT NULL,
            Visible BOOLEAN NOT NULL DEFAULT TRUE,
            Deleted BOOLEAN NOT NULL DEFAULT FALSE,
            Active BOOLEAN NOT NULL DEFAULT TRUE,

            -- Number between 0 and 100. Don't request a nudge from this group until the threshold is reached.
            RequestThreshold INTEGER NOT NULL DEFAULT 80,

            -- Don't accept more than N requests per day from the group
            MaxIncomingRequestsPerDay INTEGER NOT NULL DEFAULT 1,

            FOREIGN KEY(UserID) REFERENCES Users(ID),
            FOREIGN KEY(GroupID) REFERENCES Groups(ID)
        )''')

        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_GroupMemberMap ON GroupMembers(GroupID, UserID)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS GroupInvites (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            CreatedByUserID INTEGER NOT NULL,
            GroupID INTEGER NOT NULL,
            InviteCode TEXT NOT NULL,
            Deleted BOOLEAN NOT NULL DEFAULT FALSE,
            Active BOOLEAN NOT NULL DEFAULT TRUE,
            IsMulti BOOLEAN NOT NULL DEFAULT FALSE,
            AcceptMax INTEGER NOT NULL,
            AcceptCount INTEGER NOT NULL DEFAULT 0,
            Expires BOOLEAN NOT NULL DEFAULT FALSE,
            ExpiryDate TIMESTAMP,

            FOREIGN KEY(CreatedByUserID) REFERENCES Users(ID),
            FOREIGN KEY(GroupID) REFERENCES Groups(ID)
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS GroupInviteAccepts (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            GroupInviteID INTEGER NOT NULL,
            NewUserID INTEGER NOT NULL,

            FOREIGN KEY(NewUserID) REFERENCES Users(ID),
            FOREIGN KEY(GroupInviteID) REFERENCES GroupInvites(ID)
        )''')
        
        cursor.execute('''CREATE TABLE IF NOT EXISTS NudgeRequestResponses (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            SourceUserID INTEGER NOT NULL,
            DestUserID INTEGER NOT NULL,
            ExportID TEXT NOT NULL,
            ContextID TEXT NOT NULL,

            FOREIGN KEY(SourceUserID) REFERENCES Users(ID),
            FOREIGN KEY(DestUserID) REFERENCES Users(ID)
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS SupportMessages (
            ID SERIAL PRIMARY KEY,
            UserID INTEGER NOT NULL,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            RespondedTo BOOLEAN NOT NULL DEFAULT FALSE,
            Message TEXT NOT NULL,

            FOREIGN KEY(UserID) REFERENCES Users(ID)
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS IncomingSMS (
            ID SERIAL PRIMARY KEY,
            UserID INTEGER NOT NULL,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            PhoneNumber TEXT NOT NULL,
            MessageData TEXT NOT NULL,

            FOREIGN KEY(UserID) REFERENCES Users(ID)
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS OutgoingSMS (
            ID SERIAL PRIMARY KEY,
            UserID INTEGER NOT NULL,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            PhoneNumber TEXT NOT NULL,
            Message TEXT NOT NULL,

            FOREIGN KEY(UserID) REFERENCES Users(ID)
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS Nudges (
            ID SERIAL PRIMARY KEY,
            SourceUserID INTEGER NOT NULL,
            DestUserID INTEGER NOT NULL,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            ContextID TEXT NOT NULL,

            FOREIGN KEY(SourceUserID) REFERENCES Users(ID),
            FOREIGN KEY(DestUserID) REFERENCES Users(ID)
        )''')

        cursor.execute('''CREATE INDEX IF NOT EXISTS Index_Nudge_SourceUserID ON Nudges(SourceUserID)''')
        cursor.execute('''CREATE INDEX IF NOT EXISTS Index_Nudge_DestUserID ON Nudges(DestUserID)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS NudgeRequests (
            ID SERIAL PRIMARY KEY,
            DestUserID INTEGER NOT NULL,
            SourceUserID INTEGER NOT NULL,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),

            FOREIGN KEY(SourceUserID) REFERENCES Users(ID),
            FOREIGN KEY(DestUserID) REFERENCES Users(ID)
        )''')
        cursor.execute('''CREATE INDEX IF NOT EXISTS Index_Nudge_DestUserID ON NudgeRequests(DestUserID)''')
        cursor.execute('''CREATE INDEX IF NOT EXISTS Index_Nudge_SourceUserID ON NudgeRequests(SourceUserID)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS Products (
            ID SERIAL PRIMARY KEY,
            Active BOOLEAN NOT NULL DEFAULT TRUE,
            Name TEXT NOT NULL,
            Description TEXT NOT NULL,
            Recurring BOOLEAN NOT NULL DEFAULT FALSE,
            PurchasePeriod TEXT,
            PriceCAD INTEGER NOT NULL,
            PriceUSD INTEGER NOT NULL,
            PriceGBP INTEGER NOT NULL,
            PriceEUR INTEGER NOT NULL
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS PaymentProviders (
            ID SERIAL PRIMARY KEY,
            Name TEXT NOT NULL
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS Purchases (
            ID SERIAL PRIMARY KEY,
            UserID INTEGER NOT NULL,
            ProductID INTEGER NOT NULL,
            PaymentProviderID INTEGER NOT NULL,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            Status TEXT NOT NULL,
            Amount INTEGER NOT NULL,
            TransactionID TEXT NOT NULL,
            Details TEXT NOT NULL,

            FOREIGN KEY(UserID) REFERENCES Users(ID),
            FOREIGN KEY(ProductID) REFERENCES Products(ID),
            FOREIGN KEY(PaymentProviderID) REFERENCES PaymentProviders(ID)
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS NudgeMedia (
            ID SERIAL PRIMARY KEY,
            UserID INTEGER NOT NULL,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            MimeType TEXT NOT NULL,
            FileName TEXT NOT NULL,
            FileHash TEXT NOT NULL,
            Message TEXT NULL,
            Bucket TEXT NOT NULL,

            FOREIGN KEY(UserID) REFERENCES Users(ID)
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS NonUsers (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            PhoneNumber TEXT NULL,
            EmailAddress TEXT NULL,
            ContactMethod TEXT NOT NULL,
            Timezone TEXT NOT NULL,
            Deleted BOOLEAN NOT NULL DEFAULT FALSE,
            Active BOOLEAN NOT NULL DEFAULT TRUE,
            State TEXT NULL
        )''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS NonUserFollows (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            FollowerID INTEGER NOT NULL,
            UserID INTEGER NOT NULL,
            Active BOOLEAN NOT NULL DEFAULT TRUE,
            State TEXT,

            FOREIGN KEY(FollowerID) REFERENCES NonUsers(ID),
            FOREIGN KEY(UserID) REFERENCES Users(ID)
        )''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_NonUserFollows ON NonUserFollows(FollowerID, UserID)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS UserFollows (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            FollowerID INTEGER NOT NULL,
            UserID INTEGER NOT NULL,
            Active BOOLEAN NOT NULL DEFAULT TRUE,
            State TEXT,

            FOREIGN KEY(FollowerID) REFERENCES Users(ID),
            FOREIGN KEY(UserID) REFERENCES Users(ID)
        )''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UserFollows ON UserFollows(FollowerID, UserID)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS UserPushSubscriptions (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            ExpiryDate TIMESTAMP NULL,
            UserID INTEGER NOT NULL,
            InstanceID TEXT NOT NULL,
            Active BOOLEAN NOT NULL DEFAULT TRUE,
            Data TEXT,

            FOREIGN KEY(UserID) REFERENCES Users(ID)
        )''')
        cursor.execute('''CREATE UNIQUE INDEX IF NOT EXISTS Index_UserPushSubscriptions ON UserPushSubscriptions(UserID, Active, Data)''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS UserMessages (
            ID SERIAL PRIMARY KEY,
            DateAdded TIMESTAMP NOT NULL DEFAULT NOW(),
            UserID INTEGER NOT NULL,
            SourceUserID INTEGER NOT NULL,
            Viewed BOOLEAN NOT NULL DEFAULT FALSE,
            ViewDate TIMESTAMP NULL,
            Engaged BOOLEAN NOT NULL DEFAULT FALSE,
            EngagedDate TIMESTAMP NULL,
            Dismissed BOOLEAN NOT NULL DEFAULT FALSE,
            DismissedDate TIMESTAMP NULL,
            MessageType INTEGER NOT NULL,
            Data TEXT,

            FOREIGN KEY(UserID) REFERENCES Users(ID),
            FOREIGN KEY(SourceUserID) REFERENCES Users(ID)
        )''')

        db.commit()

        if init_system_users:
            friendly_name = 'Anonymous'
            username = 'anonymous'
            phone_number = None
            timezone = 'America/Vancouver'

            try:
                anon_user = self.get_user(user_uuid=ANON_UUID)
            except self.UserNotFound:
                anon_user = self.add_user(friendly_name, timezone, phone_number=phone_number, username=username, 
                                            sms_enabled=False, mms_enabled=False,
                                            account_type='System', user_uuid=ANON_UUID)

            friendly_name = 'Future You'
            username = 'FutureYou'
            phone_number = None
            timezone = 'America/Vancouver'

            try:
                system_user = self.get_user(user_uuid=SYSTEM_UUID)
            except self.UserNotFound:
                system_user = self.add_user(friendly_name, timezone, phone_number, username=username, 
                                            sms_enabled=False, mms_enabled=False,
                                            account_type='System', user_uuid=SYSTEM_UUID)

    def get_random_username(self):
        db = self.db

        query = '''
        SELECT
            COUNT(*)
        FROM
            Users
        WHERE
            Username = %s
        '''    

        cursor = db.cursor()
        while True:
            username = str(random.randint(1000000, 9999999))
            cursor.execute(query, (username, ))
            result = cursor.fetchone()

            if result[0] == 0:
                break

        return username

    def add_nonuser(self, phone_number=None, email_address=None, timezone=None, commit=True):
        if not phone_number and not email_address:
            raise Exception('Invalid input')

        db = self.db
        cursor = db.cursor()

        query = '''
        INSERT INTO NonUsers
            (PhoneNumber, EmailAddress, Timezone, ContactMethod)
        VALUES (%s, %s, %s, %s) RETURNING ID
        '''

        contact_method = 'None'
        if phone_number:
            contact_method = 'Phone'
        elif email_address:
            contact_method = 'Email'
            email_address = email_address.strip().lower()

        try:
            cursor.execute(query, (phone_number, email_address, timezone, contact_method))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while adding non-user: %s' % e)
        else:
            nonuser_id = cursor.fetchone()[0]

            if commit:
                db.commit()

        return self.get_nonuser(nonuser_id=nonuser_id)

    def add_group(self, user, name, description=None, invite_only=False, max_members=None, visible=True,
                        members_visible=False, request_threshold=80, max_incoming_requests=4,
                        max_outgoing_requests=1, feedback_frequency=6, commit=True):
        if not user or not name:
            raise Exception('Invalid input')

        if not description:
            description = ''

        if not max_members:
            max_members = 0

        db = self.db
        cursor = db.cursor()

        query = '''
        INSERT INTO Groups
            (CreatedByUserID, Name, Description, MaxMembers, InviteOnly, Visible, MembersVisible,
            RequestThreshold, MaxOutgoingRequestsPerDay, FeedbackFrequency, MaxIncomingRequestsPerDay)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING ID
        '''

        try:
            cursor.execute(query, (user.id, name, description, max_members, invite_only, visible, members_visible,
                    request_threshold, max_outgoing_requests, feedback_frequency, max_incoming_requests))
        except psycopg2.IntegrityError as e:
            db.rollback()
            cursor.close()
            raise Exception('A group with that name already exists')
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while adding group: %s' % e)
        else:
            group_id = cursor.fetchone()[0]

            if commit:
                db.commit()

        return self.get_group(group_id=group_id)

    def get_groups(self):
        db = self.db
        cursor = db.cursor()

        query = base_group_query

        cursor.execute(base_group_query)
        group_rows = cursor.fetchall()

        groups = []
        for group_row in group_rows:
            groups.append(self.init_group(**dict(group_row)))

        return groups

    def get_group(self, group_id):
        db = self.db
        cursor = db.cursor()

        query = base_group_query
        query += '''
            AND
            G.ID = %s'''

        try:
            cursor.execute(query, (group_id, ))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while retrieving group: %s' % e)

        group_row = cursor.fetchone()
        if not group_row:
            raise self.GroupNotFound

        return self.init_group(**dict(group_row))

    def get_user_groups(self, user):
        db = self.db
        cursor = db.cursor()

        base_group_member_query = '''
            SELECT
                M.GroupID AS group_id
            FROM
                Groups G,
                GroupMembers M
            WHERE
                G.ID = M.GroupID
                AND
                M.UserID = %s 
                AND
                G.Deleted IS FALSE
                AND
                M.Deleted IS FALSE
            '''
        cursor.execute(base_group_member_query, (user.id,))
        group_rows = cursor.fetchall()

        groups = []
        for group_row in group_rows:
            group_id = group_row[0]
            groups.append(self.get_group(group_id))

        return groups

    def add_user(self, friendly_name, timezone, phone_number=None, email_address=None, active=True, accepts_requests=True,
                requests_enabled=True, sms_enabled=True, mms_enabled=True, config=None,
                private_export=True, commit=True, account_type='Free', user_uuid=None,
                username=None, added_by=None):
        db = self.db
        cursor = db.cursor()

        if not phone_number:
            sms_enabled = False
            mms_enabled = False
            requests_enabled = False
            accepts_requests = False

        if not config:
            config = {}

        if not username:
            username = self.get_random_username()

        if not is_uuid(user_uuid):
            user_uuid = str(uuid4())

        added_by_userid = None
        if added_by:
            added_by_userid = added_by.id

        if account_type not in ('Free', 'Basic', 'Full', 'System', 'Simple'):
            raise Exception('Invalid account type')

        query = '''
        SELECT
            ID AS user_id
        FROM
            Users
        WHERE
            UUID = %s
            AND
            Deleted IS FALSE
        '''    
        cursor.execute(query, (user_uuid,))
        result = cursor.fetchone()
        if result:
            cursor.close()
            raise UUIDExistsError

        if phone_number:
            query = '''
            SELECT
                ID AS user_id
            FROM
                Users
            WHERE
                PhoneNumber = %s
                AND
                Deleted IS FALSE
            '''    
            cursor.execute(query, (phone_number,))
            result = cursor.fetchone()
            if result:
                cursor.close()
                raise PhoneNumberExistsError

        if email_address:
            email_address = email_address.strip().lower()

            query = '''
            SELECT
                ID AS user_id
            FROM
                Users
            WHERE
                LOWER(EmailAddress) = LOWER(%s)
                AND
                Deleted IS FALSE
            '''    
            cursor.execute(query, (email_address,))
            result = cursor.fetchone()
            if result:
                cursor.close()
                raise EmailExistsError

        query = '''
        SELECT
            ID AS user_id
        FROM
            Users
        WHERE
            LOWER(Username) = LOWER(%s)
            AND
            Deleted IS FALSE
        '''    
        cursor.execute(query, (username,))
        result = cursor.fetchone()
        if result:
            cursor.close()
            raise UsernameExistsError

        query = '''
        INSERT INTO Users
            (UUID, AddedBy, Username, FriendlyName, PhoneNumber, EmailAddress, Timezone, 
            AccountType, Config, Active, RequestsEnabled, AcceptsRequests, SMSEnabled, 
            MMSEnabled)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING ID
        '''

        try:
            cursor.execute(query, (user_uuid, added_by_userid, username, friendly_name,
                                phone_number, email_address, timezone, account_type, simplejson.dumps(config), active,
                                requests_enabled, accepts_requests, sms_enabled,
                                mms_enabled))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while adding user: %s' % e)
        else:
            user_id = cursor.fetchone()[0]
            query = 'INSERT INTO UserExports (UserID, Name, Private) VALUES (%s, %s, %s)'
            cursor.execute(query, (user_id, 'global', private_export))

        if commit:
            db.commit()

        return self.get_user(user_id=user_id)

    def get_nonuser(self, phone_number=None, email_address=None, nonuser_id=None):
        db = self.db
        query = base_nonuser_query

        if nonuser_id:
            param = nonuser_id
            col_name = 'ID'
        elif email_address:
            param = email_address.strip().lower()
            col_name = 'EmailAddress'
        elif phone_number:
            param = phone_number
            col_name = 'PhoneNumber'
        else:
            raise Exception('Missing parameter')

        query += '''
            AND LOWER(U.''' + col_name + '''::TEXT) = LOWER(%s::TEXT)
        '''

        cursor = db.cursor()
        cursor.execute(query, (param, ))
        nonuser_row = cursor.fetchone()
        if not nonuser_row:
            raise self.NonUserNotFound

        return self.init_nonuser(**dict(nonuser_row))

    def get_user(self, phone_number=None, email_address=None, user_id=None, username=None, user_uuid=None):
        db = self.db
        query = base_user_query

        if user_id:
            param = user_id
            col_name = 'ID'
        elif email_address:
            param = email_address.strip().lower()
            col_name = 'EmailAddress'
        elif phone_number:
            param = phone_number
            col_name = 'PhoneNumber'
        elif username:
            param = username
            col_name = 'Username'
        elif user_uuid:
            param = user_uuid
            col_name = 'UUID'
        else:
            raise Exception('Missing parameter')

        query += '''
            AND LOWER(U.''' + col_name + '''::TEXT) = LOWER(%s::TEXT)
        '''

        cursor = db.cursor()
        cursor.execute(query, (param, ))
        user_row = cursor.fetchone()
        if not user_row:
            raise self.UserNotFound

        return self.init_user(**dict(user_row))

    def get_users(self, active=True, system=False):
        db = self.db
        query = base_user_query
        query += '''
            AND
            U.Active = %s
        '''
        if not system:
            query += '''
                AND
                U.AccountType != 'System'
            '''

        cursor = db.cursor()
        cursor.execute(query, (active or False,))
        result = cursor.fetchall()
        cursor.close()

        user_list = []
        for user_row in result:
            user = self.init_user(**dict(user_row))
            user_list.append(user)

        return user_list

    def init_user(self, **properties):
        user = User(self, **properties)
        return user

    def init_nonuser(self, **properties):
        non_user = NonUser(self, **properties)
        return non_user

    def init_group(self, **properties):
        group = Group(self, **properties)
        return group

    def validate_group_invite(self, invite_code):
        db = self.db
        cursor = db.cursor()

        invite_code = str(invite_code).strip().upper()[0:5]
        query = '''
        SELECT
            GI.ID,
            G.ID,
            GI.IsMulti,
            GI.AcceptMax,
            GI.AcceptCount,
            G.RequestThreshold,
            G.MaxOutgoingRequestsPerDay
        FROM
            GroupInvites GI,
            Groups G
        WHERE
            G.ID = GI.GroupID
            AND
            GI.InviteCode = %s
            AND
            GI.Deleted IS FALSE
            AND
            GI.Active IS TRUE
            AND
            G.Deleted IS FALSE
            AND
            G.Active IS TRUE
        '''
        cursor.execute(query, (invite_code,))
        result = cursor.fetchone()

        if not result:
            raise Exception('Code not found')

        group_invite_id, group_id, is_multi, accept_max, accept_count, request_threshold, max_requests = result
        if accept_count >= accept_max:
            raise Exception('Invite code has been consumed')

        if not is_multi and accept_count > 1:
            raise Exception('Invite code has been consumed')

        return result

    def handle_group_invite(self, user, invite_code):
        db = self.db
        cursor = db.cursor()

        invite_code = str(invite_code).strip().upper()[0:5]
        group_invite_id, group_id, is_multi, accept_max, accept_count, request_threshold, max_requests = self.validate_group_invite(invite_code)

        try:
            cursor.execute('UPDATE GroupInvites SET AcceptCount = %s WHERE ID = %s', (accept_count + 1, group_invite_id))
            cursor.execute('INSERT INTO GroupInviteAccepts (GroupInviteID, NewUserID) VALUES (%s, %s)', (group_invite_id, user.id))

            query = 'INSERT INTO GroupMembers (GroupID, UserID, RequestThreshold, MaxIncomingRequestsPerDay) VALUES (%s, %s, %s, %s)'
            cursor.execute(query, (group_id, user.id, request_threshold, max_requests))
        except Exception:
            db.rollback()
            raise
        else:
            db.commit()


class Group(object):
    def __init__(self, dbi, **properties):
        self.dbi = dbi
        self.id = properties['group_id']
        
        for key, val in properties.items():
            setattr(self, key, val)

        self.active = self.active in (1, '1', True)
        self.invite_only = self.invite_only in (1, '1', True)
        self.members_visible = self.members_visible in (1, '1', True)

    def __repr__(self):
        return '<Group %s, N=%s>' % (self.id, self.name)

    def save(self):
        db = self.dbi.db

        col_map = {}
        query = '''
        UPDATE
            Groups
        SET
            Active = %s,
            Name = %s,
            Description = %s,
            MaxMembers = %s,
            InviteOnly = %s,
            Visible = %s,
            MembersVisible = %s,
            RequestThreshold = %s,
            MaxOutgoingRequestsPerDay = %s,
            FeedbackFrequency = %s,
            MaxIncomingRequestsPerDay = %s
        WHERE
            ID = %s
        '''

        param_list = [
            self.active or False,
            self.name,
            self.description,
            self.max_members,
            self.invite_only,
            self.visible,
            self.members_visible,
            self.request_threshold,
            self.max_outgoing_requests,
            self.feedback_frequency,
            self.max_incoming_requests,
            self.id
        ]
        cursor = db.cursor()
        try:
            cursor.execute(query, param_list)
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def serialize(self):
        return {
            'ID': self.id,
            'Name': self.name,
            'Description': self.description,
            'MaxMembers': self.max_members,
            'InviteOnly': self.invite_only,
            'Visible': self.visible,
            'MembersVisible': self.members_visible,
            'RequestThreshold': self.request_threshold,
            'MaxOutgoingRequestsPerDay': self.max_outgoing_requests,
            'FeedbackFrequency': self.feedback_frequency,
            'MaxIncomingRequestsPerDay': self.max_incoming_requests
        }

    def generate_invite_code(self, user, is_multi=True, accept_max=10, expires=False):
        db = self.dbi.db
        cursor = db.cursor()

        invite_code = None
        while not invite_code:
            invite_code = ''.join(random.choice('ABCDEFGHIKJLMNOPQRSTUVWXYZ') for i in range(5))
            
            query = '''SELECT COUNT(*) FROM GroupInvites WHERE InviteCode = '%s' '''
            cursor.execute(query, (self.id,))
            result = cursor.fetchone()

            if result[0] != 0:
                invite_code = None

        query = 'INSERT INTO GroupInvites (CreatedByUserID, GroupID, InviteCode, IsMulti, AcceptMax, AcceptCount, Expires) VALUES (%s, %s, %s, %s, %s, 0, %s)'
        cursor = db.cursor()
        try:
            cursor.execute(query, (user.id, self.id, invite_code, is_multi, accept_max, expires))
        except Exception:
            db.rollback()
            raise
        else:
            db.commit()

        return invite_code

    def set_deleted(self, save=True):
        db = self.dbi.db

        query = '''
        UPDATE
            Groups
        SET
            Deleted = TRUE
        WHERE
            ID = %s
        '''

        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, ))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

        return True

    def set_inactive(self, save=True):
        self.active = False
        if save:
            self.save()

        return True

    def set_active(self, save=True):
        self.active = True
        if save:
            self.save()

        return True

    def add_member(self, user, visible=True, commit=True):
        db = self.dbi.db
        cursor = db.cursor()

        query = '''
        INSERT INTO GroupMembers
            (GroupID, UserID, Visible)
        VALUES (%s, %s, %s)
        ON CONFLICT DO NOTHING
        '''

        try:
            cursor.execute(query, (self.id, user.id, visible))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while adding group member: %s' % e)
        else:
            if commit:
                db.commit()

        return True

    def remove_member(self, user, commit=True):
        db = self.dbi.db
        cursor = db.cursor()

        query = '''DELETE FROM GroupMembers WHERE GroupID = %s AND UserID = %s'''

        try:
            cursor.execute(query, (self.id, user.id))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while removing group member: %s' % e)
        else:
            if commit:
                db.commit()

        return True

    def get_members(self, visible_only=False, except_user=None):
        db = self.dbi.db
        cursor = db.cursor()

        query = base_group_member_query
        cursor.execute(query, (self.id, ))
        query_result = cursor.fetchall()

        members = []
        for member_row in query_result:
            member_row = dict(member_row)
            if except_user and member_row['user_id'] == except_user.id:
                continue

            member_user = self.dbi.get_user(user_id=member_row['user_id'])
            members.append((member_user, member_row))

        return members


class NonUser(object):
    def __init__(self, dbi, **properties):
        self.dbi = dbi
        self.id = properties['nonuser_id']
        
        for key, val in properties.items():
            setattr(self, key, val)

        self.active = self.active in (1, '1', True)
        self.state = simplejson.loads(self.state or '{}')

    def __repr__(self):
        return '<NonUser %s, P=%s, E=%s>' % (self.id, self.phone_number, self.email_address)

    def save(self):
        db = self.dbi.db

        col_map = {}
        query = '''
        UPDATE
            NonUsers
        SET
            Timezone = %s,
            Active = %s,
            State = %s
        WHERE
            ID = %s
        '''

        param_list = [
            self.timezone,
            self.active or False,
            simplejson.dumps(self.state),
            self.id
        ]
        cursor = db.cursor()
        try:
            cursor.execute(query, param_list)
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def set_deleted(self, save=True):
        db = self.dbi.db

        query = '''
        UPDATE
            NonUsers
        SET
            Deleted = TRUE
        WHERE
            ID = %s
        '''

        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, ))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

        return True

    def set_inactive(self, save=True):
        self.active = False
        if save:
            self.save()

        return True

    def set_active(self, save=True):
        self.active = True
        if save:
            self.save()

        return True

    def add_follow(self, follow_user, commit=True):
        db = self.dbi.db
        cursor = db.cursor()

        query = '''
        INSERT INTO NonUserFollows
            (FollowerID, UserID)
        VALUES (%s, %s)
        ON CONFLICT DO NOTHING
        '''

        try:
            cursor.execute(query, (self.id, follow_user.id))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while adding non-user follow: %s' % e)
        else:
            if commit:
                db.commit()

        return True

    def remove_follow(self, follow_user, commit=True):
        db = self.dbi.db
        cursor = db.cursor()

        query = '''DELETE FROM NonUserFollows WHERE FollowerID = %s AND UserID = %s'''

        try:
            cursor.execute(query, (self.id, follow_user.id))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while removing non-user follow: %s' % e)
        else:
            if commit:
                db.commit()

        return True


class ConnectionExistsError(Exception):
    pass

class ConnectionAddError(Exception):
    pass

class ConnectionNotFoundError(Exception):
    pass

class User(object):
    ConnectionExistsError = ConnectionExistsError
    ConnectionAddError = ConnectionAddError
    ConnectionNotFoundError = ConnectionNotFoundError

    def __init__(self, dbi, **properties):
        self.dbi = dbi
        self.id = properties['user_id']
        
        self.export_ids = ['global']
        self.root_context_id = None
        self.features = properties.get('features', {})

        for key, val in properties.items():
            setattr(self, key, val)

        if self.added_by:
            self.added_by = int(self.added_by)

        self.active = self.active in (1, '1', True)
        self.requests_enabled = self.requests_enabled in (1, '1', True)

        self.phone_number_verified = self.phone_number_verified in (1, '1', True)
        self.force_friendly_name = self.force_friendly_name in (1, '1', True)
        self.accepts_requests = self.accepts_requests in (1, '1', True)
        self.email_enabled = self.email_enabled in (1, '1', True)
        self.sms_enabled = self.sms_enabled in (1, '1', True)
        self.mms_enabled = self.mms_enabled in (1, '1', True)

        self.feedback_frequency = int(self.feedback_frequency)
        self.request_threshold = int(self.request_threshold)
        self.max_requests_per_day = int(self.max_requests_per_day)
        self.max_requests_per_day_per_user = int(self.max_requests_per_day_per_user)

        self.begin_time = int(self.begin_time)
        self.end_time = int(self.end_time)
        self.summary_time = int(self.summary_time)

        self.config = simplejson.loads(self.config or '{}')

    def __repr__(self):
        return '<User %s=%s, P=%s>' % (self.id, self.friendly_name, self.phone_number)

    def save(self):
        db = self.dbi.db

        col_map = {}
        query = '''
        UPDATE
            Users
        SET
            FriendlyName = %s,
            Timezone = %s,
            VerifiedDate = %s,
            PhoneNumberVerified = %s,
            RequestsEnabled = %s,
            SMSEnabled = %s,
            MMSEnabled = %s,
            EmailEnabled = %s,
            Active = %s,

            Config = %s,

            MaxRequestsPerDay = %s,

            AddedActivities = %s,
            AddedSupporters = %s,
            AddedExports = %s,
            NumNudges = %s,

            AddedActivitiesSMS = %s,
            AddedSupportersSMS = %s,
            AddedExportsSMS = %s,
            NumNudgesSMS = %s,

            MaxActivities = %s,
            MaxSupporters = %s,
            MaxExports = %s,

            NumActivities = %s,
            NumSupporters = %s,
            NumExports = %s
        WHERE
            ID = %s
        '''

        param_list = [
            self.friendly_name,
            self.timezone,
            datetime_to_sqlite(self.verified_date),
            self.phone_number_verified or False,
            self.requests_enabled or False,
            self.sms_enabled or False,
            self.mms_enabled or False,
            self.email_enabled or False,
            self.active or False,

            simplejson.dumps(self.config),

            self.max_requests_per_day,

            self.added_activities,
            self.added_supporters,
            self.added_exports,
            self.num_nudges,

            self.added_activities_sms,
            self.added_supporters_sms,
            self.added_exports_sms,
            self.num_nudges_sms,
            
            self.max_activities,
            self.max_supporters,
            self.max_exports,

            self.num_activities,
            self.num_supporters,
            self.num_exports,
            self.id
        ]
        cursor = db.cursor()
        cursor.execute(query, param_list)
        db.commit()

    def serialize(self):
        return {
            'ID': self.id,
            'UUID': self.user_uuid,
            'Username': self.username,
            'FriendlyName': self.friendly_name,
            'Timezone': self.timezone,
            'VerifiedDate': self.verified_date.isoformat(' ') if self.verified_date else None,
            'PhoneNumberVerified': self.phone_number_verified or False,
            'RequestsEnabled': self.requests_enabled or False,
            'SMSEnabled': self.sms_enabled or False,
            'MMSEnabled': self.mms_enabled or False,
            'Active': self.active or False,
            #'Config': simplejson.dumps(self.config),
            'MaxRequestsPerDay': self.max_requests_per_day,
            'AddedActivities': self.added_activities,
            'AddedSupporters': self.added_supporters,
            'AddedExports': self.added_exports,
            'NumNudges': self.num_nudges,
            'AddedActivitiesSMS': self.added_activities_sms,
            'AddedSupportersSMS': self.added_supporters_sms,
            'AddedExportsSMS': self.added_exports_sms,
            'NumNudgesSMS': self.num_nudges_sms,
            'MaxActivities': self.max_activities,
            'MaxSupporters': self.max_supporters,
            'MaxExports': self.max_exports,
            'NumActivities': self.num_activities,
            'NumSupporters': self.num_supporters,
            'NumExports': self.num_exports,
        }

    def auth(self, in_password):
        db = self.dbi.db

        query = 'SELECT Password, Salt FROM Users WHERE ID = %s'
        cursor = db.cursor()
        cursor.execute(query, (self.id,))
        result = cursor.fetchone()

        try:
            hashed_current_password, salt = result
        except:
            return False
        else:
            if not hashed_current_password:
                log.info('No password set for user %s' % user_id)
                return False
            else:
                hashed_in_password = self.get_password_hash(in_password, salt, base64=True)
                return hashed_current_password == hashed_in_password
        finally:
            pass

        return False

    def set_password(self, password):
        db = self.dbi.db
        import hashlib

        salt = hashlib.sha224(str(uuid4())).hexdigest()
        hashed_password = self.get_password_hash(password, salt, base64=True)

        query = 'UPDATE Users SET Password = %s, Salt = %s WHERE ID = %s'
        cursor = db.cursor()
        try:
            cursor.execute(query, (
                hashed_password,
                salt,
                self.id
            ))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

        return True

    def change_password(self, user_id, old_password, new_password):
        if not self.auth_user(user_id, old_password):
            return False

        return self.set_password(user_id, new_password)

    def get_config_value(self, config_key, default=None):
        return self.get_config().get(config_key, default)

    def set_config_value(self, config_key, config_value, save=True):
        self.get_config()[config_key] = config_value
        if save:
            self.save()

    def add_follow(self, follow_user, commit=True):
        '''Current user begins following another user'''
        db = self.dbi.db
        cursor = db.cursor()

        if follow_user.id == self.id:
            raise Exception('Cannot follow self')

        query = '''
        INSERT INTO UserFollows
            (FollowerID, UserID)
        VALUES (%s, %s)
        ON CONFLICT DO NOTHING
        '''

        try:
            cursor.execute(query, (self.id, follow_user.id))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while adding user follow: %s' % e)
        else:
            if commit:
                db.commit()

        return True

    def remove_follow(self, follow_user, commit=True):
        db = self.dbi.db
        cursor = db.cursor()

        query = '''DELETE FROM UserFollows WHERE FollowerID = %s AND UserID = %s'''

        try:
            cursor.execute(query, (self.id, follow_user.id))
        except Exception as e:
            db.rollback()
            cursor.close()
            raise Exception('Query error while removing user follow: %s' % e)
        else:
            if commit:
                db.commit()

        return True

    def get_followers(self):
        db = self.dbi.db
        cursor = db.cursor()

        query = 'SELECT FollowerID, State FROM UserFollows WHERE UserID = %s AND Active IS TRUE'
        cursor.execute(query, (self.id, ))
        query_result = cursor.fetchall()

        follower_users = []
        for follower_row in query_result:
            follower_row = dict(follower_row)
            state_json = follower_row['state']
            state = {}
            try:
                state = simplejson.loads(state_json)
            except:
                pass

            follower_user = self.dbi.get_user(user_id=follower_row['followerid'])
            follower_users.append((follower_user, state))

        return follower_users

    def get_following(self):
        db = self.dbi.db
        cursor = db.cursor()

        query = 'SELECT UserID, State FROM UserFollows WHERE FollowerID = %s AND Active IS TRUE'
        cursor.execute(query, (self.id, ))
        query_result = cursor.fetchall()

        following_users = []
        for following_row in query_result:
            following_row = dict(following_row)
            state_json = following_row['state']
            state = {}
            try:
                state = simplejson.loads(state_json)
            except:
                pass

            following_user = self.dbi.get_user(user_id=following_row['userid'])
            following_users.append((following_user, state))

        return following_users

    def get_groups(self):
        return self.dbi.get_user_groups(self)

    def add_push_subscription(self, subscription_info, instance_id):
        db = self.dbi.db

        query = '''
        INSERT INTO
            UserPushSubscriptions
                (UserID, InstanceID, Data)
        VALUES (%s, %s, %s)
        ON CONFLICT DO NOTHING
        RETURNING ID
        '''

        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, instance_id, simplejson.dumps(subscription_info)))
        except psycopg2.IntegrityError as e:
            db.rollback()
            print e
            cursor.close()
            print 'Subscription for user ID %s already exists: %s' % (self.id, subscription_info)
            return
        else:
            subscription_id = cursor.fetchone()[0]
            db.commit()
            
            return subscription_id

    def remove_push_subscription(self, subscription_id):
        db = self.dbi.db
        query = 'DELETE FROM UserPushSubscriptions WHERE UserID = %s AND ID = %s'
        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, subscription_id))
        except Exception:
            db.rollback()
            cursor.close()
        else:
            db.commit()

        return True

    def get_push_subscriptions(self):
        db = self.dbi.db

        query = 'SELECT ID, Data FROM UserPushSubscriptions WHERE UserID = %s AND Active IS TRUE ORDER BY DateAdded DESC'
        cursor = db.cursor()
        cursor.execute(query, (self.id, ))
        query_result = cursor.fetchall()

        subscriptions = []
        for row in query_result:
            row = dict(row)
            subscription_info_json = row['data']
            subscription_info = {}
            try:
                subscription_info = simplejson.loads(subscription_info_json)
            except:
                continue
            subscriptions.append((row['id'], subscription_info))

        return subscriptions

    def set_deleted(self, save=True):
        db = self.dbi.db

        query = '''
        UPDATE
            Users
        SET
            Deleted = TRUE
        WHERE
            ID = %s
        '''

        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, ))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def set_inactive(self, save=True):
        self.active = False
        if save:
            self.save()

    def set_active(self, save=True):
        self.active = True
        if save:
            self.save()

    def add_media(self, filename, bucket, media_type, file_hash, message=None):
        db = self.dbi.db

        query = '''
        INSERT INTO
            NudgeMedia
                (UserID, MimeType, FileName, FileHash, Message, Bucket)
        VALUES (%s, %s, %s, %s, %s, %s) RETURNING ID
        '''

        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, media_type, filename, file_hash, message, bucket))
        except:
            db.rollback()
            cursor.close()
            raise
        else:
            media_id = cursor.fetchone()[0]
            db.commit()
        
        return media_id

    def get_media(self, media_id):
        db = self.dbi.db

        query = '''
        SELECT
            MimeType, FileName, Bucket
        FROM
            NudgeMedia
        WHERE
            ID = %s
            AND
            UserID = %s
        '''

        cursor = db.cursor()
        cursor.execute(query, (media_id, self.id))
        result = cursor.fetchone()

        if not result:
            raise Exception('Media ID %s not found for user %s' % (media_id, self.id))
        
        mime_type, file_name, bucket = result
        return mime_type, file_name, bucket

    def get_password_hash(self, password, salt, base64=False):
        import scrypt
        hashed_password = scrypt.hash(str(password), str(salt))

        if base64:
            return hashed_password.encode('base64')
        else:
            return hashed_password
    
    def set_hashed_pw(self, hashed_password, salt):
        db = self.dbi.db

        query = '''
        Update
            Users
        SET
            Password = %s,
            Salt = %s
        WHERE
            ID = %s
        '''
        cursor = db.cursor()
        try:
            cursor.execute(query, (hashed_password, salt, self.id,))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def get_age(self):
        if self.date_added:
            delta = datetime.utcnow() - self.date_added
            return int(delta.total_seconds())

    def get_name(self):
        return self.username

    def get_id(self):
        return self.id

    def get_root_context_id(self):
        if not self.root_context_id:
            self.root_context_id = call_container('get_root_context_id', user=self)

        return self.root_context_id

    def get_last_id(self):
        self.last_id = call_container('get_last_id', user=self)
        return self.last_id

    def get_config(self):
        return self.config

    def get_features(self, default_features=None):
        if type(default_features) is not dict:
            default_features = {}

        features = default_features.copy()
        features.update(self.features)

        return features

    def containerized(self):
        return True

    def get_port(self):
        config = self.get_config()

        if config.get('ContainerType') == 'Local':
            return int(config.get('LocalPort'))
        # else:
        #     if self.containerized():
        #         user_id = self.get_id()
        #     else:
        #         user_id = get_global_user_id()

        #     container_manager.ensure_container(user_id, container_name=self.name, user_is_valid=True)
        #     container_manager.verify_container(user_id)

        #     try:
        #         return int(container_manager.get_user_port(user_id))
        #     except:
        #         pass

    def get_export_id(self, export_name):
        db = self.dbi.db

        query = '''
        SELECT
            E.ID AS export_id
        FROM
            Users U,
            UserExports E
        WHERE
            U.ID = %s AND
            U.ID = E.UserID AND
            LOWER(E.Name) = LOWER(%s)
        '''
        cursor = db.cursor()
        cursor.execute(query, (self.id, export_name,))
        result = cursor.fetchone()
        if result:
            return result['export_id']

    def add_connection(self, export_name, dest_phone_number, friendly_name,
                        dest_timezone=None, commit=True, verified=False):
        db = self.dbi.db

        # First see whether that connection already exists
        try:
            dest_user = self.dbi.get_user(phone_number=dest_phone_number)
        except UserNotFound:
            if not dest_timezone:
                dest_timezone = self.timezone
            dest_user = self.dbi.add_user(friendly_name, dest_timezone, phone_number=dest_phone_number,
                                            added_by=self)

        export_id = self.get_export_id(export_name)
        cursor = db.cursor()
        query = '''
        INSERT INTO
            UserConnections
                (ExportID, OutUserID, FriendlyName, Active, Verified)
        VALUES (%s, %s, %s, True, %s) RETURNING ID
        '''

        try:
            cursor.execute(query, (export_id, dest_user.id, friendly_name, verified or False))
        except psycopg2.IntegrityError as e:
            db.rollback()
            cursor.close()
            #log.info('SQL error while adding new connection: %s' % e)
            raise ConnectionExistsError
        except Exception as e:
            cursor.close()
            raise ConnectionAddError(str(e))
        else:
            connection_id = cursor.fetchone()[0]

            if commit:
                db.commit()
                return self.get_connection(connection_id)
            else:
                return connection_id

    def get_supporter(self, connection_id=None, phone_number=None, other_user=None):
        return self.get_connection(connection_id=connection_id, phone_number=phone_number, other_user=other_user)

    def get_supporters(self, export_name=None):
        return self.get_connections(export_name=export_name)

    def get_connection(self, connection_id=None, phone_number=None, other_user=None):
        db = self.dbi.db

        query = base_in_connection_query
        if connection_id:
            query += '''
            AND C.ID = %s
            '''
            param = connection_id
        elif phone_number:
            query += '''
            AND U.PhoneNumber = %s
            '''
            param = phone_number
        elif other_user:
            query += '''
            AND C.OutUserID = %s
            '''
            param = other_user.id

        query += '''
        AND C.Active IS TRUE
        AND U.Active IS TRUE
        AND E.Active IS TRUE
        '''

        query_params = (self.id, param,)
        cursor = db.cursor()
        cursor.execute(query, query_params)
        connection_row = cursor.fetchone()

        if connection_row:
            return Connection(self.dbi, self.id, **dict(connection_row))
        else:
            log.info('Connection not found, id=%s, phone_number=%s, other_user=%s' % (connection_id, phone_number, other_user))
            #log.info(query)
            #log.info(query_params)
            raise ConnectionNotFoundError

    def get_connections(self, export_name=None):
        db = self.dbi.db

        if not export_name:
            export_name = 'global'

        query = base_in_connection_query
        query += '''
        AND LOWER(E.Name) = LOWER(%s)
        '''

        query += '''
        AND C.Active IS TRUE
        AND U.Active IS TRUE
        AND E.Active IS TRUE
        '''

        connection_list = []
        cursor = db.cursor()
        cursor.execute(query, (self.id, export_name,))
        rows = cursor.fetchall()

        for connection_row in rows:
            user_connection = Connection(self.dbi, self.id, **dict(connection_row))
            connection_list.append(user_connection)

        return connection_list

    def get_nudge_stats(self, export_name=None, max_age=None):
        db = self.dbi.db

        if not export_name:
            export_name = 'global'

        if not max_age:
            max_age = 86400 * 60

        query = '''
        SELECT * FROM (
            SELECT
                U.FriendlyName as FriendlyName,
                N.SourceUserID as SourceUserID,
                EXTRACT(EPOCH FROM (NOW() - N.DateAdded)) as NudgeAge
            FROM
                NudgeRequestResponses N,
                Users U
            WHERE
                N.ExportID = LOWER(%s)
                AND
                N.SourceUserID = U.ID
                AND
                N.DestUserID = %s
        ) AS SUBQUERY
        WHERE
            NudgeAge < %s
        ORDER BY
            NudgeAge
        '''

        nudges = []
        cursor = db.cursor()
        cursor.execute(query, (export_name, self.id, max_age))
        rows = cursor.fetchall()

        for row in rows:
            nudges.append((int(row['sourceuserid']), int(row['nudgeage'])))

        stats = {
            'Users': {},
            'Overall': None
        }

        for user_id, nudge_age in nudges:
            user_stats = stats['Users'].get(user_id)
            if not user_stats:
                user_stats = {
                    'QuantityTotal': 0,
                    'QuantityWeek': 0,
                    'QuantityMonth': 0,
                }

            user_stats['QuantityTotal'] += 1
            if nudge_age <= 7 * 86400:
                user_stats['QuantityWeek'] += 1
            
            if nudge_age <= 30.5 * 86400:
                user_stats['QuantityMonth'] += 1

            stats['Users'][user_id] = user_stats

        num_nudge_sources = len(stats['Users'].keys())
        num_nudges = len(nudges)

        nudges_per_source = 0
        if num_nudge_sources > 0:
            nudges_per_source = num_nudges / float(num_nudge_sources)

        stats['Overall'] = {
            'NumNudgeSources': num_nudge_sources,
            'NumNudges': num_nudges,
            'NudgesPerSource': nudges_per_source
        }

        return stats

    def get_supporting(self, export_name=None):
        db = self.dbi.db

        if not export_name:
            export_name = 'global'

        query = '''
        SELECT
            DISTINCT E.UserID AS user_id
        FROM 
            UserConnections C,
            Users U,
            UserExports E
        WHERE
            C.ExportID = E.ID
            AND
            U.ID = E.UserID
            AND
            C.Verified IS TRUE
            AND
            C.Active IS TRUE
            AND
            U.Active IS TRUE
            AND
            E.Active IS TRUE
            AND
            E.Name = %s
            AND
            C.OutUserID = %s
        '''

        users = []
        cursor = db.cursor()
        for row in cursor.execute(query, (export_name, self.id,)):
            user = self.dbi.get_user(user_id=row['user_id'])
            users.append(user)

        return users

    def add_request_response(self, dest_user, dest_export_id, dest_context_id, dest_code):
        db = self.dbi.db

        query = '''
        INSERT INTO
            NudgeRequestResponses (SourceUserID, DestUserID, ExportID, ContextID)
        VALUES
            (%s, %s, %s, %s)
        '''
        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, dest_user.id, dest_export_id, dest_context_id))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def add_support_message(self, message):
        db = self.dbi.db

        query = '''
        INSERT INTO
            SupportMessages (UserID, Message)
        VALUES
            (%s, %s)
        '''
        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, message))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def add_incoming_sms(self, message_data, phone_number_verified=False):
        db = self.dbi.db

        query = '''
        INSERT INTO
            IncomingSMS (UserID, PhoneNumber, MessageData)
        VALUES
            (%s, %s, %s)
        '''
        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, self.phone_number, simplejson.dumps(message_data)))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def add_outgoing_sms(self, message):
        db = self.dbi.db

        query = '''
        INSERT INTO
            OutgoingSMS (UserID, PhoneNumber, Message)
        VALUES
            (%s, %s, %s)
        '''
        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, self.phone_number, message))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def add_nudge(self, dest_user, context_id):
        db = self.dbi.db

        query = '''
        INSERT INTO
            Nudges (SourceUserID, DestUserID, ContextID)
        VALUES
            (%s, %s, %s)
        '''
        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, dest_user.id, context_id))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def add_nudge_request(self, source_user):
        db = self.dbi.db

        query = '''
        INSERT INTO
            NudgeRequests (DestUserID, SourceUserID)
        VALUES
            (%s, %s)
        '''
        cursor = db.cursor()
        try:
            cursor.execute(query, (self.id, source_user.id))
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()

    def get_incoming_nudges(self, max_age=None):
        db = self.dbi.db

        age_filter = ''
        if max_age is not None:
            age_filter = '''AND
            EXTRACT(EPOCH FROM (NOW() - DateAdded)) < {0}
        '''.format(max_age)

        query = '''
        SELECT
            SourceUserID,
            EXTRACT(EPOCH FROM DateAdded) AS DateAdded,
            ContextID
        FROM
            Nudges
        WHERE
            DestUserID = %s
        {0}
        '''.format(age_filter)

        cursor = db.cursor()
        cursor.execute(query, (self.id, ))
        query_result = cursor.fetchall()

        incoming_nudges = []
        for nudge_row in query_result:
            nudge_row = dict(nudge_row)
            incoming_nudges.append((nudge_row['sourceuserid'], int(nudge_row['dateadded']), nudge_row['contextid']))

        return incoming_nudges

    def get_incoming_nudges2(self, max_age=None):
        db = self.dbi.db

        self_user = self.dbi.get_user(user_uuid=SYSTEM_UUID)
        anon_user = self.dbi.get_user(user_uuid=ANON_UUID)

        age_filter = ''
        if max_age is not None:
            age_filter = '''AND
            EXTRACT(EPOCH FROM (NOW() - Nudges.DateAdded)) < {0}
        '''.format(max_age)

        query = '''
        SELECT * FROM (
            SELECT
                SourceUserID,
                Users.ID = {0} as IsSelf,
                Users.ID = {1} as IsAnon,
                EXTRACT(EPOCH FROM Nudges.DateAdded) AS DateAdded,
                EXTRACT(EPOCH FROM (NOW() - Nudges.DateAdded)) as NudgeAge,
                ContextID
            FROM
                Nudges,
                Users
            WHERE
                Nudges.SourceUserID = Users.ID
                    AND
                Nudges.DestUserID = %s
                {2}
        ) AS SUBQUERY
        WHERE
            NudgeAge < %s
            OR
            %s IS NULL
        '''.format(self_user.id, anon_user.id, age_filter)

        supporters = self.get_supporters()
        supporter_map = {}
        for c in supporters:
            supporter_map[c.out_user.id] = True

        stats = {
            'Total': 0,
            'FromSelf': 0,
            'FromUser': 0,
            'FromSupporter': 0,
            'FromAnon': 0
        }
        nudges = []
        cursor = db.cursor()
        cursor.execute(query, (self.id, max_age, max_age))
        query_result = cursor.fetchall()
        for row in query_result:
            from_self = row['isself']
            from_anon = row['isanon']
            from_supporter = False
            from_user = False
            source_user_id = int(row['sourceuserid'])
            if not from_self and not from_anon:
                from_supporter = source_user_id in supporter_map
                from_user = not from_supporter

            nudges.append((source_user_id, int(row['dateadded']), row['contextid'], int(row['nudgeage']), from_supporter, from_user, from_self, from_anon))

            if from_supporter:
                stats['FromSupporter'] += 1

            if from_user:
                stats['FromUser'] += 1

            if from_self:
                stats['FromSelf'] += 1

            if from_anon:
                stats['FromAnon'] += 1

            stats['Total'] += 1

        return nudges, stats

    def get_activity_stats(self):
        import time

        root_context_id = self.get_root_context_id()
        contexts = call_container('get_contexts', data=root_context_id, user=self)
        
        target_contexts = filter(lambda x: x.get('1__TargetFrequencyValue') > 0 and x.get('1__TargetActive') not in (0, '0'), contexts)
        target_contexts.sort(key=lambda x: x.get('1__Timestamp'), reverse=True)

        time_since = None
        if target_contexts:
            time_since = int(time.time() - float(target_contexts[0].get('1__Timestamp')))

        return {
            'TimeSince': time_since,
            'Num': len(target_contexts)
        }

    def get_connection_stats(self):
        db = self.dbi.db

        query = '''
        SELECT
            MIN(EXTRACT(EPOCH FROM (NOW() - C.DateAdded))), COUNT(*)
        FROM
            UserConnections C,
            UserExports E
        WHERE
            C.ExportID = E.ID
            AND
            E.UserID = %s
        '''
        cursor = db.cursor()
        cursor.execute(query, (self.id, ))
        query_result = cursor.fetchone()

        print self.id, query

        time_since = None
        num_connections = None

        if query_result:
            print query_result
            num_connections = int(query_result[1])

            if num_connections:
                time_since = int(query_result[0])

        return {
            'TimeSince': time_since,
            'Num': num_connections
        }


class Connection(object):
    def __init__(self, dbi, user_id, **properties):
        self.dbi = dbi
        self.user_id = user_id

        for key, val in properties.items():
            setattr(self, key, val)

        self.out_user = self.dbi.get_user(user_id=properties['out_user_id'])

    def __repr__(self):
        s = (self.connection_id, self.export_id, self.out_user_id,
                self.outuser_friendly_name or self.friendly_name)
        return '<Connection ID=%s, EID=%s, OUID=%s, FN=%s>' % s

    def save(self):
        db = self.dbi.db

        col_map = {}
        query = '''
        UPDATE
            UserConnections
        SET
            Verified = %s
        WHERE
            ID = %s
        '''

        param_list = [
            self.verified or False,
            self.connection_id
        ]
        cursor = db.cursor()
        try:
            cursor.execute(query, param_list)
        except psycopg2.IntegrityError:
            db.rollback()
            raise
        else:
            db.commit()


class UserInitError(Exception):
    pass

class MultiDBInitError(Exception):
    pass

def initialize_user(dbi, friendly_name, phone_number=None, email_address=None, account_type=None, timezone=None, 
                        username=None, init_db=True, is_multi=True, import_data=None):

    if not account_type:
        account_type = 'Simple'
    try:
        user = dbi.add_user(friendly_name, timezone,
                            phone_number=phone_number,
                            email_address=email_address, 
                            account_type=account_type,
                            username=username)
    except (UUIDExistsError, UsernameExistsError, PhoneNumberExistsError, EmailExistsError):
        raise
    except Exception as e:
        log.exception('Error adding user: %s' % e)
        raise UserInitError
    else:
        try:
            if init_db and is_multi:
                init_multi_user_db(user, import_data=import_data)
        except MultiDBInitError as e:
            log.exception('Error initializing user DB: %s' % e)
            raise
        else:
            return user

def init_multi_user_db(user, import_data=None):
    try:
        from benome.init_db import init_db
        db_path = '/opt/benome/data/multi/{0}.db'.format(user.user_uuid)
        init_db(db_path, import_data=import_data)
    except Exception as e:
        msg = 'Error initializing multi-user DB for %s' % user
        log.exception(msg)
        raise MultiDBInitError(msg)
    else:
        return db_path

def print_user(user):
    print user.id, user.username, user.friendly_name
    print 'active:', user.active
    print 'requests_enabled:', user.requests_enabled
    print 'phone_number_verified:', user.phone_number_verified
    print 'force_friendly_name:', user.force_friendly_name
    print 'accepts_requests:', user.accepts_requests
    print 'email_enabled:', user.email_enabled
    print 'sms_enabled:', user.sms_enabled
    print 'mms_enabled:', user.mms_enabled
    print 'feedback_frequency:', user.feedback_frequency
    print 'request_threshold:', user.request_threshold
    print 'max_requests_per_day:', user.max_requests_per_day
    print 'max_requests_per_day_per_user:', user.max_requests_per_day_per_user
    print 'config: ', user.config
    print '-------------\n'

if __name__ == '__main__':
    import sys
    dbi = DBInterface('test')
    
    if 0:
        print dbi.get_users()

    if 0:
        user = initialize_user(dbi, 'test', timezone='US/Pacific', is_multi=False)
        print_user(user)

        user.active = False
        user.save()
        user = dbi.get_user(user_id=user.id)
        print 'active2', user.active

    if 0:
        nonuser = dbi.add_nonuser(phone_number=None, email_address='steve@haxel.ca', timezone='US/Pacific', commit=True)
        print 'active', nonuser.active
        print 'timezone', nonuser.timezone
        print 'state', nonuser.state

        nonuser.active = False
        nonuser.timezone = 'Blah/Blah'
        nonuser.state = {'x': 'test'}
        nonuser.save()

        nonuser = dbi.get_nonuser(nonuser_id=nonuser.id)

        print 'active', nonuser.active
        print 'timezone', nonuser.timezone
        print 'state', nonuser.state

        user = initialize_user(dbi, 'test', timezone='US/Pacific', is_multi=False)
        nonuser.add_follow(user)

        # Should have no effect
        nonuser.add_follow(user)

        # Should remove the single existing follow
        nonuser.remove_follow(user)

    if 0:
        user = initialize_user(dbi, 'test', timezone='US/Pacific', is_multi=False)
        user2 = initialize_user(dbi, 'test2', timezone='US/Pacific', is_multi=False)
        user3 = initialize_user(dbi, 'test3', timezone='US/Pacific', is_multi=False)

        print user2.add_follow(user)
        print user3.add_follow(user)

        print 'user.get_followers()', user.get_followers()
        print 'user2.get_following()', user2.get_following()
        print 'user3.get_following()', user3.get_following()

        print user3.remove_follow(user)
        print 'user.get_followers()', user.get_followers()
        print 'user2.get_following()', user2.get_following()
        print 'user3.get_following()', user3.get_following()

    if 0:
        user = initialize_user(dbi, 'test', timezone='US/Pacific', is_multi=False)
        user.add_push_subscription({'Test': 1}, str(random.randint(1000000, 9999999)))
        user.add_push_subscription({'Test': 2}, str(random.randint(1000000, 9999999)))
        user.add_push_subscription({'Test': 3}, str(random.randint(1000000, 9999999)))

        for subscription_id, subscription_info in user.get_push_subscriptions():
            print subscription_id, subscription_info
            print user.remove_push_subscription(subscription_id)

        print user.get_push_subscriptions()

    if 0:
        user = initialize_user(dbi, 'test', phone_number=str(random.randint(1000000, 9999999)), timezone='US/Pacific', is_multi=False)
        user2 = initialize_user(dbi, 'test', phone_number=str(random.randint(1000000, 9999999)), timezone='US/Pacific', is_multi=False)

        user.add_incoming_sms({'message': {'data': []}})
        user.add_outgoing_sms('message body')
        user.add_nudge(user2, 2234)

    if 0:
        phone_number = str(random.randint(1000000, 9999999))
        user = initialize_user(dbi, 'test', phone_number=phone_number, timezone='US/Pacific', is_multi=False)

        connection = user.add_connection('global', phone_number, 'test', dest_timezone='US/Pacific', verified=True)
        connection = user.add_connection('global', str(random.randint(1000000, 9999999)), 'test2', dest_timezone='US/Pacific', verified=True)
        connection = user.add_connection('global', str(random.randint(1000000, 9999999)), 'test3', dest_timezone='US/Pacific', verified=True)

        conn = user.get_connection(phone_number=phone_number)
        print 'conn', conn

        conns = user.get_connections()
        print 'conns', conns

    if 0:
        user = initialize_user(dbi, 'test', phone_number=str(random.randint(1000000, 9999999)), timezone='US/Pacific', is_multi=False)
        user2 = initialize_user(dbi, 'test2', phone_number=str(random.randint(1000000, 9999999)), timezone='US/Pacific', is_multi=False)
        user3 = initialize_user(dbi, 'test2', phone_number=str(random.randint(1000000, 9999999)), timezone='US/Pacific', is_multi=False)

        user2.add_request_response(user, 'global', 'z', 'z')
        user2.add_request_response(user, 'global', 'z', 'z')
        user2.add_request_response(user, 'global', 'z', 'z')
        user2.add_request_response(user, 'global', 'z', 'z')

        user3.add_request_response(user, 'global', 'z', 'z')
        user3.add_request_response(user, 'global', 'z', 'z')
        user3.add_request_response(user, 'global', 'z', 'z')
        user3.add_request_response(user, 'global', 'z', 'z')

        print user.get_nudge_stats()

    if 0:
        user = initialize_user(dbi, 'test', phone_number=str(random.randint(1000000, 9999999)), timezone='US/Pacific', is_multi=False)
        user2 = initialize_user(dbi, 'test2', phone_number=str(random.randint(1000000, 9999999)), timezone='US/Pacific', is_multi=False)

        print user, user2

        user.add_nudge(user2, 2234)
        user.add_nudge(user2, 2235)
        user.add_nudge(user2, 2236)
        user.add_nudge(user2, 2237)

        user2.add_nudge(user, 2234)
        user2.add_nudge(user, 2235)
        user2.add_nudge(user, 2236)
        user2.add_nudge(user, 2237)

        print user2.get_incoming_nudges2()
        print user.get_incoming_nudges2()

    if 0:
        phone_number = str(random.randint(1000000, 9999999))
        user = initialize_user(dbi, 'test', phone_number=phone_number, timezone='US/Pacific', is_multi=False)

        connection = user.add_connection('global', str(random.randint(1000000, 9999999)), 'test2', dest_timezone='US/Pacific', verified=True)
        connection = user.add_connection('global', str(random.randint(1000000, 9999999)), 'test3', dest_timezone='US/Pacific', verified=True)
        connection = user.add_connection('global', str(random.randint(1000000, 9999999)), 'test4', dest_timezone='US/Pacific', verified=True)

        time.sleep(2)

        print user.get_connection_stats()

    if 0:
        user = dbi.get_user(user_uuid=SYSTEM_UUID)

        group_name = 'Test group %s' % str(random.randint(1000000, 9999999))
        group = dbi.add_group(user, group_name)
        print group

        group.add_member(user)
        group.add_member(user)

        user2 = dbi.get_user(user_uuid=ANON_UUID)
        group.add_member(user2)

        phone_number = str(random.randint(1000000, 9999999))
        user3 = initialize_user(dbi, 'test', phone_number=phone_number, timezone='US/Pacific', is_multi=False)
        group.add_member(user3)

        print len(group.get_members())

        group.remove_member(user)
        group.remove_member(user2)
        
        print len(group.get_members())

    if 0:
        groups = dbi.get_groups()
        for g in groups:
            print g
            for m in g.get_members():
                print m[0]

    if 0:
        user = dbi.get_user(user_uuid=SYSTEM_UUID)
        
        for g in user.get_groups():
            print g
            members = g.get_members(except_user=user)
            for m in members:
                print m[0]

    if 0:
        user = dbi.get_user(user_uuid=SYSTEM_UUID)

        from redis import StrictRedis
        from benome.global_config import REDIS_HOST
        redis = StrictRedis(host=REDIS_HOST, db=0)

        period = 86400
        reset_time = ((int(time.time()) // period) * period) + period

        available_members = None
        while available_members is None or available_members:
            available_members = get_available_group_users(redis, user, 80)

            if not available_members:
                break

            member, group = random.choice(available_members)
            #print_user(member)
            print member

            # Increment the count of requests received in the last 24 hour period
            key = 'UserGroupRequests_U=%d_G=%d' % (member.id, group.id)
            p = redis.pipeline()
            p.incr(key)
            p.expireat(key, reset_time + 5)
            p.execute()
            
            # Increment the current user's group-request count
            key = 'GroupUserRequests_G=%d_U=%d' % (group.id, user.id)
            p = redis.pipeline()
            p.incr(key)
            p.expireat(key, reset_time + 5)
            p.execute()

    if 0:
        groups = dbi.get_groups()

        group = groups[0]
        user = dbi.get_user(user_uuid=SYSTEM_UUID)
        invite_code = group.generate_invite_code(user)

        phone_number = str(random.randint(1000000, 9999999))
        user = initialize_user(dbi, 'test', phone_number=phone_number, timezone='US/Pacific', is_multi=False)

        print invite_code, user
        dbi.handle_group_invite(user, invite_code)

    if 0:
        groups = dbi.get_groups()

    	import random
        group = random.choice(groups)
        user = dbi.get_user(user_uuid=SYSTEM_UUID)
        invite_code = group.generate_invite_code(user)
        print group, invite_code

    if 0:
        user = dbi.get_user(username='steve')

        group = dbi.get_group(16)
        group.add_member(user)

        group = dbi.get_group(21)
        group.add_member(user)

        group = dbi.get_group(22)
        group.add_member(user)

    if 1:
        user = dbi.get_user(username='steve')
        group_name = 'First Group'
        group = dbi.add_group(user, group_name)
        group.add_member(user)

        invite_code = group.generate_invite_code(user)
        print user, group, invite_code
