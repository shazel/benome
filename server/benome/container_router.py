# Copyright 2016 Steve Hazel
#
# This file is part of Benome.
#
# Benome is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# Benome is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Benome. If not, see http://www.gnu.org/licenses/.
#

import simplejson
import requests
from benome.utils import json_response, json_get, json_post

def map_user_to_container(user):
    return '127.0.0.1', 25999

def get_container_host(user):
    ip = '127.0.0.1'
    port = user.get_port()
    
    if not port or type(port) is not int:
        # Go for a multi container
        ip, port = map_user_to_container(user)
        if not port:
            raise Exception('Container not available')

    return 'http://%s:%d' % (ip, port)

def forward_container(user, request, url_path=None, as_response=True, return_code=True, timeout=5, as_json=False):
    import requests

    base_host = get_container_host(user)
    url_path = url_path or request.full_path
    
    headers = {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'X-Benome-User-ID': user.user_uuid
    }

    try:
        json_data = simplejson.dumps(request.json or {})
        if request.method == 'POST':
            r = requests.post(base_host + url_path, data=json_data, headers=headers, timeout=timeout)
        elif request.method == 'PUT':
            r = requests.put(base_host + url_path, data=json_data, headers=headers, timeout=timeout)
        elif request.method == 'DELETE':
            r = requests.delete(base_host + url_path, data=json_data, headers=headers, timeout=timeout)
        elif request.method == 'GET':
            r = requests.get(base_host + url_path, headers=headers, timeout=timeout)

        if callable(r.json):
            json = r.json()
        else:
            json = r.json

        status_code = r.status_code

    except Exception, e:
        print 'Forward error to %s: %s' % (url_path, e)
        json = None
        status_code = 600

    if as_json:
        if return_code:
            return json, status_code
        else:
            return json
    else:
        if return_code:
            return json_response(json), status_code
        else:
            return json_response(json)

def call_container(cmd, data=None, port=None, timeout=5, user=None, params=None, **kwargs):
    base_host = get_container_host(user)
    result = None

    headers = {
        'X-Benome-User-ID': user.user_uuid
    }

    if cmd == 'get_root_context_id':
        result, status_code = json_get('%s/get_root_context_id' % base_host, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'init_root_context_id':
        result, status_code = json_get('%s/init_root_context_id' % base_host, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'get_id_block':
        result, status_code = json_get('%s/get_id_block' % base_host, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'get_last_id':
        result, status_code = json_get('%s/get_last_id' % base_host, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'get_contexts':
        result, status_code = json_get('%s/data/contexts/%s' % (base_host, data), return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'get_points':
        query = ''

        if type(params) is dict:
            q = []
            for key, val in params.items():
                q.append('%s=%s' % (key, val))
            query = '?' + '&'.join(q)
            
        result, status_code = json_get('%s/data/points/%s%s' % (base_host, data, query), return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'get_associations':
        result, status_code = json_get('%s/data/associations/%s' % (base_host, data), return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'add_point':
        result, status_code = json_post('%s/data/point' % base_host, data, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'add_context':
        result, status_code = json_post('%s/data/context' % base_host, data, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'update_context':
        result, status_code = json_post('%s/data/context/%s' % (base_host, data['ContextID']), data, return_code=True, timeout=timeout, headers=headers, put=True)

    elif cmd == 'data_query':
        timeout = 15
        if data:
            url = '%s/data/query/%s' % (base_host, data)
        else:
            url = '%s/data/query' % base_host

        result, status_code = json_get(url, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'data_export':
        export_id = data['ExportID']
        window = data.get('Window')
        num_segments = data.get('NumSegments')
        offset = data.get('Offset')
        anonymized = data.get('Anonymized')
        image_depth = data.get('ImageDepth')
        adaptive_window = data.get('AdaptiveWindow')
        segment_size = data.get('SegmentSize')

        url_keys = []
        if window:
            url_keys.append('Window=%d' % window)
        if num_segments:
            url_keys.append('NumSegments=%d' % num_segments)
        if offset:
            url_keys.append('Offset=%d' % offset)
        if anonymized in (True, False):
            url_keys.append('Anonymized=%s' % anonymized)
        if image_depth >= 0:
            url_keys.append('ImageDepth=%d' % image_depth)
        if adaptive_window in (True, False):
            url_keys.append('AdaptiveWindow=%s' % adaptive_window)
        if segment_size >= 0:
            url_keys.append('SegmentSize=%d' % segment_size)

        url_params = ''
        if url_keys:
            url_params = '?' + '&'.join(url_keys)

        url = '%s/data/export/%s%s' % (base_host, export_id, url_params)
        result, status_code = json_get(url, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'data_unmap_contextid':
        export_id, context_id = data
        url = '%s/data/export/unmap_contextid/%s/%s' % (base_host, export_id, context_id)
        result, status_code = json_get(url, return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'get_context':
        result, status_code = json_get('%s/data/context/%s' % (base_host, data), return_code=True, timeout=timeout, headers=headers)

    elif cmd == 'behave_query':
        result, status_code = json_get('%s/data/behave/query' % base_host, return_code=True, timeout=timeout, headers=headers)

    if is_error(result):
        raise Exception('Container call error: %s' % result)
    elif status_code != 200:
        raise Exception('HTTP error response')

    return result

def is_error(err):
    return type(err) is dict and err.get('Error')

