# Copyright 2016 Steve Hazel
#
# This file is part of Benome.
#
# Benome is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# Benome is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Benome. If not, see http://www.gnu.org/licenses/.
#

import os
import simplejson
import re
import regex
import string
import time
import math
from datetime import datetime
import random
from uuid import uuid4
from functools import update_wrapper

import requests
from redis import StrictRedis
from flask import Flask, Response, request, g, make_response, render_template, \
                    redirect, session, abort
from flask_login import LoginManager, UserMixin, AnonymousUserMixin, login_required, login_user, logout_user, \
                    current_user

from benome.utils import json_response, json_get, json_post, set_interval, load_json_file
from benome.expiredict import ExpireDict
from benome.global_config import REDIS_HOST
from container_manager import ContainerManager
from benome.container_router import forward_container, call_container
from benome.user_db import DBInterface

SYSTEM_UUID = 'eeeeeeee-ffff-0000-1111-222233334444'
ANON_UUID = 'deeeeeee-ffff-0000-1111-222233334444'

keyval_cache = ExpireDict(100)

container_manager = ContainerManager('http://127.0.0.1:5200', '127.0.0.1', user_manager=False)

redis = StrictRedis(host=REDIS_HOST, db=0)
dbi = None
config = None
app = Flask(__name__)

from emoji_api import emoji_api
emoji_api.base_app = app
app.register_blueprint(emoji_api, url_prefix='/i')

SECRET_FILE = os.path.normpath(os.path.join('.', 'SECRET.key'))
try:
    secret_key = open(SECRET_FILE).read().strip()
except IOError:
    try:
        secret_key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(50))
        with open(SECRET_FILE, 'w') as f:
            f.write(secret_key)
    except IOError:
        raise Exception('Could not open %s for writing!' % SECRET_FILE)


app.config.update(
    SECRET_KEY = secret_key
)
app.secret_key = secret_key

default_features = {
    'PointDetail': True,
    'LeafFocusToggleTimer': True,
    'LeafFocusAutoAdd': True,
    'Admin': True,
    'DetailLevels': True,
    'MovableFocus': True
}

token_map = {}

base_app_map = {
    'Global': 'base.html',
    'Breakdown': 'base-breakdown.html',
    'Behave': 'base-behave.html'
}
default_app = 'Global'

class BenomeControllerException(Exception):
    pass

class BenomeAuthException(BenomeControllerException):
    pass

@app.errorhandler(Exception)
def api_exception_handler(error):
    from flask import g

    http_code = 200

    if type(error) is BenomeAuthException:
        error_type = 'Authentication Error'
        http_code = 403
    elif type(error) is BenomeControllerException:
        error_type = 'Controller Error'
    else:
        error_type = 'Internal Error'

    import traceback
    traceback.print_exc()

    error_result = {
        'Error': True,
        'Type': error_type,
        'Message': str(error)
    }

    if hasattr(g, 'jsonp') and g.jsonp:
        return '%s(%s)' % (g.jsonp, simplejson.dumps(error_result))
    else:
        return json_response(error_result), http_code

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

class User(UserMixin):
    def __init__(self, user):
        self.user = user
        self.id = user.id
        self.username = user.username
        self.name = user.username
        self.config = user.config or {}
        self.features = user.features or {}

    def get_name(self):
        return self.name

    def get_id(self):
        return self.id

    def get_root_context_id(self):
        return self.user.get_root_context_id()

    def get_config(self):
        return self.user.get_config()

    def get_features(self, default_features=None):
        return self.user.get_features(default_features=default_features)

    def containerized(self):
        return True

    def get_port(self):
        return self.user.get_port()

    def __repr__(self):
        return '<%s: %s>' % (self.id, self.get_name())

def get_global_port():
    return 25001

    # user_id = get_global_user_id()
    # container_manager.ensure_container(user_id)

    # try:
    #     return int(container_manager.get_user_port(user_id))
    # except:
    #     pass

class AnonymousUser(AnonymousUserMixin):
    def get_port(self):
        return get_global_port()

login_manager.anonymous_user = AnonymousUser

def init_user(benome_user=None, username=None, user_id=None, user_uuid=None, password=None, auth=True):
    flask_user = None

    try:
        if not benome_user:
            params = {}
            if user_uuid:
                params['user_uuid'] = user_uuid

            elif user_id:
                params['user_id'] = user_id

            elif username:
                params['username'] = username

            try:
                benome_user = dbi.get_user(**params)
            except dbi.UserNotFound:
                raise Exception('User not found: %s' % params)
    except Exception, e:
        print 'Init user error for %s: %s' % (params, e)
        import traceback ; traceback.print_exc()
        raise
    else:
        if auth is True:
            if benome_user.auth(password):
                flask_user = User(benome_user)
            else:
                print 'Auth failed for user: %s' % benome_user
        elif benome_user.username:
            flask_user = User(benome_user)

    return flask_user

@app.errorhandler(401)
def login_failed(e):
    return Response('<p>Login failed</p>')

@login_manager.user_loader
def load_user(user_id):
    return init_user(user_id=user_id, auth=False)


class RateLimit(object):
    expiration_window = 10

    def __init__(self, key_prefix, limit, per, send_x_headers):
        self.reset = (int(time.time()) // per) * per + per
        self.key = key_prefix + str(self.reset)
        self.limit = limit
        self.per = per
        self.send_x_headers = send_x_headers
        p = redis.pipeline()
        p.incr(self.key)
        p.expireat(self.key, self.reset + self.expiration_window)
        self.current = min(p.execute()[0], limit)

    remaining = property(lambda x: x.limit - x.current)
    over_limit = property(lambda x: x.current >= x.limit)

def get_view_rate_limit():
    return getattr(g, '_view_rate_limit', None)

def on_over_limit(limit):
    raise BenomeControllerException('You hit the rate limit')

def ratelimit(limit, per=300, send_x_headers=True,
              over_limit=on_over_limit,
              scope_func=lambda: request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr),
              key_func=lambda: request.endpoint):
    def decorator(f):
        def rate_limited(*args, **kwargs):
            print 3, request.environ.get('HTTP_X_FORWARDED_FOR'), request.remote_addr

            key = 'rate-limit/%s/%s/' % (key_func(), scope_func())
            rlimit = RateLimit(key, limit, per, send_x_headers)
            g._view_rate_limit = rlimit
            if over_limit is not None and rlimit.over_limit:
                return over_limit(rlimit)
            return f(*args, **kwargs)
        return update_wrapper(rate_limited, f)
    return decorator

@app.route('/test', methods=['GET'])
def client_test():
    if not current_user or current_user.is_anonymous:
        return 'Unauthorized', 403

    return json_response(auth_result(current_user)), 200

@app.route('/data/load/<context_id>', methods=['GET'])
@app.route('/data/load/', methods=['GET'])
#@login_required
def data_load(context_id=None):
    if not current_user or current_user.is_anonymous:
        return json_response({
            'ContextID': None,
            'Points': [],
            'Contexts': [],
            'Associations': []
        })

    if not context_id or str(context_id) in ('null', 'None'):
        context_id = current_user.get_root_context_id()

    contexts = call_container('get_contexts', data=context_id, user=current_user.user)
    points = call_container('get_points', data=context_id, user=current_user.user)
    associations = call_container('get_associations', data=context_id, user=current_user.user)

    return json_response({
        'ContextID': context_id,
        'Points': points,
        'Contexts': contexts,
        'Associations': associations
    })

def get_behave_app_id(user):
    # get app ID
    behave_app_id = None
    app_contexts = call_container('get_contexts', data=1003, user=user)
    for app_context in app_contexts:
        if app_context.get('1__Label') == 'Behave':
            behave_app_id = int(app_context.get('ID'))
            break

    return behave_app_id

@app.route('/get_id_block', methods=['GET'])
@app.route('/get_id_block/<block_size>', methods=['GET'])
@app.route('/data/associations/<context_id>', methods=['GET'])
@app.route('/data/points/<context_id>', methods=['GET'])
@app.route('/data/contexts/<context_id>', methods=['GET'])
@app.route('/data/association/<association_id>', methods=['POST', 'PUT', 'DELETE'])
@app.route('/data/point/<point_id>', methods=['POST', 'PUT', 'DELETE'])
@app.route('/data/context/<context_id>', methods=['POST', 'PUT', 'DELETE'])
@app.route('/data/export', methods=['GET'])
def data_interface(*args, **kwargs):
    user = current_user.user
    result, status_code = forward_container(user, request, return_code=True, as_json=True)

    if status_code == 200 and request.method == 'PUT' and '/data/context/' in request.full_path:
        context_id = int(kwargs.get('context_id'))
        behave_app_id = get_behave_app_id(user)
        if behave_app_id and context_id == behave_app_id:
            attributes = request.json
            current_balance = attributes.get('%d__Balance' % behave_app_id)
            if current_balance is not None:
                followers = user.get_followers()
                followers.append(user, {})

                # Call the websocket service once per follower (quite inefficient, obviously)
                # TODO: use redis queues instead
                for dest_user, dest_user_state in followers:

                    data = {
                        'MessageType': 'BalanceModified',
                        'OriginAppID': behave_app_id,
                        'SourceUser': {
                            'Username': user.username,
                            'UserID': user.user_id
                        },
                        'DestUser': {
                            'Username': dest_user.username,
                            'UserID': dest_user.user_id
                        },
                        'Data': {
                            'Balance': current_balance
                        }
                    }
                    json_post('http://localhost:6001/api/add_message', data, return_code=False, timeout=0.2)

    if status_code == 200 and request.method == 'POST' and '/data/point/' in request.full_path:
        behave_app_id = get_behave_app_id(user)

        if behave_app_id:
            attributes = request.json

            # Is a behave node
            total_points = attributes.get('%d__Points' % behave_app_id)
            if total_points:
                context_id = attributes['1__ContextID']
                context = call_container('get_context', context_id, user=user)
                
                source_user = user

                # Notify each follower
                if total_points > 0:
                    title = 'Points for %s!' % source_user.friendly_name
                    message = '%s earned %d points for "%s"' % (source_user.friendly_name, total_points, context['1__Label'])
                elif total_points < 0:
                    title = 'Reward for %s!' % source_user.friendly_name
                    message = '%s redeemed %d points for "%s"' % (source_user.friendly_name, abs(total_points), context['1__Label'])

                image_url = None
                if context:
                    image_code = context.get('1__ImageCode')
                    image_url = 'https://nudge.benome.ca/i/%s' % image_code
                
                # Get all followers
                followers = user.get_followers()
                for dest_user, dest_user_state in followers:
                    # Don't send more than one message per hour (and don't queue/buffer the skipped messages for now)
                    key = 'LastFollowerWebPushMessage-%s' % dest_user.id
                    if redis.exists(key):
                        continue
                    else:
                        redis.setex(key, 3600)

                    data = {
                        'UserID': dest_user.user_id,
                        'SourceUserID': source_user.user_id,
                        'Username': dest_user.username,
                        'OriginID': 'App',
                        'BaseUrl': config['base_site'],
                        'Data': {
                            'Time': time.time(),
                            'MessageType': 'Notification',
                            'Title': title,
                            'Message': message,
                            'IconUrl': source_user.avatar_url,
                            'BadgeUrl': source_user.avatar_url,
                            'ImageUrl': image_url
                        }
                    }

                    try:
                        from pywebpush import webpush
                        for subscription_id, subscription_info in dest_user.get_push_subscriptions():
                            try:
                                webpush(subscription_info,
                                        simplejson.dumps(data),
                                        vapid_private_key='/opt/benome/src/private_key.pem',
                                        vapid_claims={'sub': 'mailto:steve@haxel.ca'})
                            except Exception as e:
                                exc_str = str(e)
                                print 'Push error "%s" for user %s, info: %s' % (exc_str, dest_user, subscription_info)
                                if '410 NotRegistered' in exc_str or '400 UnauthorizedRegistration' in exc_str:
                                    dest_user.remove_push_subscription(subscription_id)
                                continue

                    except Exception, e:
                        print 'Push failed: %s %s' % (data, e)

    return json_response(result), status_code

@app.route('/cache.manifest', methods=['GET'])
def input_manifest():
    context = {}
    response = make_response(render_template('benome.mf', **context))
    response.headers['Content-Type'] = 'text/cache-manifest'
    response.headers['Cache-Control'] = 'no-cache, private'
    return response

@app.route('/local/', methods=['GET'])
@app.route('/local/<instance_id>', methods=['GET'])
def local_root(instance_id=None):
    return render_template('local.html', **{
        'InstanceID': instance_id
    })

@app.route('/demo/', methods=['GET'])
@app.route('/demo/<instance_id>', methods=['GET'])
def demo_root(instance_id=None):
    return render_template('demo.html', **{
        'InstanceID': instance_id
    })

@app.route('/daily/', methods=['GET'])
def app_daily():
    context = {}
    return render_template('index-daily.html', **context)

@app.route('/cluster/', methods=['GET'])
def app_cluster():
    context = {}
    return render_template('index-cluster.html', **context)

@app.route('/numeric/', methods=['GET'])
def app_numeric():
    context = {}
    return render_template('index-numeric.html', **context)

@app.route('/user/logout', methods=['POST', 'GET'])
def user_logout():
    if current_user and not current_user.is_anonymous:
        logout_user()
    else:
        raise BenomeControllerException('Not logged in')

    response = {
        'Success': True
    }

    return json_response(response)

def auth_result(user):
    global default_features

    graph_data = None
    return {
        'SessionID': None,
        'ContextID': user.get_root_context_id(),
        'UserID': user.get_id(),
        'Features': user.get_features(default_features),
        'GraphData': graph_data
    }

@app.route('/user/login', methods=['POST'])
def user_login():
    username = request.form.get('Username')
    password = request.form.get('Password')

    if not username:
        raise BenomeControllerException('Username required')

    try:
        benome_user = dbi.get_user(username=username)
    except Exception as e:
        raise BenomeControllerException('Login failed: %s' % e)

    user_id = None
    context_id = None

    if current_user and not current_user.is_anonymous and current_user.get_name() == username:
        user_id = current_user.get_id()
        context_id = current_user.get_root_context_id()
        flask_user = current_user
    else:
        flask_user = init_user(benome_user=benome_user, password=password, auth=True)
        if flask_user:
            login_user(flask_user, remember=True)
            user_id = flask_user.get_id()
            context_id = flask_user.get_root_context_id()
        else:
            raise BenomeControllerException('Login failed: could not init user')

    return json_response(auth_result(flask_user))

@app.route('/user/change_password', methods=['POST'])
def user_change_password():
    if current_user and current_user.is_anonymous:
        raise BenomeControllerException('Must be already authenticated')

    old_password = request.form.get('OldPassword')
    new_password = request.form.get('NewPassword')

    if not old_password or old_password == new_password:
        raise BenomeControllerException('Invalid input')

    success = user_manager.change_password(current_user.get_id(), old_password, new_password)

    response = {
        'Success': success
    }
    return json_response(response)

@app.route('/<username>', methods=['GET'])
def user_root(username):
    user = None
    display_username = ''
    current_username = ''

    if current_user and not current_user.is_anonymous:
        current_username = current_user.get_name()
        if current_username != username:
            return redirect('/' + current_username)

        else:
            display_username = '%s\'s' % current_username.title()

        user = current_user
    else:
        auth_token = request.args.get('autologin')

        if auth_token in token_map:
            username, password = token_map[auth_token]

            user = init_user(username=username, password=password, auth=True)
            if user:
                login_user(user, remember=True)
                current_username = user.get_name()
                display_username = '%s\'s' % current_username.title()

    if not user or user.is_anonymous:
        return user_status_page(username)
    else:
        base_app = default_app
        username = ''
        if user:
            user_config = user.get_config()
            base_app = user_config.get('BaseApp')

        if base_app not in base_app_map:
            base_app = default_app
        base_app_template = base_app_map[base_app]

        return render_template(base_app_template, **{
            'user': current_username,
            'Username': display_username,
            'userID': user.user.user_id
        })

@app.route('/<username>/streamgraph/<export_id>', methods=['GET'])
def user_app_streamgraph(username, export_id):
    graph_data_json = simplejson.dumps(None)
    user = init_user(username=username, auth=False)
    if user:
        window = None
        try:
            window = int(request.args.get('window', None))
        except:
            pass

        offset = None
        try:
            offset = int(request.args.get('offset', None))
        except:
            pass

        num_segments = None
        try:
            num_segments = int(request.args.get('num_segments', None))
        except:
            pass

        try:
            merged_graph = int(request.args.get('merged', None)) == 1
        except:
            merged_graph = False

        username = user.get_name()
        try:
            params = {
                'ExportID': export_id,
                'Window': window,
                'Offset': offset,
                'NumSegments': num_segments
            }
            graph_data = call_container('data_export', data=params, user=user.user)
        except Exception as e:
            print 'Error in streamgraph retrieving export data for user %s: %s' % (username, e)
        else:
            graph_data_json = simplejson.dumps(graph_data)

    return render_template('base-streamgraph.html', **{
        'Username': username,
        'ExportID': export_id,
        'GraphData': graph_data_json,
        'MergedGraph': 'True' if merged_graph else 'False'
    })

@app.route('/<username>/behave/following', methods=['GET'])
def user_behave_points(username):
    user = init_user(username=username, auth=False)
    if not user:
        raise BenomeControllerException('Invalid username')

    current_user = user.user
    following = current_user.get_following()

    result = []
    result.append({
        'User': {
            'Username': current_user.username,
            'FriendlyName': current_user.friendly_name,
            'UUID': current_user.user_uuid,
            'ID': current_user.id,
            'AvatarUrl': current_user.avatar_url or None
        },
        'Data': None
    })

    for (following_user, user_state) in following:
        try:
            query_response = call_container('behave_query', data=None, user=following_user)
        except Exception as e:
            continue

        result.append({
            'User': {
                'Username': following_user.username,
                'FriendlyName': following_user.friendly_name,
                'UUID': following_user.user_uuid,
                'ID': following_user.id,
                'AvatarUrl': following_user.avatar_url or None
            },
            'Data': query_response
        })

    return json_response(result)

def user_status_page(username):
    graph_data_json = simplejson.dumps(None)
    user = init_user(username=username, auth=False)
    if not user:
        raise BenomeControllerException('Invalid username')

    username = user.get_name()
    try:
        offset = 0
        try:
            offset = int(request.args.get('offset', None))
        except:
            pass

        transparent_background = 0
        try:
            transparent_background = 1 if int(request.args.get('transparent_background', None)) else 0
        except:
            pass

        caption = None
        try:
            caption = request.args.get('caption')
        except:
            pass

        adaptive_window = True
        num_days = 90
        num_segments = num_days * 20
        window = 86400 * num_days

        params = {
            'ExportID': 'global',
            'Offset': offset,
            'Window': window,
            'AdaptiveWindow': adaptive_window,
            'SegmentSize': 86400,
            'NumSegments': num_segments,
            'ImageDepth': 2
        }
        graph_data = call_container('data_export', data=params, user=user.user)
    except Exception as e:
        msg = 'Error in status retrieving export data for user %s: %s' % (username, e)
        raise Exception(msg)
    else:
        max_age = 0
        num_points = 0
        num_nudge_activities = 0
        num_nudges = 0
        num_leaf_contexts = 0
        num_interior_contexts = 0

        if graph_data['ExportData']:
            max_age = graph_data['ExportData']['MaxAge']
            num_points = graph_data['ExportData']['NumPoints']
            num_nudge_activities = graph_data['ExportData']['NumFinalLeafContexts']

            num_leaf_contexts = graph_data['ExportData']['NumLeafContexts']
            num_interior_contexts = graph_data['ExportData']['NumInteriorContexts']

            if adaptive_window:
                window = graph_data['ExportData']['Window']
                num_segments = graph_data['ExportData']['NumSegments']

            incoming_nudges, nudge_stats = user.user.get_incoming_nudges2(max_age=window)
            num_nudges = len(incoming_nudges)

            graph_data['ExportData']['IncomingNudges'] = incoming_nudges
            graph_data['ExportData']['NudgeStats'] = nudge_stats

        graph_data_json = simplejson.dumps(graph_data)

    return render_template('base-status2.html', **{
        'Username': username,
        'FriendlyName': user.user.friendly_name,
        'ExportID': 'global',
        'GraphData': graph_data_json,
        'Offset': offset,
        'Window': window,
        'NumSegments': num_segments,
        'MergedGraph': 'False',
        'TransparentBackground': True if transparent_background else False,
        'Caption': caption,
        'Stats': simplejson.dumps({
            'MaxAge': max_age,
            'NumLeafContexts': num_leaf_contexts,
            'NumInteriorContexts': num_interior_contexts,
            'NumPoints': num_points,
            'NumNudgeActivities': num_nudge_activities
        }),
        'NumYearsInt': int(math.floor(max_age / (365.25 * 86400))),
        'NumYearsFloat': '%.1f' % (max_age / (365.25 * 86400)),
        'MaxAge': max_age,
        'NumLeafContexts': num_leaf_contexts,
        'NumInteriorContexts': num_interior_contexts,
        'NumPoints': num_points,
        'NumNudgeActivities': num_nudge_activities,
        'NumNudges': num_nudges
    })

@app.route('/<username>/status', methods=['GET'])
@app.route('/<username>/status/', methods=['GET'])
def user_app_status(username):
    graph_data_json = simplejson.dumps(None)
    user = init_user(username=username, auth=False)
    if not user:
        raise BenomeControllerException('Invalid username')

    username = user.get_name()
    try:
        offset = 0
        try:
            offset = int(request.args.get('offset', None))
        except:
            pass

        transparent_background = 0
        try:
            transparent_background = 1 if int(request.args.get('transparent_background', None)) else 0
        except:
            pass

        caption = None
        try:
            caption = request.args.get('caption')
        except:
            pass

        num_days = 30
        num_segments = num_days
        window = 86400 * num_days

        params = {
            'ExportID': 'global',
            'Offset': offset,
            'Window': window,
            'SegmentSize': 86400,
            'NumSegments': num_segments,
            'ImageDepth': 2
        }
        graph_data = call_container('data_export', data=params, user=user.user)
    except Exception as e:
        print 'Error in status retrieving export data for user %s: %s' % (username, e)
    else:
        graph_data_json = simplejson.dumps(graph_data)

    return render_template('base-status.html', **{
        'Username': username,
        'FriendlyName': user.user.friendly_name,
        'ExportID': 'global',
        'GraphData': graph_data_json,
        'Offset': offset,
        'Window': window,
        'NumSegments': num_segments,
        'TransparentBackground': True if transparent_background else False,
        'Caption': caption
    })

@app.route('/<username>/cluster', methods=['GET'])
@app.route('/<username>/cluster/', methods=['GET'])
def user_app_cluster(username):
    graph_data_json = simplejson.dumps(None)
    user = init_user(username=username, auth=False)
    if not user:
        raise BenomeControllerException('Invalid username')

    username = user.get_name()
    try:
        offset = 0
        try:
            offset = int(request.args.get('offset', None))
        except:
            pass

        transparent_background = 0
        try:
            transparent_background = 1 if int(request.args.get('transparent_background', None)) else 0
        except:
            pass

        caption = None
        try:
            caption = request.args.get('caption')
        except:
            pass

        num_days = 30
        num_segments = num_days
        window = 86400 * num_days

        params = {
            'ExportID': 'global',
            'Offset': offset,
            'Window': window,
            'SegmentSize': 86400,
            'NumSegments': num_segments,
            'ImageDepth': 2
        }
        graph_data = call_container('data_export', data=params, user=user.user)
    except Exception as e:
        print 'Error in status retrieving export data for user %s: %s' % (username, e)
    else:
        graph_data_json = simplejson.dumps(graph_data)

    return render_template('base-cluster.html', **{
        'ExportID': 'global',
        'GraphData': graph_data_json,
        'Offset': offset,
        'Window': window,
        'NumSegments': num_segments,
        'TransparentBackground': True if transparent_background else False,
        'Caption': caption
    })

@app.route('/<username>/cluster/png', methods=['GET'])
def user_app_status_png(username):
    user = init_user(username=username, auth=False)
    if not user:
        raise BenomeControllerException('Invalid username')

    from benome.utils import ext
    phantom_path = '/usr/bin/phantomjs'
    script_path = '/opt/benome/src/scripts/toplevel.js'

    offset = 0
    try:
        offset_days = int(request.args.get('offset_days', None))
    except:
        pass
    else:
        offset = offset_days * 86400

    try:
        width = str(int(request.args.get('w', None)))
        height = str(int(request.args.get('h', None)))
    except:
        width = '640'
        height = '480'

    transparent_background = 0
    try:
        transparent_background = 1 if int(request.args.get('transparent_background', None)) else 0
    except:
        pass

    caption = None
    try:
        caption = request.args.get('caption', None)
    except:
        pass
    else:
        if caption:
            import urllib
            caption = urllib.quote_plus(caption)

    render_url = 'http://localhost:5300/%s/cluster?offset=%d&caption=%s&transparent_background=%d' % (username, offset, caption, transparent_background)
    params = [script_path, render_url.encode('base64').replace('\n', ''), width, height]
    encoded_image = ext(phantom_path, params, shell=False, wait=True, raw=True, env={'QT_QPA_PLATFORM': 'offscreen'})
    return Response(encoded_image.decode('base64'), mimetype='image/png')

@app.route('/<username>/streamgraph/<export_id>/png', methods=['GET'])
def user_app_streamgraph_png(username, export_id):
    from benome.utils import ext
    phantom_path = '/usr/bin/phantomjs'
    script_path = '/opt/benome/src/scripts/toplevel.js'

    try:
        width = str(int(request.args.get('w', None)))
        height = str(int(request.args.get('h', None)))
    except:
        width = '640'
        height = '480'

    window = 7 * 86400
    try:
        window_years = int(request.args.get('years', None))
        window = int(window_years * 86400 * 365.25)
    except:
        pass

    try:
        window_months = int(request.args.get('months', None))
        window = int(window_months * 86400 * 30.5)
    except:
        pass

    try:
        window_weeks = int(request.args.get('weeks', None))
        window = window_weeks * 86400 * 7
    except:
        pass

    try:
        window_days = int(request.args.get('days', None))
        window = window_days * 86400
    except:
        pass

    try:
        window_hours = int(request.args.get('hours', None))
        window = window_hours * 3600
    except:
        pass

    offset = 0
    try:
        offset = int(request.args.get('offset', None))
    except:
        pass

    try:
        num_segments = int(request.args.get('num_segments', None))
    except:
        num_segments = 150

    try:
        merged_graph = int(request.args.get('merged', None)) == 1
    except:
        merged_graph = False

    render_url = 'http://localhost:5300/%s/streamgraph/%s?window=%d&offset=%d&num_segments=%d&merged=%d' % (username, export_id, window, offset, num_segments, 1 if merged_graph else 0)
    params = [script_path, render_url.encode('base64').replace('\n', ''), width, height]
    encoded_image = ext(phantom_path, params, shell=False, wait=True, raw=True, env={'QT_QPA_PLATFORM': 'offscreen'})
    return Response(encoded_image.decode('base64'), mimetype='image/png')

@app.route('/<username>/situation/week/<nudger_username>/graph.png', methods=['GET'])
def user_app_situation_png(username, nudger_username):
    width = 800
    height = 450
    num_days = 7

    user = init_user(username=username, auth=False)
    if not user:
        raise BenomeControllerException('User not found')

    try:
        nudger_user = dbi.get_user(username=username)
    except Exception:
        raise BenomeControllerException('Nudger not found')
    else:
        nudger_user_id = nudger_user.id

    from nudge.nudge_functions import get_recent_nudges, generate_nudge_graph
    recent_nudges = get_recent_nudges(user.user, 7 * 86400)
    nudge_times = []
    if recent_nudges:
        for from_user_id, nudge_time, context_id in recent_nudges:
            if from_user_id == nudger_user_id:
                nudge_times.append(nudge_time)

    image = generate_nudge_graph(username, width, height, num_days, nudge_times, 
                    title='%s\'s Week' % user.user.friendly_name.title(),
                    labeled_points=None,
                    circle_radius=15,
                    day_font_size=40,
                    title_font_size=72)

    import StringIO
    output = StringIO.StringIO()
    image.save(output, format='PNG')
    png_image = output.getvalue()
    output.close()

    return Response(png_image, mimetype='image/png')

@app.route('/<username>/situation', methods=['GET'])
@app.route('/<username>/situation/', methods=['GET'])
def user_app_situation(username):
    return render_user_app_situation(username, export_id='global')

@app.route('/<username>/situation/<export_id>', methods=['GET'])
@app.route('/<username>/situation/<export_id>/', methods=['GET'])
def user_app_situation_exportid(username, export_id):
    raise Exception('Function disabled temporarily')
    
    return render_user_app_situation(username, export_id)

def render_user_app_situation(username, export_id=None):
    if not export_id:
        export_id = 'global'

    graph_data = None
    graph_data_json = simplejson.dumps(None)

    user = init_user(username=username, auth=False)
    if user:
        username = user.get_name()
        cache_key = '%s-%s-export' % (username, export_id)

        try:
            graph_data_json = keyval_cache[cache_key]
        except Exception as e:
            # Call user's container for export data
            try:
                graph_data = call_container('data_export', data={
                    'ExportID': export_id
                    }, user=user.user)
            except Exception as e:
                print 'Error retrieving export data for user %s/%s: %s' % (username, export_id, e)
            else:
                graph_data_json = simplejson.dumps(graph_data)
                keyval_cache[cache_key] = graph_data_json
                keyval_cache.set_ttl(cache_key, 10 * 60)

    return render_template('base-situation.html', **{
        'Username': username,
        'FirstName': user.user.friendly_name,
        'ExportID': export_id,
        'GraphData': graph_data_json
    })

@app.route('/<username>/situation/nudge/add', methods=['POST'])
@ratelimit(limit=5, per=60 * 10)
def situation_add_nudge(username):
    raise Exception('Function disabled temporarily')
    
    user = init_user(username=username, auth=False)
    if not user:
        return 'Unknown user', 404

    source_id = request.form.get('SourceID', '')
    if len(source_id) > 30 or source_id and not source_id.isalnum():
        raise BenomeControllerException('Invalid input')

    export_id = request.form.get('ExportID', '')
    if len(export_id) > 30 or export_id and not export_id.isalnum():
        raise BenomeControllerException('Invalid input')

    try:
        ref_id = int(request.form.get('RefID'))
    except:
        ref_id = None

    if not ref_id:
        raise BenomeControllerException('Nudge reference ID missing')

    if not export_id:
        export_id = 'global'

    # Perform the nudge
    handle_nudge(user, data={
        'Time': math.ceil(time.time()),
        'SourceID': source_id,
        'ExportID': export_id,
        'RefID': ref_id
    })

    return json_response({
        'Success': True
    })

@app.route('/<username>/api/add_point/<context_id>', methods=['GET', 'POST'])
def add_point(username, context_id):
    dest_user = dbi.get_user(username=username)
    if not dest_user:
        return 'Unknown user', 404
    
    attributes = {
        '1__ContextID': int(context_id),
        '1__Time': time.time()
    }

    print attributes
    result = call_container('add_point', data=attributes, user=dest_user)
    print result
    return json_response(result), 200

@app.route('/<username>/nudge', methods=['POST'])
@ratelimit(limit=2, per=60 * 60 * 4) # once per 4 hours
def add_nudge(username):
    dest_user = dbi.get_user(username=username)
    if not dest_user:
        return 'Unknown user', 404

    # Anonymous user
    source_user = dbi.get_user(user_uuid=ANON_UUID)
    export_id = 'global'
    context_id = None

    from nudge.messaging.utils import client_fingerprint
    nudge_details = {
        'Fingerprint': client_fingerprint(request)
    }

    val = {
        'FromUserID': source_user.id,
        'Timestamp': int(time.time()),
        'Details': nudge_details
    }

    queue_key = 'NudgeQueue-%s' % dest_user.id
    redis.lpush(queue_key, simplejson.dumps(val))
    redis.zincrby('UserNudgesQueued', dest_user.id, 1)

    return json_response({
        'Success': True
    })

@app.route('/register_push_subscriber', methods=['POST'])
def register_push_subscriber():
    data = request.json
    user_id = int(data['userID'])
    instance_id = str(data['instanceID'])
    subscription_info = data['subscription']

    try:
        user = dbi.get_user(user_id=user_id)
    except:
        raise Exception('User not found')

    user.add_push_subscription(subscription_info, instance_id=instance_id)
    return 'OK', 200

@app.route('/VAPIDPubKey')
def vapid_pubkey():
    pubkey = None
    with open('./public_key.txt', 'r') as f:
        pubkey = f.read()
    return pubkey

@app.route('/service-worker.js')
def service_worker():
    file = None
    with open('./static/js/service-worker.js', 'r') as f:
        file = f.read()
    return Response(file, mimetype='text/javascript')

if __name__ == '__main__':
    import sys
    port = 5300
    if len(sys.argv) > 1:
        try:
            port = int(sys.argv[1])
        except:
            raise Exception('Invalid port')

    config = load_json_file('/opt/benome/config.json')
    if config['test_mode']:
        db_path = '/opt/benome/users_test.db'
    else:
        db_path = '/opt/benome/users.db'

    dbi = DBInterface(db_path)
    try:
        app.run(debug=True, host='127.0.0.1', port=port, threaded=True, use_reloader=False)
    except Exception, e:
        print e
