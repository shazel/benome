from flask import Blueprint, Response
import os
from emoji import EMOJI_ALIAS_UNICODE

emoji_api = Blueprint('i', __name__, template_folder='templates')

class DownloadException(Exception):
    pass

@emoji_api.route('/<emoji_shortcut>', methods=['GET'])
def download_emoji_shortcut(emoji_shortcut):
    emoji_key = ':%s:' % emoji_shortcut
    emoji_unicode = EMOJI_ALIAS_UNICODE.get(emoji_key)

    if emoji_unicode:
        hex_chars = []
        for char in emoji_unicode:
            hex_char = hex(ord(char))[2:]
            if hex_char in ('20', '200d', 'fe0f'):
                continue
            hex_chars.append(hex_char)

        fname = '-'.join(hex_chars)
        emoji_path = '/opt/benome/emoji/png_64/%s.png' % fname
        if os.path.exists(emoji_path):
            content_type = 'image/png'
            with open(emoji_path, 'rb') as f:
                return Response(f.read(), mimetype=content_type), 200

    return 'Not found', 404
