# Copyright 2016 Steve Hazel
#
# This file is part of Benome.
#
# Benome is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# Benome is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Benome. If not, see http://www.gnu.org/licenses/.

import time
import simplejson

from flask import request, g
from benome.utils import json_response
from history_log import log_change

import re
uuid4hex = re.compile('[0-9a-f]{8}(\-[0-9a-f]{4}){3}\-[0-9a-f]{12}\Z', re.I)

def is_uuid(u):
    return u and uuid4hex.match(u)

class Routes(object):
    def __init__(self, app, container):
        self.app = app
        self.container = container

        @app.before_request
        def get_user_id():
            g.user_id = request.headers.get('X-Benome-User-ID')

        self.routes = (
            ('/get_root_context_id', 'get_root_context_id', ('GET',)),
            ('/get_apps', 'get_apps', ('GET',)),
            ('/get_sysnodes', 'get_sysnodes', ('GET',)),

            # Points
            ('/data/points/<context_id>', 'data_points', ('GET',)),
            ('/data/point', 'data_point', ('POST',)),
            ('/data/point/<point_id>', 'data_point', ('GET', 'POST', 'PUT', 'DELETE')),

            # Contexts
            ('/data/contexts/<context_id>', 'data_contexts', ('GET', )),
            ('/data/context', 'data_context', ('POST',)),
            ('/data/context/<context_id>', 'data_context', ('GET', 'POST', 'PUT', 'DELETE')),

            # Associations
            ('/data/associations/<context_id>', 'data_associations', ('GET', )),
            ('/data/association/<association_id>', 'data_association', ('GET', 'POST', 'PUT', 'DELETE')),

            # Queries
            ('/data/query', 'data_query', ('GET', )),
            ('/data/query/<context_id>', 'data_query', ('GET', )),
            ('/data/export/<export_id>', 'data_export', ('GET', )),
            ('/data/export/unmap_contextid/<export_id>/<mapped_context_id>', 'data_export_unmap_contextid', ('GET', )),

            ('/data/behave/query', 'get_behave_points', ('GET', )),

            # Other
            ('/get_report', 'get_report', ('POST', )),
        )

        self.attach(self.routes)

    def attach(self, routes):
        for url, name, methods in routes:
            func = self.__getattribute__(name)
            self.app.add_url_rule(url, name, func, methods=methods)

    def get_root_context_id(self):
        result = self.container.exec_cmd('get-root-context-id', params=(g.user_id,))
        return json_response(result), 200

    def get_apps(self):
        result = self.container.exec_cmd('get-apps', params=(g.user_id,))
        return json_response(result), 200

    def get_sysnodes(self):
        result = self.container.exec_cmd('get-sysnodes', params=(g.user_id,))
        return json_response(result), 200

    def data_contexts(self, context_id, anchor_time=None, interval=None):
        self.container.validate_auth()

        result = self.container.exec_cmd('get-contexts', params=(context_id, anchor_time, interval, g.user_id))
        return json_response(result), 200

    @log_change
    def data_context(self, context_id=None):
        self.container.validate_auth()

        if context_id and not is_uuid(context_id):
            raise Exception('Invalid ContextID')

        attributes = request.json or {}

        context = None
        if request.method == 'GET':
            include_assoc = request.args.get('IncludeAssoc') == 'True'
            response = self.container.exec_cmd('get-context', params=(context_id, include_assoc, g.user_id,))
            return json_response(response)

        elif request.method == 'POST':
            parent_id = attributes.get('ParentID')
            label = attributes.get('1__Label')
            try:
                del attributes['1__Label']
            except:
                pass
                
            timestamp = None
            success, context = self.container.exec_cmd('add-context', params=(parent_id, label, context_id, timestamp, attributes, None, g.user_id))

            response = {}
            if not success:
                response_code = 501
            else:
                response_code = 200
                response = context
            return json_response(response), response_code

        elif request.method == 'PUT':
            if 'ContextID' in attributes:
                del attributes['ContextID']
                
            success, context = self.container.exec_cmd('update-context', params=(context_id, attributes, g.user_id))
            return json_response(context)

        elif request.method == 'DELETE':
            self.container.exec_cmd('delete-context', params=(context_id, g.user_id,))
            return json_response(True)

        return json_response(True), 200
        
    def data_points(self, context_id):
        self.container.validate_auth()

        anchor_time = None
        try:
            anchor_time = int(request.args.get('AnchorTime'))
        except:
            pass

        interval = None
        try:
            interval = int(request.args.get('Interval'))
        except:
            pass

        include_contexts = None
        try:
            include_contexts = simplejson.loads(request.args.get('IncludeContexts', '[]'))
        except:
            pass

        result = self.container.exec_cmd('get-points', params=(include_contexts or context_id, anchor_time, interval, g.user_id))
        return json_response(result), 200

    @log_change
    def data_point(self, point_id=None):
        self.container.validate_auth()
        attributes = request.json or {}

        if not point_id:
            point_id = attributes.get('ID', None)

        if point_id and not is_uuid(point_id):
            raise Exception('Invalid PointID')

        if request.method == 'GET':
            response = self.container.exec_cmd('get-point', params=(point_id, g.user_id))
            return json_response(response)

        elif request.method == 'POST':
            time_offset = 0
            try:
                time_offset = int(attributes.get('1__TimeOffset', 0))
            except:
                pass

            success, point = self.container.exec_cmd('add-point', params=(attributes, point_id, g.user_id))

            response = {}
            if not success:
                response_code = 501
            else:
                response_code = 200
                response = point

            return json_response(response), response_code

        elif request.method == 'PUT':
            create = True
            point = self.container.exec_cmd('update-point', params=(point_id, attributes, create, g.user_id))

            response = {}
            if not point:
                response_code = 501
            else:
                response_code = 200
                response = point

            return json_response(response)

        elif request.method == 'DELETE':
            self.container.exec_cmd('delete-point', params=(point_id, g.user_id))
            return json_response(True)

    def data_associations(self, context_id):
        self.container.validate_auth()

        result = self.container.exec_cmd('get-associations', params=(context_id, g.user_id,))
        return json_response(result), 200

    @log_change
    def data_association(self, association_id=None):
        self.container.validate_auth()

        attributes = request.json or {}

        if request.method == 'POST':
            assoc_name = attributes.get('Name', None)
            source_context_id = attributes.get('SourceID', None)
            dest_context_id = attributes.get('DestID', None)

            success, association = self.container.exec_cmd('add-association', params=(assoc_name, source_context_id, dest_context_id, g.user_id))

            response = {}
            if not success:
                response_code = 501
            else:
                response_code = 200
                response = association

            return json_response(response), response_code

        elif request.method == 'PUT':
            assoc_name = attributes.get('Name', None)
            source_context_id = attributes.get('SourceID', None)
            dest_context_id = attributes.get('DestID', None)

            association = self.container.exec_cmd('update-association', params=(assoc_name, source_context_id, dest_context_id, g.user_id))
            return json_response(association)

        elif request.method == 'DELETE':
            self.container.exec_cmd('delete-association', params=(association_id, g.user_id))
            return json_response(True)

    def get_report(self):
        self.container.validate_auth()

        attributes = request.json

        context_id = attributes.get('ContextID')
        interval = attributes.get('Interval')
        day = attributes.get('Day', None)
        month = attributes.get('Month', None)
        year = attributes.get('Year', None)
        begin_date = attributes.get('BeginDate', None)
        end_date = attributes.get('EndDate', None)

        max_depth = attributes.get('MaxDepth', None)
        leaf_notes = attributes.get('LeafNotes', True)
        leaf_note_timing = attributes.get('LeafNoteTiming', False)
        public = attributes.get('Public', False)

        params = (context_id, interval, begin_date, end_date, max_depth, 
                    leaf_notes, leaf_note_timing, public, g.user_id)
        total_time, report = self.container.exec_cmd('get-report', params=params)
        success = not not report

        if not success:
            response_code = 501
        else:
            response_code = 200

        return json_response({
            'Success': success,
            'Data': {
                'TotalHours': total_time / 3600,
                'Report': report
            }
        }), response_code
        
    def data_query(self, context_id=None):
        result = self.container.exec_cmd('data-query', params=(g.user_id, ))
        return json_response({
            'GraphData': result
        }), 200

    def data_export(self, export_id):
        anonymized = True
        if 'Anonymized' in request.args:
            anonymized = request.args.get('Anonymized') == 'True'

        offset = None
        try:
            offset = int(request.args.get('Offset'))
        except:
            pass

        window = None
        try:
            window = int(request.args.get('Window'))
        except:
            pass

        adaptive_window = False
        try:
            adaptive_window = request.args.get('AdaptiveWindow') == 'True'
        except:
            pass

        num_segments = None
        try:
            num_segments = int(request.args.get('NumSegments'))
        except:
            pass

        segment_size = None
        try:
            segment_size = int(request.args.get('SegmentSize'))
        except:
            pass

        image_depth = 0
        try:
            image_depth = int(request.args.get('ImageDepth'))
        except:
            pass

        activities = None
        try:
            activities = simplejson.loads(request.args.get('Activities', '[]'))
        except Exception as e:
            print 'Error parsing activities: %s' % e

        if not activities or type(activities) is not list:
            activities = None

        result = self.container.exec_cmd('data-export', params=(export_id, offset, window, num_segments, g.user_id, anonymized,
                image_depth, adaptive_window, segment_size, activities))
        return json_response({
            'ExportData': result
        }), 200

    def data_export_unmap_contextid(self, export_id, mapped_context_id):
        result = self.container.exec_cmd('data-export-unmap-contextid', params=(export_id, mapped_context_id, g.user_id))
        return json_response(result), 200

    def get_behave_points(self):
        result = self.container.exec_cmd('get-behave-points', params=())
        return json_response(result), 200
