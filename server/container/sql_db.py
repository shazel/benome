# Copyright 2016 Steve Hazel
#
# This file is part of Benome.
#
# Benome is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# Benome is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Benome. If not, see http://www.gnu.org/licenses/.

import sys
import os
import time
import simplejson
import sqlite3
import math
from uuid import uuid4
from pdb import set_trace as bp

import re
uuid4hex = re.compile('[0-9a-f]{8}(\-[0-9a-f]{4}){3}\-[0-9a-f]{12}\Z', re.I)

def is_uuid(u):
    return u and uuid4hex.match(u)

class App(object):
    pass

class BehaveApp(App):
    name = 'Behave'
    namespace_id = 2001

    default_bonuses = [
        {
            'AttrID': 'Initiative',
            'Label': 'Initiative',
            'GlobalMultiplier': False,
            'MultiplierValue': 1,
            'Text': ''
        },
        {
            'AttrID': 'AttentionToDetail',
            'Label': 'AttentionToDetail',
            'GlobalMultiplier': False,
            'MultiplierValue': 1,
            'Text': ''
        },
        {
            'AttrID': 'Momentum',
            'Label': 'Momentum',
            'GlobalMultiplier': False,
            'MultiplierValue': 1,
            'Text': ''
        },
        {
            'AttrID': 'Efficiency',
            'Label': 'Efficiency',
            'GlobalMultiplier': False,
            'MultiplierValue': 1,
            'Text': ''
        },
        {
            'AttrID': 'Focus',
            'Label': 'Focus',
            'GlobalMultiplier': False,
            'MultiplierValue': 1,
            'Text': ''
        }
    ]

    def __init__(self):
        pass

    def init_struct(self, db, root_id, user_id):
        '''Given the root node, call API methods to add necessary structure and attributes'''

        app_context_id = str(uuid4())
        label = self.name
        db.add_context(context_id=app_context_id, user_id=user_id, label=label, parent_id=root_id)
        db.set_context_attr(app_context_id, 1, 'NamespaceID', self.namespace_id)
        db.set_context_attr(app_context_id, 1, 'NodeType', 'App')
        db.set_context_attr(app_context_id, self.namespace_id, 'Balance', 0)

        bonus_context_id = str(uuid4())
        label = 'Bonuses'
        db.add_context(context_id=bonus_context_id, user_id=user_id, label=label, parent_id=app_context_id)

        self.add_bonuses(db, bonus_context_id, user_id, self.namespace_id)

        # Add the base nodes to the user root so they're visible
        user_root_id = db.get_sysnode_id('UserRoot')
        earn_context_id = str(uuid4())
        label = 'Earn'
        db.add_context(context_id=earn_context_id, user_id=user_id, label=label, parent_id=user_root_id)
        db.set_context_attr(earn_context_id, self.namespace_id, 'BalanceDirection', 1)
        db.set_context_attr(earn_context_id, self.namespace_id, 'NodeType', 'BalanceEarn')
        db.set_context_attr(earn_context_id, 1, 'Immutable', 1)

        spend_context_id = str(uuid4())
        label = 'Spend'
        db.add_context(context_id=spend_context_id, user_id=user_id, label=label, parent_id=user_root_id)
        db.set_context_attr(spend_context_id, self.namespace_id, 'BalanceDirection', -1)
        db.set_context_attr(spend_context_id, self.namespace_id, 'NodeType', 'BalanceSpend')
        db.set_context_attr(spend_context_id, 1, 'Immutable', 1)

        return app_context_id

    def add_bonuses(self, db, bonus_def_root_id, user_id, namespace_id):
        for bonus_def in self.default_bonuses:
            bonus_context_id = str(uuid4())
            db.add_context(context_id=bonus_context_id, user_id=user_id, label=bonus_def.get('Label'), parent_id=bonus_def_root_id)

            for attr_name, attr_val in bonus_def.items():
                if attr_name == 'Label':
                    continue

                db.set_context_attr(bonus_context_id, self.namespace_id, attr_name, attr_val)


class GlobalApp(App):
    name = 'Global'
    namespace_id = 1

    def __init__(self):
        pass

    def init_struct(self, db, root_id, user_id):
        '''Given the root node, call API methods to add necessary structure and attributes'''

        app_context_id = str(uuid4())
        label = self.name
        db.add_context(context_id=app_context_id, user_id=user_id, label=label, parent_id=root_id)
        db.set_context_attr(app_context_id, 1, 'NamespaceID', self.namespace_id)
        db.set_context_attr(app_context_id, 1, 'NodeType', 'App')

        return app_context_id


app_idx = {
    'Global': GlobalApp,
    'Behave': BehaveApp
}


class Association(object):
    def __init__(self, graph, assoc_id, src_context, dest_context, key):
        self.graph = graph

        self.assoc_id = assoc_id
        self.src = src_context
        self.dest = dest_context
        self.key = key

    def get_id(self):
        return self.assoc_id

    def get_id2(self):
        return self.assoc_id

    def __repr__(self):
        return '<Association %s to %s, key=%s, ID=%s>' % (self.src.get_id(), self.dest.get_id(), self.key, self.assoc_id)


class Context(object):
    def __init__(self, graph, context_id, label, attributes=None, metadata=None, int_id=None):
        self.graph = graph

        self.context_id = context_id
        self.int_id = int_id
        self.label = label
        self.attributes = attributes or {}
        self.metadata = metadata or {}

        self.outAssoc = {}
        self.inAssoc = {}

    def __repr__(self):
        return '<Context %s>' % (self.get_id(), )

    def serialize(self):
        return {
            'ID': self.context_id,
            'IntID': self.int_id,
            'Label': self.label,
            'Attributes': self.attributes,
            'Metadata': self.metadata
        }

    def set_metadata(self, metadata):
        self.metadata = metadata

    def set(self, namespace_id, attr_name, attr_val):
        assert type(namespace_id) is int

        if namespace_id not in self.attributes:
            self.attributes[namespace_id] = {}

        self.attributes[namespace_id][attr_name] = attr_val
        return True

    def get(self, attr_name, namespace_id=None, default=None):
        if not namespace_id:
            namespace_id = 1
        else:
            assert type(namespace_id) is int

        if self.attributes:
            return self.attributes[namespace_id].get(attr_name, default)

        return default

    def getNS(self, namespace_id, attr_name, default=None):
        return self.get(attr_name, namespace_id=namespace_id, default=default)

    def get_id(self):
        return self.context_id

    def add_assoc(self, assoc):
        assoc_id = assoc.get_id()
        # Add to each context's appropriate direction
        assoc.src.outAssoc[assoc_id] = assoc
        assoc.dest.inAssoc[assoc_id] = assoc

        # Add to graph index
        self.graph.associations[assoc_id] = assoc

    def clone(self, graph=None, as_root=False, deep=False):
        if not graph:
            graph = self.graph
        c = Context(graph, self.context_id, self.label, attributes=self.attributes, int_id=self.int_id)

        # Shallow copies
        c.outAssoc = self.outAssoc.copy()
        c.inAssoc = self.inAssoc.copy()

        return c

    def hasOutAssoc(self, key):
        for assoc in self.outAssoc.values():
            if assoc.key == key:
                return True

        return False

    def is_interior(self):
        return self.hasOutAssoc('down')

    def is_leaf(self):
        return not self.is_interior()

    def outV(self, key, ids_only=False):
        result = []
        
        for assoc_id, assoc in self.outAssoc.items():
            if assoc.key != key:
                continue

            if ids_only:
                result.append(assoc.dest.get_id())
            else:
                result.append(assoc.dest)
        return result


class Graph(object):
    def __init__(self, root_context_id=None, contexts=None, associations=None, user_id=None, db_path=None, is_subgraph=False,
            init_db=False, include_apps=None, import_json=None, import_data=None):

        self.contexts = contexts or {}
        self.associations = associations or {}
        self.user_id = user_id or 1
        self.db_path = db_path
        self.is_subgraph = is_subgraph or False
        self.node_cache = {}
        self.app_map = {}
        self.root_context_id = None

        if not db_path:
            raise Exception('No DB path provided')

        if not init_db:
            if not os.path.exists(db_path):
                raise Exception('DB not found at %s' % db_path)

        self.db = sqlite3.connect(db_path, check_same_thread=False)
        
        if not init_db:
            if root_context_id:
                self.root_context_id = root_context_id
            else:
                self.root_context_id = self.get_root_context_id()

        if init_db:
            self.init_db(include_apps=include_apps, import_json=import_json, import_data=import_data)

    def init_db(self, include_apps=None, import_json=None, import_data=None):
        db = self.db

        user_id = 1

        self.init_tables()
        self.root_context_id = str(uuid4())
        db.execute('''INSERT INTO MetaData (Key, Value) VALUES ('RootUUID', ?)''', (self.root_context_id,))

        user_root_id = self.init_db_struct(root_context_id=self.root_context_id, user_id=user_id, include_apps=include_apps)

        if type(import_json) is tuple and len(import_json) == 3:
            import_root, replace_root, json_path = import_json
            import_root_id = self.get_sysnode_id(import_root)
            if not import_root_id:
                import_root_id = import_root

            root_context = self.get_context(import_root_id)
            if not root_context:
                raise Exception('Import root not found: %s' % import_root_id)

            self.import_json_struct(import_root_id, json_path, user_id, replace_root=replace_root)
            db.commit()

        if type(import_data) is tuple and len(import_data) == 3:
            import_root_id, replace_root, struct = import_data

            root_context = self.get_context(import_root_id)
            if not root_context:
                raise Exception('Import root not found: %s' % import_root_id)

            self.import_struct(import_root_id, struct, user_id, replace_root=replace_root)
            db.commit()
            
        db.close()

        return user_root_id

    def init_tables(self):
        db = self.db
        db.execute('''CREATE TABLE IF NOT EXISTS MetaData (
            ID INTEGER PRIMARY KEY,
            Key TEXT,
            Value TEXT
        )''')
        db.execute('''CREATE UNIQUE INDEX IF NOT EXISTS 'MetaData_Key' ON 'MetaData' ('Key')''')

        db.execute('''CREATE TABLE IF NOT EXISTS Nodes (
            ID INTEGER PRIMARY KEY,
            UUID TEXT,
            UserID INTEGER,
            Type TEXT,
            Label TEXT,
            TimeStamp FLOAT,

            CHECK (Type IN ("Context", "Point"))
        )''')

        db.execute('''CREATE TABLE IF NOT EXISTS Attributes (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            UUID TEXT,
            NodeID INTEGER,
            NodeUUID TEXT,
            NameSpaceID INTEGER DEFAULT 1,
            Name TEXT,
            Value TEXT,
            Properties TEXT
        )''')

        db.execute('''CREATE TABLE IF NOT EXISTS Associations (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            UUID TEXT,
            UserID INTEGER,
            NameSpaceID INTEGER DEFAULT 1,
            SourceID INTEGER,
            DestID INTEGER,
            SourceUUID TEXT,
            DestUUID TEXT,
            Key TEXT
        )''')

        # Required for REPLACE INTO to work
        db.execute('''CREATE UNIQUE INDEX IF NOT EXISTS 'Nodes_UUID' ON 'Nodes' ('UUID' ASC)''')

        # CREATE UNIQUE INDEX 'Associations_ID_PKey' ON 'Associations' ('ID' ASC)
        db.execute('''CREATE INDEX IF NOT EXISTS 'Associations_SourceID' ON 'Associations' ('SourceID' ASC)''')
        db.execute('''CREATE INDEX IF NOT EXISTS 'Associations_Key' ON 'Associations' ('Key' ASC)''')
        db.execute('''CREATE INDEX IF NOT EXISTS 'Attributes_NodeID' ON 'Attributes' ('NodeID' ASC)''')
        db.execute('''CREATE INDEX IF NOT EXISTS 'Associations_DestID' ON 'Associations' ('DestID' ASC)''')


        #db.execute('''CREATE INDEX IF NOT EXISTS 'Nodes_UUID' ON 'Nodes' ('UUID' ASC)''')
        db.execute('''CREATE INDEX IF NOT EXISTS 'Associations_SourceUUID' ON 'Associations' ('SourceUUID' ASC)''')
        db.execute('''CREATE INDEX IF NOT EXISTS 'Associations_DestUUID' ON 'Associations' ('DestUUID' ASC)''')
        db.execute('''CREATE INDEX IF NOT EXISTS 'Attributes_NodeUUID' ON 'Attributes' ('NodeUUID' ASC)''')

        # Required for REPLACE INTO to work
        db.execute('''CREATE UNIQUE INDEX IF NOT EXISTS 'Attributes_NodeID_AttrName_NameSpaceID' ON 
                        'Attributes' ('NodeID' ASC, 'Name' ASC, 'NameSpaceID' ASC)''')

        db.execute('''CREATE UNIQUE INDEX IF NOT EXISTS 'Attributes_NodeUUID_AttrName_NameSpaceID' ON 
                        'Attributes' ('NodeUUID' ASC, 'Name' ASC, 'NameSpaceID' ASC)''')

        # Required for REPLACE INTO to work
        db.execute('''CREATE UNIQUE INDEX IF NOT EXISTS 'Associations_UserID_SourceID_DestID_Key' ON 
                        'Associations' ('UserID' ASC, 'NamespaceID' ASC, 'SourceID' ASC, 'DestID' ASC, 'Key' ASC)''')

        db.execute('''CREATE UNIQUE INDEX IF NOT EXISTS 'Associations_UserID_SourceUUID_DestUUID_Key' ON 
                        'Associations' ('UserID' ASC, 'NamespaceID' ASC, 'SourceUUID' ASC, 'DestUUID' ASC, 'Key' ASC)''')

        db.commit()

    def init_db_struct(self, root_context_id, user_id=1, include_apps=None, base_app=None):
        db = self.db

        # Initialize the root if it doesn't exist
        root_context = self.get_context(root_context_id)
        if root_context:
            return self.get_sysnode_id('UserRoot')
            
        if not include_apps:
            include_apps = ['Global']

        if 'Global' not in include_apps:
            include_apps.append('Global')

        if base_app and base_app not in include_apps:
            raise Exception('BaseApp must be in list of included apps')

        self.init_user_struct(root_context_id=root_context_id, user_id=user_id)

        app_id_map = {}
        for app_name in include_apps:
            app_id = self.add_app(app_name, user_id)
            app_id_map[app_name] = app_id

        return self.get_sysnode_id('UserRoot')

    def init_user_struct(self, root_context_id, user_id=1):
        db = self.db

        # Start with the root
        self.add_context(context_id=root_context_id, user_id=user_id, label='Root', parent_id=None)

        # Complete the initial structure
        ui_context_id = str(uuid4())
        self.add_context(context_id=ui_context_id, user_id=user_id, label='UI', parent_id=root_context_id)
        self.set_context_attr(ui_context_id, 1, 'NodeType', 'UI')

        prefs_context_id = str(uuid4())
        self.add_context(context_id=prefs_context_id, user_id=user_id, label='Prefs', parent_id=root_context_id)
        self.set_context_attr(prefs_context_id, 1, 'NodeType', 'Prefs')

        apps_context_id = str(uuid4())
        self.add_context(context_id=apps_context_id, user_id=user_id, label='Apps', parent_id=root_context_id)
        self.set_context_attr(apps_context_id, 1, 'NodeType', 'Apps')

        user_root_context_id = str(uuid4())
        self.add_context(context_id=user_root_context_id, user_id=user_id, label='', parent_id=root_context_id)
        self.set_context_attr(user_root_context_id, 1, 'NodeType', 'UserRoot')

        state_context_id = str(uuid4())
        self.add_context(context_id=state_context_id, user_id=user_id, label='State', parent_id=root_context_id)
        self.set_context_attr(state_context_id, 1, 'NodeType', 'State')

        db.commit()

    def add_app(self, app_name, user_id):
        db = self.db
        app_cls = app_idx.get(app_name)
        if not app_cls:
            return

        app = app_cls()

        app_root_id = self.get_sysnode_id('Apps')
        app_context_id = app.init_struct(self, app_root_id, user_id)
        db.commit()
        return app_context_id

    def get_root_context_id(self):
        db = self.db
        query = '''SELECT Value FROM MetaData WHERE Key = ?'''
        return db.execute(query, ('RootUUID', )).fetchone()[0]

    def get_root(self):
        return self.contexts[self.root_context_id]

    def get_sysnodes(self):
        db = self.db

        if self.node_cache:
            return self.node_cache

        root_context = self.get_root()
        for assoc_id, assoc in root_context.outAssoc.items():
            system_context = assoc.dest
            node_type = system_context.get('NodeType')
            if node_type:
                self.node_cache[node_type] = system_context.get_id()

        return self.node_cache

    def get_sysnode_id(self, node_name):
        db = self.db

        if node_name == 'Root':
            return self.root_context_id

        sysnodes = self.get_sysnodes()
        return sysnodes[node_name]

    def get_apps(self, include_context=False):
        db = self.db

        if self.app_map:
            return self.app_map

        app_root_id = self.get_sysnode_id('Apps')
        app_root_context = self.get_context(app_root_id)

        app_map = {}
        for assoc_id, assoc in app_root_context.outAssoc.items():
            app_context = assoc.dest
            node_type = app_context.get('NodeType')
            if node_type == 'App':
                app_map[app_context.label] = {
                    'ID': app_context.get_id(),
                    'Name': app_context.label,
                    'NamespaceID': int(app_context.get('NamespaceID')),
                }

                if include_context:
                    app_map[app_context.label]['Context'] = app_context

        self.app_map = app_map
        return self.app_map

    def init_context(self, context_id, label, attributes=None, int_id=None):
        context = Context(self, context_id, label, attributes=attributes, int_id=int_id)
        self.contexts[context_id] = context
        return context

    def init_assoc(self, assoc_id, src_id, dest_id, key):
        src_context = self.contexts.get(src_id)
        if not src_context:
            print 'Assoc %s init failed, src context not found: %s' % (assoc_id, src_id)
            return
        
        dest_context = self.contexts.get(dest_id)
        if not dest_context:
            if dest_id is not None:
                print 'Assoc %s init failed, dest context not found: %s' % (assoc_id, dest_id)
            return

        # TODO: Handle case where contexts aren't yet available

        assoc = Association(self, assoc_id, src_context, dest_context, key)

        # Add to contexts
        src_context.outAssoc[assoc_id] = assoc
        dest_context.inAssoc[assoc_id] = assoc

        # Add to graph index
        self.associations[assoc_id] = assoc

        return assoc

    def set_context_attr(self, context_id, namespace_id, attr_name, attr_val, persist=True):
        context = self.get_context(context_id)
        if context:
            if persist:
                db = self.db
                set_query = '''REPLACE INTO Attributes (NodeUUID, NameSpaceID, Name, Value) VALUES (?, ?, ?, ?)'''
                db.execute(set_query, (context_id, namespace_id, str(attr_name), str(attr_val)))
                db.commit()

            return context.set(namespace_id, attr_name, attr_val)
        return False

    def get_context(self, context_id):
        return self.contexts.get(context_id)

    def delete_context(self, context_id, user_id=None):
        user_id = user_id or self.user_id
        db = self.db

        delete_context = 'DELETE FROM Nodes WHERE UserID = ? AND UUID = ? AND Type = \'Context\''

        try:
            db.execute(delete_context, (
                    user_id,
                    context_id
                ))
        except Exception, e:
            print 'Context %s delete failed: %s' % (context_id, e)
        else:
            db.commit()

            context = self.get_context(context_id)

            if context:
                for assoc_id, assoc in context.outAssoc.items():
                    del context.outAssoc[assoc_id]
                    del assoc.dest.inAssoc[assoc_id]
                    del self.associations[assoc_id]

                for assoc_id, assoc in context.inAssoc.items():
                    del context.inAssoc[assoc_id]
                    del assoc.src.outAssoc[assoc_id]
                    del self.associations[assoc_id]

                del self.contexts[context_id]

            print 'Context %s deleted' % context_id
            return True

        return False

    def add_context(self, parent_id=None, label='', context_id=None, attributes=None, user_id=None):
        user_id = user_id or self.user_id
        db = self.db
        cursor = db.cursor()

        import sqlite3
        insert_context_attr = '''
        INSERT INTO
            Attributes (UUID, NodeUUID, NameSpaceID, Name, Value, Properties)
        VALUES
            (?, ?, ?, ?, ?, ?)
        '''

        insert_assoc = '''
        INSERT INTO
            Associations (UUID, UserID, SourceUUID, DestUUID, Key)
        VALUES
            (?, ?, ?, ?, ?)
        '''

        assocs = []
        attributes = attributes or {}
        try:
            timestamp = attributes.get(1, {}).get('Timestamp')
            if not timestamp:
                timestamp = math.ceil(time.time())

                if 1 not in attributes:
                    attributes[1] = {}

                attributes[1]['Timestamp'] = timestamp

            if not context_id:
                context_id = str(uuid4())

            insert_context = '''
            INSERT INTO
                Nodes (UUID, UserID, Type, Label, TimeStamp)
            VALUES
                (?, ?, ?, ?, ?)
            '''

            result = cursor.execute(insert_context, (
                context_id,
                user_id,
                'Context',
                label,
                timestamp
            ))

            if parent_id:
                up_assoc_uuid = str(uuid4())
                cursor.execute(insert_assoc, (
                    up_assoc_uuid,
                    user_id,
                    context_id,
                    parent_id,
                    'up'
                ))
                assocs.append((up_assoc_uuid, context_id, parent_id, 'up'))

                down_assoc_uuid = str(uuid4())
                cursor.execute(insert_assoc, (
                    down_assoc_uuid,
                    user_id,
                    parent_id,
                    context_id,
                    'down'
                ))
                assocs.append((down_assoc_uuid, parent_id, context_id, 'down'))

            for namespace_id, namespace_attrs in attributes.items():
                for attr_name, attr_val in namespace_attrs.items():
                    if namespace_id == 1 and attr_name == 'Label':
                        continue
                        
                    attr_uuid = str(uuid4())
                    db.execute(insert_context_attr, (
                        attr_uuid,
                        context_id,
                        namespace_id,
                        attr_name,
                        attr_val,
                        ''
                    ))

        except sqlite3.IntegrityError, e:
            print e, context_id, attributes
            context_id = None
        else:
            db.commit()

            self.init_context(context_id, label, attributes)

            for assoc_id, source_id, dest_id, key in assocs:
                self.init_assoc(assoc_id, source_id, dest_id, key)

        return context_id

    def update_context(self, context_id, attributes):
        context = self.get_context(context_id)
        if not context:
            raise Exception('Update failed, context not found: %s' % context_id)

        db = self.db
        import sqlite3

        update_context = 'UPDATE Nodes SET Label = ? WHERE UUID = ?'
        update_context_attr = 'REPLACE INTO Attributes (NodeUUID, NameSpaceID, Name, Value, Properties) VALUES (?, ?, ?, ?, ?)'

        attributes = attributes or {}
        try:
            for namespace_id, namespace_attrs in attributes.items():
                for attr_name, attr_val in namespace_attrs.items():
                    
                    if namespace_id == 1 and attr_name == 'Label':
                        result = db.execute(update_context, (
                            attr_val or '',
                            context_id
                        ))
                        context.label = attr_val

                    else:
                        db.execute(update_context_attr, (
                            context_id,
                            namespace_id,
                            attr_name,
                            attr_val,
                            ''
                        ))
                        context.set(namespace_id, attr_name, attr_val)

        except sqlite3.IntegrityError, e:
            print e, context_id, attributes
        else:
            db.commit()

        return context_id

    def add_assoc(self, source_context_id, key, dest_context_id, namespace_id=None, user_id=None):
        # Add to memory and to DB
        user_id = user_id or self.user_id
        if not namespace_id:
            namespace_id = 1

        # , NamespaceIDs
        insert_assoc = 'INSERT INTO Associations (UUID, UserID, SourceUUID, DestUUID, Key) VALUES (?, ?, ?, ?, ?)'

        db = self.db
        cursor = db.cursor()

        assoc_id = str(uuid4())

        try:
            cursor.execute(insert_assoc, (
                assoc_id,
                user_id,
                source_context_id,
                dest_context_id,
                key,
                #namespace_id
            ))
        except sqlite3.IntegrityError, e:
            print 'Error adding Assoc', e, source_context_id, key, dest_context_id
        else:
            db.commit()

            print 'init new assoc with id %s from %s to %s key=%s' % (assoc_id, source_context_id, dest_context_id, key)
            self.init_assoc(assoc_id, source_context_id, dest_context_id, key)

        return assoc_id

    def remove_assoc(self, source_context_id, dest_context_id, key, namespace_id=None, user_id=None):
        # Remove from memory and from DB

        user_id = user_id or self.user_id
        if not namespace_id:
            namespace_id = 1

        select_assoc_id = '''
            SELECT UUID FROM Associations WHERE
                UserID = ? AND SourceUUID = ? and DestUUID = ? and Key = ?
            '''

        db = self.db
        assoc_id = None

        try:
            result = db.execute(select_assoc_id, (
                user_id,
                source_context_id,
                dest_context_id,
                key,
                #namespace_id
            )).fetchone()
            
            if result:
                assoc_id = result[0]

            if assoc_id:
                db.execute('DELETE FROM Associations WHERE UserID = ? AND UUID = ?', (
                    user_id,
                    assoc_id
                ))
        except Exception, e:
            print 'Error removing Assoc', e, source_context_id, key, dest_context_id
        else:
            db.commit()

            if assoc_id:
                assoc = self.associations.get(assoc_id)

                if assoc:
                    # Remove from contexts
                    del assoc.src.outAssoc[assoc_id]
                    del assoc.dest.inAssoc[assoc_id]
                    del self.associations[assoc_id]

    def prune_to_root(self, root_context_id, assoc_key=None):
        new_graph = Graph(db_path=self.db_path, root_context_id=root_context_id, is_subgraph=True)

        root_context = self.contexts[root_context_id].clone(new_graph)
        new_graph.contexts[root_context_id] = root_context

        # Clear out any in/down associations from root context
        for assoc_id, assoc in root_context.inAssoc.items():
            if assoc.key == 'down':
                del root_context.inAssoc[assoc_id]

        # Clear out any out/up associations from root context
        for assoc_id, assoc in root_context.outAssoc.items():
            if assoc.key == 'up':
                del root_context.outAssoc[assoc_id]

        # Continue traversing downward and cloning the contexts
        # Associations are copied
        def traverse(context):
            for assoc in context.outAssoc.values():
                if assoc.key != 'down':
                    continue

                child_context = assoc.dest
                child_context_id = child_context.get_id()

                if child_context_id not in new_graph.contexts:
                    child_context = child_context.clone(new_graph)
                    new_graph.contexts[child_context_id] = child_context
                else:
                    child_context = new_graph.contexts[child_context_id]

                # Keep the same associations as before
                # TODO: Prune associations by key
                new_graph.associations.update(child_context.inAssoc)
                new_graph.associations.update(child_context.outAssoc)

                traverse(child_context)

        traverse(root_context)

        return new_graph

    def get_points(self, anchor_time=None, end_time=None, contexts=None, namespaces=None, user_id=None):
        'Return all points linked to any of the current contexts'
        db = self.db

        user_id = user_id or self.user_id

        context_query = ''
        if not contexts and self.is_subgraph:
            contexts = self.contexts.keys()

        if contexts:
            context_query += '''
                AND
                Associations.DestUUID IN (%s)
            ''' % ','.join(map(lambda context_id: '\'%s\'' % context_id, contexts))

        namespace_query = ''
        if namespaces:
            namespace_query += '''
                AND
                (
                    Attributes.NameSpaceID IS NULL
                    OR
                    Attributes.NameSpaceID IN (%s)
                )
            ''' % (','.join(map(str, namespaces)), )

        range_query = ''
        if anchor_time:
            range_query += '''
                AND
                Nodes.TimeStamp <= %s
            ''' % anchor_time

        if end_time:
            range_query += '''
                AND
                Nodes.TimeStamp >= %s
            ''' % end_time

        query = '''
        SELECT
            Nodes.UUID, Nodes.ID, Nodes.TimeStamp, Associations.DestUUID, Attributes.NameSpaceID,
            Attributes.Name, Attributes.Value
        FROM
            Nodes
                LEFT OUTER JOIN Attributes ON Nodes.UUID = Attributes.NodeUUID
                INNER JOIN Associations ON Nodes.UUID = Associations.SourceUUID
        WHERE
            Nodes.Type = 'Point'
            AND
            Nodes.UserID = ?
            AND
            Associations.Key = 'up'
            %s
            %s
            %s
        ''' % (context_query, namespace_query, range_query)
        result = db.execute(query, (user_id,)).fetchall()
        points = self.generate_points(result)
        return points

    def get_age_oldest_points(self, contexts=None, user_id=None):
        db = self.db
        anchor_time = math.ceil(time.time())
        max_age = 0

        context_query = ''
        if not contexts and self.is_subgraph:
            contexts = self.contexts.keys()

        if contexts:
            context_query += '''
                AND
                UUID IN (%s)
            ''' % ','.join(map(lambda context_id: '\'%s\'' % context_id, contexts))

        user_id_query = ''
        if user_id:
            context_query += '''
                AND
                Nodes.UserID = ?
            ''' % user_id

        query = '''SELECT
            Nodes.UUID, Nodes.TimeStamp
        FROM
            Nodes
        WHERE
            Nodes.Type = 'Context'
            %s
            %s
        ''' % (user_id_query, context_query)
        result = db.execute(query).fetchall()

        # Default to the age of the context.
        # This will be replaced below by the age of the last point, if there is indeed a point
        age_map = {}
        for context_id, created_time in result:
            age = int(anchor_time - created_time)
            max_age = max(age, max_age)
            age_map[context_id] = age

        if contexts:
            context_query += '''
                AND
                Associations.DestUUID IN (%s)
            ''' % ','.join(map(lambda context_id: '\'%s\'' % context_id, contexts))

        query = '''
        SELECT
            Associations.DestUUID,
            MIN(Nodes.TimeStamp)
        FROM
            Nodes
            INNER JOIN Associations ON Nodes.UUID = Associations.SourceUUID
        WHERE
            Nodes.Type = 'Point'
            AND
            Nodes.UserID = ?
            AND
            Associations.Key = 'up'
            %s
        ''' % context_query
        result = db.execute(query, (user_id,)).fetchall()

        for context_id, oldest_point_ts in result:
            if not oldest_point_ts:
                continue

            try:
                oldest_point_ts = math.ceil(oldest_point_ts)
            except:
                pass
            else:
                if oldest_point_ts:
                    age = int(anchor_time - oldest_point_ts)
                    max_age = max(age, max_age)
                    age_map[context_id] = age

        return age_map, max_age

    def get_point(self, point_id, user_id=None):
        db = self.db
        user_id = user_id or self.user_id

        query = '''
        SELECT
            Nodes.UUID, Nodes.ID, Nodes.TimeStamp, Associations.DestUUID, Attributes.NameSpaceID,
            Attributes.Name, Attributes.Value
        FROM
            Nodes
                LEFT OUTER JOIN Attributes ON Nodes.UUID = Attributes.NodeUUID
                INNER JOIN Associations ON Nodes.UUID = Associations.SourceUUID
        WHERE
            Nodes.Type = 'Point'
            AND
            Nodes.UUID = ?
            AND
            Associations.Key = 'up'
        '''
        result = db.execute(query, (point_id,)).fetchall()
        if result:
            points = self.generate_points(result)
            if points and len(points) > 0:
                return points[0]

        return None

    def delete_point(self, point_id, user_id=None):
        user_id = user_id or self.user_id
        db = self.db

        delete_point = 'DELETE FROM Nodes WHERE UserID = ? AND UUID = ? AND Type = \'Point\''

        try:
            db.execute(delete_point, (
                    user_id,
                    point_id
                ))
        except Exception, e:
            print 'Point %s delete failed: %s' % (point_id, e)
        else:
            db.commit()
            return True

        return False

    def add_point(self, context_id, point_id=None, attributes=None, user_id=None):
        user_id = user_id or self.user_id

        db = self.db
        cursor = db.cursor()

        import sqlite3
        insert_point_attr = 'INSERT INTO Attributes (UUID, NodeUUID, NameSpaceID, Name, Value, Properties) VALUES (?, ?, ?, ?, ?, ?)'
        insert_assoc = 'INSERT INTO Associations (UUID, UserID, SourceUUID, DestUUID, Key) VALUES (?, ?, ?, ?, ?)'

        attributes = attributes or {}
        try:
            timestamp = attributes[1].get('Time') or time.time()
            if 'Time' in attributes[1]:
                del attributes[1]['Time']

            if not point_id:
                point_id = str(uuid4())

            insert_point = 'INSERT INTO Nodes (UUID, UserID, Type, TimeStamp) VALUES (?, ?, ?, ?)'
            result = cursor.execute(insert_point, (
                point_id,
                user_id,
                'Point',
                timestamp
            ))

            assoc_uuid = str(uuid4())
            db.execute(insert_assoc, (
                assoc_uuid,
                user_id,
                point_id,
                context_id,
                'up'
            ))

            for namespace_id, namespace_attrs in attributes.items():
                for attr_name, attr_val in namespace_attrs.items():
                    if namespace_id == 2001 and attr_name == 'Bonuses':
                        try:
                            attr_val = simplejson.dumps(attr_val)
                        except:
                            attr_val = None

                    attr_uuid = str(uuid4())
                    db.execute(insert_point_attr, (
                        attr_uuid,
                        point_id,
                        namespace_id,
                        attr_name,
                        attr_val,
                        ''
                    ))
        except sqlite3.IntegrityError, e:
            print e, point_id, attributes
        else:
            db.commit()

        return point_id

    def update_point(self, point_id, attributes):
        # attributes is a dict of namespaces
        db = self.db

        import sqlite3

        update_point = 'UPDATE Nodes SET TimeStamp = ? WHERE UUID = ? AND Type = \'Point\''
        update_point_attr = 'REPLACE INTO Attributes (NodeUUID, NameSpaceID, Name, Value, Properties) VALUES (?, ?, ?, ?, ?)'

        attributes = attributes or {}
        try:
            if 'Time' in attributes:
                timestamp = attributes.get('Time') or time.time()
                result = db.execute(update_point, (
                    timestamp,
                    point_id
                ))

            for namespace_id, namespace_attrs in attributes.items():
                for attr_name, attr_val in namespace_attrs.items():
                    if namespace_id == 2001 and attr_name == 'Bonuses':
                        try:
                            attr_val = simplejson.dumps(attr_val)
                        except:
                            attr_val = None

                    db.execute(update_point_attr, (
                        point_id,
                        namespace_id,
                        attr_name,
                        attr_val,
                        ''
                    ))

        except sqlite3.IntegrityError, e:
            print e, point_id, attributes
        else:
            db.commit()

        return point_id

    def load(self, user_id, attr_namespaces=None):
        db = self.db
        context_ids = []

        # Retrieve all contexts for a user with namespaced attributes
        context_query = '''
        SELECT
            Nodes.UUID, Nodes.ID, Nodes.Label, Nodes.Timestamp, Attributes.NameSpaceID, 
                Attributes.Name, Attributes.Value
        FROM
            Nodes
                LEFT OUTER JOIN Attributes ON Nodes.UUID = Attributes.NodeUUID
        WHERE
            Nodes.Type = 'Context'
            AND
            Nodes.UserID = ?
        '''
        # AND Attributes.NameSpaceID IS NULL OR Attributes.NameSpaceID IN (%s)
        # % ','.join(attr_namespaces)
        contexts_result = db.execute(context_query, (user_id, )).fetchall()

        for row in contexts_result:
            context_id, context_int_id, label, timestamp, namespace_id, attr_name, attr_val = row

            if context_id not in context_ids:
                context_ids.append(context_id)
                self.init_context(context_id, label, int_id=context_int_id)
                self.set_context_attr(context_id, 1, 'Timestamp', timestamp, persist=False)

            if namespace_id and attr_name:
                namespace_id = int(namespace_id)
                self.set_context_attr(context_id, namespace_id, attr_name, attr_val, persist=False)

        # Get all outgoing associations from retrieved contexts
        # TODO: Namespaced/keyed
        assoc_query = '''
        SELECT
            UUID, SourceUUID, DestUUID, Key
        FROM
            Associations
        WHERE
            UserID = ?
            AND
            SourceUUID IN (%s)
        ''' % ','.join(map(lambda context_id: '\'%s\'' % context_id, context_ids))
        assoc_result = db.execute(assoc_query, (user_id,)).fetchall()

        for row in assoc_result:
            assoc_id, source_id, dest_id, key = row
            self.init_assoc(assoc_id, source_id, dest_id, key)

        return self

    def generate_points(self, rows):
        # Produce useful point structures, merging in the attributes.
        points = {}
        for row in rows:
            point_id, point_int_id, timestamp, context_id, namespace_id, attr_name, attr_val = row

            if not namespace_id:
                namespace_id = 1

            point = points.get(point_id)
            if not point:
                point = {
                    'ID': point_id,
                    'IntID': point_int_id,
                    '1__ContextID': context_id,
                    '1__Time': timestamp
                }
                points[point_id] = point

            if attr_name:
                if attr_name == 'Bonuses' and attr_val:
                    try:
                        attr_val = simplejson.loads(attr_val)
                    except Exception as e:
                        print 'Error with point ID=%s, Attr Name=%s, Val=%s: %s' % (point_id, attr_name, attr_val, e)
                        attr_val = None

                namespaced_attr_name = '%d__%s' % (namespace_id, attr_name)
                point[namespaced_attr_name] = attr_val

        return points.values()

    def get_leaves(self):
        return filter(lambda c: c.is_leaf(), self.contexts.values())

    def get_interior(self):
        return filter(lambda c: c.is_interior(), self.contexts.values())


def get_app_id(db, app_name):
    app_root_id = db.get_sysnode_id('Apps')
    app_root_context = get_context(db, app_root_id)

    if app_root_context:
        for assoc in app_root_context['Associations']['Out']:
            key = assoc['Key']
            context_id = assoc['OtherID']

            if key != 'down':
                continue

            app_context = get_context(db, context_id)
            if not app_context:
                continue

            if app_context['Label'] == app_name:
                return context_id

def add_app_db(db, app_name, user_id=1):
    app_id = get_app_id(db, app_name)
    if app_id:
        raise Exception('App already added')

    app_id = add_app(db, app_name, user_id)
    if not app_id:
        raise Exception('Invalid app name: %s' % app_name)

    return app_id

def import_json_struct(db, import_root_id, json_path, user_id, replace_root=False):
    try:
        with open(json_path, 'r') as f:
            json_struct = f.read()
    except Exception as e:
        print e
        raise Exception('Import source not found')

    try:
        struct = simplejson.loads(json_struct)
    except Exception as e:
        print e
        raise Exception('Import source failed to load')

    import_struct(db, import_root_id, struct, user_id, replace_root=replace_root)

def import_struct(db, import_root_id, struct, user_id, replace_root=False):
    def traverse_struct(struct, parent_id, is_root=False):
        label = struct.get('label', '')
        if replace_root and is_root:
            set_context_attribute(db, parent_id, 'Label', label)
            node_id = parent_id
        else:
            node_id = get_next_id(db)
            create_context(db, node_id, user_id, label, parent_id)
            #print 'Created %s=%s from parent %s' % (node_id, label, parent_id)

        for key, val in struct.items():
            if key not in ('children', 'label'):
                set_context_attribute(db, node_id, key, val)

        for child_struct in struct.get('children', []):
            traverse_struct(child_struct, node_id);

    traverse_struct(struct, import_root_id, is_root=True)

    return True

if __name__ == '__main__':

    import string
    BASE_LIST = string.digits + string.letters
    BASE_DICT = dict((c, i) for i, c in enumerate(BASE_LIST))

    def decode_id(string):
        reverse_base = BASE_DICT
        length = len(reverse_base)
        ret = 0
        for i, c in enumerate(string[::-1]):
            ret += (length ** i) * reverse_base[c]

        return ret

    in_data_path = './graph.db'
    out_db_path = './sql.db'

    which = 1
    user_id = 1
    if which == 0:
        root_context_id = 'rI'
        g = Graph(root_context_id=decode_id(root_context_id), db_path=out_db_path)

        # Load the full structure
        attr_namespaces = map(str, [1])
        g.load(user_id, attr_namespaces)

        print len(g.contexts.keys()), len(g.associations.keys()), len(g.get_points())

        # Prune to a sub-structure
        g2 = g.prune_to_root(decode_id('wH'))
        print len(g2.contexts.keys()), len(g2.associations.keys()), len(g2.get_points())

        # Test points interval
        print len(g2.get_points(time.time() - (86400 * 120), time.time() - (86400 * 180)))

    if which == 1:
        g = Graph(root_context_id=decode_id('rI'), db_path=out_db_path)

        # Load the full structure
        attr_namespaces = map(str, [1])
        g.load(user_id, attr_namespaces)
        print len(g.contexts.keys()), len(g.associations.keys()), len(g.get_points())

        # Prune to a sub-structure
        g2 = g.prune_to_root(decode_id('wH'))
        print len(g2.contexts.keys()), len(g2.associations.keys()), len(g2.get_points())

        # Test points interval
        print len(g2.get_points(time.time() - (86400 * 120), time.time() - (86400 * 180)))

    if which == 2:
        # Retrieve all points associated with a specific context.
        # Of the point's attributes, retrieve only those which belong to the passed namespaces

        root_context_id = decode_id('mO')
        attr_namespaces = (1, 2)

        g = Graph(root_context_id=root_context_id, db_path=out_db_path, user_id=user_id)
        points = g.get_points(namespaces=attr_namespaces)
        for point in points.values():
            print point

    if which == 3:
        db = sqlite3.connect(out_db_path)

        root_context_id = decode_id('rI')
        anchor_time = math.ceil(time.time()) - (86400 * 30)
        end_time = anchor_time - (86400 * 60)
        attr_namespaces = (1, 2)

        # Points for user, under any context, within interval, with namespaced attributes
        g = Graph(root_context_id=root_context_id, db_path=out_db_path, user_id=user_id)
        points = g.get_points(anchor_time=anchor_time, end_time=end_time, namespaces=attr_namespaces)

        print len(points)
        # for point in points.values():    
        #     print point
