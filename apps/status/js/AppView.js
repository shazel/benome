/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'

// -------------
import ClusterAppView from 'benome/views/ClusterAppView'
import Data from 'benome/models/Data'
import SimpleSurfaceView from 'benome/cluster/surfaces/SimpleSurfaceView'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

// -------------

var SituationAppView = ClusterAppView.extend({
    tagName: 'div',
    className: 'benome',

    events: {
    },

    containerHost: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/situation/nudge',

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'onInitialized');

        this.exportData = options.data;
        delete options.data;

        var appOptions = _.extend({
            surfaceViewClass: SimpleSurfaceView,
            listeners: {
                'Initialized': this.onInitialized
            },

            globalClusterOptions: {
                noCompress: true
            },

            visualQuality: 1,
            bgColor: 'light',
            backgroundEnabled: false,
            helpEnabled: false,
            adminEnabled: false,
            mouseWheelEnabled: false,

            features: {
                'ActionHistory': false,
                'ActionTimeline': false,
                'ActivityPullForward': false,
                'ActivityPushBack': false,
                'AddFeedbackInteractive': false,
                'Admin': false,
                'ClusteredEdit': false,
                'ContextModify': false,
                'DetailLevels': false,
                'DragDrop': false,
                'LeafFocusAutoAdd': false,
                'LeafFocusToggleTimer': false,
                'MovableFocus': false,
                'PointAdd': false,
                'PointDetail': false,
                'PointLongPress': false,
                'PointShortPress': false
            }
        }, options);

        if (appOptions.userName) {
            var port = window.location.port,
                portStr = '';

            if (port) {
                portStr = ':' + port
            }
            this.containerHost =    window.location.protocol + '//' + 
                                    window.location.hostname + portStr + 
                                    '/' + appOptions.userName + '/situation/nudge';
        }
        
        this.firstName = appOptions.firstName || appOptions.userName;
        Global.firstName = this.firstName;

        localStorage.clear();
        ClusterAppView.prototype.initialize.call(this, appOptions);
    },

    onInitialized: function() {
        this.importData(this.exportData.ExportData, {
                                                        importType: 'Dict',
                                                        replaceRoot: true
                                                    });

        var cluster = this.globalCluster.cluster;
        cluster.render({
            forceRender: true
        });
    },

    importData: function(data, options) {
        options = options || {};
        var cluster = options.cluster || this.globalCluster.cluster,
            rootID = options.rootID || cluster.rootID,
            importType = options.importType || 'JSON',
            _this = this;

        if (importType == 'JSON') {
            try {
                var struct = JSON.parse(data);
            }
            catch (e) {};
        }
        else if (importType == 'Dict') {
            struct = data;
        }
        else {
            console.log('Unsupported import type: ' + importType);
            return;
        }

        var contexts = this.userCollection,
            associations = contexts.associations;

        function createContext(parentContext, refID, replaceRoot, graphData) {
            var label = '',
                context;

            if (replaceRoot) {
                parentContext.set('1__Label', label);
                parentContext.set('1__RefID', refID);
                context = parentContext;
            }
            else {
                var parentID = parentContext.id,
                    currentID = _this.dataLoader.nextID(),
                    saveAssoc = !!_this.options.localOnly;

                associations.addAssoc('up', currentID, parentID, {save: saveAssoc});
                associations.addAssoc('down', parentID, currentID, {save: saveAssoc});
                var context = contexts.create({
                    ID: currentID,
                    ParentID: parentID,
                    '1__Label': label,
                    '1__RefID': refID
                }, {
                    silent: false,
                    type: 'post'
                });
            }

            context.graphData = graphData;
            return context;
        }

        function traverse(struct, parentContext, replaceRoot) {
            if (!_.isObject(struct) || !parentContext) {
                return;
            }

            // Challenge: need IDs of previous to add children to it
            // Could possibly exhaust ID range if import is large

            // struct.Label

            var refID = struct.ID,
                newContext = createContext(parentContext, refID, replaceRoot, struct.GraphData ? struct.GraphData.Data.Data : null);

            _.each(struct.Children, function(childStruct) {
                traverse(childStruct, newContext);
            });
        }

        cluster.disableRender();
        traverse(struct, contexts.get(rootID), options.replaceRoot);
        cluster.enableRender();
    }
});

export default SituationAppView;