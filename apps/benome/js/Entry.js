/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'

// -------------

import BaseEntry from 'benome/BaseEntry'
import BenomeAppView from 'benome/views/BenomeAppView'
import GlobalSurfaceView from 'benome/cluster/surfaces/GlobalSurfaceView'

$(function() {

(function(window, document) {

var BenomeEntry = function() {};

_.extend(BenomeEntry.prototype, BaseEntry.prototype, {
    init: function(options) {
        options = options || {};

        var appOptions = options.appOptions || {};

        var hideLabels = appOptions.hideLabels,
            labelIDsOnly = this.QueryString.i === '1',
            clusterOnly = appOptions.clusterOnly,
            autoActionDelay = parseInt(this.QueryString.aa) || null,
            visualQuality = 1,
            backgroundColor = 'light',
            clusterFilterShift = true;

        // QueryString overrides passed parameters
        // The underlying data must be scrubbed for actual privacy
        if (this.QueryString.vq) {
            visualQuality = parseInt(this.QueryString.vq) || 0;
        }

        if (this.QueryString.bg) {
            if (this.QueryString.bg in {'dark': 1, 'white': 1}) {
                backgroundColor = this.QueryString.bg;
            }
        }

        if (this.QueryString.c) {
            clusterOnly = this.QueryString.c === '1';
        }

        if (this.QueryString.p) {
            hideLabels = this.QueryString.p === '1';
        }

        if (this.QueryString.cfs) {
            clusterFilterShift = this.QueryString.cfs === '1';
        }

        var defaultFilter = 0;
        if (parseInt(this.QueryString.d) >= 1) {
            defaultFilter = Math.min(10, parseInt(this.QueryString.d)) - 1;
        }
        _.extend(appOptions, {
            instanceID: options.instanceID,
            defaultFilter: defaultFilter,
            features: options.features,
            autoActionDelay: autoActionDelay,
            visualQuality: visualQuality,
            bgColor: backgroundColor,
            localOnly: options.localOnly || false,
            clusterOnly: clusterOnly,
            hideLabels: hideLabels,
            continuousTiming: options.continuousTiming,
            clusterFilterShift: clusterFilterShift,
            setBackgroundFilterLevel: options.setBackgroundFilterLevel,
            helpUrl: 'https://benome.ca/howtouse-basic.html',

            clusterType: 'Global',
            surfaceViewClass: GlobalSurfaceView
        });

        BaseEntry.prototype.init.call(this, {
            appView: BenomeAppView,
            appOptions: appOptions
        });
    }
});

window.BENOME = BenomeEntry;

}(window, document));

});