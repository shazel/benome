const path = require('path');
const webpack = require('webpack');

var nodeEnv = process.env.NODE_ENV || 'production';
var isProd = nodeEnv === 'production';

module.exports = {
  node: {
    __dirname: true
  },

  entry: {
    'entry': [
      'babel-polyfill',
      './js/Entry'
    ],
  },

  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    filename: 'bundle.js',
  },
  resolve: {
      alias:{
        js: path.resolve( __dirname, 'js' ),
        benome: path.resolve( __dirname, '../', '../', 'client', 'js' )
      },
      extensions: ['.js', '.json', '.css', '.html'],
  },
  devtool: 'cheap-module-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: [
            ["es2015", { modules: false }],
            "es2017",
            "stage-2",
          ],
          plugins: [
            ["transform-runtime", {
              helpers: false,
              polyfill: true,
              regenerator: true, 
            }],
            "transform-node-env-inline"
          ],
          env: {
            development: {
              plugins: []
            }
          }
        }
      },
      {
        test: /\.css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              modules: true,
              sourceMap: true,
              importLoaders: 1,
              localIdentName: "[name]--[local]--[hash:base64:8]"
            }
          },
          "postcss-loader" // has separate config, see postcss.config.js nearby
        ]
      },
      {
        test: /\.html$/,
        use: [
          "html-loader"
        ]
      },
    ]
  },

  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.LoaderOptionsPlugin({
      debug: !isProd
    }),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      sourceMap: false
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en-ca/),
    new webpack.optimize.AggressiveMergingPlugin()
  ]
}
