const reactToolboxVariables = { 
};

module.exports = {
  plugins: [
  	require('postcss-import')({
  		root: __dirname,
  	}),
  	require('postcss-mixins')({}),
  	require('postcss-each')({}),
  	/* eslint-disable global-require */
    require('postcss-cssnext')({
      features: {
        customProperties: {
          variables: reactToolboxVariables,
        },
      },
    }),
    /* eslint-enable global-require */
  ]
};
