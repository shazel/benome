/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'
import moment from 'moment'
import MarkdownParser from 'markdown-it'
var markdownParser = MarkdownParser()

// -------------

import BaseAppView from 'benome/views/BaseAppView'
import BenomeAppView from 'benome/views/BenomeAppView'
import ClusterAppView from 'benome/views/ClusterAppView'
import GraphSurfaceView from 'benome/modules/Cluster/Surfaces/GraphSurfaceView'
import GraphVisual from 'benome/GraphVisual'
import sendFile from 'benome/modules/Util/SendFile'
import Context from 'benome/modules/Data/Context'
import FriendRoot from './FriendRoot'
import _Global from 'benome/modules/GlobalState'
var Global = _Global()

var BehaveAppView = BenomeAppView.extend({
    tagName: 'div',
    className: 'benome',
    //containerHost: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '',

    events: {
    },

	pointAttributes: {
		'Bonuses': {
            AttrID: 'Bonuses',
            Label: 'Bonuses',
            Type: 'ClusterContainer',
            Def: {
			    Type: 'Cluster',
			    ClusterType: 'Boolean',
			    RootType: 'BooleanRoot',
			    Constructor: false,

			    // Optional. Will default to cluster preference.
			    ConstructType: 'BooleanOption',

			    // Optional. Will default to cluster preference.
			    ConstructDef: {
			        AttrID: 'NewBonus',
			        Label: null,
			        Name: '',
			        GlobalMultiplier: false,
			        MultiplierValue: 1
			    },

			    Container: null,
			    Layer: false,
			    Name: 'Bonuses',
			    Render: true,
			    RenderOptions: {},
			    Options: {},
			    EventHandlers: {
			        onEdit: function(clusterController, viewID, view, surfaceView, contextModel) {
			            return contextModel.get('Type') == 'BooleanOption';
			        }
			    },
			    Nodes: []
			}
        },
        'Timing': {
            AttrID: 'Timing',
            Label: 'Timing',
            Type: 'Interval',
            Def: {}
        }
	},

	contextAttributes: {
		'Label': {
			'AttrID': 'Label',
			Label: 'Activity name',
            Type: 'TextLine',
            DefaultValue: ''
		},
		'BaseValue': {
			AttrID: 'BaseValue',
            Label: 'BaseValue',
            Type: 'Numeric',
            DefaultValue: 10
		},
        'BaseDurationValue': {
            AttrID: 'BaseDurationValue',
            Label: 'DurationValue',
            Type: 'Numeric',
            DefaultValue: 0
        },
		'Bonuses': {
            AttrID: 'Bonuses',
            Label: 'Bonuses',
            Type: 'ClusterContainer',
            Def: {
			    Type: 'Cluster',
			    ClusterType: 'ContextDef',
			    RootType: 'ContextDefRoot',
			    Constructor: true,

			    // Optional. Will default to cluster preference.
			    ConstructType: 'ContextDef',

			    // Optional. Will default to cluster preference.
			    ConstructDef: {
			        AttrID: 'NewBonus',
			        Type: 'ContextDef',
			        Label: 'Bonus',
			        Name: '',
			        GlobalMultiplier: false,
			        MultiplierValue: 1,
			        EventHandlers: {
			        	onInit: function(clusterController, contextID) {
			        		var view = clusterController.cluster.getView(contextID);
			        		if (!view) {
			        			return;
			        		}

			        		clusterController.showEditCluster(view);
			        	}
			        }
			    },

			    Editable: true,
			    EditDef: {
				    Type: 'Cluster',
				    ClusterType: 'Attribute',
				    Constructor: false,
				    Container: null,
				    Layer: true,
				    Name: 'New Bonus',
				    Render: true,
				    RenderOptions: {},
				    Options: {
				    	visibleUIElements: [],
				    	clusterSize: 'Expanded'
				    },
				    EventHandlers: {
				    },
				    Nodes: [
				        {
				            'AttrID': 'Label',
				            'Label': 'Set the name of this bonus',
				            'Type': 'TextLine',
				            'DefaultValue': ''
				        },
				        {
				            'AttrID': 'Text',
				            'Label': 'Any notes for this bonus?',
				            'Type': 'Text',
				            'DefaultValue': ''
				        },
				        {
				            'AttrID': 'MultiplierValue',
				            'Label': 'How big is this bonus?',
				            'Type': 'Numeric',
				            'DefaultValue': 1
				        },
				        {
				            'AttrID': 'GlobalMultiplier',
				            'Label': 'Is this a super-bonus?',
				            'Type': 'Boolean',
				            'DefaultValue': false
				        }
				    ]
				},

			    Container: null,
			    Layer: false,
			    Name: 'Bonuses',
			    Render: true,
			    RenderOptions: {},
			    Options: {},
			    EventHandlers: {
			        onEdit: function(clusterController, viewID, view, surfaceView, contextModel) {
			            //return contextModel.get('Type') == 'ContextDef';
			            return true;
			        }
			    },
			    Nodes: []
			}
        }
	},

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'onEventInitialized', 'renderValue', 'updateScore', 'calcGraphData', 'bonusDefCommit', 'pointCommit',
        				'pointInitValues', 'pointBeforeSave', 'contextInitValues', 'updateBalance', 'setLocked', 'setUnlocked',
                        'onNewWebSocketMessage');

        this.username = options.username

        var _this = this,
        	lockStateOn = true,
        	appOptions = _.extend(options, {
        		helpEnabled: false,
        		lockEnabled: true,
        		lockStateOn: true,
        		adminEnabled: !lockStateOn,
	            clusterType: 'Global',
	            surfaceViewClass: GraphSurfaceView,
	            globalClusterOptions: {
	            	noCompress: true,
	            	fontSizeProportion: 0.1,
	            	minDepth: 2
	            },
	            features: {
		            'LeafFocusToggleTimer': false,
		            'MovableFocus': false,
		            'ActivityPullForward': false,
        			'ActivityPushBack': false,
        			'PointShortPress': false,
        			'PointLongPress': true,
        			'PointAdd': true
            	},
	            listeners: {
	                'Initialized': this.onEventInitialized
	            },
                webSocketHandlers: {
                    'NewMessage': this.onNewWebSocketMessage
                }
	        });

	    Global.registerContextType('Behave_FriendRoot', FriendRoot)

        Global.on('ExportText', function(cluster, rootID) {
            _this.exportData(cluster, rootID, 'Text');
        });

        Global.on('Markdown', function(cluster, rootID) {
            _this.exportData(cluster, rootID, 'Markdown');
        });

        Global.on('ExportHTML', function(cluster, rootID) {
            _this.exportData(cluster, rootID, 'HTML');
        });

        Global.on('ExportJSON', function(cluster, rootID) {
            _this.exportData(cluster, rootID, 'JSON');
        });

        Global.on('ImportJSON', function(cluster, rootID, data) {
        	_this.importData(cluster, rootID, 'JSON', data);
    	});

    	Global.on('LockOn', this.setLocked)
    	Global.on('LockOff', this.setUnlocked)

		this.$availBalance = $('<div>')
								.addClass('avail-balance')
								.text('')
								.appendTo(this.$el);

        BenomeAppView.prototype.initialize.call(this, appOptions);
    },

    onNewWebSocketMessage: function(message) {
        if (message.MessageType == 'BalanceModified') {
            if (message.UserID == message.Data.UserID) {
                // Update the balance display
                this.setBalance(message.Data.Balance, true)
            }
            else {
                var contextID = this.userContextMap[message.Data.UserID],
                    view = this.cluster.getView(contextID)

                if (view) {
                    view.surfaceView.setBalance(message.Data.Balance)
                }
            }
        }
    },

    setLocked: function() {
    	Global.lockStateOn = true;
    	Global.adminEnabled = false;

    	Global.setFeatures({
        	'Admin': false,
            'HistoryPointMove': false,
	        'AddFeedbackInteractive': false,
	        'ContextModify': false,
	        'ClusteredEdit': false,
	        'DetailLevels': false,
	        'ActivityAdd': false,
        	'ActivityMove': false
        })

        this.hideDestroyerView()
        //this.renderIcons()
		this.getAdminView && this.getAdminView.hide()
    },

    setUnlocked: function() {
    	Global.lockStateOn = false;
    	Global.adminEnabled = true;

    	Global.setFeatures({
        	'Admin': true,
            'HistoryPointMove': true,
	        'AddFeedbackInteractive': true,
	        'ContextModify': true,
	        'ClusteredEdit': true,
	        'DetailLevels': true,
	        'ActivityAdd': true,
        	'ActivityMove': true
        })

        this.showDestroyerView()
        //this.renderIcons()
        this.getAdminView && this.getAdminView.show()
    },

    onEventInitialized: function() {
        this.cluster = this.globalCluster.cluster;
        this.contexts = this.cluster.contexts;
		this.contexts.points.on('add remove change', this.updateScore);

		this.exportAttrs = ['Label', ['BaseValue', this.getNamespaceID()], ['BaseDurationValue', this.getNamespaceID()]];

		var _this = this;

		this.contexts.points.on('add', function(a, b, c) {
			_this.updateBalance(a, b, c, 'add');
		});

		this.contexts.points.on('remove', function(a, b, c) {
			_this.updateBalance(a, b, c, 'remove');
		});

		this.contexts.points.on('change', function(a, b, c) {
			_this.updateBalance(a, b, c, 'update');
		});

		// Define set of attributes
		this.initPointAttributes();

		Global.on('GlobalClusterRendered', this.updateScore);
		this.updateScore({
			noRenderGraph: true
		});
		this.renderBalance()

		if (Global.lockStateOn) {
			this.setLocked()
		}
		else {
			this.setUnlocked()
		}

		this.getFriendData()
	},

	getFriendData: function() {
        var _this = this;

        var struct = {
			Label: 'Friends',
			Type: 'FriendsRoot',
			Children: []
		}

        var s = function(response, textStatus, jqXHR) {
            if (response && response.Success) {
            }

        }

        function success(result) {
        	var friends = struct.Children
            _.each(result, function(friend) {
            	if (friend.User.Username == _this.username) {
            		_this.userData = friend
            		return
            	}

            	friends.push({
            		Label: friend.User.FriendlyName,
                    UserID: friend.User.ID,
            		AvatarUrl: friend.User.AvatarUrl,
            		Data: {
            			CurrentBalance: friend.Data ? friend.Data.CurrentBalance : 0
            		}
            	})
            })
            struct.Children = friends
            _this.importTransientData(_this.cluster, _this.cluster.rootID, struct)
        }

        function complete() {
        }

        _.delay(function() {
            Global.ajax.jsonGet('/' + _this.username + '/behave/following', null, success)
        });
	},

	importTransientData: function(cluster, rootID, struct) {
        var rootID = rootID || cluster.rootID,
            _this = this,
            contexts = this.contexts

        this.userContextMap = {}

        function createContext(parentContext, struct) {
            var parentID = parentContext.id,
                currentID = _this.dataLoader.nextID(),
                label = struct.Label || '',
                saveAssoc = false,
                contextType

            if (struct.Type == 'FriendsRoot') {
            	contextType = null
            }
            else {
            	contextType = 'Behave_FriendRoot'
            }

            var context = new Context({
                ID: currentID,
                Type: contextType,
                Options: {
                	dragDisabled: true,
                	dropDisabled: true
                },
                ParentID: parentID,
                '1__Label': label,
                '1__AvatarUrl': struct.AvatarUrl || '',
                '1__UserID': struct.UserID || null,
                'Data': struct.Data || {}
            }, {silent: true, save: false})
            context.url = null
            context.remote = false

            contexts.add(context, {save: false})

            contexts.addAssoc('up', currentID, parentID, {save: saveAssoc})
            contexts.addAssoc('down', parentID, currentID, {save: saveAssoc})

            if (struct.UserID) {
                _this.userContextMap[struct.UserID] = currentID
            }

            return context
        }

        function traverse(struct, parentContext) {
            if (!_.isObject(struct) || !parentContext) {
                return
            }
            // Challenge: need IDs of previous to add children to it
            // Could possibly exhaust ID range if import is large

            var newContext = createContext(parentContext, struct)
            _.each(struct.Children, function(childStruct) {
                traverse(childStruct, newContext)
            })
        }

        cluster.disableRender()
        
        var rootContext = contexts.get(rootID)
        rootContext.set({
        	Type: 'Behave_FriendRoot',
        	'1__Label': this.userData.User.FriendlyName,
        	'1__AvatarUrl': this.userData.User.AvatarUrl || '',
        	'Data': {}
        })
        cluster.viewCache[rootID].hide()
        delete cluster.viewCache[rootID]
        delete cluster.surfaceViewCache[rootID]
        traverse(struct, rootContext)

        cluster.enableRender()
        cluster.render()
    },

	importData: function(cluster, rootID, importType, data) {
        cluster = cluster || Global.globalCluster.cluster;
        rootID = rootID || cluster.rootID;
        importType = importType || 'JSON';

        var _this = this;

        if (importType == 'JSON') {
            try {
                var struct = JSON.parse(data);
            }
            catch (e) {};

            var contexts = cluster.contexts,
                associations = contexts.associations;

            function createContext(parentContext, label) {
                var parentID = parentContext.id,
                    label = label || '',
                    currentID = _this.dataLoader.nextID();

                var saveAssoc = !!Global.localOnly;

                associations.addAssoc('up', currentID, parentID, {save: saveAssoc});
                associations.addAssoc('down', parentID, currentID, {save: saveAssoc});
                var context = contexts.create({
                    ID: currentID,
                    ParentID: parentID,
                    '1__Label': label
                }, {
                    silent: false,
                    type: 'post'
                });
                return context;
            }

            function traverse(struct, parentContext) {
                if (!_.isObject(struct) || !parentContext) {
                    return;
                }

                // Challenge: need IDs of previous to add children to it
                // Could possibly exhaust ID range if import is large

                var newContext = createContext(parentContext, struct.Label);
                _.each(struct.Children, function(childStruct) {
                    traverse(childStruct, newContext);
                });
            }

            traverse(struct, cluster.contexts.get(rootID));
            cluster.render();
        }
    },

	exportData: function(cluster, rootID, exportType) {
        cluster = cluster || this.cluster;
        rootID = rootID || cluster.rootID;
        exportType = exportType || 'JSON';

        var exportedData = cluster.contexts.exportTree(rootID, this.exportAttrs),
            readyData,
            contentType,
            fileExt;

        if (exportType == 'JSON') {
            readyData = JSON.stringify(exportedData, null, 4);
            contentType = 'application/json';
            fileExt = 'json'
        }
        else if (exportType == 'Text') {
            readyData = this.formatTextExport(exportedData, 4);
            contentType = 'text/plain';
            fileExt = 'txt'
        }
        else if (exportType == 'Markdown') {
            readyData = this.formatMarkdownExport(exportedData, 1);
            contentType = 'text/markdown';
            fileExt = 'markdown'
        }
        else if (exportType == 'HTML') {
            var markdownData = this.formatMarkdownExport(exportedData, 1);

            readyData = markdownParser.render(markdownData);
            contentType = 'text/html';
            fileExt = 'html'
        }

        var url = 'data:' + contentType + ';charset=US-ASCII;base64,' + btoa(readyData),
            fileName = 'BreakdownExport_' + Global.getID() + '.' + fileExt;

        sendFile(url, fileName);
    },

    formatTextExport: function(struct, numIndents) {
        var text = '',
            numIndents = numIndents || 4,
            indentStr = ' '.repeat(numIndents);

        function traverse(struct, level) {
            level = level || 0;
            text += indentStr.repeat(level) + (struct.Text || struct.Label || '') + '\n';

            _.each(struct['Children'], function(childStruct) {
                traverse(childStruct, level + 1);
            }, this);
        }
        traverse(struct);

        return text;
    },

    formatMarkdownExport: function(struct, numHeadingLevels) {
        var text = '',
            numIndents = 2,
            indentStr = ' '.repeat(numIndents);

        function traverse(struct, level) {
            level = level || 0;
            var structText = (struct.Text || struct.Label || '');

            if (level < 1 + numHeadingLevels) {
                text += '#'.repeat(level + numHeadingLevels) + ' ' + structText + '\n';
            }
            else {
                text += indentStr.repeat(level - 1 - numHeadingLevels) + '- ' + structText + '\n';
            }

            _.each(struct['Children'], function(childStruct) {
                traverse(childStruct, level + 1);
            }, this);
        }
        traverse(struct);

        return text;
    },

	initPointAttributes: function() {
		this.appContext = this.getAppContext();

		if (!this.appContext) {
			console.log('AppContext is missing')
			return;
		}

		this.bonusDefContext = _.find(this.appContext.getAssocModels('down'), function(outContext) {
			return outContext.getNS('Label') == 'Bonuses';
		});
	},

	getBonuses: function() {
		return this.getContextBonusDefs(this.bonusDefContext);
	},

	getContextBonusDefs: function(bonusDefContext) {
		var namespaceID = this.getNamespaceID(),
			result = {};

		_.each(bonusDefContext.getAssocModels('down'), function(bonusContext) {
			var bonusID = bonusContext.id;
			result[bonusID] = {
				'Label': bonusContext.getNS('Label', 1),
				'AttrID': bonusContext.getNS('AttrID', namespaceID),
				'OriginalContextID': bonusContext.id,
				//'Type': 'ContextDef',
		        'GlobalMultiplier': !!parseInt(bonusContext.getNS('GlobalMultiplier', namespaceID)),
		        'MultiplierValue': parseInt(bonusContext.getNS('MultiplierValue', namespaceID)) || 1,
		        'Text': bonusContext.getNS('Text', namespaceID)
			}
		}, this);

		return result;
	},

	getPointAttributeDefs: function() {
		var attributeDefs = [];
		_.each(this.pointAttributes, function(attrDef, attrID) {
			var attrDef = $.extend(true, {}, attrDef);

			if (attrID == 'Bonuses') {
				_.each(this.getBonuses(), function(bonusDef, bonusID) {
					attrDef.Def.Nodes.push($.extend(true, {}, bonusDef));
				}, this);
			}

			attributeDefs.push(attrDef);
		}, this);

		return attributeDefs;
	},

	getContextAttributeDefs: function() {
		var attributeDefs = [];
		_.each(this.contextAttributes, function(attrDef, attrID) {
			var attrDef = $.extend(true, {}, attrDef);

			if (attrID == 'Bonuses') {
				_.each(this.getBonuses(), function(bonusDef, bonusID) {
					attrDef.Def.Nodes.push($.extend(true, {}, bonusDef));
				}, this);
			}

			attributeDefs.push(attrDef);
		}, this);

		return attributeDefs;
	},

	getPointEventHandlers: function(contextModel) {
		return {
			onValueChange: this.renderValue,
			onReady: this.renderValue,
			onCommit: this.pointCommit,
			onInitValues: this.pointInitValues,
            onBeforeSave: this.pointBeforeSave
		}
	},

    pointBeforeSave: function(contextID, attributes) {
        var points = this.calcAttributePoints(contextID, attributes)
        attributes[this.getNamespaceID() + '__Points'] = points
        return attributes;
    },

	pointInitValues: function(point, options) {
		return {
			'Bonuses': point.get('Bonuses', this.getNamespaceID())
		}
	},

	pointCommit: function(values, point, options) {
		var attributes = {};
		if ('Bonuses' in values) {
			attributes[this.getNamespaceID() + '__' + 'Bonuses'] = values['Bonuses']
        }

        return attributes;
	},

	getContextEventHandlers: function(contextModel) {
		return {
			onInitValues: this.contextInitValues,
			onCommit: this.bonusDefCommit
		}
	},

	contextInitValues: function(context, options) {
		return {
			'BaseValue': context.getNS('BaseValue', this.getNamespaceID()) || 10,
            'BaseDurationValue': context.getNS('BaseDurationValue', this.getNamespaceID()) || 0
		}
	},

	bonusDefCommit: function(values, context, options) {
		var namespaceID = this.getNamespaceID(),
			updateAttributes = {};

		_.each(values, function(val, attributeName) {
			if (attributeName == 'Label' || attributeName == 'BaseValue' || attributeName == 'BaseDurationValue') {
				updateAttributes[namespaceID + '__' + attributeName] = val;

				if (attributeName == 'BaseValue' || attributeName == 'BaseDurationValue') {
					// Clear the cache so the change takes immediate effect
					this.baseValueCache = null;
				}
			}
			else if (attributeName == 'Bonuses') {
				_.each(val, function(bonusDef, originalContextID) {
					if (!originalContextID) {
						return;
					}

					if (_.isString(originalContextID) && originalContextID.substr(0, 8) == 'NewBonus') {
						//console.log('New bonus', bonusDef);
						var clusterID = context.collection.clusterController.cluster.clusterID,
							parentContextID = this.bonusDefContext.id;

						Global.trigger('AddContext', parentContextID, clusterID, bonusDef); //, loadCallback)
					}
					else {
						var contextID = parseInt(originalContextID);
						if (!_.isNaN(contextID)) {
							//console.log('Existing bonus', originalContextID, bonusDef);
							Global.trigger('UpdateContext', contextID, bonusDef);
								 //, loadCallback, loadCallbackOptions) {
						}
					}
				}, this);
			}
		}, this);

		return updateAttributes;
	},

	surfaceRender: function(cluster, clusterMode, surfaceView, surfaceModeView, options) {
		options = options || {};
		if (clusterMode == 'Expanded' || clusterMode == 'Exclusive') {
			var regionWidth = surfaceModeView.regionWidth,
				regionHeight = surfaceModeView.regionHeight,

				graphWidth = options.graphWidth,
				graphHeight = options.graphHeight,

				top = 0,
				left = 0

				graphOptions = {};

			if (clusterMode == 'Expanded') {
				var numDays = 7;
				_.extend(graphOptions, {
					numDays: numDays,
					graphWindow: numDays * 86400,
					numSegments: numDays * 5,
					numLabels: numDays
				});

                graphWidth = graphWidth || regionHeight;
                graphHeight = graphHeight || regionWidth;
			}
			else {
				var numDays = 14;
				_.extend(graphOptions, {
					numDays: numDays,
					graphWindow: numDays * 86400,
					numSegments: numDays * 5,
					numLabels: numDays
				});

				graphWidth = graphWidth || (regionWidth * 0.9);
	            graphHeight = graphHeight || (regionHeight * 0.7);

		        left = regionWidth * 0.05;
		        top = regionHeight * 0.15;
			}

	        if (!surfaceModeView.Graph) {
	            surfaceModeView.Graph = new GraphVisual(surfaceModeView, this.calcGraphData, {
	            	labels: true,
	            	labelFunc: function(rawTotal, total, numSegments, layers, segmentIdx, layerData) {
	            		return rawTotal;
	            	}
	            });
	        }
	        surfaceModeView.Graph.render(graphWidth, graphHeight, graphOptions, left, top);
	    }
	},

	getAppContext: function(force) {
		if (!force && this.appContext) {
			return this.appContext;
		}

        var baseAppContext = this.globalCollection.find(function(model) {
            return model.attributes['1__NodeType'] == 'Apps'
        });
        this.appContext = baseAppContext.getAssocByAttr('down', 'Label', 'Behave')[0];
		return this.appContext;
	},

	getNamespaceID: function(force) {
		if (!force && this.namespaceID) {
			return this.namespaceID;
		}

        var appContext = this.getAppContext()
        this.namespaceID = appContext.get('1__NamespaceID')
		return this.namespaceID;
	},

	renderBalance: function() {
		this.$availBalance.text(this.getBalance());
	},

	getBalance: function() {
		return parseInt(this.getAppContext().getNS('Balance', this.namespaceID) || 0);
	},

	updateBalance: function(point, pointCollection, options, action) {
		var currentBalance = this.getBalance(),
			newBalance = currentBalance,
			currentPointValue = this.getPointValue(point);

		if (action == 'add') {
			newBalance += currentPointValue;
		}
		else if (action == 'remove') {
			newBalance -= currentPointValue;
		}
		else if (action == 'update') {
			var prevPointValue = this.getPointValue(point, true);
			newBalance -= prevPointValue;
			newBalance += currentPointValue;
		}
        this.setBalance(newBalance)
    },

    setBalance: function(newBalance, localOnly) {
        var updateAttr = {};
        updateAttr[this.getNamespaceID() + '__Balance'] = newBalance;

        if (!localOnly) {
            var _this = this;
            this.getAppContext().save(updateAttr, {
                success: function() {
                    _this.renderBalance();
                }
            });
        }
        else {
            this.getAppContext().set(updateAttr, {
                silent: true
            })
            this.renderBalance();
        }
	},

	updateScore: function(options) {
		options = options || {};

		var clusterController = Global.globalCluster,
			cluster = clusterController.cluster,
			dayRange = clusterController.clusterMode == 'Expanded' ? 7 * 86400 : 'today';

        var _this = this;
        function updateScores(focusContextID, parentContextID) {
        	var focusView = cluster.getView(focusContextID);

        	if (focusView && focusView.surfaceView) {
        		var rangePoints = _this.calcRangePoints(clusterController, focusContextID, dayRange);
	        	rangePoints = rangePoints || '';
	        	focusView.surfaceView.setExtra(rangePoints);

		        _.each(focusView.model.getNeighbourModels(), function(childModel) {
		        	var childContextID = childModel.id;
		        	if (childContextID != parentContextID) {
		        		updateScores(childContextID, focusContextID);
		        	}
		        }, _this);
		    }
	    }

	    updateScores(cluster.focusID);

        if (!options.noRenderGraph) {
	        // Re-render surface if graph is visible
	        if (clusterController.clusterMode == 'Expanded' || clusterController.clusterMode == 'Exclusive' ) {
		        var focusID = cluster.focusID,
		        	focusView = cluster.getView(focusID);
	        	focusView.surfaceView.render();
	        }
	    }
	},

	calcGraphData: function(contextID, surfaceView, graphOptions) {
        var renderEOD = moment(Global.getNow()).endOf('day');

        var clusterContexts = surfaceView.baseView.cluster.contexts,
        	contextModel = clusterContexts.get(contextID),
        	neighbours = contextModel.getAssoc('down');

        // Include focus
        neighbours.push(contextID);

        var points = clusterContexts.points.getRecentPoints(graphOptions.graphWindow, contextID);

        _.each(points, function(point) {
        	var contextID = point.get('ContextID'),
        		contextModel = clusterContexts.get(contextID);

        	while (contextModel) {
        		if (_.indexOf(neighbours, contextModel.id) >= 0) {
        			point.neighbourContextID = contextModel.id;
        			break;
        		}
        		contextModel = contextModel.getParent();
        	}
        });

        var dayPoints = _.groupBy(points, function(p) {
		        	var pointTime = moment(p.get('Time') * 1000),
			            daysDiff = renderEOD.diff(pointTime, 'days');
			        return daysDiff;
			    });

        var graphData = _.object(_.map(neighbours, function(contextID) {
        	return [contextID, []];
        }));

        _.each(_.range(0, graphOptions.numDays), function(dayIdx) {
            var points = dayPoints[dayIdx],
            	dayContextPoints = _.groupBy(points, function(p) {
			        return p.neighbourContextID;
			    });

			_.each(neighbours, function(contextID) {
				graphData[contextID][dayIdx] = this.sumPoints(dayContextPoints[contextID] || []);
			}, this);
        }, this);

        var graphLayers = _.map(graphData, function(data, contextID) {
        	return {
        		'LayerID': contextID,
        		'Color': clusterContexts.get(contextID).getColor(null, 0.5),
        		'Data': data
        	}
        });

        return graphLayers;
	},

	calcTodaysPoints: function(clusterController, contextID) {
		var daySeconds = moment().diff(moment().startOf('day')) / 1000;
		return this.calcRangePoints(clusterController, contextID, daySeconds);
	},

	calcRangePoints: function(clusterController, contextID, rangeSeconds) {
		if (rangeSeconds == 'today') {
			return this.calcTodaysPoints(clusterController, contextID);
		}
		var rangePoints = clusterController.cluster.contexts.points.getRecentPoints(rangeSeconds, contextID);
		return this.sumPoints(rangePoints);
	},

	calcPointBalance: function(clusterController) {
		var pointsCollection = clusterController.cluster.contexts.points,
			points = pointsCollection.models;

		return this.sumPoints(points);
	},

	getPointValue: function(point, prevValue) {
		  var contextID = point.getContextID(),
            attributes

        if (prevValue) {
            attributes = point.previousAttributes()
        }
        else {
            attributes = point.toJSON()
        }

		return this.calcAttributePoints(contextID, attributes)
	},

    calcAttributePoints: function(contextID, attributes) {
        var namespaceID = this.getNamespaceID(),
            [baseValue, baseDurationValue] = this.getBaseValue(contextID),
            duration = attributes['1__Duration'] || 0,
            bonuses = attributes[namespaceID + '__' + 'Bonuses'] || {},
            pointVal

        // Only use durationValue if duration is also set.
        if (_.isNumber(baseDurationValue) && duration) {
            pointVal = this.calcDurationPoints(baseDurationValue, duration)
        }
        else {
            pointVal = this.calcPoints(baseValue, bonuses)
        }

        return pointVal
    },

	sumPoints: function(points) {
		var sum = 0;
		var pointValues = _.map(points, function(point) {
			var pointVal = this.getPointValue(point);
			sum += pointVal;
        	return [point, point.getContextID(), pointVal];
		}, this);

		return sum;
	},

	calcDurationPoints: function(baseDurationValue, duration) {
		// baseDurationValue is points per hour
        var baseDurationValueSign = baseDurationValue < 0 ? -1 : 1
        baseDurationValue = Math.abs(baseDurationValue)
		duration = duration || 0
        
        var minPoints = Math.max(1, baseDurationValue / 12) // 5 minutes worth, minimum 1 point
		return baseDurationValueSign * parseInt(Math.round(Math.max(minPoints, (duration / 3600) * baseDurationValue)))
	},

	calcPoints: function(baseValue, bonusFlags) {
	    bonusFlags = bonusFlags || {};

	    var total = baseValue,
	        globalMultiplier = 1;

	    _.each(bonusFlags, function(bonusEnabled, bonusID) {
	        if (!bonusEnabled) {
	            return;
	        }

	        var bonusDef = this.getBonuses()[bonusID] || {
	            'GlobalMultiplier': false,
	            'MultiplierValue': 1,
	            'Text': ''
	        };

	        var multiplier = bonusDef.MultiplierValue;
	        if (bonusDef.GlobalMultiplier) {
	            globalMultiplier *= multiplier;
	        }
	        else {
	            total += baseValue * multiplier;
	        }
	    }, this);
	    total *= globalMultiplier;

	    return Math.round(total);
	},

	getBaseValue: function(contextID) {
        if (!this.baseValueCache) {
            this.baseValueCache = {};
        }
        var namespaceID = this.getNamespaceID(),
        	baseValue,
        	baseDurationValue;

        if (contextID in this.baseValueCache) {
        	[baseValue, baseDurationValue] = this.baseValueCache[contextID]
        }

        if (!_.isNumber(baseValue)) {

	        var context = this.contexts.get(contextID);
	        while (context) {
	        	if (!_.isNumber(baseValue)) {
		        	var contextBaseValue = context.getNS('BaseValue', namespaceID);
		        	if (contextBaseValue) {
		        		baseValue = parseInt(contextBaseValue) || 10;
		        	}
		        }

		        if (!_.isNumber(baseDurationValue)) {
		        	var contextBaseDurationValue = context.getNS('BaseDurationValue', namespaceID);
		        	if (contextBaseDurationValue) {
		        		baseDurationValue = parseInt(contextBaseDurationValue) || null;
		        	}
		        }

	        	if (_.isNumber(baseValue) && _.isNumber(baseDurationValue)) {
	        		break
	        	}
	            context = this.contexts.get(context.getParentID());
	        }
	        baseValue = baseValue || 10;

	        var context = this.contexts.get(contextID),
	        	modifier = 1;
	        while (context) {
	        	var contextNodeType = context.getNS('NodeType', namespaceID)
	        	if (contextNodeType == 'BalanceSpend' || contextNodeType == 'BalanceEarn') {
	        		if (contextNodeType == 'BalanceSpend') {
	        			modifier = -1
	        		}
	        		break
	        	}
	            context = this.contexts.get(context.getParentID())
	        }

            if (baseValue) {
	            baseValue *= modifier
            }

            if (baseDurationValue) {
                baseDurationValue *= modifier
            }

	        this.baseValueCache[contextID] = [baseValue, baseDurationValue]
        }

        return [baseValue, baseDurationValue]
    },

	renderValue: function(clusterController, newValue, allValues) {
        allValues = allValues || {};

        // FIXME: SUPER HACKY FOR NOW

        var bonusSurfaceViewID = clusterController.contextCollection.findWhere({'AttrID': 'Bonuses'}).id - 0,
            bonusSurfaceView = clusterController.cluster.surfaceViewCache[bonusSurfaceViewID],
            bonusClusterController;

        if (bonusSurfaceView) {
        	bonusClusterController = bonusSurfaceView.cluster;
        }
        else {
        	return;
        }

        // Calculate and display the point value
        var contextID = clusterController.struct.Origin.viewID,
        	[baseValue, baseDurationValue] = this.getBaseValue(contextID),
        	bonusText,
        	totalPoints,
            numBonusPoints,

            // The current value of the underlying point.
            // FIXME: need to update the value displayed on the root node after it has been modified
            duration = clusterController.struct.Value.Timing.Duration

        if (_.isNumber(baseDurationValue) && duration > 0) {
        	bonusText = baseDurationValue + ' / hr'
            totalPoints = this.calcDurationPoints(baseDurationValue, duration)
        }
        else {
        	totalPoints = this.calcPoints(baseValue, allValues.Bonuses)
            numBonusPoints = totalPoints - baseValue,
            bonusText = numBonusPoints > 0 ? '+' + numBonusPoints : '';
        }

        // FIXME: SUPER HACKY FOR NOW

        // Update the attribute cluster root
        var rootID = clusterController.contextCollection.rootID,
            rootSurfaceView = clusterController.cluster.surfaceViewCache[rootID];
        rootSurfaceView.$el.html('<br><br>' + rootSurfaceView.contextModel.get('Label') + '<br><br><span style="font-size: 2em">' + totalPoints + '</span>');

        // Update the bonus node surface
        var bonusSurfaceModeView = bonusSurfaceView.getModeView('View');
        bonusSurfaceModeView.$el.html('<br>' + bonusSurfaceView.contextModel.get('Label') + '<br><br><span style="font-size: 2em">' + bonusText + '</span>');

        if (bonusClusterController) {
            // Update the bonus cluster root
            var bonusClusterRootID = bonusClusterController.contextCollection.rootID,
                bonusRootSurfaceView = bonusClusterController.cluster.surfaceViewCache[bonusClusterRootID];

            bonusRootSurfaceView.$el.html('<br><br>' + bonusRootSurfaceView.contextModel.get('Label') + '<br><br><span style="font-size: 2em">' + bonusText + '</span>');
        }
    }
});

module.exports = BehaveAppView;