// Libs
import $ from 'jquery'
import _ from 'underscore'
import Backbone from 'backbone'

import SurfaceView from 'benome/cluster/SurfaceView'

var FriendRootSurfaceView = SurfaceView.extend({
    className: 'friendroot-surface-view',

    initialize: function(options) {
        options = options || {};
        SurfaceView.prototype.initialize.call(this, options);

        this.dropDisabled = true
        this.dragDisabled = true

        this.$label = $('<div>')
                            .addClass('label')
                            .appendTo(this.$el);

        var label = this.contextModel.getNS('Label') || '';
        this.$label.html(label);

        this.$balance = $('<div>')
                            .addClass('balance')
                            .appendTo(this.$el)

        var currentBalance = this.contextModel.get('Data').CurrentBalance
        this.setBalance(currentBalance)

        this.$backgroundImage = $('<img>')
                            .css({
                                'position': 'absolute',
                                'z-index': '-5',
                                'width': '60%',
                                'height': '60%',
                                'left': '20%',
                                'top': '20%'
                            })
                            .attr('src', this.contextModel.getNS('AvatarUrl') || '')
                            .appendTo(this.$el);
    },

    setImageSrc: function(imageSrc) {
        if (imageSrc) {
            this.$backgroundImage
                .show()
                .attr('src', imageSrc);
        }
        else {
            this.$backgroundImage
                .hide();
        }
    },

    setLabel: function(label) {
        this.$label.html(label);
    },

    setBalance: function(balance) {
        this.$balance.html(balance)
    },

    setExtra: function() {}
});

export default FriendRootSurfaceView