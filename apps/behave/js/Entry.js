/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'

// -------------

import BaseEntry from 'benome/BaseEntry'
import BehaveAppView from './AppView'

$(function() {

(function(window, document) {

var BehaveEntry = function() {};

_.extend(BehaveEntry.prototype, BaseEntry.prototype, {
    init: function(options) {
        options = options || {};

        BaseEntry.prototype.init.call(this, {
            appView: BehaveAppView,
            appOptions: options.appOptions || {}
        });
    }
});

window.BEHAVE = BehaveEntry;

}(window, document));

});