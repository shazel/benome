/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'

// -------------
import BaseAppView from 'benome/views/BaseAppView'
import Colors from 'benome/modules/Util/Colors'
import StreamGraphD3_Class from 'benome/modules/StreamGraphD3'
import AggregateLayerData from 'benome/modules/Functions/AggregateLayerData'
import Data from 'benome/models/Data'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

// -------------

var StreamgraphAppView = BaseAppView.extend({
    tagName: 'div',
    className: 'benome',

    events: {
    },

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'onInitialized');

        this.exportData = options.data;
        delete options.data;
        this.mergedGraph = options.mergedGraph;

        var appOptions = _.extend(options, {
            helpEnabled: false,
            adminEnabled: false,
            listeners: {
                'Initialized': this.onInitialized
            }
        });

        this.streamGraph = new StreamGraphD3_Class({
            preWeighted: true
        });
        this.$backgroundImage = $('<canvas>')
                                    .css({
                                        'position': 'absolute',
                                        'width': '100%',
                                        'height': '100%',
                                        'opacity': '1',
                                        'top': 0,
                                        'left': 0
                                    })
                                    .appendTo(this.$el);

        BaseAppView.prototype.initialize.call(this, appOptions);
    },

    onInitialized: function() {
        var contexts = this.userCollection;

        this.colors = new Colors(contexts, {
            rootContextID: contexts.rootID,
            minLightness: 0.15
        });

        this.importData(this.exportData.ExportData, {
                                                        importType: 'Dict',
                                                        replaceRoot: true
                                                    });

        AggregateLayerData(contexts.getRoot(), true);

        var data = this.getData(contexts.getRoot(), this.mergedGraph);
        if (data && data.length > 0) {
            var $graphEl = this.$backgroundImage;
            this.streamGraph.render({
                data: data,
                outputType: 'Canvas',
                destEl: $graphEl.get()[0],
                width: this.$backgroundImage.width(),
                height: this.$backgroundImage.height(),
                antiAlias: !this.mergedGraph,
            }, function(canvas, yMax, yTotal) {});
        }
    },

    getData: function(context, merged) {
        var downAssocModels = context.getAssocModels('down');
        if (downAssocModels.length == 0) {
            downAssocModels = [
                context
            ];
        }

        var layers = _.compact(_.map(downAssocModels, function(assocModel) {
            if (assocModel.id == context.id) {
                var data = assocModel.aggregateGraphData
            }
            else {
                var data = assocModel.weightedAggregateGraphData;
            }
            
            if (data && data.length) {
                var colorContext = merged ? context : assocModel;
                var color = this.colors.getColor(colorContext.id, true, 0.65, true);
                return {
                    'Data': data,
                    'Color': color
                }
            }
        }, this));

        return layers;
    },

    importData: function(data, options) {
        options = options || {};
        var contexts = this.userCollection,
            associations = contexts.associations,
            rootID = options.rootID || contexts.rootID,
            importType = options.importType || 'JSON',
            _this = this;

        if (importType == 'JSON') {
            try {
                var struct = JSON.parse(data);
            }
            catch (e) {};
        }
        else if (importType == 'Dict') {
            struct = data;
        }
        else {
            console.log('Unsupported import type: ' + importType);
            return;
        }

        function createContext(parentContext, refID, replaceRoot, graphData) {
            var label = '',
                context;

            if (replaceRoot) {
                parentContext.set('1__Label', label);
                parentContext.set('1__RefID', refID);
                context = parentContext;
            }
            else {
                var parentID = parentContext.id,
                    currentID = _this.dataLoader.nextID(),
                    saveAssoc = !!_this.options.localOnly;

                associations.addAssoc('up', currentID, parentID, {save: saveAssoc});
                associations.addAssoc('down', parentID, currentID, {save: saveAssoc});
                var context = contexts.create({
                    ID: currentID,
                    ParentID: parentID,
                    '1__Label': label,
                    '1__RefID': refID
                }, {
                    silent: false,
                    type: 'post'
                });
            }

            context.graphData = graphData;
            return context;
        }

        function traverse(struct, parentContext, replaceRoot) {
            if (!_.isObject(struct) || !parentContext) {
                return;
            }

            // Challenge: need IDs of previous to add children to it
            // Could possibly exhaust ID range if import is large

            // struct.Label

            var refID = struct.ID,
                newContext = createContext(parentContext, refID, replaceRoot, struct.GraphData ? struct.GraphData.Data.Data : null);

            _.each(struct.Children, function(childStruct) {
                traverse(childStruct, newContext);
            });
        }

        traverse(struct, contexts.get(rootID), options.replaceRoot);
    }
});

module.exports = StreamgraphAppView;