#!/usr/bin/python

import os

def shell(cmd):
    import subprocess
    p = subprocess.Popen(cmd, shell=True, stdout=None, stderr=None)
    p.wait()

def render_template(src_path, dest_path, template_vars):
    from string import Template

    rendered_template = ''
    with open(src_path, 'r') as f:
        template = Template(f.read())
        rendered_template = template.substitute(**template_vars)

    with open(dest_path, 'w') as f:
        f.write(rendered_template)

    return True

if __name__ == '__main__':
    import sys

    frontend_name = sys.argv[1].title()
    fel = frontend_name.lower()

    os.mkdir(fel)
    shell('cp -a skel/* {0}/'.format(fel))

    template_vars = {
        'fe': frontend_name,
        'fel': fel,
        'feu': frontend_name.upper(),
        'fef': frontend_name[0].lower()
    }

    # Add symlink to framework code
    symlink_path = '{0}/js/benome'.format(fel)
    framework_path = '../../../client/js/'
    os.symlink(framework_path, symlink_path)

    # Add symlink to CSS
    symlink_path = '{0}/html/css/Benome.css'.format(fel)
    script_path = '../../../../client/css/Benome.css'.format(fel)
    os.symlink(script_path, symlink_path)

    # Add node_modules symlinks
    symlink_path = '{0}/node_modules'.format(fel)
    modules_path = '../../client/node_modules/'
    os.symlink(modules_path, symlink_path)

    symlink_path = '{0}/build/node_modules'.format(fel)
    modules_path = '../../benome/build/node_modules/'
    os.symlink(modules_path, symlink_path)

    # Add symlink from js dir to dev build
    symlink_path = '{0}/html/js/bundle_{1}.js'.format(fel, fel)
    script_path = '../../build/dev/bundle_{0}.js'.format(fel)
    os.symlink(script_path, symlink_path)

    # Prepare the AppView
    js_path = '{0}/js/AppView.js'.format(fel)
    render_template(js_path, js_path, template_vars)

    # Prepare the Entrypoint
    js_path = '{0}/js/Entry.js'.format(fel)
    render_template(js_path, js_path, template_vars)

    # Prepare the build script
    gulpfile_path = '{0}/build/gulpfile.js'.format(fel)
    render_template(gulpfile_path, gulpfile_path, template_vars)

    # Prepare index.html and base.html
    index_path = '{0}/html/index.html'.format(fel)
    render_template(index_path, index_path, template_vars)

    base_path = '{0}/html/base.html'.format(fel)
    render_template(base_path, base_path, template_vars)
