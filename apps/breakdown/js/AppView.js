/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'
import Mousetrap from 'mousetrap'
import MarkdownParser from 'markdown-it'
var markdownParser = MarkdownParser()

// -------------
import ClusterAppView from 'benome/views/ClusterAppView'
import HTMLView from 'benome/modules/Views/HTMLView'
import _Global from 'benome/modules/GlobalState'
var Global = _Global()

import sendFile from 'benome/modules/Util/SendFile'
import FluidText from 'benome/cluster/surfaces/FluidText'

import Data from 'benome/models/Data'

// -------------

var BreakdownAppView = ClusterAppView.extend({
    tagName: 'div',
    className: 'benome',

    events: {
    },

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'onInitialized', 'textFocusTimer');

        var appOptions = _.extend(options, {
            features: {
                'ActionHistory': false,
                'ActionTimeline': false,
                'ActivityPullForward': false,
                'ActivityPushBack': false,
                'AddFeedbackInteractive': false,
                'Admin': true,
                'ClusteredEdit': false,
                'ContextModify': false,
                'DetailLevels': false,
                'LeafFocusAutoAdd': false,
                'LeafFocusToggleTimer': false,
                'MovableFocus': false,
                'PointAdd': false,
                'PointDetail': false,
                'PointLongPress': false,
                'PointShortPress': true
            },
            visualQuality: 1,
            bgColor: 'light',
            clusterOnly: true,
            helpUrl: 'https://benome.ca/Breakdown/howtouse.html',
            surfaceViewClass: FluidText,

            // Bunch of hacks until App-related refactor is finished
            clusterControllerOptions: {
                surfaceModifierFunc: this.surfaceModifierFunc
            },
            clusterEvents: {
                'BeforeFocusChanged': this.onBeforeFocusChanged
            },
            globalClusterOptions: {
                noCompress: true
            },

            listeners: {
                'Initialized': this.onInitialized
            }
        });

        ClusterAppView.prototype.initialize.call(this, appOptions);

        this.htmlView = new HTMLView({
            $overlay: this.$overlay
        });
        this.htmlView.render().$el.appendTo(this.$el);

        this.textFocusTimer();
        Global.exportAttrs = ['Text'];
    },

    surfaceModifierFunc: function(cluster, surfaceView, viewID, viewState, baseView, contextModel) {
        if (!viewState.parentID) {
            surfaceView.setDisplayMode('Edit');
            surfaceView.focusText();
        }
        else {
            surfaceView.setDisplayMode('View');
        }
    },

    onBeforeFocusChanged: function(cluster, newFocusID, newFocusContext, prevFocusID, prevFocusContext) {
        var baseView = cluster.getView(prevFocusID),
            surfaceView = baseView.surfaceView;

        if (_.isString(surfaceView.newValue) && prevFocusContext) {
            Global.trigger('UpdateContext', prevFocusContext.id, {
                'Text': surfaceView.newValue
            }, null, function() {
                surfaceView.newValue = null;
            });
        }
    },

    textFocusTimer: function() {
        if (Global.globalCluster) {
            if (!this.overlayVisible()) {
                var focusView = Global.globalCluster.cluster.getFocusView();
                focusView.surfaceView.focusText({
                    noEnable: true
                });
            }
        }

        _.delay(this.textFocusTimer, 200);
    },

    onInitialized: function() {
        var _this = this;
        Global.on('ExportJSON', function(cluster, rootID) {
            _this.exportData(cluster, rootID, 'JSON');
        });

        Global.on('ExportText', function(cluster, rootID) {
            _this.exportData(cluster, rootID, 'Text');
        });

        Global.on('ExportMarkdown', function(cluster, rootID) {
            _this.exportData(cluster, rootID, 'Markdown');
        });

        Global.on('ExportHtml', function(cluster, rootID) {
            _this.exportData(cluster, rootID, 'HTML');
        });

        Global.on('ImportJSON', function(cluster, rootID, data) {
            _this.importData(cluster, rootID, 'JSON', data);
        });


        var app = this,
            keyboardListener = this.keyboardHandler.keyboardHandler;

        Mousetrap.bind('enter', function(e, combo) {
            if (!app.overlayVisible()) {
                keyboardListener.trigger('CreateSiblingContext', keyboardListener, app.globalCluster.cluster);
                e.preventDefault();
                return false;
            }
        });

        Mousetrap.bind('tab', function(e, combo) {
            if (!app.overlayVisible()) {
                keyboardListener.trigger('CreateContext', keyboardListener, app.globalCluster.cluster);
                e.preventDefault();
                return false;
            }
        });

        Mousetrap.bind('shift+tab', function(e, combo) {
            if (!app.overlayVisible()) {
                // Move current focus up a level, if possible.
                keyboardListener.moveFocusUp();
                e.preventDefault();
                return false;
            }
        });

        var isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0,
            platformModifier = isMac ? 'ctrl' : 'alt';

        Mousetrap.bind(platformModifier + '+p', function(e, combo) {
            if (!app.overlayVisible()) {
                // Show an HTML preview
                app.showHTMLPreview(app.globalCluster.cluster);
                e.preventDefault();
                return false;
            }
            else {
                if (app.htmlView.visible) {
                    app.htmlView.hide();
                }
            }
        });
    },

    showHTMLPreview: function(cluster) {
        var focusID = cluster.focusID,
            exportedData = cluster.contexts.exportTree(focusID, Global.exportAttrs),
            markdownData = this.formatMarkdownExport(exportedData, 0),
            html = markdownParser.render(markdownData);

        this.htmlView.show({
            html: html
        });
    },

    importData: function(cluster, rootID, importType, data) {
        cluster = cluster || Global.globalCluster.cluster;
        rootID = rootID || cluster.rootID;
        importType = importType || 'JSON';

        var _this = this;

        if (importType == 'JSON') {
            try {
                var struct = JSON.parse(data);
            }
            catch (e) {};

            var contexts = cluster.contexts,
                associations = contexts.associations;

            function createContext(parentContext, text) {
                var parentID = parentContext.id,
                    text = text || '',
                    currentID = _this.dataLoader.nextID();

                var saveAssoc = !!Global.localOnly;

                associations.addAssoc('up', currentID, parentID, {save: saveAssoc});
                associations.addAssoc('down', parentID, currentID, {save: saveAssoc});
                var context = contexts.create({
                    ID: currentID,
                    ParentID: parentID,
                    '1__Text': text
                }, {
                    silent: false,
                    type: 'post'
                });
                return context;
            }

            function traverse(struct, parentContext) {
                if (!_.isObject(struct) || !parentContext) {
                    return;
                }

                // Challenge: need IDs of previous to add children to it
                // Could possibly exhaust ID range if import is large

                var newContext = createContext(parentContext, struct.Text);
                _.each(struct.Children, function(childStruct) {
                    traverse(childStruct, newContext);
                });
            }

            traverse(struct, cluster.contexts.get(rootID));
            cluster.render();
        }
    },

    exportData: function(cluster, rootID, exportType) {
        cluster = cluster || Global.globalCluster.cluster;
        rootID = rootID || cluster.rootID;
        exportType = exportType || 'JSON';

        var exportedData = cluster.contexts.exportTree(rootID, Global.exportAttrs),
            readyData,
            contentType,
            fileExt;

        if (exportType == 'JSON') {
            readyData = JSON.stringify(exportedData, null, 4);
            contentType = 'application/json';
            fileExt = 'json'
        }
        else if (exportType == 'Text') {
            readyData = this.formatTextExport(exportedData, 4);
            contentType = 'text/plain';
            fileExt = 'txt'
        }
        else if (exportType == 'Markdown') {
            readyData = this.formatMarkdownExport(exportedData, 1);
            contentType = 'text/markdown';
            fileExt = 'markdown'
        }
        else if (exportType == 'HTML') {
            var markdownData = this.formatMarkdownExport(exportedData, 1);

            readyData = markdownParser.render(markdownData);
            contentType = 'text/html';
            fileExt = 'html'
        }

        var url = 'data:' + contentType + ';charset=US-ASCII;base64,' + btoa(readyData),
            fileName = 'BreakdownExport_' + Global.getID() + '.' + fileExt;

        sendFile(url, fileName);
    },

    formatTextExport: function(struct, numIndents) {
        var text = '',
            numIndents = numIndents || 4,
            indentStr = ' '.repeat(numIndents);

        function traverse(struct, level) {
            level = level || 0;
            text += indentStr.repeat(level) + (struct.Text || '') + '\n';

            _.each(struct['Children'], function(childStruct) {
                traverse(childStruct, level + 1);
            }, this);
        }
        traverse(struct);

        return text;
    },

    formatMarkdownExport: function(struct, numHeadingLevels) {
        var text = '',
            numIndents = 2,
            indentStr = ' '.repeat(numIndents);

        function traverse(struct, level) {
            level = level || 0;
            var structText = (struct.Text || '');

            if (level < 1 + numHeadingLevels) {
                text += '#'.repeat(level + numHeadingLevels) + ' ' + structText + '\n';
            }
            else {
                text += indentStr.repeat(level - 1 - numHeadingLevels) + '- ' + structText + '\n';
            }

            _.each(struct['Children'], function(childStruct) {
                traverse(childStruct, level + 1);
            }, this);
        }
        traverse(struct);

        return text;
    }
});

module.exports = BreakdownAppView;