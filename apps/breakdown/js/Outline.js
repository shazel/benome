/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
var $ = require('jquery'),
    _ = require('underscore'),
    Backbone = require('backbone'),
    moment = require('moment');

var Global = require('benome/modules/GlobalState')();

// Inherit from general App class
// Encapsulate custom UI and logic to be applied when necessary

function OutlineApp(appID, options) {
	options = options || {};
	this.name = 'Outline';
	this.appID = appID;

	//_.bindAll(this, '');

	// Define set of attributes
	this.initPointAttributes();
}
_.extend(OutlineApp.prototype, {
	postInit: function() {

	},

	initPointAttributes: function() {
		this.pointAttributes = {
	        'Text': {
	            AttrID: 'Text',
	            Label: 'Notes',
	            Type: 'Text',
	            Def: {}
	        },
	        'Timing': {
	            AttrID: 'Timing',
	            Label: 'Timing',
	            Type: 'Interval',
	            Def: {}
	        }
		};

		this.contextAttributes = {
	        'Text': {
	            AttrID: 'Text',
	            Label: 'Text',
	            Type: 'Text',
	            Def: {}
	        }
		};
	},

	getPointAttributeDefs: function() {
		var attributeDefs = [];
		_.each(this.pointAttributes, function(attrDef, attrID) {
			var attrDef = $.extend(true, {}, attrDef);

			if (attrID == 'Timing') {
				attrDef.Value = {
	                'Time': Date.now() / 1000,
	                'Duration': 0
	            }
			}

			attributeDefs.push(attrDef);
		}, this);

		return attributeDefs;
	},

	getContextAttributeDefs: function() {
		var attributeDefs = [];
		_.each(this.contextAttributes, function(attrDef, attrID) {
			var attrDef = $.extend(true, {}, attrDef);
			attributeDefs.push(attrDef);
		}, this);

		return attributeDefs;
	},

	getPointEventHandlers: function() {
		return {};
	},

	getContextEventHandlers: function() {
		return {};
	},

	surfaceRender: function(clusterController, clusterMode, surfaceView, surfaceModeView, options, featuresAlreadyRendered) {
		var context = surfaceView.contextModel;

		options = options || {};
		if (clusterMode == 'Expanded' || clusterMode == 'Exclusive' || clusterMode == 'Compact') {
			var clusterFocusID = clusterController.cluster.focusID,
            	isFocus = surfaceView.contextModel.id == clusterFocusID;

            if (isFocus) {
            	surfaceView.setDisplayMode('Edit');
            }
            else {
            	surfaceView.setDisplayMode('View');
            }
	    }

	    return {
	    	'Background': true
	    }
	}
});

module.exports = OutlineApp;
