/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import Numeric from 'benome/cluster/surfaces/Numeric'
import Interval from 'benome/cluster/surfaces/Interval'
import Frequency from 'benome/cluster/surfaces/Frequency'
import Text from 'benome/cluster/surfaces/Text'
import TextLine from 'benome/cluster/surfaces/TextLine'
import SingleChoice from 'benome/cluster/surfaces/SingleChoice'
import TestSingleChoice from 'benome/cluster/surfaces/TestSingleChoice'
import BooleanSurface from 'benome/cluster/surfaces/Boolean'
import SingleChoiceConstructor from 'benome/cluster/surfaces/SingleChoiceConstructor'

import TestView from 'benome/cluster/surfaces/Test'
import LabelAndName from 'benome/cluster/surfaces/LabelAndName'
import BonusDetails from 'benome/cluster/surfaces/BonusDetails'
import ClusterContainer from 'benome/cluster/surfaces/ClusterContainer'
import SimpleSurfaceView from 'benome/cluster/surfaces/SimpleSurfaceView'

export default {
    'Numeric': Numeric,
    'Interval': Interval,
    'Frequency': Frequency,
    'Text': Text,
    'TextLine': TextLine,
    'SingleChoice': SingleChoice,
    'TestSingleChoice': TestSingleChoice,
    'Boolean': BooleanSurface,
    'SingleChoiceConstructor': SingleChoiceConstructor,

    'TestView': TestView,
    'LabelAndName': LabelAndName,
    'BonusDetails': BonusDetails,
    'ClusterContainer': ClusterContainer,
    'SimpleSurfaceView': SimpleSurfaceView
}

