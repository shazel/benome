/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import $ from 'jquery'
import     _ from 'underscore'
import     Backbone from 'backbone'

var FlexAttributeModeView = Backbone.View.extend({
    tagName: 'div',
    className: 'flex-attribute-mode-view',

    events: {
        //'click': ''
    },

    initialize: function(options) {
        options = options || {};

        this.surfaceView = options.surfaceView;
        this.name = options.name || this.name;
        this.sourceData = options.sourceData;
    },

    render: function(options) {
        options = options || {};
        return this;
    },

    show: function() {
        this.$el.show();
    },

    hide: function() {
        this.$el.hide();
    },

    getValue: function() {
        return null;
    }
});

export default FlexAttributeModeView;