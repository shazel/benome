/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import $ from 'jquery'
import _ from 'underscore'
import Backbone from 'backbone'

import BaseClusterController from 'benome/cluster/BaseClusterController'
import ClusterStructures from 'benome/modules/Cluster/Structures'
import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function GlobalCluster(struct, contextCollection, options) {
    options = options || {};
    struct = struct || {};

    if (options.getSurfaceViewClassFunc) {
        this.getSurfaceViewClassFunc = options.getSurfaceViewClassFunc;
    }

    if (options.surfaceModifierFunc) {
        this.surfaceModifierFunc = options.surfaceModifierFunc;
    }
    
    _.bindAll(this, 'layerReady', 'onInputClusterLongPress', 'onShowPointEdit',
                'onClusterInitialized', 'surfaceModifierFunc', 'getSurfaceViewClassFunc');

    // Setup and render the cluster
    var defaultClusterOptions = {
        hideRoot: false,
        layoutChange: true,
        hideLabels: false,
        labelIDsOnly: false,
        noCompress: false,

        radiusScaleFactor: 0.4,
        scaleFactor: 0.6,
        spaceFactor: 0.7,
        focusAngleArc: 360,
        focusStartAngle: 30,
        childAngleArc: 210,
        maxDepth: null,
        numDetailLevels: 6
    };
    struct.Options = _.extend({}, defaultClusterOptions, struct.Options || {});

    this.on('ShowPointEdit', this.onShowPointEdit);
    this.on('ClusterInitialized', this.onClusterInitialized);

    // Call the base constructor
    BaseClusterController.call(this, struct, contextCollection);

    // FIXME: drop Exclusive down to Expanded until load sort order is fixed
    // The cluster isn't fully ready here yet
    var clusterMode = options.clusterMode == 'Exclusive' ? 'Expanded': options.clusterMode;
    this.setClusterMode(clusterMode || 'Compact', {
        render: false
    });
}

_.extend(GlobalCluster.prototype, BaseClusterController.prototype, {
    onClusterShortPress: function(e, view) {
        var cluster = this.cluster;

        if (cluster.focusID != view.viewID && cluster.config.layoutChange) {
            // Set focus to pressed view
            cluster.setFocus(view.viewID, false);
        }

        if (this.clusterMode == 'Expanded') {
            this.setClusterMode('Compact', {render: true})
        }
        else if (this.clusterMode == 'Compact') {
            this.setClusterMode('Expanded', {render: true})
        }
        else if (this.clusterMode == 'Exclusive') {
            this.unsetExclusive();
        }
    },

    setClusterMode: function(clusterMode, options) {
        options = options || {};

        if (clusterMode == 'Compact') {
            this.setCompact(options);
        }
        else if (clusterMode == 'Expanded') {
            this.setExpanded(options);
        }
        else if (clusterMode == 'Exclusive') {
            this.setExclusive(options);
        }
    },

    onClusterLongPress: function(e, view) {
        if (this.clusterMode == 'Exclusive') {
            this.unsetExclusive();
        }
        else {
            this.setClusterMode('Exclusive', {
                render: true,
                focusID: view.viewID
            });
        }
    },

    onBeforeClusterClicked: function(e, view) {
        Global.trigger('GlobalClusterClicked', e, view);
    },
    
    setCompact: function(options) {
        options = options || {};
        this.clusterMode = 'Compact';
        Global.setLastClusterMode(this.clusterMode);

        var cluster = this.cluster;
        cluster.setRadius(this.getClusterSize());
        cluster.setConfig(_.extend({}, this.clusterConfig, {
            scaleFactor: 0.6,
            spaceFactor: 0.7
        }));

        if (options.render) {
            cluster.render();
        }
    },

    setExpanded: function(options) {
        options = options || {};
        this.clusterMode = 'Expanded';
        Global.setLastClusterMode(this.clusterMode);

        var cluster = this.cluster;
        cluster.setRadius(this.getClusterSize() * 1.8);
        cluster.setConfig(_.extend({}, this.clusterConfig, {
            scaleFactor: 0.29,
            spaceFactor: 0.45
        }));

        if (options.render) {
            cluster.render();
        }
    },

    setExclusive: function(options) {
        options = options || {};
        this.prevClusterMode = this.clusterMode;
        this.clusterMode = 'Exclusive';
        Global.setLastClusterMode(this.clusterMode);

        var cluster = this.cluster,
            globalSize = Global.globalSize(),
            refSize = Math.min(globalSize.width, globalSize.height);

        cluster.setRadius((refSize / 2) * 1.25);
        cluster.setConfig({
            hideChildren: true
        });

        this.prevClusterFocusID = null;
        if (options.focusID) {
            cluster.setFocus(options.focusID);

            if (options.focusID != cluster.lastFocusID) {
                this.prevClusterFocusID = cluster.lastFocusID;
            }
        }

        var focusID = options.focusID || cluster.focusID,
            view = cluster.getView(focusID);

        view.el.setAttribute('BDragSource', '0');
        Global.trigger('SetExclusive', cluster);

        if (options.render) {
            cluster.render();
        }

        if (view.darkColor3) {
            view.$el.css({
                'background': view.darkColor3.toRgbaString()
            });
        }
        else {
            console.log('setExclusive: View has no color yet');
        }

        var viewState = cluster.lastLayoutData.data[focusID];
        Global.trigger('PushContext', view, viewState);
    },

    unsetExclusive: function() {
        var cluster = this.cluster,
            view = cluster.getView(cluster.focusID),
            viewState = cluster.lastLayoutData.data[cluster.focusID];

        if (this.prevClusterFocusID) {
            cluster.setFocus(this.prevClusterFocusID);
        }

        cluster.setConfig({
            hideChildren: true
        });

        view.el.setAttribute('BDragSource', '1');
        view.renderColor(null, true);

        Global.trigger('PopContext', view, viewState);

        if (this.prevClusterMode == 'Expanded') {
            this.setClusterMode('Expanded', {render: true});
        }
        else if (this.prevClusterMode == 'Compact') {
            this.setClusterMode('Compact', {render: true});
        }
    },

    onClusterInitialized: function(controller, cluster) {
        if (!cluster.focusID) {
            cluster.setFocus(this.rootContextID);
        }
        cluster.setFilterLevel(0, {noRender: true});
        cluster.setRadius(this.getClusterSize());
        this.setPosition();
    },

    onShowPointEdit: function(pointID, contextID, changeCallback, initialPos) {
        var view = this.cluster.getView(contextID);

        var clusterDef = ClusterStructures.generatePointAttributeCluster(view.model);
        clusterDef.RenderOptions.initialPos = initialPos;
        clusterDef.Options.radiusScaleFactor = 0.4;

        var color = view.cluster.getColor(view.viewID);
        clusterDef.Options.rootColor = color;
        clusterDef.Origin = view;

        var _this = this;

        this.pushCluster({
            originalView: view,
            clusterDef: clusterDef,
            pointID: pointID,
            point: view.cluster.contexts.points.get(pointID),
            initialPos: initialPos,
            initValues: function(clusterDef, options) {
                var point = options.point,
                    attributes = {},
                    eventHandlers = options.clusterDef && options.clusterDef.EventHandlers;

                if (_.isFunction(eventHandlers.onInitValues)) {
                    attributes = eventHandlers.onInitValues(point, options);
                }

                return _.extend({
                    'Text': point.get('Text', 1),
                    'Timing': {
                        'Time': point.get('Time',1),
                        'Duration': point.get('Duration', 1)
                    }
                }, attributes);
            },
            commit: function(options) {
                var point = options.point,
                    updateAttributes = options.values,
                    namespacedUpdateAttributes,
                    eventHandlers = options.clusterDef && options.clusterDef.EventHandlers;

                if (_.isFunction(eventHandlers.onCommit)) {
                    namespacedUpdateAttributes = eventHandlers.onCommit(updateAttributes, point, options);
                }

                if (point) {
                    Global.trigger('UpdatePoint', _this.cluster.clusterID, point, updateAttributes, namespacedUpdateAttributes);
                }
            },
            complete: changeCallback
        });
    },

    addNode: function(nodeDef, parentContextID, contextCollection) {
        var contextModel = BaseClusterController.prototype.addNode.call(this, nodeDef, parentContextID, contextCollection);

        if (contextModel) {
            contextModel.save({}, {type: 'post'});
        }

        var parentView = this.cluster.getView(parentContextID);
        Global.trigger('ContextCreated', contextModel.id, parentView);
        return contextModel;
    },

    getNextID: function() {
        return Global.nextID();
    },

    getSurfaceViewClassFunc: function(cluster, viewID, viewState, baseView, contextModel) {
        return Global.mapContextType(contextModel.get('Type')) || this.struct.NodeType;
    },

    surfaceModifierFunc: function(cluster, surfaceView, viewID, viewState, baseView, contextModel) {
        if (this.clusterMode == 'Expanded' && !viewState.parentID) {
            surfaceView.setDisplayMode('Edit');
        }
        else if (this.clusterMode == 'Exclusive' && viewState.depth == 1) {
            surfaceView.setDisplayMode('Exclusive');
        }
        else {
            surfaceView.setDisplayMode('View');
        }
    }
});

export default GlobalCluster;