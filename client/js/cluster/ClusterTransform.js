/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import _ from 'underscore'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function ClusterTransform(options) {
    this.bindEvents(options);
}

_.extend(ClusterTransform.prototype, {
    bindEvents: function(options) {
        options = options || {};
        _.bindAll(this, 'modelErrorCallback', 'beforeFocusChanged', 'addPoint', 
                    'updatePoint', 'renameContext', 'reparentContext',
                    'deleteContext', 'deletePoint', 'adjustContext',
                    'updateContext', 'addContext');

        Global.on('BeforeFocusChanged', this.beforeFocusChanged);

        Global.on('AddContext', this.addContext);
        Global.on('RenameContext', this.renameContext);
        Global.on('ReparentContext', this.reparentContext);
        Global.on('DeleteContext', this.deleteContext);
        Global.on('UpdateContext', this.updateContext);
        Global.on('AdjustContext', this.adjustContext);

        Global.on('AddPoint', this.addPoint);
        Global.on('UpdatePoint', this.updatePoint);
        Global.on('DeletePoint', this.deletePoint);

        Global.on('FilterLevelChanged', function(filterLevel) {
            Global.updateLastActivity();
            Global.util.localSet('LastFilterLevel', filterLevel);
        });

        Global.on('AddLinkedContext', function(destViewID, srcViewID, destClusterID, srcClusterID) {
            console.log('AddLinkedContext');
        });
    },

    deleteContext: function(contextID, clusterID, noFocus, successCallback) {
        var cluster = Global.clusters.get(clusterID);
        var context = cluster.contexts.get(contextID),
            parentContextID = context && context.getParentID();

        if (!parentContextID) {
            return;
        }

        Global.setWorking();

        var points = cluster.contexts.getContextPoints(contextID, null);

        var s = _.bind(function(response, textStatus, jqXHR) {
            // Remove links to parent
            var cluster = Global.clusters.get(clusterID);

            cluster.contexts.removeAssoc('down', parentContextID, contextID);
            cluster.contexts.removeAssoc('up', contextID, parentContextID);

            // TODO: Remove links to any children (and children too?)

            // And all points
            _.each(points, function(point) {
                point.destroy();
            });

            if (!noFocus && clusterID) {
                // Shift focus to parent if the deleted context was the focus
                var cluster = Global.clusters.get(clusterID);
                //if (cluster.focusID == contextID) {
                cluster.setFocus(parentContextID, true);
                //}
            }

            if (successCallback) {
                successCallback();
            }
            Global.unsetWorking();

        }, this);

        var context = cluster.contexts.get(contextID);
        context.destroy({ success: s, error: this.modelErrorCallback});
    },

    reparentContext: function(contextID, oldParentID, newParentID, clusterID, callback) {
        if (!contextID || !oldParentID || !newParentID || !clusterID) {
            return;
        }

        if (contextID == newParentID || newParentID == oldParentID) {
            return;
        }

        var cluster = Global.clusters.get(clusterID),
            contexts = cluster.contexts;

        // Ensure that target does not have origin as a parent
        var newParentModel = contexts.get(newParentID),
            tmpParentID = newParentModel.getParentID(),
            isAncestor = false;

        while (tmpParentID) {
            if (tmpParentID == contextID) {
                isAncestor = true;
                break;
            }

            tmpParentID = contexts.get(tmpParentID).getParentID();
        }

        if (isAncestor) {
            return;
        }

        Global.setWorking();

        var s = _.bind(_.after(4, function(response, textStatus, jqXHR) {
            if (callback) {
                callback(contextID);
            }
            Global.unsetWorking();
        }), this);

        var assoc1 = contexts.getAssociation('up', contextID, oldParentID);
        assoc1.destroy({ success: s, error: this.modelErrorCallback});

        var assoc2 = contexts.getAssociation('down', oldParentID, contextID);
        assoc2.destroy({ success: s, error: this.modelErrorCallback});

        var assoc3 = contexts.addAssoc('up', contextID, newParentID);
        assoc3.save({}, {success: s, error: this.modelErrorCallback});

        var assoc4 = contexts.addAssoc('down', newParentID, contextID);
        assoc4.save({}, {success: s, error: this.modelErrorCallback});
    },

    renameContext: function(contextID, clusterID, newLabel) {
        var cluster = Global.clusters.get(clusterID);
        var contextModel = cluster.contexts.get(contextID);
        if (contextModel.isLink()) {
            return;
        }
        Global.setWorking();

        var _this = this;
        var s = function(context, response, callOptions) {
            Global.unsetWorking();
        }

        contextModel.save({'1__Label': newLabel}, {
            success: s,
            error: this.modelErrorCallback
        });
    },

    addContext: function(parentContextID, clusterID, attributes, loadCallback, options) {
        options = options || {};

        var cluster = Global.clusters.get(clusterID);
        if (!cluster) {
            console.log('Invalid cluster ID to addContext(): ' + clusterID);
            return;
        }

        Global.setWorking();

        var associationCollection = cluster.controller ? cluster.controller.contextCollection.associations : cluster.contexts.associations;

        var s = _.bind(function(context, response, callOptions) {
            if (response && response.Error) {
                Global.unsetWorking();
                return;
            }
            var contextID = context.id;

            var saveAssoc = !!Global.localOnly;
            associationCollection.addAssoc('up', contextID, parentContextID, {save: saveAssoc});
            associationCollection.addAssoc('down', parentContextID, contextID, {save: saveAssoc});

            if (options.setFocus) {
                cluster.setFocus(contextID);
            }

            // Have to wait until the associations are added
            cluster.contexts.trigger('add', context);

            Global.unsetWorking();
            if (loadCallback) {
                loadCallback(context, parentContextID, clusterID);
            }
        }, this);

        var saveAttrs = {
            'ParentID': parentContextID,
            'ID': Global.nextID()
        };
        if ('Label' in attributes) {
            this.addNamespacedAttr(saveAttrs, 1, {
                'Label': attributes.Label || ''
            });
        }

        if ('TargetFrequency' in attributes) {
            this.addNamespacedAttr(saveAttrs, 1, {
                'TargetFrequency': attributes.TargetFrequency
            });
        }

        var createOptions = {
            type: 'post',
            silent: true,
            wait: true,
            success: s,
            error: this.modelErrorCallback
        }
        cluster.contexts.create(saveAttrs, createOptions);
    },

    updateContext: function(contextID, updatedAttributes, namespacedAttributes, successCallback, successCallbackOptions, cluster) {
        Global.setWorking();

        var _this = this;
        var s = function(response, textStatus, jqXHR) {
            if (successCallback) {
                successCallback(successCallbackOptions);
            }
            Global.unsetWorking();
        }

        var saveAttrs = {};
        if ('Label' in updatedAttributes) {
            this.addNamespacedAttr(saveAttrs, 1, {
                'Label': updatedAttributes.Label || ''
            });
        }

        if ('TargetFrequency' in updatedAttributes) {
            this.addNamespacedAttr(saveAttrs, 1, {
                'TargetFrequency': updatedAttributes.TargetFrequency
            });
        }

        if ('Text' in updatedAttributes) {
            this.addNamespacedAttr(saveAttrs, 1, {
                'Text': updatedAttributes.Text || ''
            });
        }

        if (namespacedAttributes) {
            _.extend(saveAttrs, namespacedAttributes)
        }

        var cluster = cluster || Global.globalCluster.cluster,
            context = cluster.contexts.get(contextID);
            
        if (context) {
            context.save(saveAttrs, {
                success: s,
                error: this.modelErrorCallback
            });
        }
        else {
            console.log('Could not update context ' + contextID + ', not found');
            Global.unsetWorking();
        }
    },

    adjustContext: function(contextID, adjustDir, clusterID) {
        var cluster = Global.clusters.get(clusterID);
        var contextModel = cluster.contexts.get(contextID);

        Global.setWorking();

        var s = _.bind(function(context, response, callOptions) {
            var newAdjustDelta = 0,
                adjustDir = context.get('adjustDir');

            if (adjustDir) {
                // When there's no server
                var scoreDetails = context.calcContextScore(),
                    currentDelta = context.getNS('AdjustDelta') || 0,
                    recentInterval = scoreDetails['RecentInterval_5'],
                    timeSince = scoreDetails['TimeSince'],
                    timeSinceAdjusted = scoreDetails['TimeSinceAdjusted'];

                if (recentInterval && timeSince) {
                    if (adjustDir == 'forward') {
                        newAdjustDelta = currentDelta + (recentInterval * 0.66);
                    }
                    else {
                        newAdjustDelta = currentDelta - (timeSinceAdjusted * 0.66);
                    }

                    context.set('1__AdjustDelta', newAdjustDelta + 0.1);
                    context.save();
                }
            }
            else {
                newAdjustDelta = context.getNS('AdjustDelta') || 0;
                context.set('1__AdjustDelta', newAdjustDelta + 0.1);
            }

            Global.unsetWorking();
        }, this);

        contextModel.save({'adjustDir': adjustDir}, {success: s, error: this.modelErrorCallback});
    },

    deletePoint: function(clusterID, pointID, parentContextID, successCallback) {
        if (!parentContextID) {
            return;
        }

        Global.setWorking();

        var s = _.bind(function(response, textStatus, jqXHR) {
            if (successCallback) {
                successCallback();
            }
            Global.unsetWorking();
        }, this);

        var cluster = Global.clusters.get(clusterID),
            point = cluster.contexts.points.get(pointID);
        point.destroy({ success: s, error: this.modelErrorCallback});
    },

    addPoint: function(contextID, clusterID, details, loadCallback, options) {
        details = details || {};
        options = options || {};
        var cluster = Global.clusters.get(clusterID);
        var updatedAttributes = details.UpdatedAttributes || {};

        Global.setWorking();

        var s = _.bind(function(point, response, callOptions) {
            if (response && response.Error) {
                Global.unsetWorking();

                if (loadCallback) {
                    loadCallback(false);
                }
                return;
            }
            
            var pointID = point.id,
                context = cluster.contexts.get(contextID),
                contextLabel = context.getNS('Label'),
                color = cluster.getColor(contextID);

            context.set({
                '1__AdjustDelta': 0
            }, {
                silent: true
            });

            Global.trigger('PointAdded', contextID, pointID, contextLabel, color, {
                underCursor: options.feedbackUnderCursor,
                showDelay: 200,
                feedbackRequested: options.showAddFeedback,
                options: options.options,
                point: point,
                cluster: cluster
            });

            if (cluster && options.toParent) {
                if (options.toParent && !cluster) {
                    cluster = Global.clusters.get('Root');
                }

                // If already focused, shift focus to its parent
                if (contextID == cluster.focusID) {
                    cluster.focusParent(contextID);
                }
            }

            if (options.showDetail) {
                Global.trigger('ShowPointDetail', pointID, contextID);
            }

            cluster.contexts.points.add(point);
            Global.unsetWorking();

            if (loadCallback) {
                loadCallback(true, point);
            }
        }, this);

        var beginTime = updatedAttributes.Timing ? updatedAttributes.Timing.Time || null : Date.now() / 1000;

        var createAttrs = {
            'ID': Global.nextID()
        };
        
        this.addNamespacedAttr(createAttrs, 1, {
            // Global/universal attributes
            'ContextID': contextID,
            'Time': beginTime,
            'TimeOffset': Global.getTimeOffset(),

            // Core app attributes    
            'Duration': updatedAttributes.Timing ? parseInt(updatedAttributes.Timing.Duration) || null : null,
            'Text': updatedAttributes.Text || '',
            'Color': details.Color || null
        });

        if ('Open' in updatedAttributes) {
            this.addNamespacedAttr(createAttrs, 1, {
                'Open': updatedAttributes.Open || false
            });
        }

        var onBeforeSave = Global.app.getPointEventHandlers().onBeforeSave;
        if (onBeforeSave) {
            createAttrs = onBeforeSave(contextID, createAttrs)
        }

        var point = cluster.contexts.points.create(createAttrs, {
            type: 'post',
            wait: true,
            success: s,
            error: this.modelErrorCallback
        });
    },

    updatePoint: function(clusterID, point, updatedAttributes, namespacedAttributes, loadCallback, loadCallbackOptions) {
        Global.setWorking();

        var _this = this;
        var s = function(response, textStatus, jqXHR) {
            if (loadCallback) {
                loadCallback(loadCallbackOptions);
            }
            Global.unsetWorking();
        }

        var saveAttrs = {}
        if ('Timing' in updatedAttributes) {
            if ('Time' in updatedAttributes.Timing) {
                this.addNamespacedAttr(saveAttrs, 1, {
                    'Time': updatedAttributes.Timing.Time || null
                });
            }
            if ('Duration' in updatedAttributes.Timing) {
                this.addNamespacedAttr(saveAttrs, 1, {
                    'Duration': parseInt(updatedAttributes.Timing.Duration) || 0
                });
            }
        }
        if ('Open' in updatedAttributes) {
            this.addNamespacedAttr(saveAttrs, 1, {
                'Open': updatedAttributes.Open || false
            });
        }
        if ('Text' in updatedAttributes) {
            this.addNamespacedAttr(saveAttrs, 1, {
                'Text': updatedAttributes.Text || ''
            });
        }

        if (namespacedAttributes) {
            _.extend(saveAttrs, namespacedAttributes)
        }

        point.save(saveAttrs, {
            success: s,
            error: this.modelErrorCallback
        });
    },

    modelErrorCallback: function(model, jqXHR, options) {
        if (jqXHR && jqXHR.responseJSON && jqXHR.responseJSON.Type == 'Authentication Error') {
            Global.trigger('AuthLost');
        }
        else {
            console.log('Model error', model, jqXHR, options);
        }
        
        Global.unsetWorking();
    },

    beforeFocusChanged: function(cluster, focusID) {
        Global.updateLastActivity();

        if (Global.clusters.isGlobal(cluster)) {
            Global.util.localSet('LastFocus', focusID);
        }
    },

    onCreateContext: function(contextID, parentView) {
        Global.updateLastActivity();

        if (Global.FEATURE('ContextModify')) {
            Global.trigger('ShowContextRename', parentView, {
                autoFocus: true,
                autoHide: 5000,
                renameContextID: contextID
            });
        }
    },

    addNamespacedAttr: function(attrBase, namespaceID, newAttrs) {
        namespaceID = namespaceID || 1;
        _.each(newAttrs, function(val, key) {
            attrBase[namespaceID + '__' + key] = val;
        });
    },

    addLinkedContext: function(contextID, linkContextID, clusterID, linkClusterID) {
        Global.updateLastActivity();

        var linkCluster = Global.clusters.get(linkClusterID),
            linkModel = linkCluster.contexts.get(linkContextID),
            label = 'link-' + linkModel.id;

        var data = {
            'Label': label
        }

        Global.setWorking();

        var s = function(response, textStatus, jqXHR) {
            if (response && response.Success) {
                Global.loadData(null, true);
            }
            else {
                Global.unsetWorking();
            }
        }

        Global.ajax.get('/data/context/' + contextID + '/create', data, s);
    },

    addLinkedPoint: function(contextID, linkContextID, clusterID, linkClusterID) {
        var data = {
            'ParentContextID': contextID,
            'LinkContextID': linkContextID
        };

        Global.updateLastActivity();

        var s = _.bind(function(response, textStatus, jqXHR) {
            if (response && response.Success) {
                Global.toggleOverlay(true);

                var cluster = Global.clusters.get(clusterID),
                    linkCluster = Global.clusters.get(linkClusterID);

                // FIXME?
                var contextModel = cluster.contexts.get(contextID);
                contextModel.metaData.set('CurrentScore', 0);

                var linkContextModel = linkCluster.contexts.get(linkContextID);
                linkContextModel.metaData.set('CurrentScore', 0);

                // Refresh current focus
                // TODO: All affected clusters need to be updated
                cluster.render();

                linkCluster.render();
            }
        }, this);

        Global.ajax.jsonGet('/data/context/' + contextID + '/add_point?callback=?', data, s);
    }
});

export default ClusterTransform;