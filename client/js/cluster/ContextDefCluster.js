/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import $ from 'jquery'
import _ from 'underscore'
import Backbone from 'backbone'

import SurfaceView from 'benome/cluster/SurfaceView'
import BaseClusterController from 'benome/cluster/BaseClusterController'
import AttributeSurfaces from 'benome/cluster/AttributeSurfaces'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function ContextDefCluster(struct, contextCollection, options) {
    struct = struct || {};

    _.bindAll(this, 'surfaceModifierFunc', 'getSurfaceViewClassFunc');

    // Setup and render the cluster
    var defaultClusterOptions = {
        hideRoot: false,
        layoutChange: false,
        hideLabels: false,
        labelIDsOnly: false,
        noCompress: true,

        radiusScaleFactor: 0.4,
        scaleFactor: !Global.isTablet && Global.isMobile ? 0.6 : 0.6,
        spaceFactor: 0.7,
        focusAngleArc: 360,
        focusStartAngle: 30,
        childAngleArc: 210,
        maxDepth: null,
        numDetailLevels: 6
    };
    struct.Options = _.extend({}, defaultClusterOptions, struct.Options || {});

    // Call the base constructor
    BaseClusterController.call(this, struct, contextCollection);

    var cluster = this.cluster;

    this.contextCollection.each(function(model) {
        var viewID = model.id;
        if (model.get('Type') == 'ContextDefRoot') {
            // Enable drop only on the root (better to disable on all leaves)
            cluster.setViewState(viewID, {
                dropDisabled: false
            });
        }
    }, this);

    cluster.setFocus(this.rootContextID);
    cluster.setFilterLevel(0, {noRender: true});
    cluster.setRadius(this.getClusterSize());
    this.setPosition();
}

_.extend(ContextDefCluster.prototype, BaseClusterController.prototype, {
    onClusterShortPress: function(e, view) {
        
    },

    onClusterLongPress: function(e, view) {
        //this.trigger('LongPress', e, view);
        BaseClusterController.prototype.onClusterLongPress.call(this, e, view);
    },

    onBeforeClusterClicked: function(e, view) {
    },

    getSurfaceViewClassFunc: function(cluster, viewID, viewState, baseView, contextModel) {
        var attributeType = contextModel.get('Type'),
            attributeType = 'SimpleSurfaceView',
            SurfaceViewCls = AttributeSurfaces[attributeType] || SurfaceView;

        return SurfaceViewCls;
    },

    surfaceModifierFunc: function(cluster, surfaceView, viewID, viewState, baseView, contextModel) {
        surfaceView.setDisplayMode('View');
    },

    getValues: function() {
        var values = {};
        this.contextCollection.each(function(model) {
            var viewID = model.id;
            var bonusDef = _.pick(model.attributes, 'Label', 'GlobalMultiplier', 'MultiplierValue', 'Text');
            var bonusID = model.get('OriginalContextID') || model.get('AttrID');
            values[bonusID] = bonusDef;
        }, this);

        return values;
    }
});

export default ContextDefCluster;