import $ from 'jquery'
import _ from 'underscore'
import Backbone from 'backbone'
import moment from 'moment'

import SurfaceView from 'benome/cluster/SurfaceView'
import AttributeModeView from 'benome/cluster/AttributeModeView'

function updateFontSize($el, text, scale, baselineChars, maxChars, width, height) {
    var baselineChars = baselineChars || 30,
        maxChars = maxChars || 60,
        text = text || $el.text() || $el.val(),
        scale = scale || 1.1,
        width = width || $el.width(),
        height = height || $el.height(),
        fitChars = Math.min(maxChars, Math.max(baselineChars, text.length)),
        fontSize = Math.sqrt((width * height) / fitChars) * scale;

    $el.css('font-size', fontSize + 'px')
}

var FluidText_View = AttributeModeView.extend({
    tagName: 'div',
    className: 'attribute-fluidtext-mode-view',

    events: {
        //'click': ''
    },

    name: 'View',

    initialize: function(options) {
        AttributeModeView.prototype.initialize.call(this, options);
    },

    render: function(options) {
        options = options || {};
        AttributeModeView.prototype.render.call(this, options);

        var viewState = options.viewState,
            size = (viewState.radius * 2) / 1.414,
            text = this.surfaceView.getValue();

        this.$el.html(text);
        updateFontSize(this.$el, text, 0.95, 40, 140, size, size);
        return this;
    }
});

var FluidText_Edit = AttributeModeView.extend({
    tagName: 'div',
    className: 'attribute-fluidtext-mode-edit',

    events: {
        //'click': ''
    },

    name: 'Edit',

    initialize: function(options) {
        AttributeModeView.prototype.initialize.call(this, options);
        _.bindAll(this, 'textChanged', 'textInputted');

        this.$textArea = $('<textarea>')
                .attr('maxlength', 140)
                .attr('spellcheck', 'false')
                .addClass('attribute-fluidtext-textarea')
                .appendTo(this.$el);

        this.$textArea
            .val(this.getValue())
            .on('keydown', this.textInputted)
            .on('input', this.textInputted)
            .on('propertychange', this.textInputted);
            //.on('change', this.textChanged);
    },

    textInputted: function(e) {
        updateFontSize(this.$textArea, null, 0.95, 40, 140);
        this.textChanged(e);
    },

    textChanged: function(e) {
        var newText = this.$textArea.val();
        if (this.surfaceView.contextModel.getNS('Text') != newText) {
            this.surfaceView.valueChanged(newText);
        }
    },

    render: function(options) {
        AttributeModeView.prototype.render.call(this, options);

        var viewState = options.viewState,
            size = (viewState.radius * 2) / 1.414,
            text = this.surfaceView.getValue();

        this.$textArea.val(text);
        updateFontSize(this.$textArea, text, 0.95, 40, 140, size, size);

        var _this = this;
        _.delay(function() {
            _this.$textArea.focus();
        }, 200);
        
        return this;
    },

    getValue: function() {
        return this.$textArea.val();
    },

    focusText: function() {
        this.$textArea.focus();
    },

    enableTextInput: function() {
        this.$textArea.prop('disabled', false);
    },

    disableTextInput: function() {
        this.$textArea.prop('disabled', true);
    }
});

var FluidText = SurfaceView.extend({
    className: 'attribute-view-fluidtext',

    modeClasses: {
        'View': FluidText_View,
        'Edit': FluidText_Edit
    },

    initialize: function(options) {
        SurfaceView.prototype.initialize.call(this, options);
    },

    getValue: function() {
        if (_.isString(this.newValue)) {
            return this.newValue;
        }
        else {
            return this.contextModel.getNS('Text') || '';
        }
    },

    focusText: function(options) {
        options = options || {};
        if (this.displayMode == 'Edit') {
            var modeView = this.modeView[this.displayMode];
            if (modeView) {
                if (!options.noEnable) {
                    modeView.enableTextInput();
                }
                modeView.focusText();
            }
        }
    },

    enableTextInput: function() {
        if (this.displayMode == 'Edit') {
            var modeView = this.modeView[this.displayMode];
            if (modeView) {
                modeView.enableTextInput();
            }
        }
    },

    disableTextInput: function() {
        if (this.displayMode == 'Edit') {
            var modeView = this.modeView[this.displayMode];
            if (modeView) {
                modeView.disableTextInput();
            }
        }
    }
});

export default FluidText;