/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import _ from 'underscore'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

import KeyboardHandler from 'benome/modules/Keyboard'

function ClusterKeyboardHandler(transformHandler, options) {
    this.transformHandler = transformHandler;
    this.keyboardHandler = new KeyboardHandler();
    this.bindEvents(options);
}

_.extend(ClusterKeyboardHandler.prototype, {
    setCluster: function(cluster) {
        this.keyboardHandler.setCluster(cluster);
        this.cluster = cluster;
    },

    bindEvents: function(options) {
        options = options || {};

        var transformHandler = this.transformHandler;

        this.keyboardHandler.on('CreateContext', function(keyboardHandler, cluster) {
            transformHandler.addContext(cluster.focusID, cluster.clusterID, {}, null, {
                setFocus: true
            });
        });

        this.keyboardHandler.on('CreateSiblingContext', function(keyboardHandler, cluster) {
            var context = cluster.contexts.get(cluster.focusID),
                parentContext = context.getParent(),
                parentContextID = parentContext ? parentContext.id : cluster.focusID;

            transformHandler.addContext(parentContextID, cluster.clusterID, {}, null, {
                setFocus: true
            });
        });

        this.keyboardHandler.on('DeleteContext', function(keyboardHandler, cluster) {
            transformHandler.deleteContext(cluster.focusID, cluster.clusterID);
        });

        this.keyboardHandler.on('AddPoint', function(keyboardHandler, cluster) {
            if (!cluster.contexts.get(cluster.focusID).isLeaf()) {
                return;
            }
            transformHandler.addPoint(cluster.focusID, cluster.clusterID, {
                UpdatedAttributes: {
                    Timing: {
                        Time: Date.now() / 1000,
                        Duration: 0
                    }
                }
            }, null, {
                showHistory: false,
                showAddFeedback: true,
                feedbackUnderCursor: false,
                toParent: false,
                showDetail: false
            });
        });

        if (Global.FEATURE('DetailLevels')) {
            this.keyboardHandler.on('NarrowFilter', function(keyboardHandler, cluster) {
                cluster.setFilterLevel(1, { relative: true, noRender: true});
                cluster.debounceRender();
            });

            this.keyboardHandler.on('NarrowestFilter', function(keyboardHandler, cluster) {
                cluster.setFilterLevel(cluster.config.numDetailLevels, { relative: false, noRender: true});
                cluster.debounceRender();
            });

            this.keyboardHandler.on('WidenFilter', function(keyboardHandler, cluster) {
                cluster.setFilterLevel(-1, { relative: true, noRender: true});
                cluster.debounceRender();
            });

            this.keyboardHandler.on('WidestFilter', function(keyboardHandler, cluster) {
                cluster.setFilterLevel(0, { relative: false, noRender: true});
                cluster.debounceRender();
            });
        }

        this.keyboardHandler.on('ToggleViewMode', function(keyboardHandler, cluster) {
            var clusterController = cluster.controller;
            if (!_.isFunction(clusterController.setClusterMode)) {
                return;
            }
            var clusterMode = clusterController.clusterMode;
            if (clusterMode == 'Compact') {
                clusterController.setExpanded({render: true});
            }
            else if (clusterMode == 'Expanded') {
                clusterController.setCompact({render: true});
            }
        });
    }
});

export default ClusterKeyboardHandler;