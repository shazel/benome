/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import Backbone from 'backbone'
import _ from 'underscore'
import Hammer from 'hammerjs'
Backbone.$ = $;

// -------------

import _Global from '../modules/GlobalState'
var Global = _Global()

var LockView = Backbone.View.extend({
    tagName: 'div',
    className: 'lock-view',

    events: {
    },

    initialize: function(options) {
        _.bindAll(this, 'onClick');
        this.setState(!!options.locked);

        var mc = new Hammer(this.el);
        mc.on('tap', this.onClick);
    },

    setLocked: function() {
        this.$el.text('🔒')
        Global.util.localSet('LockState', 'on')
        Global.trigger('LockOn')
    },

    setUnlocked: function() {
        this.$el.text('🔓')
        Global.util.localSet('LockState', 'off')
        Global.trigger('LockOff')
    },

    setState: function(locked) {
        this.lockOn = !!locked
        if (this.lockOn) {
            this.setLocked()
        }
        else {
            this.setUnlocked()
        }
    },

    onClick: function(e) {
        this.setState(!this.lockOn)
    },

    render: function() {
        return this;
    },

    hide: function() {
        this.visible = false;
        this.$el.hide();
    },

    show: function() {
        this.visible = true;
        this.$el.show();
    }
});
_.extend(LockView.prototype, Backbone.Events)

export default LockView