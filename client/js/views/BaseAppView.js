/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery';
import _ from 'underscore'

import _jQueryMouseWheel from 'benome/lib/jquery.mousewheel'
const jQueryMouseWheel = _jQueryMouseWheel($);
import Backbone from 'backbone'
window.Backbone = Backbone;
window._ = _;
window.$ = window.jQuery = $;

import BackboneDualStorage from 'benome/lib/backbone.dualstorage.amd'

// -------------
import ContextStack from 'benome/ContextStack'
import _Global from 'benome/modules/GlobalState'
var Global = _Global()

import Ajax from 'benome/modules/Util/Ajax'
import Auth from 'benome/modules/Util/Auth'
import DragDrop from 'benome/modules/DragDrop'
import Intervals from 'benome/Intervals'

import LoginView from 'benome/views/LoginView'
import AdminView from 'benome/modules/Views/AdminView'
import GetHelpView from 'benome/views/GetHelpView'
import LockView from 'benome/views/GetLockView'
import GetAdminView from 'benome/views/GetAdminView'
import HelpView from 'benome/views/HelpView'
import PressIndicator from 'benome/views/PressIndicator'

import DataLoad_Remote from 'benome/modules/Data/DataLoad_Remote'
import DataLoad_LocalOnly from 'benome/modules/Data/DataLoad_LocalOnly'

// -------------

var BaseAppView = Backbone.View.extend({
    tagName: 'div',
    className: 'benome',

    events: {
    },

    idleThreshold: 2 * 60 * 1000, // 2 minutes
    appTimerInterval: 5 * 1000,

    defaultBackgroundColor: '#222',

    isMobile: ('ontouchstart' in document.documentElement),
    isAndroid: (/android/i.test(navigator.userAgent)),
    isApple: (/iphone|ipod|ipad/i.test(navigator.userAgent)),
    isMac: (/Macintosh/.test(navigator.userAgent)),
    isTablet: (/ipad/i.test(navigator.userAgent) || ((/android/i.test(navigator.userAgent)) && !(/mobile/i.test(navigator.userAgent)))),

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'handleResize', 'mouseWheel', 'appTimer', 'onUserAuthenticated',
                        'onDataInitialized', 'onSetWorking', 'onUnsetWorking',
                        'showAdmin', 'showHelp', 'logoutUser', 'changePassword');

        var _this = this;

        this.controller = options.controller;
        this.options = options;
        this.$container = this.$el;

        this.userID = options.userID

        this.localOnly = !!options.localOnly;
        this.idleWait = options.idleWait === false ? false : true;
        this.idleThreshold = options.idleThreshold || this.idleThreshold;

        this.instanceID = options.instanceID || ('' + parseInt(Math.random() * 100000));
        this.el.setAttribute('BDropTarget', '1');
        this.el.setAttribute('DropHighlightDisabled', '1');
        this.$el
            .data('ViewRef', this)
            .attr('id', this.instanceID)
            .css({
                'width': '100%',
                'height': '100%'
            });

        if (this.isMobile) {
            $('body').on({
                focus: _.bind(function() {
                    this.resizeDisabled = true;
                }, this),
                blur: _.bind(function() {
                    this.resizeDisabled = false;
                }, this)
            }, 'textarea, input[type=text]');
        }

        if (_.isFunction(options.hacks)) {
            options.hacks();
        }

        _.each(options.listeners, function(func, eventName) {
            this.on(eventName, func);
        }, this);


        // Delay resize until there is a pause
        this.handleResize = _.debounce(this.handleResize, 400);
        $(window).bind('resize', this.handleResize);

        this.visualQuality = _.isNumber(options.visualQuality) ? options.visualQuality : (this.isMobile ? 0.0 : 1.0)

        var ajax = new Ajax({
                sessionID: this.sessionID,
                host: this.containerHost
            });

        Global.init({
            app: this,
            instanceID: this.instanceID,
            $el: this.$container,
            sessionID: null,
            ajax: ajax,
            auth: new Auth({
                ajax: ajax
            }),
            DD: new DragDrop(this.$el, {
                dragThreshold: this.dragThreshold
            }),
            lockEnabled: options.lockEnabled,
            lockStateOn: options.lockStateOn,
            adminEnabled: options.adminEnabled !== false ? true : false,
            helpEnabled: options.helpEnabled !== false ? true : false,
            Intervals: new Intervals(),
            bgColor: options.bgColor || this.defaultBackgroundColor,
            timeOffset: options.timeOffset || 0,
            dragThreshold: 11,
            tapThreshold: 8,
            pressThreshold: 10,
            localOnly: this.localOnly,
            visualQuality: this.visualQuality,
            idleMax: 180 * 1000,

            darkText: options.darkText || '#444',
            lightText: options.lightText || '#aaa'
        }, options.features);

        this.setBackground(options.bgColor, this.visualQuality);
        this.initUI(options);

        window.E = this;
        _.delay(this.appTimer, this.appTimerInterval);

        this.loginView.on('AuthenticateCredentials', function(username, password, errorCallback) {
            _this.authenticateCredentials(username, password, errorCallback);
        });

        Global.auth.on('UserAuthenticated', this.onUserAuthenticated);
        Global.auth.on('UserNotAuthenticated', function() {
            _this.render();
        });

        Global.on('UserLoggedOut', function(contextID, userID) {
            Global.auth.isAuthenticated = false;
            window.location = window.location;
        });

        Global.auth.on('UserLoggedIn', function(contextID, userID) {
            console.log('UserLoggedIn');
        });

        Global.auth.on('ServerOffline', function() {

        });

        this.contextStack = new ContextStack({
            $container: this.$container
        });

        this.$el.appendTo(options.container)
        this.postAttach();
        this.load();
    },

    postAttach: function() {
        this.updateFontSize();
    },

    load: function() {
        if (this.localOnly) {
            console.log('Local Only. InstanceID is ' + this.instanceID);
            this.loadData('localonly');
        }
        else {
            Global.auth.checkOnlineState();
        }
    },

    getPointAttributeDefs: function() {
        var attributeDefs = [];
        _.each(this.pointAttributes, function(attrDef, attrID) {
            var attrDef = $.extend(true, {}, attrDef);

            if (attrID == 'Timing') {
                attrDef.Value = {
                    'Time': Date.now() / 1000,
                    'Duration': 0
                }
            }

            attributeDefs.push(attrDef);
        }, this);

        return attributeDefs;
    },

    getContextAttributeDefs: function() {
        var attributeDefs = [];
        _.each(this.contextAttributes, function(attrDef, attrID) {
            var attrDef = $.extend(true, {}, attrDef);
            attributeDefs.push(attrDef);
        }, this);

        return attributeDefs;
    },

    getPointEventHandlers: function() {
        return {};
    },

    getContextEventHandlers: function() {
        return {};
    },

    getContextAttributes: function(contextModel, clusterDef) {
        _.extend(clusterDef.EventHandlers, this.getContextEventHandlers(contextModel));
        return this.getContextAttributeDefs(contextModel);
    },

    getPointAttributes: function(clusterDef) {
        _.extend(clusterDef.EventHandlers, this.getPointEventHandlers());
        return this.getPointAttributeDefs();
    },

    initUI: function(options) {
        options = options || {};

        Global.on('ShowAdmin', this.showAdmin);
        Global.on('ShowHelp', this.showHelp);

        this.$overlay = $('<div>')
                                .addClass('overlay')
                                .addClass('overlay-backing')
                                .appendTo(this.$el);

        this.$overlay2 = $('<div>')
                                .addClass('overlay')
                                .addClass('overlay-backing2')
                                .appendTo(this.$el);

        this.$workingOverlay = $('<div>')
                                    .html('<div class="text"></div>')
                                    .addClass('overlay')
                                    .addClass('working-overlay')
                                    .appendTo(this.$el);

        this.on('SetWorking', this.onSetWorking);
        this.on('UnsetWorking', this.onUnsetWorking);

        this.loginView = new LoginView();
        this.loginView.render().$el.appendTo(this.$el);

        this.adminView = new AdminView({
            $overlay: this.$overlay,
            localOnly: this.localOnly
        });
        this.adminView.on('LogoutUser', this.logoutUser);
        this.adminView.on('SwitchApp', this.switchApp);
        this.adminView.on('ChangePassword', this.changePassword);
        Global.adminView = this.adminView;
        this.adminView.render().$el.appendTo(this.$el);

        /*this.helpView = new HelpView({
            $overlay: this.$overlay,
            helpUrl: this.options.helpUrl
        });
        Global.helpView = this.helpView;
        this.helpView.render().$el.appendTo(this.$el);

        this.getHelpView = new GetHelpView({});
        this.getHelpView.render().$el.appendTo(this.$el);
        if (options.helpEnabled === false) {
            this.getHelpView.hide();
        }
        var _this = this;
        this.getHelpView.on('Click', function() {
            Global.trigger('ShowHelp');
        });*/

        this.lockView = new LockView({
            locked: Global.util.localGet('LockState') ? Global.util.localGet('LockState') == 'on' : options.lockEnabled
        });
        if (options.lockEnabled !== true) {
            this.lockView.hide();
        }
        this.lockView.render().$el.appendTo(this.$el);

        if (Global.FEATURE('Admin')) {
            this.getAdminView = new GetAdminView({});
            if (options.adminEnabled !== true) {
                this.getAdminView.hide();
            }
            this.getAdminView.render().$el.appendTo(this.$el);
            var _this = this;
            this.getAdminView.on('Click', function() {
                Global.trigger('ShowAdmin');
            });
        }

        this.pressIndicator = new PressIndicator();

        // Disable scrolling for multi-touch mouse. Need a better way to detect it.
        if (!this.isMac && this.options.mouseWheelEnabled !== false) {
            this.$el.mousewheel(this.mouseWheel);
        }
    },

    showAdmin: function() {
        Global.updateLastActivity();

        if (this.adminView && Global.FEATURE('Admin')) {
            this.adminView.show();
        }
    },

    showHelp: function() {
        Global.updateLastActivity();

        if (this.helpView) {
            this.helpView.show();
        }
    },

    switchApp: function(appName, successCallback) {
        appName = appName || 'Functional'
        var data = {
            'App': appName
        };
        Global.setWorking();

        var s = _.bind(function(response, textStatus, jqXHR) {
            Global.unsetWorking();

            if (response && response.Success) {
                if (successCallback) {
                    successCallback();
                }
                else {
                    window.location = '/'
                }
            }
        }, this);

        Global.ajax.jsonPost('/user/switch_app', data, s);
    },

    logoutUser: function(successCallback) {
        var data = {};
        Global.setWorking();

        var s = _.bind(function(response, textStatus, jqXHR) {
            Global.unsetWorking();

            if (response && response.Success) {
                Global.trigger('UserLoggedOut');

                if (successCallback) {
                    successCallback();
                }
            }
        }, this);


        Global.ajax.get('/user/logout', data, s);
    },

    changePassword: function(oldPassword, newPassword, successCallback) {
        var data = {
            'OldPassword': oldPassword,
            'NewPassword': newPassword
        }

        Global.setWorking();

        var s = _.bind(function(response, textStatus, jqXHR) {
            Global.unsetWorking();

            if (response && response.Success) {
                if (successCallback) {
                    successCallback();
                }
            }
        }, this);

        Global.ajax.post('/user/change_password', data, s);
    },

    overlayVisible: function() {
        return this.$overlay.css('display') != 'none';
    },

    onSetWorking: function(text) {
        Global.updateLastActivity();

        if (this.localOnly || !this.$workingOverlay) {
            return;
        }

        //console.log('setWorking', text);

        text = text || 'Working...'
        $('.text', this.$workingOverlay).text(text);
        
        var randNum = Math.round(Math.random() * 1000000);
        this.lastWorkingRandNum = randNum;

        var _this = this;
        this.workingOverlayTimer = setTimeout(function() {
            if (_this.lastWorkingRandNum == randNum) {
                _this.$workingOverlay.show();
            }
        }, 200);
    },

    onUnsetWorking: function() {
        if (!this.$workingOverlay) {
            return;
        }
        
        this.lastWorkingRandNum = null;
        clearTimeout(this.workingOverlayTimer);
        this.$workingOverlay.hide();
    },

    updateFontSize: function() {
        var scaleFactor = this.isMobile ? 30 : 40;
        this.fontSize = Math.max(10, ((this.$el.width() + this.$el.height()) / 2) / scaleFactor);
        this.$el.css('font-size', this.fontSize + 'px');
        Global.fontSize = this.fontSize;
    },

    onUserAuthenticated: function(userData, alreadyAuthenticated) {
        if (!alreadyAuthenticated) {
            window.location = window.location;
            return;
        }

        this.render();

        // Server is available and user is authenticated
        var rootContextID = userData.ContextID
        
        Global.graphData = userData.GraphData || {};
        Global.setFeatures({}); //userData.Features);

        if (!rootContextID) {
            console.log('No rootContextID returned from server');
            return;
        }

        Global.loadBeginTime = Date.now();
        console.log('Remote data is accessible');
        this.loadData('remote', rootContextID);
    },

    loadData: function(loadType, rootContextID) {
        if (loadType == 'remote') {
            this.dataLoader = new DataLoad_Remote({
                ajax: Global.ajax,
                instanceID: Global.instanceID,
            });
            Global.dataHandler = this.dataLoader;
            this.dataLoader.on('LoadFinished', this.onDataInitialized);
            this.dataLoader.initData(rootContextID);
        }
        else if (loadType == 'localonly') {
            this.dataLoader = new DataLoad_LocalOnly({
                instanceID: Global.instanceID,
            });
            Global.dataHandler = this.dataLoader;
            this.dataLoader.on('LoadFinished', this.onDataInitialized);
            this.dataLoader.initData();
            this.render();
        }
    },

    onDataInitialized: function(globalCollection, userCollection) {
        this.globalCollection = globalCollection;
        this.userCollection = userCollection;
        this.trigger('DataInitialized', globalCollection, userCollection);

        Global.unsetWorking();
        
        // Defer so a reference to this app view can be attached to the
        // listener's context before the event is triggered.
        var _this = this;
        _.defer(function() {
            _this.trigger('Initialized');
        });
    },

    setBackground: function(bgType, visualQuality) {
        var bgColor = '#000';

        if (bgType == 'dark') {
            bgColor = '#000';
        }
        else if (bgType == 'light') {
            bgColor = '#fff';
        }

        if (visualQuality >= 0.7 && this.options.backgroundEnabled !== false) {
            if (bgType == 'dark') {
                this.$el.addClass('benome-container-bg-dark');
            }
            else if (bgType == 'light') {
                this.$el.addClass('benome-container-bg-light');
            }
        }

        this.bgColor = bgColor;
        this.$el.css({
            'background-color': bgColor
        });
    },

    appTimer: function() {
        var idleTime = Date.now() - Global.lastActivity;
        if (!this.idleWait || idleTime > this.idleThreshold) {
            this.trigger('UserIdle');
        }
        this.trigger('AppTick');
        _.delay(this.appTimer, this.appTimerInterval);
    },

    authenticateCredentials: function(username, password, errorCallback) {
        if (username && password) {
            username = username || _.last(_.compact(window.location.pathname.split('/'))),
            password = password || username;
            Global.auth.loginUser(username, password, errorCallback);
        }
    },

    mouseWheel: function(e, delta, deltaX, deltaY) {
        Global.updateLastActivity();
        this.trigger('MouseWheel', e, delta, deltaX, deltaY);
        e.preventDefault();
        return false;
    },

    handleResize: function() {
        if (this.resizeDisabled) {
            return;
        }
        //this.toggleOverlay(false);

        this.render();
    },

    renderIcons: function(options) {
        var globalSize = Global.globalSize(),
            d = (globalSize.height + globalSize.width) / 2;

        var helpLeft = (d * 0.01),
            helpSize = (d * 0.075),
            helpTop = (d * 0.01);

        if (this.getHelpView) {
            this.getHelpView.$el.css({
                'left': helpLeft + 'px',
                'top':  helpTop + 'px',
                'width': helpSize + 'px',
                'height': (d * 0.075) + 'px',
                'font-size': (d * 0.075 * 0.9) + 'px'
            });

            if (Global.helpEnabled !== false) {
                this.getHelpView.show()
            }
        }

        if (this.lockView) {
            this.lockView.$el.css({
                'left': helpLeft + 'px',
                'top':  helpTop + 'px',
                'width': helpSize + 'px',
                'height': (d * 0.075) + 'px',
                'font-size': (d * 0.075 * 0.4) + 'px',
                'line-height': (d * 0.075 * 0.9) + 'px'
            });

            if (Global.lockEnabled === true) {
                this.lockView.show()    
            }
        }

        if (this.getAdminView) {
            this.getAdminView.$el.css({
                'left': helpLeft + 'px',
                'top': helpTop + helpSize + (d * 0.01) + 'px',
                'width': (d * 0.075) + 'px',
                'font-size': (d * 0.075 * 0.4) + 'px',
                'line-height': (d * 0.075 * 0.9) + 'px'
            });

            if (Global.adminEnabled !== false) {
                this.getAdminView.show()    
            }
        }
    },

    render: function(options) {
        options = options || {};

        this.updateFontSize();
        this.renderIcons(options);

        var globalSize = Global.globalSize(),
            d = (globalSize.height + globalSize.width) / 2;

        if (Global.auth.isAuthenticated || this.localOnly) {
            this.loginView.$el.hide();

            this.renderAuthenticated(options, globalSize, d);
        }
        else {
            var loginSize = d * 0.4;

            this.loginView.$el
                .show()
                .css({
                    'left': ((globalSize.width - loginSize) / 2) + 'px',
                    'top': ((globalSize.height - loginSize) / 2) + 'px',
                    'width': loginSize + 'px',
                    'height': loginSize + 'px'
                });

            this.renderUnauthenticated(options, globalSize, d);
        }

        return this;
    },

    renderAuthenticated: function(options, globalSize, d) {},
    renderUnauthenticated: function(options, globalSize, d) {}
});

_.extend(BaseAppView.prototype, Backbone.Events);

export default BaseAppView;