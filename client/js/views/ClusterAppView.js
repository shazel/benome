/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'

// -------------
import BaseAppView from 'benome/views/BaseAppView'
import ClusterKeyboardHandler from 'benome/cluster/ClusterKeyboardHandler'
import ClusterTransformHandler from 'benome/cluster/ClusterTransform'
import ClusterStructures from 'benome/modules/Cluster/Structures'
import DetailSpectrum from 'benome/views/DetailSpectrum'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

// -------------

var ClusterAppView = BaseAppView.extend({
    tagName: 'div',
    className: 'benome',

    events: {
    },

    hideLabels: false,
    labelIDsOnly: false,

    lastClusterRefresh: Date.now(),
    containerHost: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/app',

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'dropHandler', 'onDataInitialized', 'onUserIdle', 'onAppTick',
                    'onMouseWheel');

        BaseAppView.prototype.initialize.call(this, options);

        var _this = this;
        this.options.globalClusterOptions = this.options.globalClusterOptions || {};

        this.hideLabels = options.hideLabels !== null ? options.hideLabels : this.hideLabels;
        this.labelIDsOnly = options.labelIDsOnly !== null ? options.labelIDsOnly : this.labelIDsOnly;
        this.localOnly = !!options.localOnly;
        this.clusterOnly = !!options.clusterOnly;
        this.clusterFilterShift = !!options.clusterFilterShift;

        if (_.isNumber(options.clusterRefreshInterval)) {
            this.clusterRefreshInterval = options.clusterRefreshInterval;
        }

        this.transformHandler = new ClusterTransformHandler();
        this.keyboardHandler = new ClusterKeyboardHandler(this.transformHandler);

        this.on('GlobalClusterRendered', function(globalCluster) {
            Global.unsetWorking();
            //console.log(Date.now() - window.jsBeginTime);
            _this.keyboardHandler.setCluster(globalCluster.cluster);
        });

        if (this.globalCluster) {
            Global.unsetWorking();
            this.keyboardHandler.setCluster(this.globalCluster.cluster);
        }
    },

    postAttach: function() {
        BaseAppView.prototype.postAttach.call(this);

        if (Global.FEATURE('DetailLevels')) {
            this.detailSpectrum = new DetailSpectrum({
                numDetailLevels: 6
            });
            this.detailSpectrum.render().$el.appendTo(this.$container);
        }
    },

    initUI: function(options) {
        options = options || {};
        BaseAppView.prototype.initUI.call(this, options);

        this.on('UserIdle', this.onUserIdle);
        this.on('AppTick', this.onAppTick);
        this.on('MouseWheel', this.onMouseWheel);
    },

    onDataInitialized: function(globalCollection, userCollection) {
        BaseAppView.prototype.onDataInitialized.call(this, globalCollection, userCollection);

        var renderBeginTime = Date.now();

        this.defaultGlobalClusterOptions = {
            dropDisabled: false,
            noCompress: false,
            newContextBumpTime: 20,
            setBackgroundFilterLevel: this.options.setBackgroundFilterLevel,
            radiusScaleFactor: this.isMobile ? 0.50 : 0.35,
            fontFraction: this.isMobile ? 0.27 : 0.22
        }

        var globalClusterOptions = _.extend({}, this.defaultGlobalClusterOptions, this.options.globalClusterOptions);
        var globalSize = Global.globalSize();
        var globalClusterDef = {
            Type: 'Cluster',
            ClusterType: this.options.clusterType || 'Global',
            ConstructType: null,
            ConstructDef: {
                '1__Label': ''
            },
            Container: this.$container,
            Name: '',
            NodeType: this.options.surfaceViewClass,
            Render: false, // Render happens after load finishes
            RenderOptions: {
                initialPos: {
                    x: globalSize.width / 2,
                    y: globalSize.height / 2
                },
                noAnimate: true
            },
            Options: globalClusterOptions,
            EventHandlers: {},
            Nodes: null
        };

        var globalCluster = ClusterStructures.renderStructure(globalClusterDef, userCollection, this.options.clusterControllerOptions);
        this.globalCluster = globalCluster;
        Global.globalCluster = globalCluster;

        globalCluster.cluster.updateScores();

        _.each(this.options.clusterEvents, function(func, eventName) {
            this.globalCluster.cluster.on(eventName, func);
        }, this);

        this.globalCollection.clusterController = globalCluster;
        this.userCollection.clusterController = globalCluster;

        globalCluster.render(globalClusterDef.RenderOptions);

        console.log('Render', Date.now() - renderBeginTime);
        console.log('Load', Date.now() - Global.loadBeginTime);
        Global.trigger('GlobalClusterRendered', globalCluster);
        this.trigger('GlobalClusterRendered', globalCluster);
    },

    onUserIdle: function() {
        if (this.clusterFilterShift && this.globalCluster) {
            var cluster = this.globalCluster.cluster;

            console.log('Update scores');
            cluster.updateScores();

            console.log('Bump filter level');
            cluster.setFilterLevel(1, {
                relative: true,
                animateDuration: 2000,
                hideDuration: 1200,
                forceRender: true
            });
            Global.updateLastActivity();
        }
    },

    onAppTick: function() {
        if (Date.now() - this.lastClusterRefresh >= this.clusterRefreshInterval) {
            if (this.timelineView && !this.continuousTiming) {
                console.log('Refresh timeline');
                this.timelineView.render();
            }

            this.lastClusterRefresh = Date.now();
        }
    },

    onMouseWheel: function(e, delta, deltaX, deltaY) {
        var deltaLevel = 0;
        if (deltaY < 0) {
            // Reduce detail level
            deltaLevel = 1;
        }
        else if (deltaY > 0) {
            // Increase detail level
            deltaLevel = -1;
        }

        if (deltaLevel != 0 && Global.FEATURE('DetailLevels')) {
            //this.clearDebug();
            var globalCluster = this.globalCluster;
            globalCluster.cluster.setFilterLevel(deltaLevel, { relative: true, noRender: true});
            globalCluster.cluster.debounceRender();
        }
    },

    renderAuthenticated: function(options, globalSize, d) {
        options = options || {};
        var globalCluster = this.globalCluster;
        if (globalCluster) {
            globalCluster.containerWidth = globalSize.width;
            globalCluster.containerHeight = globalSize.height;
            globalCluster.cluster.setRadius(globalCluster.getClusterSize());
            globalCluster.setPosition();

            globalCluster.render(_.extend({
                noAnimate: true
            }, options));
        }
    },

    dropHandler: function(dropView, dragView, dragDetails, dropDetails) {
        if (dragView.className != 'simple-view') {
            return;
        }

        var dragViewID = dragView.viewID,
            dragClusterID = dragView.clusterID,
            dragCluster = Global.clusters.get(dragClusterID),
            dragFocusID = dragCluster.focusID,
            dragIsFocus = dragViewID == dragFocusID;

        if (!dragIsFocus) {
            return;
        }

        if (Global.FEATURE('MovableFocus')) {
            // If a cluster focus is dropped onto space then move the cluster
            var x = dragDetails.dragProxyX + (dragDetails.dragProxyWidth / 2),
                y = dragDetails.dragProxyY + (dragDetails.dragProxyHeight / 2);

            dragCluster.setPosition(x, y);
        }

        // If moved to the left edge then simplify it
        /*
        var globalSize = B.globalSize()
        if (x <= globalSize.width * 0.1) {
            if (!dragCluster.isMinimized) {
                dragCluster.setRadius(B.getDefaultClusterSize() / 2);
                dragCluster.lastMaxDepth = dragCluster.maxDepth;
                dragCluster.maxDepth = 1;
                dragCluster.isMinimized = true;
            }
        }
        // If moved to the top right corner, and not the root cluster, then delete it
        else if (dragViewID != B.rootContextID && x >= globalSize.width * 0.8 && y <= globalSize.height * 0.2) {
            dragCluster.setRadius(0);
            dragCluster.maxDepth = null;
        }
        else {
            dragCluster.setRadius(B.getDefaultClusterSize());
            dragCluster.maxDepth = dragCluster.lastMaxDepth || null;
            dragCluster.isMinimized = false;
        }
        */

        // If not the focus and dropped over space, then create a new cluster there
        /*
        else if (!dragIsFocus && !dropModel) {
            // Prevent more than one root cluster
            if (dragViewID == B.rootContextID) {
                return false;
            }

            B.lastClusterID = B.lastClusterID || 0;
            B.lastClusterID += 1;

            var clusterID = 'Cluster-' + B.lastClusterID,
                newCollection = B.globalCollection.collectionFromRoot(dragViewID),
                newCluster = new Cluster(clusterID, SimpleView, newCollection, B.getClusterOptions());
            
            B.clusters[clusterID] = newCluster;

            newCluster.setPosition(dragDetails.dragProxyX + (dragDetails.dragProxyWidth / 2), dragDetails.dragProxyY + (dragDetails.dragProxyHeight / 2));
            newCluster.setRadius(B.getDefaultClusterSize());
            newCluster.setFocus(dragViewID);
            newCluster.filterValue = 0;
            newCluster.render();
        }
        */

        dragCluster.render();
    }
});

export default ClusterAppView;