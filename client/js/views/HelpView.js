/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import     Backbone from 'backbone'
import     _ from 'underscore'
Backbone.$ = $;

// -------------

var HelpView = Backbone.View.extend({
    tagName: 'div',
    className: 'help-view',

    events: {
    },

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'hide', 'show', 'onOverlayClick');

        this.$el
                .html('<iframe src="about:blank"></iframe>')
                .addClass('content-box');

        this.helpUrl = options.helpUrl;
        this.$overlay = options.$overlay;

        this.$close = $('.close-help', this.$el)
                        .click(this.hide);
    },

    render: function(options) {
        options = options || {};

        return this;
    },

    show: function(options) {
        options = options || {};

        if (!this.loaded) {
            $('iframe', this.$el).attr('src', this.helpUrl);
            this.loaded = true;
        }

        this.visibleTimestamp = Date.now();
        this.$overlay.off('click')
        this.$overlay.on('click', this.onOverlayClick);

        this.$overlay.show();
        this.$el.show();
        this.visible = true;
    },

    hide: function() {
        this.visible = false;
        this.$overlay.hide();
        this.$el.hide();
    },

    onOverlayClick: function() {
        if (!this.visibleTimestamp) {
            return
        }

        if (Date.now() - this.visibleTimestamp > 1000) {
            this.hide();
        }

        return false;
    }
});

export default HelpView; 