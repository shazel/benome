/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'
import io from 'socket.io-client'

// -------------

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

import ClusterAppView from 'benome/views/ClusterAppView'
import ClusterStructures from 'benome/modules/Cluster/Structures'
import computeClusterGraphs from 'benome/modules/Functions/ComputeClusterGraphs'
import AutoActionView from 'benome/views/AutoActionView'
import QuickContextActions from 'benome/views/QuickContextActions'
import TimelineView from 'benome/modules/Views/TimelineListView'
import ActivityPath from 'benome/ActivityPath'
import DestroyerView from 'benome/modules/Views/DestroyerView'
import CreatorView from 'benome/modules/Views/CreatorView'

// -------------

var BenomeAppView = ClusterAppView.extend({
    tagName: 'div',
    className: 'benome',

    events: {
    },

    autoActionDelay: 25,
    pointAttributes: {
        'Text': {
            AttrID: 'Text',
            Label: 'Notes',
            Type: 'Text',
            Def: {}
        },
        'Timing': {
            AttrID: 'Timing',
            Label: 'Timing',
            Type: 'Interval',
            Def: {}
        }
    },

    contextAttributes: {
        'Label': {
            AttrID: 'Label',
            Label: 'Label',
            Type: 'TextLine',
            Def: {}
        },
        'TargetFrequency': {
            AttrID: 'TargetFrequency',
            Label: 'Target Frequency',
            Type: 'Frequency',
            Def: {}
        }
    },

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'onDestroyerIncomingDrop', 'onCreatorIncomingDrop',
                    'onCreateContext', 'showPointDetail', 'showAddFeedback',
                    'clusterClicked', 'onShowContextRename', 'beforeFocusChanged',
                    'onClusterDataChanged');

        this.autoActionDelay = options.autoActionDelay || this.autoActionDelay;
        this.hideLabels = options.hideLabels !== null ? options.hideLabels : this.hideLabels;
        this.labelIDsOnly = options.labelIDsOnly !== null ? options.labelIDsOnly : this.labelIDsOnly;
        this.localOnly = !!options.localOnly;
        this.continuousTiming = !!options.continuousTiming;
        this.clusterOnly = !!options.clusterOnly;

        this.defaultGlobalClusterOptions = {
            dropDisabled: false,
            noCompress: false,
            newContextBumpTime: 20,
            setBackgroundFilterLevel: options.setBackgroundFilterLevel,
            radiusScaleFactor: this.isMobile ? 0.50 : 0.35,
            fontFraction: this.isMobile ? 0.27 : 0.22
        }

        var globalClusterOptions = _.extend({}, this.defaultGlobalClusterOptions, options.globalClusterOptions);

        var appOptions = _.extend(options, {
            globalClusterOptions: globalClusterOptions
        });

        appOptions.features = _.extend({}, {
            'ActionHistory': true,
            'ActionTimeline': true,
            'ActivityPullForward': true,
            'ActivityPushBack': true,
            'AddFeedbackInteractive': true,
            'Admin': true,
            'ClusteredEdit': true,
            'ContextModify': true,
            'DetailLevels': true,
            'LeafFocusAutoAdd': true,
            'LeafFocusToggleTimer': true,
            'MovableFocus': true,
            'PointAdd': true,
            'PointDetail': true,
            'PointLongPress': true,
            'PointShortPress': true
        }, appOptions.features || {});

        ClusterAppView.prototype.initialize.call(this, appOptions);

        Global.on('ContextCreated', this.onCreateContext);
        Global.on('PointAdded', this.showAddFeedback);

        Global.on('ShowPointDetail', this.showPointDetail);
        Global.on('BeforeFocusChanged', this.beforeFocusChanged);
        Global.on('GlobalClusterClicked', this.clusterClicked);
        Global.on('ShowContextRename', this.onShowContextRename);

        var _this = this;
        Global.on('DragBegin', function() {
            //Global.trigger('ClearUIElements')
            Global.updateLastActivity();
            _this.contextRename.setPointerTransparent();
            Global.trigger('SetAddFeedbackPointerTransparent');
        });

        Global.on('DragEnd', function() {
            //Global.trigger('UnClearUIElements')
            Global.updateLastActivity();
            _this.contextRename.restorePointer();
            Global.trigger('RestoreAddFeedbackPointer');
        });

        Global.on('SetExclusive', function(cluster) {
            _this.timelineView && _this.timelineView.hide();
        });

        this.keyboardHandler.keyboardHandler.on('ModifyContext', function(keyboardHandler, cluster) {
            // FIXME
            if (_this.controller.layerStack.length > 0) {
                return;
            }

            if (Global.FEATURE('ContextModify')) {
                var focusView = cluster.getView(cluster.focusID);
                cluster.trigger('DestroyerDrop', focusView, null, null, {});
            }
        });

        this.keyboardHandler.keyboardHandler.on('CancelLayer', function(keyboardHandler, cluster) {
            // FIXME
            if (_this.controller.layerStack.length > 0) {
                Global.trigger('PopLayer');
            }
            else if (Global.adminView && Global.adminView.visible) {
                Global.adminView.hide();
            }
            else if (Global.helpView && Global.helpView.visible) {
                Global.helpView.hide();
            }
        });

        this.keyboardHandler.keyboardHandler.on('SaveLayer', function(keyboardHandler, cluster) {
            cluster.controller.trigger('LongPress');
        });

        this.on('GlobalClusterRendered', function(globalCluster) {
            var cluster = globalCluster.cluster;
            _this.updateGraphs(cluster);
            cluster.on('DataChanged', _this.onClusterDataChanged);
        });

        this.initServiceWorker()

        this.webSocketHandlers = options.webSocketHandlers || []
        this.initWebSocket()
    },

    initServiceWorker: function() {
        var _this = this;

        // Register a Service Worker.
        navigator.serviceWorker.register('/app/service-worker.js')
          .then(subscribe)
          .then(save_subscription)
          .catch(function(x) {
            console.log('failed', x)
          });

        // This function is needed because Chrome doesn't accept a base64 encoded string
        // as value for applicationServerKey in pushManager.subscribe yet
        // https://bugs.chromium.org/p/chromium/issues/detail?id=802280
        function urlBase64ToUint8Array(base64String) {
          var padding = '='.repeat((4 - base64String.length % 4) % 4);
          var base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/');
         
          var rawData = window.atob(base64);
          var outputArray = new Uint8Array(rawData.length);
         
          for (var i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
          }
          return outputArray;
        }

        function subscribe(registration) {
          // Use the PushManager to get the user's subscription to the push service.
          return registration.pushManager.getSubscription()
            .then(async function(subscription) {
              // If a subscription was found, return it.
              if (subscription) {
                return subscription;
              }

              // Get the server's public key
              const response = await fetch('VAPIDPubKey');
              const vapidPublicKey = await response.text();
              // Chrome doesn't accept the base64-encoded (string) vapidPublicKey yet
              const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);

              // Otherwise, subscribe the user (userVisibleOnly allows to specify that we don't plan to
              // send notifications that don't have a visible effect for the user).
              return registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: convertedVapidKey
              });
            });
        }

        function save_subscription(subscription) {
            console.log('Saving subscription', subscription)

            // Send the subscription details to the server using the Fetch API.
            fetch('/app/register_push_subscriber', {
                method: 'post',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    userID: _this.userID,
                    instanceID: _this.instanceID,
                    subscription: subscription
                })
            });
        }
    },

    initWebSocket: function() {
        function setupSocket(namespace, resourceName, listeners) {
            namespace = namespace || ''
            var socket = io('//' + document.domain + ':' + location.port + namespace,
                    {
                        path: resourceName,
                        'force new connection': true
                    })

            _.each(listeners, function(func, eventName) {
                socket.on(eventName, func)
            })

            return socket
        }

        var _this = this
        var socket = setupSocket('/messages', '/message_socket', {
            connect: function() {
                // Immediately verify the connection
                this.emit('init', {
                    UserID: _this.userID,
                    InstanceID: _this.instanceID
                })
            },
            control: function(msg) {
                if (msg.Msg == 'StreamReady') {
                    console.log('Connection verified');
                }
            }
        })

        socket.on('newmessage', function(msg) {
            console.log('message via socketio', msg)
            _.each(_this.webSocketHandlers, function(socketHandlerFunc, socketHandlerName) {
                if (socketHandlerName == 'NewMessage') {
                    socketHandlerFunc(msg)
                }
            });
        })
    },

    displayNotification: function(message, icon) {
        Notification.requestPermission(function (permission) {
            console.log(permission, message)
            var notification = new Notification(message, {
                icon: icon,
                requireInteraction: true,
                body: message,
                actions: [  
                   {action: 'like', title: '👍Like'},  
                   {action: 'reply', title: '⤻ Reply'}
                ]
            });
            notification.onclick = function(a) {
                console.log(arguments);
            };
            
            var audio = new Audio('https://benome.ca/open-ended.mp3');
            audio.play();
        });
    },

    updateGraphs: function(cluster) {
        var graphWindow = 86400 * 7,
            numGraphSegments = 12 * 7,
            anchorTime = Date.now() / 1000;

        computeClusterGraphs(cluster.contexts.get(cluster.rootID), graphWindow, 
                                numGraphSegments, anchorTime, {
                                    force: true,
                                    explicitTargetsOnly: true
                                });
    },

    onClusterDataChanged: function(cluster, scoresUpdated) {
        this.updateGraphs(cluster);
    },

    postAttach: function() {
        ClusterAppView.prototype.postAttach.call(this);

        if (!this.continuousTiming && Global.FEATURE('LeafFocusAutoAdd')) {
            this.autoActionView = new AutoActionView({
                delay: this.autoActionDelay
            });
            this.autoActionView.render().$el.appendTo(this.$container);
            Global.autoActionView = this.autoActionView;
        }
    },

    initUI: function(options) {
        options = options || {};
        ClusterAppView.prototype.initUI.call(this, options);

        this.setDragTargets(!this.clusterOnly);
        this.$addFeedback = $('<div>')
                                    .addClass('point-add-feedback')
                                    .appendTo(this.$el);

        this.contextRename = new QuickContextActions({
            $overlay: this.$overlay2
        });
        Global.contextRename = this.contextRename;
        this.contextRename.render().$el.appendTo(this.$el);

        if (!options.timelineDisabled && Global.FEATURE('ActionTimeline')) {
            if (!this.continuousTiming) {
                this.timelineView = new TimelineView({
                    hideLabels: this.hideLabels,
                    dragHandler: Global.commonPointDragHandler
                });
            }
            else {
                this.timelineView = new ActivityPath({
                    hideLabels: this.hideLabels,
                    dragHandler: Global.commonPointDragHandler,
                    minimumDuration: 25,
                    refreshInterval: 60,
                    displayInterval: 60 * 60 * 2
                });
            }
        }

        if (this.timelineView) {
            this.$timelineContainer = this.timelineView.$el;
            this.$timelineContainer.addClass('timeline-container');
            this.timelineView.render().$el.appendTo(this.$container);
        }
    },

    onDataInitialized: function(globalCollection, userCollection) {
        ClusterAppView.prototype.onDataInitialized.call(this, globalCollection, userCollection);

        if (this.timelineView) {
            this.timelineView.clusterController = this.globalCluster;
            this.timelineView.setPointsCollection(userCollection.points);
        }
    },

    renderAuthenticated: function(options, globalSize, d) {
        options = options || {};

        this.renderCreatorView(true);
        this.renderDestroyerView();

        var timelineWidth = Global.fontSize * 8;
        if (this.$timelineContainer) {
            this.$timelineContainer.css({
                'left': (globalSize.width - timelineWidth) + 'px',
                'width': timelineWidth + 'px'
            });
        }

        var globalCluster = this.globalCluster;
        if (globalCluster) {
            globalCluster.containerWidth = globalSize.width;
            globalCluster.containerHeight = globalSize.height;
            globalCluster.cluster.setRadius(globalCluster.getClusterSize());
            globalCluster.setPosition();

            globalCluster.render(_.extend({
                noAnimate: true
            }, options));
        }
    },

    renderUnauthenticated: function(options, globalSize, d) {
        Global.trigger('HideAddPointFeedback');
        if (this.$timelineContainer) {
            this.$timelineContainer.hide();
        }

        this.hideCreatorView();
        this.hideDestroyerView();
    },

    setDragTargets: function(enabled, whichEnabled) {
        if (enabled) {
            if (!whichEnabled || _.contains(whichEnabled, 'Destroyer')) {
                this.showDestroyerView();
            }

            if (!whichEnabled || _.contains(whichEnabled, 'Creator')) {
                this.showCreatorView();
            }
        }
        else {
            this.hideDestroyerView();
            this.hideCreatorView();
        }
    },

    showDestroyerView: function() {
        this.initDestroyerView();
        this.renderDestroyerView(true);
    },

    showCreatorView: function() {
        this.initCreatorView();
        this.renderCreatorView(true);
    },

    initDestroyerView: function() {
        if (!this.destroyerView) {
            this.destroyerView = new DestroyerView({});
            this.destroyerView.render().$el.appendTo(this.$container);
            this.destroyerView.on('IncomingDrop', this.onDestroyerIncomingDrop);
        }
    },

    onDestroyerIncomingDrop: function(dragView, dragDetails, dropDetails, callback) {
        var view = dragView;

        var immutable = dragView.model.getNS && dragView.model.getNS('Immutable') == '1';

        if (!immutable) {
            if (view.className == 'simple-view') {
                var cluster = dragView.cluster,
                    clusterID = cluster.clusterID;

                // Delete the context
                var contextID = view.viewID,
                    noFocus = true,
                    successCallback = null;

                if (contextID != cluster.rootID) {
                    Global.trigger('DeleteContext', contextID, clusterID, noFocus, successCallback);
                }
            }
            else if (view.className == 'point-list-view' || view.className == 'activity-interval-view') {
                if (view.model && !view.nonDeletable) {
                    // Delete the point
                    var successCallback = null,
                        pointModel = dragView.model,
                        pointID = pointModel.id,
                        clusterID = view.clusterController.clusterID,
                        parentContextID = pointModel.get('ContextID');

                    Global.trigger('DeletePoint', clusterID, pointID, parentContextID, successCallback);
                }
            }
        }

        if (callback) {
            callback(true, dragDetails, dropDetails);
        }
    },

    initCreatorView: function() {
        if (!this.creatorView) {
            this.creatorView = new CreatorView({});
            this.creatorView.render().$el.appendTo(this.$container);

            var _this = this;
            this.creatorView.on('Click', function() {
                _this.timelineView && _this.timelineView.toggleVisible();
            });

            this.creatorView.on('Press', function() {
                Global.trigger('ShowAdmin');
            });

            this.creatorView.on('IncomingDrop', this.onCreatorIncomingDrop);
        }
    },

    onCreatorIncomingDrop: function(dragView, dragDetails, dropDetails, callback) {
        var view = dragView;

        if (view.className != 'simple-view' || !view.model.isLeaf()) {
            callback(false, dragDetails);
            return;
        }

        var color = view.cluster.getColor(view.viewID);

        if (Global.FEATURE('PointAdd')) {
            if (dropDetails.isHold) {
                var clusterDef = ClusterStructures.generatePointAttributeCluster(view.model);
                clusterDef.RenderOptions.initialPos = {
                    x: dropDetails.currentX,
                    y: dropDetails.currentY
                };

                clusterDef.Options.rootColor = color;
                clusterDef.Origin = view;

                dragView.cluster.controller.pushCluster({
                    originalView: view,
                    clusterDef: clusterDef,
                    color: color,
                    commit: function(options) {
                        var values = options.values,
                            originalView = options.originalView;

                        var dropDetails = {};
                        Global.trigger('AddPoint', originalView.viewID, originalView.clusterID, {
                            UpdatedAttributes: values,
                            Color: color
                        }, null, {
                            showHistory: false,
                            showAddFeedback: false,
                            toParent: false,
                            showDetail: false
                        });
                    }
                });
            }
            else {
                var dropDetails = {};
                Global.trigger('AddPoint', view.viewID, view.clusterID, {
                    UpdatedAttributes: {
                        Timing: {
                            Time: Date.now() / 1000,
                            Duration: 0
                        }
                    }
                }, null, {
                    showHistory: false,
                    showAddFeedback: true,
                    toParent: false,
                    showDetail: false
                });
            }
        }

        if (callback) {
            callback(true, dragDetails);
        }
    },

    renderCreatorView: function(show) {
        if (this.creatorView) {
            var globalSize = Global.globalSize(),
                d = (globalSize.height + globalSize.width) / 2;

            this.creatorView.$el
                .css({
                    'right': (d * 0.01) + 'px',
                    'top': (d * 0.01) + 'px',
                    'width': (d * 0.15) + 'px',
                    'height': (d * 0.15) + 'px'
                });

            if (show) {
                this.creatorView.$el.show();
            }
        }
    },

    renderDestroyerView: function(show) {
        if (this.destroyerView) {
            var globalSize = Global.globalSize(),
                d = (globalSize.height + globalSize.width) / 2;

            this.destroyerView.$el
                .css({
                    'left': (d * -0.125) + 'px',
                    'bottom': (d * -0.125) + 'px',
                    'width': (d * 0.30) + 'px',
                    'height': (d * 0.30) + 'px'
                });

            if (show) {
                this.destroyerView.$el.show();
            }
        }
    },

    hideDestroyerView: function() {
        this.destroyerView && this.destroyerView.hide();
    },

    hideCreatorView: function() {
        this.creatorView && this.creatorView.hide();
    },

    showAddFeedback: function(contextID, pointID, pointLabel, backgroundColor, options) {
        if (!options.feedbackRequested) {
            return;
        }
        var creatorView = this.creatorView;

        if (!this.$addFeedback || !creatorView) {
            return;
        }
        options = options || {};
        var underCursor = options.underCursor !== false ? true : false;
        var showDelay = options.showDelay || 0;

        if (this.overlayVisible() && !options.force) {
            return;
        }

        var textColor = Global.getTextContrastColor(backgroundColor),
            x = Global.DD.currentCursorX - Global.$el.offset().left,
            y = Global.DD.currentCursorY - Global.$el.offset().top;

        if (!underCursor) {
            x = creatorView.$el.offset().left + (creatorView.$el.width() / 2);
            y = creatorView.$el.offset().top + (creatorView.$el.height() / 2);
        }

        var $addFeedback = this.$addFeedback;

        $addFeedback
            .hide()
            .css({
                'background-color': backgroundColor,
                'color': textColor,
                'opacity': 0,
                'left': (x - ($addFeedback.width() / 2)) + 'px',
                'top': (y - ($addFeedback.height() / 2)) + 'px'
            })
            .off('click')
            .text(pointLabel);



        if (Global.FEATURE('AddFeedbackInteractive')) {
            $addFeedback
                .on('click', function(e) {
                    var pos = {
                        x: x,
                        y: y
                    }

                    Global.trigger('ShowPointDetail', pointID, contextID, null, pos);
                    return false;
                });
        }

        _.delay(function() {
            $addFeedback
                .show()
                .animate({
                        opacity: 1
                    },
                    {
                        duration: 150
                    });
        }, showDelay);

        this.lastShowID = Math.round(Math.random() * 10000000);
        var _this = this;

        _.delay(function(showID) {
            return function() {
                if (showID != _this.lastShowID) {
                    return;
                }
                $addFeedback.animate({
                        opacity: 0
                    },
                    {
                        duration: 150,
                        complete: function() {
                            $(this).hide();
                        }
                    });
            }
        }(this.lastShowID), 2000);
    },

    showPointDetail: function(pointID, contextID, changeCallback, refPos) {
        Global.updateLastActivity();

        if (this.globalCluster) {
            this.globalCluster.trigger('ShowPointEdit', pointID, contextID, changeCallback, refPos);
        }
    },

    clusterClicked: function(e, view) {
        Global.updateLastActivity();
        if (!view) {
            return;
        }

        if (this.autoActionView) {
            if (view.cluster.focusID == view.viewID && view.model.isLeaf()) {
                this.autoActionView.trigger('FocusAction', view.viewID, view.cluster);
            }
        }

        Global.updateLastActivity();
    },

    beforeFocusChanged: function(cluster, focusID) {
        Global.updateLastActivity();

        if (Global.clusters.isGlobal(cluster)) {
            Global.util.localSet('LastFocus', focusID);
        }

        if (this.autoActionView) {
            this.autoActionView.trigger('ClusterFocusChanged', cluster, focusID);
        }
    },

    onCreateContext: function(contextID, parentView) {
        Global.updateLastActivity();

        if (Global.FEATURE('ContextModify')) {
            Global.trigger('ShowContextRename', parentView, {
                autoFocus: true,
                autoHide: 10000,
                renameContextID: contextID
            });
        }
    },

    onShowContextRename: function(view, options) {
        this.contextRename.show(view, options);
    }
});

export default BenomeAppView;