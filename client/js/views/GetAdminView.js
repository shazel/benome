/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import Backbone from 'backbone'
import _ from 'underscore'
import Hammer from 'hammerjs'
Backbone.$ = $;

// -------------

var GetAdminView = Backbone.View.extend({
    tagName: 'div',
    className: 'getadmin-view',

    events: {
    },

    initialize: function(options) {
        _.bindAll(this, 'onClick');
        this.$el.text('🔧');

        var mc = new Hammer(this.el);
        mc.on('tap', this.onClick);
    },

    onClick: function(e) {
        this.trigger('Click');
    },

    render: function() {
        return this;
    },

    hide: function() {
        this.visible = false;
        this.$el.hide();
    },

    show: function() {
        this.visible = true;
        this.$el.show();
    }
});
_.extend(GetAdminView.prototype, Backbone.Events)

export default GetAdminView;