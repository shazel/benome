/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import _ from 'underscore'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function AppController(options) {
    
    if (!options.appOptions.listeners) {
        options.appOptions.controller = this;
    }

    this.app = new options.appView(options.appOptions);
    this.bindEvents();
}

_.extend(AppController.prototype, {
    bindEvents: function() {
        var _this = this;
        _.bindAll(this, 'pushLayer', 'popLayer');

        Global.on('LogoutUser', this.logoutUser);
        Global.on('ChangePassword', this.changePassword);

        Global.on('AuthLost', function(contextID, userID) {
            console.log('AuthLost');
        });

        this.layerStack = [];
        Global.on('PushLayer', this.pushLayer);
        Global.on('PopLayer', this.popLayer);
    },

    pushLayer: function(zIndex, callback, visibleUIElements, passThru) {
        this.setUILayers(zIndex, visibleUIElements);
        Global.trigger('SetExclusive');

        var $overlay = $('<div>')
                        .addClass('layer-overlay')
                        .css({
                            'z-index': zIndex
                        })
                        .appendTo(Global.$el),

            $container = $('<div>')
                        .addClass('layer-container')
                        .css({
                            'z-index': zIndex + 1
                        })
                        .appendTo(Global.$el);

        this.layerStack.push({
            zIndex: zIndex,
            $overlay: $overlay,
            $container: $container,
            cluster: null
        });

        if (callback) {
            var _this = this;
            callback($container, passThru, function(clusterController) {
                if (clusterController) {
                    var cluster = clusterController.cluster;
                    _this.layerStack[_this.layerStack.length - 1].cluster = cluster;
                    _this.app.keyboardHandler.setCluster(cluster);
                }
            });
        }
    },

    popLayer: function(callback) {
        var layerDef = this.layerStack.pop();

        if (layerDef) {
            layerDef.$overlay.remove();
            layerDef.$container.remove();

            if (this.layerStack.length) {
                var nextCluster = this.layerStack[this.layerStack.length - 1].cluster;
                this.app.keyboardHandler.setCluster(nextCluster);
            }
            else {
                // Back to global cluster
                this.app.keyboardHandler.setCluster(this.app.globalCluster.cluster);
            }
        }

        if (callback) {
            callback();
        }

        this.unsetUILayers();
    },

    setUILayers: function(refZIndex, above) {
        above = above || [];

        this.layerZIndexStacks = this.layerZIndexStacks || {
            'Timeline': [],
            //'History': [],
            'Creator': [],
            'Destroyer': []
        }

        var aboveZIndex = refZIndex + 100000,
            belowZIndex = refZIndex - 1;

        _.each(this.layerZIndexStacks, function(stack, elementID) {
            var zIndex = _.contains(above, elementID) ? aboveZIndex : belowZIndex;

            if (elementID == 'Timeline') {
                if (this.timelineView) {
                    this.layerZIndexStacks['Timeline'].push(this.timelineView.$el.css('z-index'));
                    this.timelineView.$el.css({
                        'z-index': zIndex
                    });
                }
            }
            else if (elementID == 'Creator') {
                if (this.creatorView) {
                    this.layerZIndexStacks['Creator'].push(this.creatorView.$el.css('z-index'));
                    this.creatorView.$el.css({
                        'z-index': zIndex
                    });
                }
            }
            else if (elementID == 'Destroyer') {
                if (this.destroyerView) {
                    this.layerZIndexStacks['Destroyer'].push(this.destroyerView.$el.css('z-index'));
                    this.destroyerView.$el.css({
                        'z-index': zIndex
                    });
                }
            }
        }, this);
    },

    unsetUILayers: function() {
        _.each(this.layerZIndexStacks, function(stack, elementID) {
            if (stack.length == 0) {
                return;
            }

            var prevZIndex = stack.pop();
            if (elementID == 'Timeline') {
                if (this.timelineView) {
                    this.timelineView.$el.css({
                        'z-index': prevZIndex
                    });
                }
            }
            else if (elementID == 'Creator') {
                if (this.creatorView) {
                    this.creatorView.$el.css({
                        'z-index': prevZIndex
                    });
                }
            }
        }, this);
    },

    addFeedback: function(contextID, val) {
        val = val || 0;

        var data = {
            'ParentContextID': contextID,
            'FeedbackVal': val
        }

        var s = function(response, textStatus, jqXHR) {
            if (response && response.Success) {
            }

        }

        Global.ajax.jsonGet('/data/point/add2?callback=?', data, s);
    }
});

export default AppController;