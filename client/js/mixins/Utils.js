/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import _ from 'underscore'
import _  from 'benome/modules/GlobalState'
console.log(_Global)
var Global = _Global()

var Utils = {

    localGet: function(key) {
        return localStorage.getItem(Global.instanceID + '-' + key)
    },

    localSet: function(key, val) {
        return localStorage.setItem(Global.instanceID + '-' + key, val)
    },

    clearDebug: function() {
        if (this.$debug) {
            this.$debug.html('');
        }
    },

    debugMsg: function(msg) {
        if (!this.$debug) {
            this.$debug = $('<div>')
                                .appendTo(Global.$el)
                                .css({
                                    width: '75%',
                                    height: '50%',
                                    left: '50px',
                                    top: '50px',
                                    'font-size': '0.5em',
                                    'pointer-events': 'none',
                                    'z-index': 9999999
                                });
        }
        this.$debug
            .show()
            .html(msg + '<br>' + this.$debug.html());
    },

    centerOn: function($refEl, $curEl) {
        var xMid = $refEl.offset().left + ($refEl.width() / 2) - Global.$el.offset().left,
            yMid = $refEl.offset().top + ($refEl.height() / 2) - Global.$el.offset().top,

            width = $curEl.width(),
            height = $curEl.height(),

            left = (xMid - (width / 2)),
            top = (yMid - (height / 2)),

            size = Global.globalSize();

        if (top + height > size.height) {
            top = size.height - height;
        }
        else if (top < 0) {
            top = 0;
        }

        if (left + width > size.width) {
            left = size.width - width;
        }
        else if (left < 0) {
            left = 0;
        }

        $curEl.css({
            top:  top + 'px',
            left: left + 'px'
        });
    },

    formatDuration: function(duration, noSeconds) {
        var hours = Math.floor(duration / 3600),
            minutes = Math.floor((duration - (hours * 3600)) / 60),
            seconds = duration - (hours * 3600) - (minutes * 60),
            durationStr;

        if (hours || noSeconds) {
            durationStr = hours + ':' + this.zeroPad(minutes);
        }
        else {
            durationStr = this.zeroPad(minutes) + ':' + this.zeroPad(seconds);
        }

        return durationStr;
    },

    zeroPad: function(n) {
        var pad = '00';
        return (pad + n).slice(-pad.length);
    },

    getCookie: function(name) {
        var keyValue = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    },

    sum: function(vals) {
        return _.reduce(vals, function(memo, num) { return memo + num; }, 0);
    },

    // All arrays must be of same length or column sum will be NaN
    sumArrays: function(arrayList) {
        var zipped = _.zip.apply(_, arrayList),
            sum = this.sum;
        return _.map(zipped, function(col) {
            return sum(col);
        });
    }
};

export default Utils;