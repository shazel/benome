/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function ContextStack(options) {
    options = options || {};
    _.bindAll(this, 'onPopContext', 'onPushContext');

    this.$container = options.$container;
    this.contextStack = [];

    Global.on('PopContext', this.onPopContext);
    Global.on('PushContext', this.onPushContext);
}
_.extend(ContextStack.prototype, {
    onPopContext: function(view, viewState) {
        this.contextStack.pop();
        this.renderContextStack(view, viewState);
    },

    onPushContext: function(view, viewState) {
        var zIndex = view.$el.css('z-index'),
            bg = view.getColor();

        this.contextStack.push({
            zIndex: zIndex,
            bgColor: bg
        });

        this.renderContextStack(view, viewState);
    },

    renderContextStack: function(refView, viewState) {
        $('.context-stack', this.$container).remove();

        var refSize = viewState.radius * 2,
            x = viewState.x - viewState.radius,
            y = viewState.y - viewState.radius;

        _.each(this.contextStack.reverse(), function(t, i) {
            /*if (i == this.contextStack.length - 1) {
                // Skip the most recent
                return;
            }*/
            var adjustY = (i + 1) * Math.pow(Global.fontSize, 1 - (i / 10)),
                adjustX = adjustY;

            var $x = $('<div>')
                        .addClass('context-stack')
                        .css({
                            'background': t.bgColor,
                            'border-radius': '1000px',
                            'zIndex': t.zIndex - i - 1,
                            'top': y - adjustY,
                            'left': x - adjustX,
                            'position': 'absolute',
                            'width': refSize + (adjustX * 2) + 'px',
                            'height': refSize + (adjustY * 2) + 'px'
                        })
                        .appendTo(this.$container);
        }, this);
    }
});

export default ContextStack;