/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import _ from 'underscore'
import Backbone from 'backbone'

window._ = _
window.$ = window.jQuery = $
window.Backbone = Backbone


// -------------

import BaseAppView from './views/BaseAppView'
import AppController from './AppController'

var BaseEntry = function() {};
_.extend(BaseEntry.prototype, {
    isMobile: ('ontouchstart' in document.documentElement),
    isAndroid: (/android/i.test(navigator.userAgent)),
    isApple: (/iphone|ipod|ipad/i.test(navigator.userAgent)),
    isMac: (/Macintosh/.test(navigator.userAgent)),
    isTablet: (/ipad/i.test(navigator.userAgent) || ((/android/i.test(navigator.userAgent)) && !(/mobile/i.test(navigator.userAgent)))),

    init: function(options) {
        options = options || {};
        var appOptions = options.appOptions || {};

        var $container = appOptions.container ? $(appOptions.container) : $('body');
        $container.addClass('benome-container');
        this.$container = $container

        var localOnly = (appOptions.localOnly !== false) ? true : false;
        this.instanceID = appOptions.instanceID || this.getInstanceID(localOnly);

        appOptions = _.extend({
            localOnly: localOnly,
            instanceID: this.instanceID,
            container: this.$container
        }, appOptions);
        
        this.controller = new AppController({
            appView: options.appView || BaseAppView,
            appOptions: appOptions
        });
        this.app = this.controller.app;
    },

    getInstanceID: function(localOnly) {
        var paramInstanceID = this.QueryString.LocalID,
            instanceID = parseInt(paramInstanceID) || parseInt(10000 + Math.random() * 1000000);

        instanceID = instanceID.toString();

        if (!paramInstanceID && localOnly) {
            var queryString = window.location.search,
                separator = queryString ? '&' : '?',
                port = window.location.port ? ':' + window.location.port : '';

            var instanceUrl = window.location.protocol + '//' + window.location.hostname + port + window.location.pathname + queryString + separator + 'LocalID=' + instanceID;

            var stateObject = {
                'Website' : instanceUrl
            };
            window.history.replaceState(stateObject, window.document.title, instanceUrl);
        }

        return instanceID;
    },

    QueryString: function() {
        var queryString = {};
        var query = window.location.search.substring(1);
        var vars = query.split('&');

        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('='),
                first = pair[0],
                second = pair[1];

            if (typeof queryString[first] === 'undefined') {
                queryString[first] = second;
            }
            else if (typeof queryString[first] === 'string') {
                queryString[first] = [queryString[first], second];
            }
            else {
                queryString[first].push(second);
            }
        } 
        return queryString;
    }()
});

export default BaseEntry