/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'
import     DataLoadBase from 'benome/modules/Data/DataLoadBase'
import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function DataLoad_Remote(options) {
    options = options || {};
    DataLoadBase.call(this, options);
}
_.extend(DataLoad_Remote.prototype, DataLoadBase.prototype, {
    initData: function(contextID) {
        // Proceed to online init
        this.localSet('RootContextID', contextID);
        this.initCollections(contextID);

        var _this = this;

        // Initialize IDs
        this.initIDs(function() {
            // Fetch data locally
            _this.fetchData({
                success: function() {
                    console.log('local fetch success', Date.now());
                    // Sync any local changes back to server
                    _this.syncDirty(
                        // Load remote data afresh
                        _this.fetchData
                    );
                }, 
                localOnly: true
            });
        });
    },

    syncDirty: function(successCallback) {
        Global.setWorking('Synchronizing offline changes');

        var dirtyCollections = _.filter(this.dataCollections, function(col) {
            return col.dirtyModels().length > 0 || col.destroyedModelIds().length > 0;
        });

        var numDirtyCollections = _.values(dirtyCollections).length;
        if (!numDirtyCollections) {
            console.log('No dirty collections to sync');
            successCallback();
            return;
        }

        var colSyncFinished = _.after(numDirtyCollections, function() {
            console.log(numDirtyCollections + ' dirty collections syncd');
            successCallback();
        });

        _.each(dirtyCollections, function(col) {
            var numCalls = col.dirtyModels().length + col.destroyedModelIds().length;

            var dirtyModelSuccess = _.after(numCalls, function(model, response, options) {
                console.log('dirtyModelSuccess');
                colSyncFinished();
            });

            col.syncDirtyAndDestroyed({
                success: dirtyModelSuccess
            });
        }, this);
    },

    initIDs: function(successCallback) {
        successCallback();
    }
});

export default DataLoad_Remote;