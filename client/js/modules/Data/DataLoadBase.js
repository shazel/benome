/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'
import Backbone from 'backbone'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

import Data from 'benome/models/Data'
import uuid4 from 'benome/modules/Util/UUID4.js'

function DataLoadBase(options) {
    options = options || {};
    this.ajax = options.ajax;
    this.instanceID = options.instanceID;

    _.bindAll(this, 'fetchData', 'loadFinished', 'nextID');
}
_.extend(DataLoadBase.prototype, {
    initCollections: function(rootContextID) {
        this.rootContextID = rootContextID;

        var instanceID = this.instanceID;

        var points = this.points;
        if (!points) {
            points = new Data.Points([], {rootID: rootContextID});
            points.storeName = 'BenomeStore-' + instanceID + '-Points';
        }
        points.url = '/app/data/points/' + rootContextID;
        this.points = points;

        var associations = this.associations;
        if (!associations) {
            associations = new Data.Associations([], {});
            associations.storeName = 'BenomeStore-' + instanceID + '-Associations';
        }
        associations.url = '/app/data/associations/' + rootContextID;
        this.associations = associations;

        var contexts = this.contexts;
        if (!contexts) {
            contexts = new Data.Contexts([], {
                rootID: rootContextID,
                associations: associations,
                points: points
            });
            contexts.storeName = 'BenomeStore-' + instanceID + '-Contexts';
        }
        contexts.url = '/app/data/contexts/' + rootContextID;
        this.contexts = contexts;

        this.dataCollections = {
            'Points': points,
            'Contexts': contexts,
            'Associations': associations
        }

        return this.dataCollections;
    },

    fetchData: function(options) {
        options = options || {};

        var successCallback = options.success || this.loadFinished,
            localOnly = options.localOnly || false,
            initialData = options.initialData || null,
            dataCollections = options.dataCollections || this.dataCollections;

        if (!localOnly) {
            Global.setWorking('Loading data');
        }
        var colFetchBeginTimes = {};
        var fetchDataBeginTime = Date.now();

        var numCollections = _.values(dataCollections).length;
        var fetchSuccess = _.after(numCollections, function(collection, textStatus, jqXHR) {
            successCallback(initialData);
        });

        _.each(dataCollections, function(col, colName) {
            colFetchBeginTimes[col.url] = Date.now();
            var options = {
                success: function x(collection) {
                    console.log('Fetch ' + collection.url, Date.now() - colFetchBeginTimes[collection.url]);
                    fetchSuccess(collection);
                }
            };

            if (localOnly) {
                options.remote = false;
            }
            col.fetch(options);
        });

        console.log('Data fetch', Date.now() - fetchDataBeginTime);
    },

    loadFinished: function(initialData) {
        Global.setWorking('Rendering');

        if (initialData) {
            _.each(initialData, function(initialData, collectionID) {
                var col = this.dataCollections[collectionID],
                    models = col.add(initialData);

                _.each(models, function(model) {
                    model.save({}, {
                        'silent': true
                    });
                })
            }, this);
        }

        this.points.on('change add remove', function(pointModel, response) {
            var pointContext = pointModel.getContext();
            if (pointContext) {
                pointContext.trigger('PointChanged', pointModel);
            }
        });

        var userRootContext = this.contexts.find(function(model) {
            return model.attributes['1__NodeType'] == 'UserRoot'
        });
        this.userRootContextID = userRootContext.id
        var rootCollection = this.contexts.collectionFromRoot(this.userRootContextID);
        rootCollection.storeName = 'BenomeStore-' + this.instanceID + '-Contexts';

        this.trigger('LoadFinished', this.contexts, rootCollection);
    },

    nextID: function(preAdd) {
        return uuid4()
    },

    localGet: function(key) {
        return localStorage.getItem(this.instanceID + '-' + key)
    },

    localSet: function(key, val) {
        return localStorage.setItem(this.instanceID + '-' + key, val)
    }
});
_.extend(DataLoadBase.prototype, Backbone.Events);

export default DataLoadBase;