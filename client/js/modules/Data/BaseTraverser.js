/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

// -------------

function BaseTraverser(options) {
    options = options || {};
    this.options = options;
}
_.extend(BaseTraverser.prototype, {
	init: function(state) {
		this.state = state || {};
	},

	/*
		Direct the traversal by returning which associations to visit next
	*/
    getNextLevel: function(context, localState, traverseState, options) {
    	throw 'NotImplemented';
    },

    /*
		Exit condition for traversal. Can be depth/distance-based.
    */
    terminate: function(context, localState, traverseState, options) {
    	throw 'NotImplemented';
    },

    /*
		Determine whether a given context should be included in the results
    */
    filter: function(context, localState, traverseState, options) {
    	throw 'NotImplemented';
    }
});

export default BaseTraverser;