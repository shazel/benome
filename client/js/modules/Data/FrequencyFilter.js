/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

// -------------

import BaseTraverser from 'benome/modules/Data/BaseTraverser'

function FrequencyFilter(options) {
    BaseTraverser.call(this, options);
}
_.extend(FrequencyFilter.prototype, BaseTraverser.prototype, {
    getNextLevel: function(context, localState, traverseState, options) {
    	return context.getNeighbourModels();
    },

    terminate: function(context, localState, traverseState, options) {
    	return context.isLeaf();
    },

    filter: function(context, traverseOptions, options) {
        // Is a leaf, has a score, and score passes filter.
        if (context.isLeaf()) {
            var score = context.getDistanceScore('Frequency');
            return _.isNumber(score) && score >= traverseOptions.filterValue;
        }
        return false;
    },

    filter2: function(context, localState, traverseState, options) {
    	// Is a leaf, has a score, and score passes filter.
    	if (context.isLeaf()) {
    		var passFilter = context.getDistanceScore('Frequency') >= traverseState.traverseOptions.filterValue;

    		if (passFilter) {
	    		var lineageMap = traverseState.leafLineageMap || {};
	    		_.each(localState.lineage, function(context) {
	    			lineageMap[context.id] = context;
	    		});
	    		traverseState.leafLineageMap = lineageMap;
	    	}

    		return passFilter;
    	}

    	// Is not a leaf and an included leaf is a descendent.
    	else {
    		return traverseState.leafLineageMap && (context.id in traverseState.leafLineageMap);
    	}
    }
});

export default FrequencyFilter;