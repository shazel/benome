/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

import DataLoadBase from 'benome/modules/Data/DataLoadBase'

function DataLoad_LocalOnly(options) {
    options = options || {};
    this.localOnly = true;
    DataLoadBase.call(this, options);
}
_.extend(DataLoad_LocalOnly.prototype, DataLoadBase.prototype, {
    initData: function(serializedData) {
        var result = this.initIDs();

        var rootContextID = result.rootContextID,
            initialData = null,
            nextID = this.nextID;

        if (result.newSession) {
            var uiContextID = nextID(),
                prefsContextID = nextID(),
                appsContextID = nextID(),
                userRootContextID = nextID(),
                stateContextID = nextID();

            initialData = {
                Contexts: [],
                Associations: [],
                Points: []
            }

            var addContext = this.addContext;
            addContext(initialData, rootContextID, null, 'Root');
            addContext(initialData, uiContextID, rootContextID, 'UI');
            addContext(initialData, prefsContextID, rootContextID, 'Prefs');
            addContext(initialData, appsContextID, rootContextID, 'Apps');
            addContext(initialData, userRootContextID, rootContextID, '', {'1__NodeType': 'UserRoot'});
            addContext(initialData, stateContextID, rootContextID, 'State');

            var globalAppID = nextID();
            addContext(initialData, globalAppID, appsContextID, 'Global');

            var projectAppID = nextID();
            addContext(initialData, projectAppID, appsContextID, 'Project');

            var behaveAppID = nextID(),
                bonusContextID = nextID();

            addContext(initialData, behaveAppID, appsContextID, 'Behave');
            addContext(initialData, bonusContextID, behaveAppID, 'Bonuses');

            this.appsContextID = appsContextID;
            this.userRootContextID = userRootContextID;

            Global.appsContextID = this.appsContextID;
        }
        else {
            this.appsContextID = nextID();
            this.userRootContextID = nextID();
        }

        if (serializedData) {
            if (_.isString(serializedData)) {
                try {
                    serializedData = JSON.parse(serializedData);
                }
                catch (e) {
                    console.log('Error parsing data: ' + e.toString(), e);
                }
            }
            else if (_.isObject(serializedData)) {
                if (_.isArray(serializedData.Contexts)) {
                    initialData.Contexts = initialData.Contexts.concat(serializedData.Contexts);
                }

                if (_.isArray(serializedData.Associations)) {
                    initialData.Associations = initialData.Associations.concat(serializedData.Associations);
                }

                if (_.isArray(serializedData.Points)) {
                    initialData.Points = initialData.Points.concat(serializedData.Points);
                }
            }
        }

        this.initCollections(rootContextID);

        this.contexts.__proto__.local = true;
        this.contexts.model.prototype.local = true;
        this.points.__proto__.local = true;
        this.points.model.prototype.local = true;
        this.associations.__proto__.local = true;
        this.associations.model.prototype.local = true;

        this.fetchData({
            localOnly: true,
            initialData: initialData
        });
    },

    addContext: function(initialData, contextID, parentID, label, extraAttributes) {
        var attributes = {
            'ID': contextID,
            '1__Label': label,
            '1__Time': Global.getNow() / 1000,
            ...extraAttributes
        }
        initialData.Contexts.push(attributes);

        if (parentID) {
            initialData.Associations.push({
                'ID': parentID + '|down|' + contextID,
                'SourceID': parentID,
                'Name': 'down',
                'DestID': contextID
            });

            initialData.Associations.push({
                'ID': contextID + '|up|' + parentID,
                'SourceID': contextID,
                'Name': 'up',
                'DestID': parentID
            });
        }
    },

    initIDs: function() {
        var newSession = false,
            currentRootContextID = this.localGet('LocalRootContextID') || null;

        if (!currentRootContextID) {
            currentRootContextID = this.nextID();
            this.localSet('LocalRootContextID', currentRootContextID);
            newSession = true;
        }
        
        return {
            rootContextID: currentRootContextID, 
            newSession: newSession
        }
    }
});

export default DataLoad_LocalOnly;