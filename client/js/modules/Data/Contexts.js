/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'
import Backbone from 'backbone'

// -------------

import Context from 'benome/modules/Data/Context.js'
import sum from 'benome/modules/Util/Sum.js'

var Contexts = Backbone.Collection.extend({
    model: Context,

    initialize: function(models, options) {
        options = options || {};
        _.bindAll(this, 'contextAddedToParent', 'contextRemovedFromParent', 'contextChangedOnParent', 
                    'parentReset', 'contextAddedToChild', 'contextRemovedFromChild');

        this.parentCollection = options.parentCollection || this;
        this.rootID = options.rootID || null;
        this.newContextBumpTime = _.isNumber(options.newContextBumpTime) ? options.newContextBumpTime : 60 * 10; // 10 minutes

        this.points = options.points;
        this.associations = options.associations;

        if (this.points) {
            this.points.contexts = this;
        }
    },

    getRoot: function() {
        return this.get(this.rootID);
    },

    isChildOf: function(model, rootID) {
        rootID = rootID || this.rootID;
        if (!rootID) {
            return true;
        }

        var parentModel = model,
            isChild = false;

        while (parentModel) {
            if (parentModel.id == rootID) {
                isChild = true;
                break;
            }
            parentModel = this.get(parentModel.getAssoc('up')[0]);
        }

        return isChild;
    },

    collectionFromRoot: function(rootID) {
        rootID = rootID || this.rootID;

        var models = this.getFromRoot(rootID);
        var rootCollection = new Contexts(models, {
            parentCollection: this,
            associations: this.associations,
            points: this.points,
            rootID: rootID
        });
        rootCollection.get(rootID).isRoot = true;
        rootCollection.bindCollection(this);

        return rootCollection;
    },

    bindCollection: function(parentCollection) {
        parentCollection.on('add', this.contextAddedToParent);
        parentCollection.on('remove', this.contextRemovedFromParent);
        parentCollection.on('change', this.contextChangedOnParent);
        parentCollection.on('reset', this.parentReset);

        this.on('add', parentCollection.contextAddedToChild);
        this.on('remove', parentCollection.contextRemovedFromChild);
    },

    contextAddedToChild: function(model) {
        if (!this.get(model.id)) {
            this.add(model, {'silent': true});
        }
    },

    contextRemovedFromChild: function(model) {
        if (this.get(model.id)) {
            this.remove(model, {'silent': true});
        }
    },

    parentReset: function() {
        this.reset([]);
    },

    contextAddedToParent: function(model) {
        if (this.rootID && this.isChildOf(model, this.rootID)) {
            this.add(model);
        }
    },

    contextRemovedFromParent: function(model) {
        this.remove(model);
    },

    contextChangedOnParent: function(model) {
        if (this.rootID && this.isChildOf(model, this.rootID)) {
            this.trigger('change', model);
        }
    },

    getFromRoot: function(rootID, options) {
        options = options || {};

        var rootID = rootID || this.rootID,
            models = this.parentCollection.models;

        // Limit to specific root ID
        return this.parentCollection.filter(function(m, i) {
            return this.isChildOf(m, rootID)
        }, this);
    },

    getNeighbours: function(contextID) {
        var context = this.get(contextID);
        if (!context) {
            return [];
        }

        return context.getNeighbours();
    },

    getScores: function(scoreID, anchorTime) {
        return this.chain()
                        .filter(function(context) {
                            return context.isLeaf();
                        })
                        .map(function(context) {
                            return context.getScore(scoreID);
                        })
                    .value();
    },

    getBaseContext: function(contextID) {
        var context = this.get(contextID),
            baseContextID = contextID,
            parentID;

        while (context) {
            parentID = context.getParentID();

            if (parentID == this.rootID) {
                baseContextID = context.id;
                break;
            }

            context = this.get(parentID);
        }

        return baseContextID;
    },

    getAssociation: function(assocName, sourceContextID, destContextID) {
        if (!this.associations) {
            return null;
        }
        var assocID = assocName;
        if (sourceContextID && destContextID) {
            assocID = sourceContextID + '|' + assocName + '|' + destContextID;
        }
        return this.associations.get(assocID);
    },

    addAssoc: function(assocName, sourceContextID, destContextID, options) {
        return this.associations.addAssoc(assocName, sourceContextID, destContextID, options);
    },

    removeAssoc: function(assocName, sourceContextID, destContextID) {
        return this.associations.removeAssoc(assocName, sourceContextID, destContextID);
    },

    getContextPoints: function(contextID, minAge) {
        return this.points.getContextPoints(contextID, minAge);
    },

    export: function(noJSON) {
        var contextCollection = this;

        // Yes, there is contextCollection.toJSON, but there's likely to be attribute cleanup later on
        var contexts = contextCollection.map(function(contextModel) {
                return contextModel.toJSON();
            }),
            associations = contextCollection.associations.map(function(associationModel) {
                return associationModel.toJSON();
            }),
            points = contextCollection.points.map(function(pointModel) {
                return pointModel.toJSON();
            });

        var exportStruct = {
            'RootID': contextCollection.rootID,
            'Contexts': contexts,
            'Associations': associations,
            'Points': points
        }

        if (!noJSON) {
            return JSON.stringify(exportStruct, null, 4);
        }
        else {
            return exportStruct;
        }
    },

    exportTree: function(rootContextID, includeAttrs) {
        rootContextID = rootContextID || this.rootID;
        var rootContext = this.get(rootContextID);

        function traverse(context) {
            /*'label': context.getNS('Label'),
            'TargetFrequency': context.getNS('TargetFrequency') || null,*/

            var contextStruct = {
                'Children': []
            }

            _.each(includeAttrs, function(attrName) {
                if (Array.isArray(attrName)) {
                    contextStruct[attrName[0]] = context.getNS(attrName[0], attrName[1]);
                }
                else {
                    if (attrName == 'ID') {
                        contextStruct[attrName] = context.id
                    }
                    else {
                        contextStruct[attrName] = context.getNS(attrName);
                    }
                }
            });

            _.each(context.getAssocModels('down'), function(childContext) {
                contextStruct['Children'].push(traverse(childContext));
            }, this);

            return contextStruct;
        }

        return traverse(rootContext);
    }
});

export default Contexts;