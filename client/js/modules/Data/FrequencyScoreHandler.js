/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

// -------------

import calcPeriod from 'benome/modules/Functions/CalcTimelinePeriod_WeightedInterval_StdDev'
import getNow from 'benome/modules/Util/GetNow'

function FrequencyScore(contexts, options) {
    options = options || {};
    this.contexts = contexts;
    this.options = options;
}
_.extend(FrequencyScore.prototype, {
    updateScores: function(focusContext, options) {
        this.contexts.each(function(context) {
            this.calcContextScore(context, options);
        }, this);
    },

    calcContextScore: function(context, options) {
        var options = options || {};
        var anchorTime = options.anchorTime || getNow() / 1000;

        var includeAdjustment = true,
            points = context.getPoints(0, null, anchorTime);

        var timeSince = null,
            timeSinceAdjusted = null,
            score = null,
            recentInterval_5 = null,
            recentInterval_10 = null,
            adjustDelta = context.getNS('AdjustDelta') || 0,
            targetInterval = context.getNS('TargetFrequency') || 0;

        // Simplify structure
        var pts = _.chain(points)
                        .map(function(p) {
                            return p.get('Time');
                        })
                        // FIXME: Causes problems if client and server times are not properly sync'd
                        // Remove points ahead of the anchor time (in case it is in the past or points are in the future)
                        .filter(function(t) {
                            return t <= anchorTime;
                        })
                        // Now largest (newest) to smallest (oldest)
                        .sortBy(function(t) {
                            return -t
                        })
                        .first(10)
                    .value();

        if (pts.length) {
            timeSince = anchorTime - pts[0];
            timeSinceAdjusted = timeSince;
        }

        if (pts.length > 1) {
            //recentInterval_10 = calcPeriod(_.first(pts, 10));

            /*
            Linear proportion between time since last action and recent average interval
            Clamped to between 0.0 and 1.0 for now to keep it simple
            0 = just done
            0.5 = do soon
            1.0 = way overdue
            */

            var scoreInterval;
            if (targetInterval) {
                scoreInterval = targetInterval;
            }
            else {
                recentInterval_5 = calcPeriod(_.first(pts, 5));
                scoreInterval = recentInterval_5;
            }

            timeSinceAdjusted = timeSince;
            if (includeAdjustment) {
                timeSinceAdjusted += adjustDelta;
            }

            score = Math.max(0, Math.min(1.0, 0.5 * (timeSinceAdjusted / scoreInterval)));
        }

        context.setScore('Frequency', score);

        /*context.metaData.set({
            'TimeSince': timeSince,
            'TimeSinceAdjusted': timeSinceAdjusted,
            'CurrentScore': score,
            'RecentInterval_5': recentInterval_5,
            'RecentInterval_10': recentInterval_10,
            'Weight': 1.0
        });*/
    },

    calcInteriorScore: function(context, noTraverse) {
        if (context.isLeaf()) {
            var parentModel = context.getParent();
            parentModel && this.calcInteriorScore(parentModel);
            return;
        }

        var childContexts = _.compact(context.getAssocModels('down'));
        if (!childContexts || !childContexts.length) {
            return;
        }
            
        var totalScore = 0,
            numScores = 0,
            currentScore = 0,
            lastScore;

        _.each(childContexts, function(context) {
            var childScore = context.getScore('Frequency');
            if (!_.isNumber(childScore)) {
                return;
            }

            totalScore += childScore;
            numScores += 1;
        });

        lastScore = context.getScore('Frequency');
        if (numScores > 0) {
            currentScore = totalScore / numScores;
        }

        if (currentScore != lastScore) {
            context.setScore('Frequency', currentScore);

            if (!noTraverse) {
                var parent = context.getParent();
                if (parent) {
                    this.calcInteriorScore(parent);
                }
            }
        }
    },

    updateDistanceScores: function(currentID, previousID, traverseState, result) {
        var currentContext = this.contexts.get(currentID),
            result = result || [];

        if (!currentContext) {
            return result;
        }

        var isFocus = !previousID,
            traverseState = traverseState || {
                'FocusDistance': 0,
                'FocusDescendant': false,
                'FocusID': currentID
            };

        var children = currentContext.getAssoc('down').slice(),
            parents = currentContext.getAssoc('up').slice(),
            neighbours = children.concat(parents),
            score = 0;

        if ((Date.now() / 1000) - currentContext.getNS('Timestamp') < this.options.newContextBumpTime) {
            score = 1.0;
        }
        else {
            //score = currentContext.metaData.get('CurrentScore') || 0;
            score = currentContext.getScore('Frequency') || 0;
        }

        var distanceScore = score;
        if (traverseState.FocusDistance) {
            distanceScore /= traverseState.FocusDistance;
        }

        result.push(currentContext);
        currentContext.setDistanceScore('Frequency', distanceScore);
        
        traverseState = _.extend({}, traverseState);
        traverseState.FocusDistance += 1;

        _.each(neighbours, function(neighbourID) {
            if (neighbourID == previousID) {
                return;
            }

            var nextContext = this.contexts.get(neighbourID);
            if (!nextContext) {
                //console.log(('Neighbour not in contexts: ' + neighbourID));
                return;
            }
            this.updateDistanceScores(neighbourID, currentID, traverseState, result);
        }, this);

        if (isFocus) {
            this.contexts.trigger('ClusterScoresUpdated', this);
        }

        return result;
    }
});

export default FrequencyScore;