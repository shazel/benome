/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

// -------------

function GraphQuery(context, traverser, options) {
	this.options = options || {};
    this.context = context;
    this.traverser = traverser
}
_.extend(GraphQuery.prototype, {
	exec: function(options) {
		options = options || {};

		var traverseState = {},
			localState = {};

		this.nodeList = [];
		this.lineages = {};
		this.getNodes(this.context, this.traverser, localState, traverseState, _.extend({}, this.options, options));

		this.filteredNodes = this.filterNodes(this.context, this.traverser, this.nodeList);

		// Transform into a flat structure
		this.nodeMap = this.fillInterior(this.context, this.traverser, this.filteredNodes, this.lineages);

		// Extract the relationships
		this.resultMap = this.formatResults(this.context, this.traverser, this.nodeMap);
		return this.resultMap;
	},

	getNodes: function(context, traverser, localState, traverseState, options) {
		var currentLineage = localState.lineage || [];
		var depth = localState.depth || 1;

		if (depth == 1) {
			currentLineage = currentLineage.concat(context);
		}

		// Make sure depth is > 1 so that we don't terminate immediately if focus is a leaf node
		if (depth > 1 && traverser.terminate(context, localState, traverseState, options)) {
			this.nodeList.push(context);
    		this.lineages[context.id] = localState.lineage;
		}
		else {
			var parentID = localState.parentID;
			var nextLevel = traverser.getNextLevel(context, localState, traverseState, options);
			_.each(nextLevel, function(neighbourContext) {
				var neighbourLocalState = {
					depth: depth + 1,
					lineage: currentLineage.concat(neighbourContext),
					parentID: context.id
				};

				if (neighbourContext.id != parentID) {
					this.getNodes(neighbourContext, traverser, neighbourLocalState, traverseState, options);
				}
			}, this);
		}
	},

	filterNodes: function(context, traverser, nodeList, options) {
		return _.filter(nodeList, function(context) {
			return traverser.filter(context, traverser.options);
		});
	},

	fillInterior: function(context, traverser, nodeList, lineages, options) {
		var nodeMap = {};
		_.each(nodeList, function(context) {
			_.each(lineages[context.id], function(interiorContext) {
				nodeMap[interiorContext.id] = interiorContext;
			});
		});

		return nodeMap;
	},

	formatResults: function(context, traverser, nodeMap, options) {
		var result = {};

		_.each(nodeMap, function(context) {
			var neighbourIDs = context.getNeighbours(),
				neighbourList = [];

			_.each(neighbourIDs, function(neighbourID) {
				if (neighbourID in nodeMap) {
					neighbourList.push(neighbourID)
				}
			});

			var childSortOrder = traverser.options.sortOrder[context.id] || {};
			result[context.id] = _.sortBy(neighbourList, function(contextID) {
	            return childSortOrder[contextID] || 0;
	        });
		});

		return result;
	},

	depthFirstTraverse: function(context, traverser, localState, traverseState, options) {
		options = options || {};
		var depth = localState.depth || 1;

		traverseState.traverseOptions = traverser.options;
		options.traverseState = traverseState;

		var currentLineage = localState.lineage || [];

		if (depth == 1) {
			currentLineage = currentLineage.concat(context);
		}

		var localState = {
			depth: depth,
			lineage: currentLineage,
			parentID: localState.parentID || null
		};

		var tmpResult = [];
		if (traverser.terminate(context, localState, traverseState, options)) {
			if (traverser.filter(context, localState, traverseState, options)) {
				this.resultMap[context.id] = [localState.parentID];
			}
		}
		else {
			var levelResult = [];
			var parentID = localState.parentID; //context.getParentID();
			var nextLevel = traverser.getNextLevel(context, localState, traverseState, options);

			_.each(nextLevel, function(neighbourContext) {
				var neighbourLocalState = {
					depth: depth + 1,
					lineage: currentLineage.concat(neighbourContext),
					parentID: context.id
				};

				if (neighbourContext.id == parentID) {
					if (traverser.filter(neighbourContext, neighbourLocalState, traverseState, options)) {
						levelResult.push(neighbourContext.id);
					}
				}
				else {
					this.depthFirstTraverse(neighbourContext, traverser, neighbourLocalState, traverseState, options);
					if (traverser.filter(neighbourContext, neighbourLocalState, traverseState, options)) {
						levelResult.push(neighbourContext.id);

						/*var x = '';
						_.each(neighbourLocalState.lineage, function(context) {
							x += ' -> ' + context.getNS('Label')
						});
						console.log(x, context.id, neighbourContext.id);*/
					}
				}
			}, this);

			if (levelResult.length) {
				var childSortOrder = traverser.options.sortOrder[context.id] || {};
		        var levelResult = _.sortBy(levelResult, function(contextID) {
		            return childSortOrder[contextID] || 0;
		        });

				this.resultMap[context.id] = levelResult;
			}
		}
	}
});

export default GraphQuery;