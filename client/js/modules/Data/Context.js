/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'
import Backbone from 'backbone'

// -------------

import getNow from 'benome/modules/Util/GetNow'

// -------------

var ContextProperties = Backbone.Model.extend({
    modelType: 'ContextProperties',
    initialize: function() {}
});

var ContextMetaData = Backbone.Model.extend({
    modelType: 'ContextMetaData',
    initialize: function() {}
});

var Context = Backbone.Model.extend({
    modelType: 'Context',
    idAttribute: 'ID',

    url: function() {
        return '/app/data/context/' + this.id;
    },

    initialize: function(attributes, options) {
        options = options || {};
        _.bindAll(this, 'onPointChanged');

        if (!this.getNS('Timestamp')) {
            this.set('1__Timestamp', getNow() / 1000, {silent: true});
        }

        this.properties = new ContextProperties(this.getLocal('Properties') || {});
        this.metaData = new ContextMetaData(this.getLocal('MetaData') || {});

        this.scores = {};

        this.linkID = null;
        this.linkModel = null;

        this.on('PointChanged', this.onPointChanged);
        this.on('change:1__AdjustDelta', this.onPointChanged);
    },

    getColor: function(fresh, baseLightnessAdjust) {
        if (this.collection && this.collection.clusterController) {
            return this.collection.clusterController.cluster.getColor(this.id, fresh, baseLightnessAdjust);
        }
    },

    traverseDown: function(cb, state, depth) {
        state = state || {};
        depth = depth || 1;

        cb(this, depth, state);
        _.each(this.getAssocModels('down'), function(childContext) {
            childContext.traverseDown(cb, state, depth + 1);
        }, this);
    },

    getAssocByAttr: function(assocName, attrName, attrVal) {
        var assocModels = this.getAssocModels(assocName);
        return _.filter(assocModels, function(model) {
            if (model.getNS(attrName) == attrVal) {
                return true;
            }
        }, this);
    },

    getNS: function(name, namespaceID) {
        namespaceID = namespaceID || 1;
        var key = namespaceID + '__' + name;
        return Backbone.Model.prototype.get.call(this, key);
    },

    onPointChanged: function(point) {
        this.set('PointChanged', new Date().getTime());
        /*var scoreChanged = this.updateScore();
        if (scoreChanged) {
            this.set('PointChanged', new Date().getTime());
        }*/
    },

    parse: function(response, x, y) {
        if (this.properties) {
            response && response.Properties && this.properties.set(response.Properties);
        }
        else {
            this.properties = new ContextProperties(response.Properties || {});
        }

        if (this.metaData) {
            response && response.MetaData && this.metaData.set(response.MetaData);
        }
        else {
            this.metaData = new ContextMetaData(response.MetaData || {});
        }

        return response;
    },

    toJSON: function(options) {
        var attributes = _.clone(this.attributes);

        delete attributes.MetaData;
        delete attributes.Points;

        attributes.Properties = this.properties.toJSON();

        return attributes;
    },

    getLabel: function() {
        return this.getNS('Label');
    },

    getPoints: function(minAge, refTime) {
        minAge = minAge || 0;
        if (this.collection) {
            return this.collection.points.getContextPoints(this.id, minAge, refTime);
        }
    },

    isLink: function() {
        var label = this.getLocalNS('Label') || '';
        label += '';
        return label.indexOf('link-') == 0;
    },

    isLeaf: function() {
        return this.getAssoc('down').length == 0;
    },

    hasChild: function(contextID) {
        return _.indexOf(this.getAssoc('down'), contextID) != -1;
    },

    hasChildren: function() {
        return this.numChildren() > 0;
    },

    numChildren: function() {
        return this.getAssoc('down').length;
    },

    getLinkID: function() {
        var label = this.getLocalNS('Label') || '';
        label += '';
        return label.substring(5);
    },

    initLink: function() {
        if (this.linkID) {
            return;
        }

        if (this.isLink()) {
            this.linkID = this.getLinkID();
            this.linkModel = this.collection.get(this.linkID);
        }
    },

    get: function(attrName) {
        var val = null;
        this.initLink();

        if (this.linkModel) {
            val = this.linkModel.get(attrName);
        }
        else {
            val = this.getLocal(attrName);
        }
        return val;
    },

    getLocal: function(attrName) {
        return Backbone.Model.prototype.get.apply(this, arguments);
    },

    getLocalNS: function(attrName, namespaceID) {
        namespaceID = namespaceID || 1;
        var key = namespaceID + '__' + attrName;
        return Backbone.Model.prototype.get.call(this, key);
    },

    set: function(attrs, options) {
        this.initLink();

        if (this.linkModel) {
            return this.linkModel.set(attrs);
        }
        else {
            return Backbone.Model.prototype.set.apply(this, arguments);
        }
    },

    setParent: function(parentID) {
        this.collection && this.collection.associations.setAssoc('up', this.id, parentID);
    },

    getParentID: function() {
        if (this.isRoot) {
            return null;
        }
        return this.getAssoc('up')[0];
    },

    getParent: function() {
        return this.collection && this.collection.get(this.getParentID());
    },

    getDepth: function() {
        var depth = 1,
            parent = this.getParent();

        while (parent) {
            parent = parent.getParent();
            depth += 1;
        }

        return depth;
    },

    toDepth: function(targetDepth, baseDepth) {
        // If baseDepth is provided, targetDepth becomes relative
        var currentDepth = this.getDepth();
        if (baseDepth) {
            targetDepth += baseDepth;
        }

        if (targetDepth == 0 || currentDepth <= targetDepth) {
            return this;
        }

        var nextContext = this;
        while (currentDepth > targetDepth) {
            nextContext = nextContext.getParent();
            currentDepth -= 1;
        }

        return nextContext;
    },

    getAssoc: function(assocName) {
        if (this.collection) {
            var contextIDs = this.collection.associations.getContextAssoc(this.id, assocName);
            return _.filter(contextIDs, function(contextID) {
                return this.collection.get(contextID);
            }, this);
        }
        return [];
    },

    getAssocModels: function(assocName) {
        var _this = this;
        return _.compact(_.map(this.getAssoc(assocName), function(contextID) {
            return _this.collection.get(contextID);
        }));
    },

    getNeighbours: function() {
        return this.getAssoc('up').concat(this.getAssoc('down'));
    },

    getNeighbourModels: function() {
        return this.getAssocModels('up').concat(this.getAssocModels('down'));
    },

    hasNeighbour: function(neighbourID) {
        return _.contains(this.getNeighbours(), neighbourID);
    },

    getScore: function(scoreID) {
        var score = this.scores[scoreID];
        return _.isNumber(score) ? score : null;
    },

    setScore: function(scoreID, scoreValue) {
        this.scores[scoreID] = scoreValue;
    },

    getDistanceScore: function(scoreID) {
        return this.getScore(scoreID + 'Distance');
    },

    setDistanceScore: function(scoreID, scoreValue) {
        this.setScore(scoreID + 'Distance', scoreValue);
    }
});

export default Context;