/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

// -------------

import streamToFrequencyTarget from 'benome/modules/Functions/StreamToFrequencyTarget'
import     calcPeriod from 'benome/modules/Functions/CalcTimelinePeriod_WeightedInterval_StdDev'
import     aggregateLayerData from 'benome/modules/Functions/AggregateLayerData'

function computeClusterGraphs(rootContext, graphWindow, numSegments, anchorTime, options) {
    options = options || {};
    anchorTime = anchorTime || Date.now() / 1000;

    if (!rootContext) {
        console.log('computeClusterGraphs: missing rootContext')
        return;
    }

    if (options.force) {
        rootContext.collection.each(function(context) {
            context.graphData = null;
            context.aggregateGraphData = null;
        });
    }

    // Calculate graph data and target interval for each leaf context
    // This is so the data can be easily aggregated
    // FIXME: Should be done elsewhere and invalidated appropriately
    rootContext.collection.each(function(context) {
        if (!context.isLeaf() || context.graphData) {
            return;
        }

        var pointTimes = _.map(context.getPoints(), function(point) {
            return point.get('Time');
        });

        var targetInterval = context.getNS('TargetFrequency');
        if (!targetInterval) {
            if (options.explicitTargetsOnly) {
                return;
            }
            else {
                targetInterval = calcPeriod(pointTimes);
            }
        }

        context.graphData = streamToFrequencyTarget(pointTimes, graphWindow, numSegments, {
            targetInterval: targetInterval,
            anchorTime: anchorTime
        });
    });

    aggregateLayerData(rootContext, true);
}

export default computeClusterGraphs;