/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

import DataConstructor from 'benome/modules/Util/DataConstructor'

/*
    Transform a JSON structure into a collection-friendly form, assigning IDs to each node
*/
function transformJSONToCollection(data, rootContextID) {
    rootContextID = rootContextID || 1
    var DC = new DataConstructor(data.ID || rootContextID);

    function traverse(struct, parentID) {
        const children = struct.children || struct.Children,
            label = struct.label || struct.Label

        if (!parentID) {
            var attributes = {
                ...struct.attributes,
                ...struct.Attributes
            }
            Object.keys(struct).forEach(key => {
                if (key in {Children: 1, children: 1, Label: 1, label: 1, ID: 1}) {
                    return
                }
                var attrKey = key
                if (key.indexOf('__') < 0) {
                    attrKey = '1__' + key
                }
                attributes[attrKey] = struct[key]
            })
            parentID = DC.add(label, null, null, attributes)
        }

        _.each(children, function(childStruct) {
            const childLabel = childStruct.label || childStruct.Label

            var childAttributes = {
                ...childStruct.attributes,
                ...childStruct.Attributes
            }
            Object.keys(childStruct).forEach(key => {
                if (key in {Children: 1, children: 1, Label: 1, label: 1, ID: 1}) {
                    return
                }
                var attrKey = key
                if (key.indexOf('__') < 0) {
                    attrKey = '1__' + key
                }
                childAttributes[attrKey] = childStruct[key]
            })

            var nextParentID = DC.add(childLabel, parentID, childStruct.ID || null, childAttributes);
            traverse(childStruct, nextParentID);
        });
    }

    traverse(data);
    return DC.getData();
}

export default transformJSONToCollection;