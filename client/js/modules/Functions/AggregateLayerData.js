/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

// -------------

import sumArrays from 'benome/modules/Util/SumArrays'

function aggregateLayerData(baseContext, weighted) {
    baseContext.traverseDown(function(context, traverseDepth, state) {
        if (context.aggregateGraphData) {
            return;
        }
        if (context.isLeaf()) {
            context.aggregateGraphData = context.graphData;
            context.aggregateCount = 1;
            return;
        }

        var aggregateList = [],
            weightedAggregateList = [],
            subAggregateCount = 0,
            downAssocContexts = context.getAssocModels('down');

        _.each(downAssocContexts, function(assocContext) {
            if (!assocContext.aggregateGraphData) {
                aggregateLayerData(assocContext, weighted);
            }
            if (assocContext.aggregateGraphData) {
                aggregateList.push(assocContext.aggregateGraphData)
                subAggregateCount += assocContext.aggregateCount;
            }
        });

        // Second pass re-weights
        _.each(downAssocContexts, function(assocContext) {
            if (assocContext.aggregateGraphData) {
                var aggregateCount = assocContext.aggregateCount,
                    aggregateData = assocContext.aggregateGraphData,
                    weightFactor = aggregateCount / subAggregateCount;

                var adjustedAggregateGraphData = _.map(aggregateData, function(val) {
                    return parseInt(val * weightFactor);
                });

                assocContext.weightedAggregateGraphData = adjustedAggregateGraphData;
                weightedAggregateList.push(adjustedAggregateGraphData);
            }
        });

        if (aggregateList.length > 0) {
            context.aggregateGraphData = _.map(sumArrays(aggregateList), function(val) {
                return val / aggregateList.length;
            });
            context.weightedAggregateGraphData = sumArrays(weightedAggregateList);
            context.aggregateCount = subAggregateCount;

            /*var adjustedAggregateGraphData = _.map(aggregateList, function(aggregateDataPair) {
                var aggregateCount = aggregateDataPair[0],
                    aggregateData = aggregateDataPair[1],
                    weightFactor = aggregateCount / subAggregateCount;

                console.log(context.id, weightFactor);

                return _.map(aggregateData, function(val) {
                    return parseInt(val * weightFactor);
                });
            });

            context.aggregateGraphData = sumArrays(adjustedAggregateGraphData);
            console.log(context.id, subAggregateCount, context.aggregateGraphData);

            _.map(sumArrays(adjustedAggregateGraphData), function(val) {
                return val / aggregateList.length;
            });
            */
            
        }
    });
}

/*
Weighting the aggregates:
    Before summing the arrays, modify the values according to relative weight of its lineage
    Get the 
    Figure out what the divisor is:


*/

export default aggregateLayerData;