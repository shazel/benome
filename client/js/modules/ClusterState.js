/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import _ from 'underscore'

function ClusterState(globalCluster) {
    _.bindAll(this, 'hide');
    this.clusters = {};
    this.globalCluster = globalCluster;
};

_.extend(ClusterState.prototype, {
    add: function(cluster) {
    	this.clusters[cluster.clusterID] = cluster;
    },

    get: function(clusterID) {
        return this.clusters[clusterID];
    },

    hide: function() {
        _.each(this.clusters, function(cluster) {
            cluster.hideAll();
        });
    },

    getZIndex: function(clusterID) {
        var lastClusterZIndex = this.lastClusterZIndex || 20000;
        if (clusterID != this.lastClusterID) {
            lastClusterZIndex += 1000;
            this.lastClusterZIndex = lastClusterZIndex;
        }
        return lastClusterZIndex;
    },

    isGlobal: function(cluster) {
        return !this.globalCluster || this.globalCluster.cluster.clusterID == cluster.clusterID;
    }
});

export default ClusterState;