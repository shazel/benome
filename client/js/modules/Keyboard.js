/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import $ from 'jquery'
import Backbone from 'backbone'
import _ from 'underscore'
import Mousetrap from 'mousetrap'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function Keyboard(options) {
    this.initKeyboard(options);
}

_.extend(Keyboard.prototype, {
    initKeyboard: function(options) {
        _.bindAll(this, 'onFocusChanged', 'onNavKey');

        this.rhKeyMap = {
            'm': 1,
            'j': 4,
            'u': 7,
            'i': 8,
            'o': 9,
            'l': 6,
            '.': 3,
            ',': 2,
            'k': 5
        };

        var keypadStartAngle = 135,
            keypadAngleIncrement = 45,
            normalizeAngle = this.normalizeAngle;

        this.keypadAngles = _.map([1,4,7,8,9,6,3,2], function(keyID, i) {
            var keypadAngle = keypadStartAngle + (i * keypadAngleIncrement);
            return [
                keyID,
                normalizeAngle(keypadAngle),
                normalizeAngle(keypadAngle - (keypadAngleIncrement / 2)),
                normalizeAngle(keypadAngle + (keypadAngleIncrement / 2))
            ];
        });

        function arcOverlaps(testLow, testHigh, baseLow, baseHigh) {
            testLow = normalizeAngle(testLow);
            testHigh = normalizeAngle(testHigh);
            baseLow = normalizeAngle(baseLow);
            baseHigh = normalizeAngle(baseHigh);

            return (testLow >= baseLow && testLow <= baseHigh) ||
                    (testHigh <= baseHigh && testHigh >= baseLow);
        }

        var _this = this;
        Mousetrap.prototype.stopCallback = function() {
            return false;
        }

        // Completely hijack the alt key to prevent accidental triggering of browser functions.
        $(document).on('keydown', function(e) {
            if (e.altKey || e.key == 'Alt') {
                Global.trigger('AltDown');
                e.preventDefault();
                return false;
            }
        });

        $(document).on('keyup', function(e) {
            if (e.key == 'Alt') {
                Global.trigger('AltUp');
                e.preventDefault();
                return false;
            }
        });

        var isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0,
            platformModifier = isMac ? 'ctrl' : 'alt';
        
        // Bind to home row keys
        var homeRow = _.map(['m', 'j', 'u', 'i', 'o', 'l', '.', ',', 'k'], function(key) {
            return platformModifier + '+' + key;
        });
        Mousetrap.bind(homeRow, this.onNavKey);

        // Bind to keypad numbers (depends on numlock being on)
        Mousetrap.bind(['1', '2', '3', '4', '5', '6', '7', '8', '9', '.'], this.onNavKey);

        Mousetrap.bind('enter', function(e) {
            if (e.code == 'NumpadEnter') {
                _this.trigger('AddPoint', _this, _this.cluster);
                return false;
            }
            else {
                _this.trigger('SaveLayer', _this, _this.cluster);
            }
        });

        Mousetrap.bind('+', function(e) {
            if (e.code == 'NumpadAdd') {
                _this.trigger('NarrowFilter', _this, _this.cluster);
                return false;
            }
        });

        Mousetrap.bind('alt+plus', function(e) {
            if (e.code == 'NumpadAdd') {
                _this.trigger('NarrowestFilter', _this, _this.cluster);
                return false;
            }
        });

        Mousetrap.bind('-', function(e) {
            if (e.code == 'NumpadSubtract') {
                _this.trigger('WidenFilter', _this, _this.cluster);
                return false;
            }
        });

        Mousetrap.bind('alt+-', function(e) {
            _this.trigger('WidestFilter', _this, _this.cluster);
            return false;
        });

        Mousetrap.bind('*', function(e) {
            _this.trigger('ModifyContext', _this, _this.cluster);
            return false;
        });

        Mousetrap.bind('/', function(e) {
            if (e.code == 'NumpadDivide') {
                _this.trigger('ToggleViewMode', _this, _this.cluster);
                return false;
            }
        });

        Mousetrap.bind(['0', 'alt+0'], function(e, combo) {
            if (e.code != 'Numpad0') {
                return;
            }

            if (e.altKey) {
                _this.trigger('CreateSiblingContext', _this, _this.cluster);
            }
            else {
                _this.trigger('CreateContext', _this, _this.cluster);
            }
            return false;
        });

        Mousetrap.bind(['alt+ctrl+space', 'alt+shift+space'], function(e, combo) {
            if (e.altKey && e.ctrlKey) {
                _this.trigger('CreateSiblingContext', _this, _this.cluster);
            }
            else if (e.altKey && e.shiftKey) {
                _this.trigger('CreateContext', _this, _this.cluster);
            }
            e.preventDefault();
            return false;
        });

        Mousetrap.bind(['alt+ins', 'alt+shift+ins'], function(e, combo) {
            if (e.altKey && e.shiftKey) {
                _this.trigger('CreateSiblingContext', _this, _this.cluster);
            }
            else if (e.altKey) {
                _this.trigger('CreateContext', _this, _this.cluster);
            }
            e.preventDefault();
            return false;
        });

        Mousetrap.bind(['alt+shift+numpad5', 'alt+shift+k'], function(e) {
            _this.focusRoot();
            return false;
        });

        Mousetrap.bind('esc', function(e) {
            _this.trigger('CancelLayer', _this, _this.cluster);
            return false;
        });

        Mousetrap.bind('alt+del', function(e) {
            _this.trigger('DeleteContext', _this, _this.cluster);
             return false;
        });

        Mousetrap.bind('alt+shift+`', function(e) {
            Global.trigger('ShowHelp', _this, _this.cluster);
            e.preventDefault();
            return false;
        });

        Mousetrap.bind('alt+pagedown', function(e) {
            Global.trigger('ExportJSON', _this.cluster, _this.cluster.focusID);
            e.preventDefault();
            return false;
        });

        Mousetrap.bind('alt+pageup', function(e) {
            Global.trigger('ExportText', _this.cluster, _this.cluster.focusID);
            e.preventDefault();
            return false;
        });

        Mousetrap.bind('alt+end', function(e) {
            Global.trigger('ExportMarkdown', _this.cluster, _this.cluster.focusID);
            e.preventDefault();
            return false;
        });

        Mousetrap.bind('alt+home', function(e) {
            Global.trigger('ExportHtml', _this.cluster, _this.cluster.focusID);
            e.preventDefault();
            return false;
        });

        Mousetrap.bind('alt+`', function(e) {
            if (e.key == '`') {
                Global.trigger('ShowAdmin', _this, _this.cluster);
                return false;
            }
        });

        this.on('AltNumpadDecimal', function(e) {
            _this.trigger('DeleteContext', _this, _this.cluster);
            return false;
        });
    },

    setKeyboardState: function(contextList) {
        this.keyboardContextList = contextList || null;
    },

    normalizeAngle: function(angle, highAngle) {
        var result = Math.atan2(Math.sin(angle * Math.PI / 180), Math.cos(angle * Math.PI / 180)) * (180 / Math.PI)
        return result + 180;
    },

    angleContained: function(testAngle, baseLow, baseHigh) {
        testAngle = this.normalizeAngle(testAngle);
        baseLow = this.normalizeAngle(baseLow, baseHigh);
        baseHigh = this.normalizeAngle(baseHigh);

        return testAngle >= baseLow && testAngle <= baseHigh;
    },

    onFocusChanged: function(cluster, focusID, focusModel, lastFocusID, lastFocusModel, layoutData) {
        if (!focusModel) {
            return;
        }

        this.clusterStateChanged(cluster, layoutData);
    },

    clusterStateChanged: function(cluster, layoutData) {
        if (!layoutData) {
            return;
        }

        var focusID = cluster.focusID,
            focusData = layoutData.data[focusID];

        if (!focusData) {
            console.log('focusData not found');
            return;
        }

        var startAngle = focusData.startAngle,
            orderedVisibleNeighbours = focusData.orderedNeighbours;

        if (!orderedVisibleNeighbours) {
            return;
        }

        var angleIncrement = 360 / orderedVisibleNeighbours.length;

        var y = _.map(orderedVisibleNeighbours, function(neighbourID, i) {
            var neighbourAngle = startAngle + (i * angleIncrement);
            return [
                neighbourID,
                this.normalizeAngle(neighbourAngle),
                this.normalizeAngle(neighbourAngle - (angleIncrement / 2)),
                this.normalizeAngle(neighbourAngle + (angleIncrement / 2))
            ];
        }, this);
        this.setKeyboardState(y);
    },

    setCluster: function(cluster) {
        this.cluster = cluster;
        this.cluster.on('FocusChanged', this.onFocusChanged);
        this.clusterStateChanged(cluster, cluster.lastLayoutData);
    },

    onNavKey: function(e) {
        var key = e.key;
        if (key == '.') {
            if (e.code == 'NumpadDecimal') {
                if (e.altKey) {
                    // Numpad decimal is bound to something else
                    this.trigger('AltNumpadDecimal', e);
                }

                return false;
            }
            else {
                if (!e.altKey) {
                    return;
                }
            }
        }

        var topRowDigits = ['Digit1', 'Digit2', 'Digit3', 'Digit4', 'Digit5', 'Digit6', 'Digit7', 'Digit8', 'Digit9', 'Digit0'];
        if (_.contains(topRowDigits, e.code)) {
            return;
        }

        var keyID = this.rhKeyMap[key] || parseInt(key),
            _this = this;

        if (this.keyboardContextList && this.cluster) {
            if (keyID == 5) {
                this.focusParent();
                return false;
            }

            var keypadDetails = _.find(this.keypadAngles, function(x) {
                return x[0] === keyID;
            });

            var matchedContexts = _.filter(this.keyboardContextList, function(contextDetails) {
                return this.angleContained(contextDetails[1], keypadDetails[2], keypadDetails[3])
            }, this);

            if (matchedContexts.length == 0) {
                // Grab the closest context
                var result = _.chain(this.keyboardContextList)
                    .map(function(contextDetails) {
                        var angle1 = _this.normalizeAngle(contextDetails[1]),
                            angle2 = _this.normalizeAngle(keypadDetails[1]);

                        if (Math.abs(angle1 - angle2) > 180) {
                            // All angles are to be acute.
                            if (angle1 > angle2) {
                                angle1 -= 360;
                            }
                            else if (angle2 > angle1) {
                                angle2 -= 360;
                            }
                        }

                        var distance = Math.abs(angle1 - angle2);
                        return [contextDetails, distance];
                    })
                    .sortBy(function(distances) {
                        return distances[1];
                    })
                    .first()
                .value();

                if (result) {
                    this.cluster.setFocus(result[0][0], true);
                }
            }
            else if (matchedContexts.length == 1) {
                // Switch focus directly
                this.cluster.setFocus(matchedContexts[0][0], true);
            }
            else {
                // Disambiguate somehow
                _.each(matchedContexts, function(x) {
                    console.log(this.cluster.contexts.get(x[0]).getNS('Label'));
                });
            }
        }

        return false;
    },

    focusParent: function() {
        var focusContext = this.cluster.contexts.get(this.cluster.focusID);

        if (focusContext) {
            var parentContext = focusContext.getParent();
            if (parentContext) {
                this.cluster.setFocus(parentContext.id, true);
            }
        }
    },

    moveFocusUp: function() {
        var cluster = this.cluster,
            focusID = cluster.focusID,
            focusContext = cluster.contexts.get(focusID);

        if (focusContext) {
            var parentContext = focusContext.getParent();
            if (parentContext) {
                var grandParentContext = parentContext.getParent();
                if (grandParentContext) {
                    var successCallback = function(contextID) {
                        cluster.setFocus(grandParentContext.id);

                        // Hack: double-rendered because re-render while keeping focus
                        //  but changing the structure did not work as expected
                        cluster.render({
                            noAnimate: true
                        });
                        cluster.setFocus(focusID, true);
                    }

                    Global.trigger('ReparentContext', focusContext.id, parentContext.id, grandParentContext.id, cluster.clusterID, successCallback);
                }
            }
        }
    },

    focusRoot: function() {
        this.cluster.setFocus(this.cluster.rootID, true);
    }
});

_.extend(Keyboard.prototype, Backbone.Events);
export default Keyboard;