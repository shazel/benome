/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import     Backbone from 'backbone'
import     _ from 'underscore'
import     Hammer from 'hammerjs'
Backbone.$ = $;

// -------------

var DestroyerView = Backbone.View.extend({
    tagName: 'div',
    className: 'destroyer-view',

    events: {
    },

    initialize: function(options) {
        _.bindAll(this, 'dragHandler', 'dropHandler', 'dropFinished');

        this.el.setAttribute('BDragSource', '1');
        this.el.setAttribute('BDropTarget', '1');
        this.el.setAttribute('BDragHoldTarget', '0');
        this.$el.data('ViewRef', this);
    },

    dragHandler: function(dragView, dragDetails) {
        return {
            '$dragProxyEl': dragView.$el,
            'proxyClass': 'drag-proxy-destroyer'
        }
    },

    dropHandler: function(dropView, dragView, dragDetails, dropDetails) {
        this.trigger('IncomingDrop', dragView, dragDetails, dropDetails, this.dropFinished);
    },

    dropFinished: function(result, dragDetails, dropDetails) {
        if (result) {
            dragDetails.$dragProxy.animate({
                width: '0px',
                height: '0px',
                left: dropDetails.currentX,
                top: dropDetails.currentY
            }, 
            {
                duration: 400,
                complete: function() {
                    $(this).remove();
                }
            });

            return {
                keepProxy: true
            }
        }
    },

    render: function() {
        return this;
    },

    hide: function() {
        this.$el.hide();
    },

    show: function() {
        this.$el.show();
    }
});
_.extend(DestroyerView.prototype, Backbone.Events)

export default DestroyerView;