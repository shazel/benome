/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import     Backbone from 'backbone'
import     _ from 'underscore'
Backbone.$ = $;

// -------------

var HTMLView = Backbone.View.extend({
    tagName: 'div',
    className: 'html-view',

    events: {
    },

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'hide');

        this.$el.addClass('content-box');
        this.$overlay = options.$overlay;
        this.$overlay.click(this.hide);

        this.$close = $('.close-html', this.$el)
                        .click(this.hide);
    },

    render: function(options) {
        options = options || {};

        return this;
    },

    show: function(options) {
        options = options || {};

        if (options.html) {
            this.$el
                .empty()
                .append(options.html);
        }

        this.$overlay.show();
        this.$el.show();
        this.visible = true;
    },

    hide: function() {
        this.visible = false;
        this.$overlay.hide();
        this.$el.hide();
    }
});

export default HTMLView; 