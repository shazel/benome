/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import $ from 'jquery'
import Backbone from 'backbone'
import _ from 'underscore'
import screenfull from 'benome/lib/screenfull.js'
Backbone.$ = $;

// -------------

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

var viewHTML = '' +
    '<div class="content-box admin-view">' +
    '    <div class="section">' +
    '        <h4>Import &amp; Export</h4>' +
    '    <div class="export">' +
    '        <div class="button export-markdown">Export Markdown</div>' +
    '        <div class="button export-html">Export HTML</div>' +
    '        <div class="button export-text">Export Text</div>' +
    '        <div class="button export-json">Export JSON</div>' +
    '    </div>' +
    '    <br>' + 
    '    <div class="import">' +
    '        <textarea id="import-json-text" style="height: 2em;"></textarea> <div class="button import-json">Import Pasted</div>' +
    '    </div>' +
    '    </div>' +
    '' + 
    '    <div class="section change-auth">' +
    '        <h4>Change Password</h4>' +
    '        <form>' +
    '            <div class="old-passphrase">' +
    '                <input type="password" placeholder="Old passphrase"></input>' +
    '            </div>' +
    '            <div class="new-passphrase">' +
    '                <input type="password" placeholder="New passphrase"></input>' +
    '            </div>' +
    '            <div class="new-passphrase2">' +
    '                <input type="password" placeholder="New passphrase again"></input>' +
    '            </div>' +
    '            <div class="button-container">' +
    '                <div class="button change">Go</div>' +
    '            </div>' +
    '        </form>' +
    '    </div>' +
    '' + 
    '    <div class="section">' +
    '        <h4>Functions</h4>' +
    '        <div class="button toggle-fullscreen">Toggle Fullscreen</div>' +
    '        <div class="button switch-app">WebApp View</div>' +
    '        <div class="button logout-session">Logout</div>' +
    '    </div>' + 
    '' + 
    '    <div class="button close-admin">Close</div>' +
    '</div>';

var AdminView = Backbone.View.extend({
    tagName: 'div',
    className: 'admin-view',

    events: {
        'click .change': 'changePassword',
        'click .logout-session': 'logout',
        'click .toggle-fullscreen': 'onToggleFullScreen',
        'click .switch-app': 'onSwitchApp',
    },

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'hide', 'newPasswordKeydown', 'onToggleFullScreen', 'onSwitchApp', 'onOverlayClick');

        this.localOnly = options.localOnly;
        this.$el = $(viewHTML);
        this.delegateEvents();

        this.$overlay = options.$overlay;

        this.$oldPassword = $('.old-passphrase input', this.$el);
        this.$newPassword = $('.new-passphrase input', this.$el);
        this.$newPassword2 = $('.new-passphrase2 input', this.$el);
        this.$newPassword2.bind('keydown', this.newPasswordKeydown);

        this.$importJSONText = $('#import-json-text', this.$el);

        this.$close = $('.close-admin', this.$el)
                        .click(this.hide);

        $('.export > .export-json', this.$el).click(function() {
            Global.trigger('ExportJSON');
        });

        $('.export > .export-text', this.$el).click(function() {
            Global.trigger('ExportText');
        });

        $('.export > .export-html', this.$el).click(function() {
            Global.trigger('ExportHtml');
        });

        $('.export > .export-markdown', this.$el).click(function() {
            Global.trigger('ExportMarkdown');
        });

        var _this = this;
        $('.import > .import-json', this.$el).click(function() {
            Global.trigger('ImportJSON', null, null, _this.$importJSONText.val());
        });

        if (this.localOnly) {
            $('.change-auth, .logout-session', this.$el).hide();
        }
    },

    onOverlayClick: function() {
        if (!this.visibleTimestamp) {
            return
        }

        if (Date.now() - this.visibleTimestamp > 1000) {
            this.hide();
        }

        return false;
    },

    onToggleFullScreen: function() {
        if (screenfull.enabled) {
            screenfull.toggle();
        }
    },

    onSwitchApp: function() {
        this.trigger('SwitchApp');
    },

    newPasswordKeydown: function(e) {
        if (e.keyCode == 13) {
            this.changePassword();
        }
        else if (e.keyCode == 27) {
            this.hide();
        }
    },

    logout: function() {
        this.trigger('LogoutUser', this.hide);
    },

    changePassword: function() {
        var oldPassword = this.$oldPassword.val(),
            newPassword = this.$newPassword.val(),
            newPassword2 = this.$newPassword2.val();

        if (!oldPassword || !newPassword || newPassword != newPassword2) {
            return false;
        }

        if (oldPassword !== newPassword) {
            this.trigger('ChangePassword', oldPassword, newPassword, this.hide);
        }
        return false;
    },

    render: function(options) {
        options = options || {};

        this.$oldPassword.val('');
        this.$newPassword.val('');

        return this;
    },

    show: function(options) {
        options = options || {};
        this.visible = true;
        this.visibleTimestamp = Date.now();

        this.$overlay.off('click');
        this.$overlay.on('click', this.onOverlayClick);
        this.$overlay.show();

        this.$oldPassword.val('');
        this.$newPassword.val('');
        this.$newPassword2.val('');

        this.$el.show();
    },

    hide: function() {
        this.visible = false;
        this.$overlay.hide();
        this.$el.hide();
    }
});

export default AdminView; 