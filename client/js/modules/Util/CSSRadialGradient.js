/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

function cssRadialGradient(colors, gradientStops, baseColor, cssPrefix) {
    var cssStops = [],
        stopDistances = _.map(gradientStops, function(v) { return v * 0.71 }), // 0.71 adjusts to circle's radius instead of square's
        numStops = stopDistances.length,
        color = null;

    _.each(stopDistances, function(stopDistance, i) {
        if (numStops - i > colors.length) {
            color = baseColor || 'rgba(0,0,0,0)';
        }
        else {
            color = colors[i - (numStops - colors.length)];
        }

        if (i == 0) {
            color + ' ' + (stopDistance * 100) + '%'
        }
        cssStops.push(color + ' ' + Math.round(stopDistance * 100) + '%');
    });

    var background = 'radial-gradient(' + cssStops.join(', ') + ')';
    if (cssPrefix == '-webkit-') {
        background = '-webkit-' + background;
    }

    return background;
}

export default cssRadialGradient;