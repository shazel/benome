/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import _ from 'underscore'
import createPoint from 'benome/modules/Util/CreatePoint'
import     randomPointStream from 'benome/modules/Util/RandomPointStream2'
import     initCollections from 'benome/modules/Util/InitCollections'

function initDemoClusterData(DC, rootContextID, options) {
    options = options || {};

    DC.add('');

    var nextID = DC.add('', rootContextID),
        nextIDs = DC.addMulti(3, nextID);
    DC.addMulti(3, nextIDs[2]);

    var nextID = DC.add('', rootContextID),
        nextIDs = DC.addMulti(5, nextID);
    //
    DC.addMulti(3, nextIDs[1]);

    var nextID = DC.add('', rootContextID),
        nextIDs = DC.addMulti(4, nextID);
    DC.addMulti(3, nextIDs[2]);
    
    var nextID = DC.add('', rootContextID),
        nextIDs = DC.addMulti(6, nextID);
    //
    DC.addMulti(3, nextIDs[1]);

    var nextID = DC.add('', rootContextID),
        nextIDs = DC.addMulti(4, nextID);
    DC.addMulti(3, nextIDs[1]);
    DC.addMulti(2, nextIDs[3]);

    var contexts = initCollections(rootContextID, DC.getData());

    var anchorTime = options.anchorTime || Date.now() / 1000,
        graphWindow = options.graphWindow || 86400,
        numGraphSegments = options.numGraphSegments || 24,
        
        pointWindow = graphWindow * 2,
        numRandomPoints = 10;

    _.each(DC.contexts, function(contextDef) {
        var contextID = contextDef.ID,
            context = contexts.get(contextID);

        if (!context.isLeaf()) {
            return;
        }

        var targetInterval = (graphWindow / 4) + (Math.random() * graphWindow),
            numPoints = 1 + parseInt(4 * Math.random());

        context.set('1__TargetFrequency', targetInterval);

        var minAge = Math.random() * targetInterval,
            rawPoints = randomPointStream({
                numPoints: numPoints,
                farTime: anchorTime - pointWindow,
                nearTime: anchorTime,
                minDistance: minAge,
                variance: Math.random()
            });

        var points = _.map(rawPoints, function(pointTime) {
            return createPoint(DC.nextID(), contextID, {
                UpdatedAttributes: {
                    Timing: {
                        Time: pointTime,
                        Duration: 0
                    }
                }
            });
        }, this);

        contexts.points.add(points, {
            silent: true
        });
    }, this);

    return contexts;
}

export default initDemoClusterData;