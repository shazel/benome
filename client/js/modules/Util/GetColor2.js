/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import $ from 'jquery'
import     jQueryColor from 'benome/lib/jquery.color'

function getColor(idx, numHues) {
    var numHues = numHues || 16;
    idx = Math.max(0, Math.min(numHues, idx));

    var hue = (360 / numHues) * idx,
        saturation = 0.55,
        lightness = 0.55,
        color = $.Color()
                    .hue(hue)
                    .saturation(saturation)
                    .lightness(lightness)
                    .alpha(1),
        hexColor = color.toHexString();

    return hexColor;
}

export default getColor;