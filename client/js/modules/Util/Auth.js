/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import Backbone from 'backbone'
import     _ from 'underscore'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function Auth(options) {
    options = options || {};
    this.ajax = options.ajax;
}
_.extend(Auth.prototype, {
    loginUser: function(userName, password, errorCallback) {
        var data = {
            'Username': userName,
            'Password': password
        }

        Global.setWorking();

        var s = _.bind(function(response, textStatus, jqXHR) {
            Global.unsetWorking();
            response = response || {};
            var success = false;

            if (response.UserID) {
                success = true;
                this.isAuthenticated = true;
                this.trigger('UserLoggedIn', response);
            }

            if (success) {
                this.trigger('UserAuthenticated', response);
            }
            else {
                this.trigger('UserNotAuthenticated');
            }
        }, this);

        var e = _.bind(function() {
            Global.unsetWorking();
            this.trigger('UserNotAuthenticated');
            if (errorCallback) {
                errorCallback('Auth failed');
            }
        }, this);

        this.ajax.post('/user/login', data, s, e);
    },

    logoutUser: function(successCallback, errorCallback) {
        var s = _.bind(function(response, textStatus, jqXHR) {
            Global.unsetWorking();
            this.isAuthenticated = false;
            this.trigger('UserLoggedOut');
            if (successCallback) {
                successCallback();
            }
        }, this);

        var e = _.bind(function() {
            Global.unsetWorking();
            if (errorCallback) {
                errorCallback('Auth failed');
            }
        }, this);

        this.ajax.post('/user/logout', {}, s, e);
    },

    checkOnlineState: function() {
        this.isAuthenticated = false;
        var s = _.bind(function(response, textStatus, jqXHR) {
            if (jqXHR.status == 403) {
                console.log('User not yet authorized');
                this.trigger('UserNotAuthenticated');
            }
            else if (jqXHR.status === 200) {
                this.isAuthenticated = true;
                this.trigger('UserAuthenticated', response, true);
            }
        }, this);

        var e = _.bind(function(response, textStatus, jqXHR) {
            if (response.status == 403) {
                console.log('User not yet authorized');
                this.trigger('UserNotAuthenticated');
            }
            else {
                console.log('Server is inaccessible: ' + textStatus);
                // Proceed to offline init
                this.trigger('ServerOffline');
            }
        }, this);

        this.ajax.get('/test', {}, s, e);
    }
});
_.extend(Auth.prototype, Backbone.Events);

export default Auth;