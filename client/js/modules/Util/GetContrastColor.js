/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

function contrastColor(c, darkColor, lightColor) {
    if (_.isString(c)) {
        c = $.Color(c);
    }

    // method taken from https://gist.github.com/960189
    var r = c._rgba[0],
        g = c._rgba[1],
        b = c._rgba[2];

    return (((r*299)+(g*587)+(b*144))/1000) >= 131.5 ? darkColor : lightColor;
}

export default contrastColor;