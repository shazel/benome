/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import $ from 'jquery'
import     jQueryColor from 'benome/lib/jquery.color'
    
import idToInt from 'benome/modules/IDToInt'

function seedRandom(s) {
    return function() {
        s = Math.sin(s) * 10000; return s - Math.floor(s);
    }
}

function getColor(id) {
    var random = seedRandom(idToInt(id)),
        numHues = 16,
        hue = (360 / numHues) * (random() * numHues),
        saturation = 0.5,
        lightness = 0.6,
        color = $.Color()
                    .hue(hue)
                    .saturation(saturation)
                    .lightness(lightness)
                    .alpha(1),
        hexColor = color.toHexString();

    return hexColor;
}

export default getColor;