/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

function idToInt(idStr) {
    var base;
    if (idStr.indexOf('-') >= 0) {
        // It's an offline UUID
        base = '0123456789abcdef';
        idStr = idStr.replace(/-/g, '');
    }
    else {
        base = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }

    var length = base.length,
        result = 0;

    for (var i = 0; i < idStr.length; i++) {
        var c = idStr[idStr.length - i - 1];
        result += Math.pow(length, i) * base.indexOf(c);
    }

    return result;
}

export default idToInt;