/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

function randomPointStream(options) {
	options = options || {};

    var numPoints = options.numPoints,
    	farTime = options.farTime,
    	nearTime = options.nearTime || Date.now() / 1000,
    	minDistance = options.minDistance || 0;
    	variance = options.variance || 0;

    nearTime -= minDistance;
    farTime -= minDistance;

    var timeRange = nearTime - farTime,
    	interval = timeRange / numPoints;

    return _.map(_.range(0, numPoints), function(i) {
    	var pos = (interval / 2) + (i * interval),
    		maxVariance = interval * variance,
    		actualVariance = maxVariance * Math.random();

    	pos -= maxVariance / 2;
    	pos += actualVariance;
        return farTime + pos;
    });
}

export default randomPointStream;