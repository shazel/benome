/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import _ from 'underscore'
import Point from 'benome/modules/Data/Point'

function createPoint(pointID, contextID, details) {
    var updatedAttributes = details.UpdatedAttributes || {};

    var createAttrs = {
        'ID': pointID
    };

    function addNamespacedAttr(attrBase, namespaceID, newAttrs) {
        namespaceID = namespaceID || 1;
        _.each(newAttrs, function(val, key) {
            attrBase[namespaceID + '__' + key] = val;
        });
    }

    var beginTime = updatedAttributes.Timing ? updatedAttributes.Timing.Time || null : Date.now() / 1000;
    addNamespacedAttr(createAttrs, 1, {
        // Global/universal attributes
        'ContextID': contextID,
        'Time': beginTime,
        'TimeOffset': 0,

        // Core app attributes    
        'Duration': updatedAttributes.Timing ? parseInt(updatedAttributes.Timing.Duration) || null : null,
        'Text': updatedAttributes.Text || '',
        'Color': details.Color || null
    });

    var point = new Point(createAttrs, { silent: true });
    return point;
}

export default createPoint;