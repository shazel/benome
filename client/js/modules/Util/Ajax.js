/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

function Ajax(options) {
    options = options || {};
    this.sessionID = options.sessionID;
    this.host = options.host;
}
_.extend(Ajax.prototype, {
    get: function(url, data, success, error, complete, timeout) {
        data = data || {};
        error = error || this.defaultErrorCallback;
        success = success || this.defaultSuccessCallback;
        timeout = timeout || 10000;

        data.SessionID = this.sessionID;

        $.ajax(this.host + url, {
            data: data,
            //contentType : 'application/json',
            type: 'GET',
            //dataType: 'json',
            success: success,
            error: error,
            complete: complete,
            timeout: timeout
        });
    },

    post: function(url, data, success, error, complete, timeout) {
        data = data || {};
        error = error || this.defaultErrorCallback;
        success = success || this.defaultSuccessCallback;
        timeout = timeout || 10000;

        data.SessionID = this.sessionID;

        $.ajax(this.host + url, {
            data: data,
            type: 'POST',
            success: success,
            error: error,
            complete: complete,
            timeout: timeout
        });
    },

    jsonGet: function(url, data, success, error, complete, timeout) {
        data = data || {};
        error = error || this.defaultErrorCallback;
        success = success || this.defaultSuccessCallback;
        timeout = timeout || 10000;

        data.SessionID = this.sessionID;

        $.ajax(this.host + url, {
            data: {
                'Params': JSON.stringify(data)
            },
            contentType : 'application/json',
            type: 'GET',
            dataType: 'json',
            success: success,
            error: error,
            complete: complete,
            timeout: timeout
        });
    },

    jsonPost: function(url, data, success, error, complete, timeout) {
        data = data || {};
        error = error || this.defaultErrorCallback;
        success = success || this.defaultSuccessCallback;
        timeout = timeout || 10000;

        data.SessionID = this.sessionID;

        $.ajax(this.host + url, {
            data: JSON.stringify(data),
            contentType : 'application/json',
            type: 'POST',
            dataType: 'json',
            success: success,
            error: error,
            complete: complete,
            timeout: timeout
        });
    },

    defaultSuccessCallback: function(response, textStatus, jqXHR) {
        console.log('success', response);
        Global.unsetWorking();
    },

    defaultErrorCallback: function(jqXHR, textStatus, errorThrown) {
        console.log('error', textStatus);
        Global.unsetWorking();
    },

    modelErrorCallback: function(model, jqXHR, options) {
        if (jqXHR && jqXHR.responseJSON && jqXHR.responseJSON.Type == 'Authentication Error') {
            this.trigger('AuthLost');
        }
        else {
            console.log('Model error', model, jqXHR, options);
        }
        
        Global.unsetWorking();
    }
});

export default Ajax;