/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

function randomPointStream(numPoints, farTime, nearTime) {
    nearTime = nearTime || Date.now() / 1000;
    var timeRange = nearTime - farTime;

    return _.map(_.range(0, numPoints), function() {
        return farTime + (Math.random() * timeRange);
    });
}

export default randomPointStream;