import $ from 'jquery'
import _ from 'underscore'
import Backbone from 'backbone'
import Hammer from 'hammerjs'

// Classes
import SurfaceView from 'benome/cluster/SurfaceView'
import AttributeModeView from 'benome/cluster/AttributeModeView'
import StreamGraphD3_Class from 'benome/modules/StreamGraphD3'
import HistoryView from 'benome/views/HistoryView'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

var SurfaceModeView = AttributeModeView;

var GraphSurface_View = SurfaceModeView.extend({
    tagName: 'div',
    className: 'graph-mode-view',

    events: {},

    name: 'View',

    initialize: function(options) {
        _.bindAll(this, 'updateLabel');
        SurfaceModeView.prototype.initialize.call(this, options);
        this.surfaceView.on('LabelChanged', this.updateLabel);

        this.maxGraphDepth = options.maxGraphDepth || 2;

        this.$el
            .html('<span class="label"></span>');

        this.$backgroundImage = $('<canvas>')
                                    .css({
                                        'position': 'absolute',
                                        'z-index': '-5',
                                        'width': '100%',
                                        //'height': '50%',
                                        //'top': '25%',
                                        'left': 0,
                                        'transform': 'scale(-1, 1)'
                                    })
                                    .appendTo(this.$el);

        this.$label = $('.label', this.$el);
        this.streamGraph = new StreamGraphD3_Class({
            preWeighted: true
        });
    },

    render: function(options) {
        options = options || {};
        SurfaceModeView.prototype.render.call(this, options);
        this.updateLabel();

        var surfaceModeView = this,
            regionWidth = surfaceModeView.regionWidth,
            regionHeight = surfaceModeView.regionHeight,

            graphWidth = options.graphWidth || regionWidth,
            graphHeight = options.graphHeight || regionHeight;

        var $graphEl = this.$backgroundImage;

        if (!this.graphRendered) {
            if (this.surfaceView.viewState.depth <= this.maxGraphDepth) {
                var data = this.surfaceView.getData();
                if (data && data.length > 0) {
                    $graphEl.show();

                    this.streamGraph.render({
                        data: data,
                        outputType: 'Canvas',
                        destEl: $graphEl.get()[0],
                        width: graphWidth, //$backgroundImage.width(),
                        height: graphHeight, //$backgroundImage.height(),
                        antiAlias: true,
                    }, function(canvas, yMax, yTotal) {});

                    this.graphRendered = true;
                }
                else {
                    $graphEl.hide();
                }
            }
            else {
                $graphEl.hide();
            }
        }

        return this;
    },

    updateLabel: function() {
        var label = '';
        if (!this.hideLabels) {
            label = this.surfaceView.contextModel.getNS('Label');
        }
        this.$label.html(label);
    },

    dataReactive: function() {
        return true;
    }
});


var GlobalSurface_Exclusive = SurfaceModeView.extend({
    tagName: 'div',
    className: 'global-mode-exclusive',

    events: {
        //'click': ''
    },

    name: 'Exclusive',

    initialize: function(options) {
        options = options || {};
        _.bindAll(this, 'updateLabel');
        SurfaceModeView.prototype.initialize.call(this, options);
        
        this.maxGraphDepth = options.maxGraphDepth || 2;
        var fontSize = Global.fontSize * 0.6;

        this.$label = $('<div>')
                            .css({
                                'position': 'absolute',
                                'top': '0',
                                'left': '15%',
                                'width': '70%',
                                'height': '1.3em',
                                'text-align': 'center',
                                'font-size': (fontSize * 1.5) + 'px',
                                'z-index': '2',
                            })
                            .appendTo(this.$el);

        this.historyView = new HistoryView({
            container: this.$el,
            hideLabels: Global.hideLabels,
            dragHandler: Global.commonPointDragHandler
        });
        this.historyView.$el.appendTo(this.$el);

        var _this = this;
        this.historyView.on('Pressed', function() {
            if (_this.surfaceView.baseView.cluster.controller) {
                _this.surfaceView.baseView.cluster.controller.unsetExclusive();
            }
        });

        this.historyView.$el.css({
            'position': 'absolute',
            'top': (fontSize * 2) + 'px',
            'font-size': fontSize + 'px'
        });

        this.$backgroundImage = $('<canvas>')
                                    .css({
                                        'position': 'absolute',
                                        'z-index': '-5',
                                        'width': '90%',
                                        'height': '70%',
                                        'top': '15%',
                                        'left': '5%',
                                        'opacity': '0.55',
                                        'transform': 'scale(-1, 1)'
                                    })
                                    .appendTo(this.$el);

        this.surfaceView.on('LabelChanged', this.updateLabel);
    },

    updateLabel: function() {
        var label = '';
        if (!Global.hideLabels) {
            label = this.surfaceView.contextModel.getNS('Label');
        }
        this.$label.html(label);
    },

    render: function(options) {
        options = options || {};
        SurfaceModeView.prototype.render.call(this, options);

        this.updateLabel();

        var contextID = this.surfaceView.baseView.viewID;
        this.historyView.render({
            contextID: contextID,
            keepScrollPos: true
        });

        var surfaceModeView = this,
            regionWidth = surfaceModeView.regionWidth,
            regionHeight = surfaceModeView.regionHeight,

            graphWidth = options.graphWidth || regionWidth,
            graphHeight = options.graphHeight || regionHeight;

        var $graphEl = this.$backgroundImage;

        if (!this.graphRendered) {
            if (this.surfaceView.viewState.depth <= this.maxGraphDepth) {
                var data = this.surfaceView.getData();
                if (data && data.length > 0) {
                    $graphEl.show();

                    this.surfaceView.streamGraph.render({
                        data: data,
                        outputType: 'Canvas',
                        destEl: $graphEl.get()[0],
                        width: graphWidth, //$backgroundImage.width(),
                        height: graphHeight, //$backgroundImage.height(),
                        antiAlias: true,
                    }, function(canvas, yMax, yTotal) {});

                    this.graphRendered = true;
                }
                else {
                    $graphEl.hide();
                }
            }
            else {
                $graphEl.hide();
            }
        }

        /*var clusterController = this.surfaceView.baseView.cluster.controller;
        Global.apps.renderAppSurface(clusterController, clusterController.clusterMode, this.surfaceView, this, options);*/

        return this;
    },

    dataReactive: function() {
        return true;
    }
});

var GraphSurface = SurfaceView.extend({
    tagName: 'div',
    className: 'graph-surface',

    events: {
        //'click': ''
    },

    modeClasses: {
        'View': GraphSurface_View,
        'Exclusive': GlobalSurface_Exclusive
    },

    initialize: function(options) {
        SurfaceView.prototype.initialize.call(this, options);

        var _this = this;
        function onLabelChange() {
            _this.trigger('LabelChanged');
        }
        this.contextModel.on('change:1__Label', onLabelChange);
    },

    dataReactive: function() {
        return this.getModeView().dataReactive();
    },

    getValue: function() {
        if (_.isNumber(this.newValue)) {
            return this.newValue;
        }
        else {
            return this.contextModel.get('Value') || 0;
        }
    },

    setExtra: function(extra) {},

    getData: function() {
        var context = this.contextModel,
            downAssocModels = context.getAssocModels('down');

        if (downAssocModels.length == 0) {
            downAssocModels = [
                context
            ];
        }

        var layers = _.compact(_.map(downAssocModels, function(assocModel) {
            if (assocModel.id == context.id) {
                var data = assocModel.aggregateGraphData
            }
            else {
                var data = assocModel.weightedAggregateGraphData;
            }

            if (data && data.length) {
                return {
                    'Data': assocModel.aggregateGraphData,
                    'Color': this.baseView.cluster.getColor(assocModel.id, true, 0.65, true)
                }
            }
        }, this));

        return layers;
    }
});

export default GraphSurface