/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

import GlobalCluster from 'benome/cluster/GlobalCluster'
import BooleanCluster from 'benome/cluster/BooleanCluster'
import AttributeCluster from 'benome/cluster/AttributeCluster'
import ContextDefCluster from 'benome/cluster/ContextDefCluster'

import _Global from 'benome/modules/GlobalState'
var Global = _Global()

export default {
    pointAttributeClusterDef: {
        Type: 'Cluster',
        ClusterType: 'Attribute',
        RootType: 'SimpleSurfaceView',
        Render: true,
        RenderOptions: {},
        Options: {
            clusterSize: 'Expanded',
            dragDisabled: true,
            dropDisabled: true
        },
        EventHandlers: {},
        Nodes: []
    },

    clusterAttributeClusterDef: {
        Type: 'Cluster',
        ClusterType: 'Attribute',
        RootType: 'SimpleSurfaceView',
        Render: true,
        RenderOptions: {},
        Options: {
            clusterSize: 'Expanded',
            dragDisabled: true,
            dropDisabled: true,
            visibleUIElements: ['Creator']
        },
        EventHandlers: {},
        Nodes: []
    },

    transformNode: function(targetType, nodeDef) {
        nodeDef = nodeDef || {};

        if (targetType == 'BooleanOption') {
            var outDef = _.extend({}, nodeDef);
            if (!nodeDef.Type || nodeDef.Type == targetType) {
                outDef.Type = targetType;
                outDef.Label = outDef.Label || '';
            }
            else {
                // add support for other node types later on
                return null;
            }

            return outDef;
        }
        else {
            return nodeDef;
        }
    },

    renderStructure: function(struct, clusterCollection, controllerOptions) {
        struct = struct || {};
        controllerOptions = controllerOptions || {};

        if (!struct.Type || !struct.Container) {
            return null;
        }

        var base = null,
            _this = this;

        if (struct.Type == 'Cluster') {
            if (struct.ClusterType == 'Global') {
                var clusterOptions = {
                    label: struct.Name
                };

                struct.Options = struct.Options || {};
                _.extend(struct.Options, clusterOptions);

                var lastFocusID = Global.util.localGet('LastFocus'),
                    lastFilterLevel = parseInt(Global.util.localGet('LastFilterLevel')) || 0,
                    lastGlobalClusterDisplayMode = Global.util.localGet('LastGlobalClusterDisplayMode');

                base = new GlobalCluster(struct, clusterCollection, _.extend({
                    clusterMode: lastGlobalClusterDisplayMode,
                    filterLevel: lastFilterLevel,
                    focusID: lastFocusID
                }, controllerOptions));

                if (lastFilterLevel) {
                    base.cluster.setFilterLevel(lastFilterLevel, {
                        noRender: true
                    });
                }
                if (lastFocusID && base.cluster.contexts.has(lastFocusID)) {
                    base.cluster.setFocus(lastFocusID, false);
                }
            }
            else if (struct.ClusterType == 'Boolean') {
                if (!clusterCollection) {
                    var nodes = _.compact(_.map(struct.Nodes, function(nodeDef) {
                        return _this.transformNode('BooleanOption', nodeDef);
                    }));
                    struct.Nodes = nodes;
                }

                var clusterOptions = {
                    moveDisabled: true,
                    label: struct.Name
                };

                struct.Options = struct.Options || {};
                _.extend(struct.Options, clusterOptions);
                base = new BooleanCluster(struct, clusterCollection, controllerOptions);
            }
            else if (struct.ClusterType == 'Attribute') {
                var validTypes = {
                    'Text': 1,
                    'TextLine': 1,
                    'Interval': 1,
                    'Numeric': 1,
                    'Group': 1,
                    'Boolean': 1,
                    'ClusterContainer': 1,
                    'Frequency': 1
                }

                if (!clusterCollection) {
                    var nodes = _.compact(_.map(struct.Nodes, function(nodeDef) {
                        if (!(nodeDef.Type in validTypes)) {
                            console.log('Invalid type for Attribute cluster: ' + nodeDef.Type);
                            return null;
                        }

                        var outDef = _.extend({}, nodeDef);
                        outDef.Label = outDef.Label || '';
                        outDef.Value = outDef.Value || null;

                        return outDef;
                    }));
                    struct.Nodes = nodes;
                }

                var clusterOptions = {
                    moveDisabled: true,
                    dragDisabled: true,
                    dropDisabled: true,
                    label: struct.Name
                };

                struct.Options = struct.Options || {};
                _.extend(struct.Options, clusterOptions);
                _.extend(struct.Options, struct.OptionOverrides || {});
                
                base = new AttributeCluster(struct, clusterCollection, controllerOptions);
            }
            else if (struct.ClusterType == 'ContextDef') {
                var validTypes = {
                    'ContextDef': 1
                }

                if (!clusterCollection) {
                    var nodes = _.compact(_.map(struct.Nodes, function(nodeDef) {
                        /*if (!(nodeDef.Type in validTypes)) {
                            console.log('Invalid type for ContextDef cluster: ' + nodeDef.Type);
                            return null;
                        }*/

                        var outDef = _.extend({}, nodeDef);
                        outDef.Label = outDef.Label || '';
                        return outDef;
                    }));
                    struct.Nodes = nodes;
                }

                var clusterOptions = {
                    moveDisabled: true,
                    dragDisabled: true,
                    dropDisabled: true,
                    label: struct.Name
                };

                struct.Options = struct.Options || {};
                _.extend(struct.Options, clusterOptions);
                _.extend(struct.Options, struct.OptionOverrides || {});
                
                base = new ContextDefCluster(struct, clusterCollection, {});
            }
        }

        if (struct.Render && base) {
            base.render(struct.RenderOptions);
        }
        
        return base;
    },

    generatePointAttributeCluster: function(contextModel) {
        // Deep copy
        var clusterDef = $.extend(true, {}, this.pointAttributeClusterDef);
        clusterDef.Name = contextModel.getNS('Label');

        var appAttributes = Global.app.getPointAttributes(clusterDef);
        _.each(appAttributes, function(attrDef) {
            clusterDef.Nodes.push(attrDef);
        });

        return clusterDef;
    },

    generateContextAttributeCluster: function(contextModel) {
        // Deep copy
        var clusterDef = $.extend(true, {}, this.clusterAttributeClusterDef);
        clusterDef.Name = contextModel.getNS('Label');
        var appAttributes = Global.app.getContextAttributes(contextModel, clusterDef);

        _.each(appAttributes, function(attrDef) {
            clusterDef.Nodes.push(attrDef);
        });

        return clusterDef;
    }
}