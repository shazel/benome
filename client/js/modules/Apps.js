/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

// Libs
import _ from 'underscore'

import GlobalApp from 'benome/apps/global/Global'
import BehaveApp from 'benome/apps/behave/Behave'
import ProjectApp from 'benome/apps/project/Project'
import BreakdownApp from 'benome/apps/breakdown/Breakdown'

function Apps(options) {
    options = options || {};
    _.bindAll(this, 'renderAppSurface');

    this.dataLoader = options.dataLoader;
    this.globalAppOnly = options.globalAppOnly;
    this.enabledApps = options.enabledApps;

    this.initApps();
}
_.extend(Apps.prototype, {
    initApps: function() {
        this.apps = {};

        // This map is here to ensure this global class is already initialized and available
        var appClsMap = {
            'Global': GlobalApp
        }

        if (!this.globalAppOnly) {
            // Require paths are static strings so browserify can work
            if (!_.isArray(this.enabledApps)) {
                _.extend(appClsMap, {
                    'Behave': BehaveApp
                    'Project': ProjectApp
                });
            }
            else {
                if (_.contains(this.enabledApps, 'Behave')) {
                    appClsMap['Behave'] = BehaveApp
                }

                if (_.contains(this.enabledApps, 'Project')) {
                    appClsMap['Project'] = ProjectApp
                }

                if (_.contains(this.enabledApps, 'Breakdown')) {
                    appClsMap['Breakdown'] = BreakdownApp
                }
            }
        };

        _.each(this.getAppNames(), function(appName) {
            var appCls = appClsMap[appName];
            if (appCls) {
                this.apps[appName] = new appCls(this, this.getAppID(appName), {});
            }
        }, this);
    },

    postInitApps: function(globalCluster) {
        _.each(this.apps, function(app, appName) {
            app.postInit(globalCluster);
        });
    },

    getApp: function(appName) {
        return this.apps[appName];
    },

    getAppNames: function() {
        var appContext = this.dataLoader.contexts.get(Global.appsContextID),
            appNames = [];

        if (appContext) {
            var appModels = appContext.getAssocModels('down');
            var appNames = _.map(appModels, function(appModel) {
                return appModel.getNS('Label');
            });

            if (!_.contains(appNames, 'Project') && _.contains(this.enabledApps, 'Project')) {
                appNames.push('Project');
            }

            if (!_.contains(appNames, 'Outline') && _.contains(this.enabledApps, 'Outline')) {
                appNames.push('Outline');
            }
        }

        return appNames;
    },

    getAppID: function(appName) {
        if (!this.appIDCache) {
            var appIDCache = {},
                contexts = this.dataLoader.contexts;

            _.each(contexts.getNeighbours(Global.appsContextID), function(appID) {
                appIDCache[contexts.get(appID).getNS('Label')] = appID;
            });

            this.appIDCache = appIDCache;
        }

        return this.appIDCache[appName];
    },

    renderAppSurface: function(cluster, clusterMode, surfaceView, surfaceModeView, options) {
        var _this = this,
            sortedApps = _.sortBy(_.values(this.apps), function(app) {
                return app.name == 'Global' ? 0 : 1;
            });

        var featuresRendered = {};
        _.each(sortedApps, function(app, appName) {
            if (app.surfaceRender) {
                var newFeaturesRendered = app.surfaceRender(cluster, clusterMode, surfaceView, surfaceModeView, options, featuresRendered);
                _.extend(featuresRendered, newFeaturesRendered || {});
            }
        }, this);
    },

    getContextAttributes: function(contextModel, clusterDef) {
        // Iterate all active apps to pull in all of their attribute defs
        var contextAttributes = _.object(_.compact(_.map(this.apps, function(app, appID) {
            // TODO/FIXME: Support duplicate handlers from multiple apps, with some kind of
            // sequencing or prioritization
            var appEventHandlers = app.getContextEventHandlers(contextModel) || {};
            _.extend(clusterDef.EventHandlers, appEventHandlers);

            var a = app.getContextAttributeDefs(contextModel);
            if (a && a.length > 0) {
                return [appID, a];
            }
        })));

        return contextAttributes;
    },

    getPointAttributes: function(clusterDef) {
        // Iterate all active apps to pull in all of their attribute defs
        var pointAttributes = _.object(_.compact(_.map(this.apps, function(app, appID) {
            // TODO/FIXME: Support duplicate handlers from multiple apps, with some kind of
            // sequencing or prioritization
            var appEventHandlers = app.getContextEventHandlers() || {};
            _.extend(clusterDef.EventHandlers, appEventHandlers);

            var a = app.getPointAttributeDefs();
            if (a && a.length > 0) {
                return [appID, a];
            }
        })));

        return pointAttributes;
    }
});

export default Apps;