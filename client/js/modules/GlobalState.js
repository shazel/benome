/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import Backbone from 'backbone'
import _ from 'underscore'

import ClusterState from 'benome/modules/ClusterState'
import contrastColor from 'benome/modules/Util/GetContrastColor'

var GG;

function GlobalState() {}

_.extend(GlobalState.prototype, {
    isMobile: ('ontouchstart' in document.documentElement),
    isAndroid: (/android/i.test(navigator.userAgent)),
    isApple: (/iphone|ipod|ipad/i.test(navigator.userAgent)),
    isMac: (/Macintosh/.test(navigator.userAgent)),
    isTablet: (/ipad/i.test(navigator.userAgent) || ((/android/i.test(navigator.userAgent)) && !(/mobile/i.test(navigator.userAgent)))),

    featureState: {
        'ActionHistory': true,
        'ActionTimeline': true,
        'ActivityPullForward': true,
        'ActivityPushBack': true,
        'AddFeedbackInteractive': true,
        'Admin': true,
        'ContextModify': true,
        'ClusteredEdit': true,
        'DetailLevels': true,
        'DragDrop': true,
        'LeafFocusAutoAdd': true,
        'LeafFocusToggleTimer': true,
        'MovableFocus': true,
        'ActivityAdd': true,
        'ActivityMove': true,
        'PointAdd': true,
        'PointDetail': true,
        'PointLongPress': true,
        'PointShortPress': true,
        'HistoryPointMove': true
    },

    contextTypes: {},

    init: function(options, features) {
		options = options || {};
		_.extend(this, options);
		this.setFeatures(features);
		this.util = Utils;
		this.clusters = new ClusterState(this.globalCluster);
        this.updateLastActivity();

        this.lockStateOn = _.isBoolean(options.lockStateOn) || this.util.localGet('LockState') == 'on' 
    },

    initContextTypes: function(typeMap) {
        this.contextTypes = _.extend({}, this.contextTypes, typeMap)
    },

    registerContextType: function(typeName, surfaceViewClass) {
        this.contextTypes[typeName] = surfaceViewClass
    },

    mapContextType: function(contextType) {
        return this.contextTypes[contextType] || null
    },

    setFeatures: function(features) {
        _.extend(this.featureState, features);
        return this.featureState;
    },

    getFeatures: function() {
        return this.featureState
    },

    FEATURE: function(feautureID) {
        return this.featureState[feautureID] !== false;
    },

    getID: function() {
        return this.instanceID;
    },

    getNow: function() {
        return Date.now() + (this.getTimeOffset() * 1000);
    },

    getTimeOffset: function() {
        return -this.timeOffset
    },

    globalSize: function() {
        return {
            width: this.$el.width(),
            height: this.$el.height()
        }
    },

    isAuthenticated: function() {
        return this.auth.isAuthenticated;
    },

    isGlobalCluster: function(cluster) {
        return !this.globalCluster || this.globalCluster.cluster.clusterID == cluster.clusterID;
    },

    nextID: function() {
        return this.dataHandler.nextID();
    },

    updateLastActivity: function() {
        this.lastActivity = Date.now();
    },

	checkAnimEnabled: function(numViews) {
        if (this.animDisabled) {
            return false;
        }

        if (this.isTablet) {
            return numViews < 30;
        }
        else if (this.isMobile) {
            return numViews < 20;
        }
        else {
            return numViews < 60;
        }
    },

    getTextContrastColor: function(c) {
        return contrastColor(c, this.darkText, this.lightText);
    },

    commonPointDragHandler: function(dragView, dragDetails) {
        return {
            '$dragProxyEl': dragView.$el,
            'proxyClass': 'drag-proxy-point'
        }
    },

    setLastClusterMode: function(displayMode) {
        this.util.localSet('LastGlobalClusterDisplayMode', displayMode);
    },

    setWorking: function(text) {
    	this.trigger('SetWorking', text);
    },

    unsetWorking: function() {
    	this.trigger('UnsetWorking');
    }
});
_.extend(GlobalState.prototype, Backbone.Events);

var Utils = {
    localGet: function(key) {
        return localStorage.getItem(GG.instanceID + '-' + key)
    },

    localSet: function(key, val) {
        return localStorage.setItem(GG.instanceID + '-' + key, val)
    },

    clearDebug: function() {
        if (this.$debug) {
            this.$debug.html('');
        }
    },

    debugMsg: function(msg) {
        if (!this.$debug) {
            this.$debug = $('<div>')
                                .appendTo(GG.$el)
                                .css({
                                    width: '75%',
                                    height: '50%',
                                    left: '50px',
                                    top: '50px',
                                    'font-size': '0.5em',
                                    'pointer-events': 'none',
                                    'z-index': 9999999
                                });
        }
        this.$debug
            .show()
            .html(msg + '<br>' + this.$debug.html());
    },

    centerOn: function($refEl, $curEl) {
        var xMid = $refEl.offset().left + ($refEl.width() / 2) - GG.$el.offset().left,
            yMid = $refEl.offset().top + ($refEl.height() / 2) - GG.$el.offset().top,

            width = $curEl.width(),
            height = $curEl.height(),

            left = (xMid - (width / 2)),
            top = (yMid - (height / 2)),

            size = GG.globalSize();

        if (top + height > size.height) {
            top = size.height - height;
        }
        else if (top < 0) {
            top = 0;
        }

        if (left + width > size.width) {
            left = size.width - width;
        }
        else if (left < 0) {
            left = 0;
        }

        $curEl.css({
            top:  top + 'px',
            left: left + 'px'
        });
    },

    formatDuration: function(duration, noSeconds) {
        var hours = Math.floor(duration / 3600),
            minutes = Math.floor((duration - (hours * 3600)) / 60),
            seconds = duration - (hours * 3600) - (minutes * 60),
            durationStr;

        if (hours || noSeconds) {
            durationStr = hours + ':' + this.zeroPad(minutes);
        }
        else {
            durationStr = this.zeroPad(minutes) + ':' + this.zeroPad(seconds);
        }

        return durationStr;
    },

    zeroPad: function(n) {
        var pad = '00';
        return (pad + n).slice(-pad.length);
    },

    getCookie: function(name) {
        var keyValue = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    },

    sum: function(vals) {
        return _.reduce(vals, function(memo, num) { return memo + num; }, 0);
    },

    // All arrays must be of same length or column sum will be NaN
    sumArrays: function(arrayList) {
        var zipped = _.zip.apply(_, arrayList),
            sum = this.sum;
        return _.map(zipped, function(col) {
            return sum(col);
        });
    }
};

export default function(options) {
	if (!GG) {
		GG = new GlobalState(options);
	}

	return GG;
}