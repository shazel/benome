/*
Copyright 2016 Steve Hazel

This file is part of Benome.

Benome is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Benome is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Benome. If not, see http://www.gnu.org/licenses/.
*/

import Context from 'benome/modules/Data/Context'
import Contexts from 'benome/modules/Data/Contexts'
import Point from 'benome/modules/Data/Point'
import Points from 'benome/modules/Data/Points'
import Association from 'benome/modules/Data/Association'
import Associations from 'benome/modules/Data/Associations'

export default {
    'Context': Context,
    'Contexts': Contexts,
    'Point': Point,
    'Points': Points,
    'Association': Association,
    'Associations': Associations
}
